

import decimal

# create a new context for this task
ctx = decimal.Context()

# 20 digits should be enough for everyone :D
ctx.prec = 20

def float_to_str(f):
    """
    Convert the given float to a string,
    without resorting to scientific notation
    """
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')

import sys

if __name__ == "__main__":

    if len(sys.argv) < 4:
        print "usage: python AddBranchLengthToAle.py <input ale file> <output file> <branch length> <resetAllBranches>"
        print " will output the ale file modified so that an clade with a branch length equal to 0 (or all of them of resetAllBranches is set to 1) now has the desired branch length."
        print "example: python AddBranchLengthToAle.py i.ale o.ale 0.1 1"
        exit(1)

    IN = open (  sys.argv[1] , "r")

    phase = 0

    OUT = open( sys.argv[2] , 'w')

    newbl = float( sys.argv[3] )

    resetAll = False
    if len(sys.argv)>4 and sys.argv[4]=="1":
        resetAll = True

    l = IN.readline()

    bips = {}
    MaxBS = 0 
    bipsDistSeen = {}
    default = 0

    while l != "":

        if l.startswith('#'):
            phase += 1
        elif phase == 2: #observationa
            default = float(l.strip())
            l = float_to_str(default) + "\n"
        elif phase == 3:#bip_counts
            sl = l.strip().split()
            bips[sl[0]] = float(sl[1])
            l = sl[0] + "\t" + float_to_str( bips.get( sl[0], default )  ) + "\n"
            MaxBS = max(MaxBS,int(sl[0]))
        elif phase == 4:#bip bls
            sl = l.strip().split()
            bl = float( sl[1] )

            if bl == 0 or resetAll:
                bl = bips.get( sl[0], default ) * newbl
                bipsDistSeen[int(sl[0])] = True

            l = sl[0] + "\t" + float_to_str( bl ) + "\n"
        if phase == 5:
            ## adding distance to clade that have not been seen in bip bls.
            for i in range(MaxBS+1):
                if not bipsDistSeen.get(i,False):
                    OUT.write( str(i) + "\t" + float_to_str( bips.get( str(i) , default ) * newbl ) + "\n" )
            phase +=1

        OUT.write(l)

        l = IN.readline()

    IN.close()
    OUT.close()
