import sys

if __name__ == "__main__":

	
	if len(sys.argv)<2:
		print "please give the name of a file containing recPhyloXML trees"
		exit(1)

	IN  =open( sys.argv[1] ,'r')

	print "fam sample Nleaf Ndup Nloss"

	curr = None
	ED = {}

	l =  IN.readline()
	while l != "":
		ls = l.strip()
		if ls.startswith("</phylogeny"):
			print curr , ED.get("leaf" , 0) , ED.get("dup" , 0) , ED.get("loss" , 0)
		elif ls.startswith("<description"):
			sl = ls.split()
			curr = sl[2] + " " + sl[-1].partition("<")[0]

			ED={"dup":0 , "loss":0 , "leaf":0}
		elif ls.startswith("<duplication"):
			ED["dup"] += 1
		elif ls.startswith("<loss"):
			ED["loss"] += 1
		elif ls.startswith("<leaf"):
			ED["leaf"] += 1

		l =  IN.readline()

	IN.close()

