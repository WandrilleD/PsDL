

from ete3 import Tree,TreeNode

import sys

if __name__ == "__main__":

    if len(sys.argv)<3:
        print " usage : python pruneTree.py <toRemovefile> <treefile> "
        print "  <toRemovefile> : contains 1 leaf name per line "
        print "  <treefile> : contains newick trees "

    IN = open(sys.argv[1],"r")

    Lremove = []

    l = IN.readline()

    while l!="":
        Lremove.append( l.strip() )
        l = IN.readline()
    IN.close()

    IN = open(sys.argv[2],"r")


    l = IN.readline()

    while l!="":

        t = Tree( l.strip() )
        L = t.get_leaf_names()
        for x in Lremove:
            if L.count(x) >0:
                L.remove(x)

        t.prune( L , preserve_branch_length=True)

        print t.write()
        l = IN.readline()

    IN.close()