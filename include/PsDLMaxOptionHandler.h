#ifndef PSDLMAX_OPTION_HANDLER_H
#define PSDLMAX_OPTION_HANDLER_H
/*

 * This class ensures the clean starting and ending of the program for the expectation maximisation algorithm of the PsDL model:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 21-02-2015
by: Wandrille Duchemin

Last modified the: 27-04-2018
by: Wandrille Duchemin


*/



#include "PsDLOptionHandler.h"


class PsDLMaxOptionHandler : public PsDLOptionHandler
{
protected:

    //unsigned int maxIteration;


    bool writeIntermediary;



public:
    PsDLMaxOptionHandler();

    // Getters
    //unsigned int get_maxIteration() const;
    bool get_writeIntermediary() const;

    // Setters

    // Actions
    void printUsage();
    bool verify();

};

#endif
