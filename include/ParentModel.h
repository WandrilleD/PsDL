#ifndef PARENT_MODEL_H_
#define PARENT_MODEL_H_


/*

This file defines a parent class for the PsDL and PsDTL model

Created the: 02-05-2018
by: Wandrille Duchemin

Last modified the: 07-05-2018
by: Wandrille Duchemin

*/

#include <CCPCPP/treeset_metadata.h>

#include "ReconciledTree.h"

#include <stdlib.h>
#include <iostream>
#include <bitset>
#include <math.h>
#include <boost/dynamic_bitset.hpp>
#include <limits>       // std::numeric_limits

typedef long double FLOAT_TYPE ;

typedef boost::dynamic_bitset<> CCPcladeIdType ;

typedef unsigned int LIndexType ;

typedef unsigned int CIndexType ;


using namespace std;


//CCPcladeIdType getSisterClade(CCPcladeIdType parent, CCPcladeIdType child){return ( ~child & parent );}

class ParentModel
{
protected:

//////// ATTRIBUTES ///////////////

    shared_ptr<TreesetMetadata> geneCCPdistribution ; // ccp distribution of the gene
    shared_ptr<TreesetMetadata> speciesCCPdistribution ; //ccp distribution of the species

    vector < CCPcladeIdType > speciesCladeIndexToCCPid; // from integer type id to the bitset that are used inside the CCP object
    map < CCPcladeIdType , CIndexType > speciesCCPidToCladeIndex; // from the bitset that are used inside the CCP object to the integer type id 
    /// setup some dummy value for the first, empty, element : empty bitset


    vector < CCPcladeIdType > geneCladeIndexToCCPid; // from integer type id to the bitset that are used inside the CCP object
    map < CCPcladeIdType , CIndexType > geneCCPidToCladeIndex; // from the bitset that are used inside the CCP object to the integer type id 


    vector < CIndexType > geneSpeciesCorrespondence ; //correspondence between the gene and species leaves
                                                      // index: gene clade id
                                                      // value: species clade id

    vector < vector< pair< CIndexType, CIndexType > > > SpeciesIndexV ; 
    vector < vector< FLOAT_TYPE > > SpeciesProbaV ;
    vector < vector< pair< CIndexType, CIndexType > > > GeneIndexV ; 
    vector < vector< FLOAT_TYPE > > GeneProbaV ;



    //// this is only used by the DTL
    vector < vector < CIndexType > > ListTransferIncompatibilities; // 1st index : species clade id
                                                                    // value     : list of species clade id whose transfer toward is impossible



    // rates

    vector < FLOAT_TYPE > DuplicationRates; //  index : species clade id ; value : associated, branch-wise, duplication rate
    vector < FLOAT_TYPE > LossRates; //         index : species clade id ; value : associated, branch-wise, loss rate

    
    // species clades branch length division

    FLOAT_TYPE rootBranchLength;

    // probability maps

    map< CIndexType , FLOAT_TYPE > probaExtantGeneSampled;


    bool recMatrixComputed; // true if the matrix has been computed and is ready to be backtracked

//////// METHODS ///////////////



    /*
        Takes:
            - CIndexType id : index of a clade
        Returns:
            (bool): true if the clade corresponds to a leaf in the Species CCP distribution
    */
    bool is_SpeciesLeaf(CIndexType id )
    { return ( SpeciesProbaV[ id ].size() == 0 ) ; }//  speciesCCPdistribution->is_leaf( speciesCladeIndexToCCPid[id] );}
    
    /*
        Takes:
            - CIndexType id : index of a clade
        Returns:
            (bool): true if the clade corresponds to a leaf in the Gene CCP distribution
    */
    bool is_GeneLeaf(CIndexType id )
    { return ( GeneProbaV[ id ].size() == 0 ) ;     }//geneCCPdistribution->is_leaf( geneCladeIndexToCCPid[id] );}


    virtual void initializeRecMatrix() =0;

    virtual FLOAT_TYPE computeProbaLeafExtremity(CIndexType Sid, CIndexType Gid ) = 0;

    void fillVccp(CIndexType Cid , vector< pair< CIndexType, CIndexType > >& IndexV , vector< FLOAT_TYPE >& ProbaV , bool isSpecies);


    //// resets
    virtual void reset_GeneLeavesProbas() =0;
    virtual void reset_recMat() = 0;


    //output preparation
    void annotateReconciledTreeAux(shared_ptr<RecNode> RT);


    void fillSpeciesIndexAndProbaCCP();

public:

    ParentModel();

    virtual ~ParentModel()
    {
      //cout << "deleting ParentModel" << endl;

      geneCCPdistribution.reset() ; // ccp distribution of the gene
      speciesCCPdistribution.reset() ; //ccp distribution of the species

      speciesCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
      speciesCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 

      geneCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
      geneCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 

      geneSpeciesCorrespondence.clear() ; //correspondence between the gene and species leaves

      DuplicationRates.clear(); //  index : species clade id ; value : associated, branch-wise, duplication rate
      LossRates.clear(); //         index : species clade id ; value : associated, branch-wise, loss rate

      probaExtantGeneSampled.clear();
      
      for( unsigned i = 0 ; i < SpeciesIndexV.size() ; i++ )
      {
        SpeciesIndexV[i].clear() ; 
        SpeciesProbaV[i].clear() ;

      }
      SpeciesIndexV.clear() ;
      SpeciesProbaV.clear() ;
      for( unsigned i = 0 ; i < GeneIndexV.size() ; i++ )
      {
        GeneIndexV[i].clear() ; 
        GeneProbaV[i].clear() ;

      }
      GeneIndexV.clear() ;
      GeneProbaV.clear() ;

    }


    //getter
    vector<FLOAT_TYPE> get_DuplicationRates() const { return DuplicationRates; }
    vector<FLOAT_TYPE> get_LossRates() const { return LossRates; }

    FLOAT_TYPE get_DuplicationRates(CIndexType spId) const { return DuplicationRates[spId]; }
    FLOAT_TYPE get_LossRates(CIndexType spId) const{ return LossRates[spId]; }

    bool get_recMatrixComputed() const{return recMatrixComputed;}// true if the matrix has been computed and is ready to be backtracked

    shared_ptr<TreesetMetadata> get_geneCCPdistribution() const { return geneCCPdistribution; }
    shared_ptr<TreesetMetadata> get_speciesCCPdistribution() const { return speciesCCPdistribution; }

    vector < CIndexType > get_geneSpeciesCorrespondence() const { return geneSpeciesCorrespondence; }

    // returns an empty bitset if the given is invalid
    CCPcladeIdType get_CCPindexFromSpeciesCladeIndex( CIndexType Sid ) const
    {
        if ( (Sid == 0) || (Sid >= speciesCladeIndexToCCPid.size()  ) )
            return CCPcladeIdType(speciesCCPdistribution->get_number_of_leaves(),0ul);
    
        return speciesCladeIndexToCCPid[ Sid ];
    }

    // returns 0 if the given is invalid
    CIndexType get_CCPindexFromSpeciesCladeIndex( CCPcladeIdType sigma ) const
    {
        auto it = speciesCCPidToCladeIndex.find( sigma );
        if ( it == speciesCCPidToCladeIndex.end() )
            return 0;
        return it->second;
    }


    virtual FLOAT_TYPE get_ProbaGeneExtinct( boost::dynamic_bitset<> sigma ) = 0;


    //// getters for the copy constructor 
    FLOAT_TYPE get_rootBranchLength() const { return rootBranchLength; }

    FLOAT_TYPE get_probaExtantGeneSampled( CIndexType Sid  ) const 
    {
        auto it = probaExtantGeneSampled.find(Sid);
        if( it == probaExtantGeneSampled.end() )
            return 1;
        //cout << "get_probaExtantGeneSampled " << Sid << " " << it->second << endl; 
        return it->second; 
    }

    map< CIndexType , FLOAT_TYPE > get_probaExtantGeneSampled(  ) const 
    {
        return probaExtantGeneSampled; 
    }

    //// this is only used by the DTL
    vector < CIndexType >  get_ListTransferIncompatibilities( int Sid ) const
    {
        if( ListTransferIncompatibilities.size() < Sid )
            return vector < CIndexType >();
        return ListTransferIncompatibilities[ Sid ];
    }

    ; // 1st index : species clade id
                                                                    // value     : list of species clade id whose transfer toward is impossible


    ////setters
    void set_DuplicationRates(CIndexType spId, FLOAT_TYPE value) { DuplicationRates[spId] = value; }//  takes : species clade id ; associated, branch-wise, duplication rate
    void set_LossRates(CIndexType spId, FLOAT_TYPE value) { LossRates[spId] = value; } //  takes : species clade id ;  associated, branch-wise, loss rate

    //  takes : species clade id ; associated, branch-wise, duplication rate
    void set_DuplicationRates(CCPcladeIdType spId, FLOAT_TYPE value)
    {
        auto it = speciesCCPidToCladeIndex.find( spId );
        if(it != speciesCCPidToCladeIndex.end())
            DuplicationRates[it->second] = value;
    }

    //  takes : species clade id ;  associated, branch-wise, loss rate
    void set_LossRates(CCPcladeIdType spId, FLOAT_TYPE value)
    {
        auto it = speciesCCPidToCladeIndex.find( spId );
        if(it != speciesCCPidToCladeIndex.end())
            LossRates[it->second] = value;
    }

    //  takes : duplication rate and set it as the global duplication rate for all the species tree
    void set_DuplicationRates( FLOAT_TYPE value )
    {
        for(unsigned i = 0 ; i < DuplicationRates.size() ; i++)
            DuplicationRates[i]=value;
    }

    //  takes : loss rate and set it as the global duplication rate for all the species tree
    void set_LossRates( FLOAT_TYPE value )
    {
        for(unsigned i = 0 ; i < LossRates.size() ; i++)
            LossRates[i] = value;
    }

    void set_rootBranchLength( FLOAT_TYPE value ) { rootBranchLength = value; }


    void set_probaExtantGeneSampled( FLOAT_TYPE value )
    {
        if( ( value > 1 ) || (value <= 0) )
        {
            cerr << "ERROR : set_probaExtantGeneSampled : provided a value that is either > 1 or <= 0. abort." << endl;
            exit(1);
        } 
        //cout << "set_probaExtantGeneSampled " << value << endl; 
        CIndexType i = 1;
        while( speciesCCPdistribution->is_leaf( speciesCladeIndexToCCPid[i] ))
        {
            //cout << i << " " << speciesCladeIndexToCCPid[i] << " " << speciesCCPdistribution->is_leaf( speciesCladeIndexToCCPid[i] ) << endl;
            probaExtantGeneSampled[ i ] = value;
            i++;
        }
    }

    void set_probaExtantGeneSampled( map < string, FLOAT_TYPE >  value )
    {
        for( auto it = value.begin() ; it != value.end() ; ++it)
        {
            CCPcladeIdType ccpId = speciesCCPdistribution->get_bitset_of_leaf( it->first );
            if( ccpId.count() == 0 )
            {
                cerr << "Warning : unknown species leaf " << it->first << " when assigning extant sampling probabilities. Ignored." << endl;
                continue ;
            }
            //cout << it->first << " > " << ccpId << " > " << speciesCCPidToCladeIndex[ ccpId ] << " > "<< it->second << endl;
            probaExtantGeneSampled[ speciesCCPidToCladeIndex[ ccpId ] ] = it->second;
        }
    }

    void set_probaExtantGeneSampled( map < CIndexType, FLOAT_TYPE >  value )
    {
        for( auto it = value.begin() ; it != value.end() ; ++it)
        {
            probaExtantGeneSampled[ it->first ] = it->second;
        }
    }



    //manipulation
    
    virtual void initializeGeneLeavesProbas() = 0;

    virtual void computeProbaGeneExtinct(unsigned int verboseLevel=0) =0;  // 

    virtual void addSpeciesCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel=0) =0;// makes some precomputations With ids

    void addGeneCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel=0 );// makes some precomputations With ids
    void addgeneSpeciesCorrespondence(map <string,string> &CorrMap);


    virtual FLOAT_TYPE getOverAllProbability()=0;

    virtual shared_ptr<RecNode> backtrack() =0;

    virtual void reset() =0; //reset probaExtinct calls reset_PoneGene as well (wchich also resets the recMatrix) 


    void prepareForNewGeneFamily();

    void annotateReconciledTree(shared_ptr<RecNode> RT);

    virtual void reMapSpeciesCCPdistribution()=0;

    //void quickCheck(); /// tmp
    virtual void prepareForComputation(shared_ptr<TreesetMetadata> speciesCCPdistrib , vector <  FLOAT_TYPE  > DefaultRates, map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates, map< string, FLOAT_TYPE > extantSampling , unsigned int verboseLevel=0 , double deltal = 0.05 , int minNbInt = 5 ) =0;

    virtual void computeReconciliationMatrix( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask , unsigned int verboseLevel=0) =0;

    virtual void resetForNewParameters( unsigned int verboseLevel=0)=0;

    virtual vector< vector<long double> > get_Rates();

    virtual void set_Rates( vector< vector<long double> > rateList );
    virtual void set_Rates( vector<long double> rateList );
    virtual void set_Rates( CIndexType Sid,  vector<long double> rateList );





};


#endif
