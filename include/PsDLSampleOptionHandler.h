#ifndef PSDLSAMPLE_OPTION_HANDLER_H
#define PSDLSAMPLE_OPTION_HANDLER_H
/*

 * This class ensures the clean starting and ending of the program for the expectation maximisation algorithm of the PsDL model:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 23-02-2015
by: Wandrille Duchemin

Last modified the: 26-02-2018
by: Wandrille Duchemin


*/



#include "PsDLOptionHandler.h"


using namespace std;


class PsDLSampleOptionHandler : public PsDLOptionHandler
{
protected:

    unsigned int sampleSize;

    unsigned int burnin;
    unsigned int thinning;

    string outputFile;

public:
    PsDLSampleOptionHandler();

    // Getters
    //string get_outputFile() const;
    unsigned int get_sampleSize() const;
    unsigned int get_burnin() const;
    unsigned int get_thinning() const;

    // Setters

    // Actions
    void printUsage();

};

#endif
