#ifndef PSDL_OPTION_HANDLER_H
#define PSDL_OPTION_HANDLER_H
/*

 * This class ensures the clean starting and ending of the program:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 14-02-2015
by: Wandrille Duchemin

Last modified the: 27-04-2018
by: Wandrille Duchemin


*/
#include <iostream>
#include <string>
#include <boost/program_options.hpp>

using namespace std;


class PsDLOptionHandler
{
protected:
    std::string gene_input_filename_;
    std::string species_input_filename_;

    std::string corr_input_filename_;

    std::string separator_; //separator between species and gene name

    bool input_is_ale_;

    unsigned int verboseLevel;

    int seed;

    double initialDupRate;
    double initialLossRate;

    unsigned int outputSample;
    std::string outputFile;

    bool unrooted_species_tree;
    bool multiple_gene_families;

    unsigned int SpCCPUpdatingNbRound;
    double SpCCPUpdatingWeight;

    double cutOff;
    bool isRelativeCutoff;

    double extant_sampling;

    bool branchWiseRates;
    string input_rate_file;

    boost::program_options::options_description desc_;
    boost::program_options::variables_map vm_;




public:
    PsDLOptionHandler();

    void readArguments(int argc , char* argv[]);
    // Getters
    std::string get_gene_input_filename() const;
    std::string get_species_input_filename() const;
    std::string get_corr_input_filename() const;
    std::string get_separator() const;
    bool get_input_is_ale() const;
    unsigned int get_verbose_level() const;
    int get_seed() const;
    unsigned int get_outputSample() const ;
    std::string get_outputFile() const ;



    double get_initialDupRate() const;
    double get_initialLossRate() const;

    bool get_unrooted_species_tree() const;
    bool get_multiple_gene_families() const;

    unsigned int get_SpCCPUpdatingNbRound() const;
    double get_SpCCPUpdatingWeight() const;

    double get_cutOff() const;
    bool get_isRelativeCutoff() const;

    double get_extant_sampling() const;

    bool get_branchWiseRates() const;
    string get_input_rate_file() const;

    // Setters

    // Actions
    bool verify();
    void printUsage();

};

#endif
