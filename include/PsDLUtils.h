#ifndef PSDL_UTILS_H_
#define PSDL_UTILS_H_


/*

This file defines helper functions 
for the reconciliation of a species and a gene CCP distributions

Created the: 13-02-2018
by: Wandrille Duchemin

Last modified the: 29-03-2018
by: Wandrille Duchemin

*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <boost/algorithm/string/find.hpp>


// includes of the CCP library. local for now
#include <CCPCPP/presets.h>
#include <CCPCPP/treeset.h>
#include <CCPCPP/treeset_metadata.h>

#include "ReconciledTree.h"
#include "ParentModel.h"
#include "PsDLModel.h"

using namespace std;

int readGeneSpeciesCorrespondenceFile(string filename, map< string , string > &correspondenceMap);

int buildCCPDistributionFromFile(string inputFilename, shared_ptr<TreesetMetadata> CCPdistrib, bool isAle =false, bool handleAsUnrooted =true);
int buildCCPDistributionFromFile(string inputFilename, shared_ptr<TreesetMetadata> CCPdistrib, int fileFormat, bool handleAsUnrooted = true);

int buildCCPDistributionFromTreeSetFile(string inputFilename, shared_ptr<TreesetMetadata> CCPdistrib, bool handleAsUnrooted );

int readGeneDistributionFile( string filename, vector < string > &toFill);

pair <string , string> splitLeafName(string leafName, string separator);


pair< int , int > getCountEvents( vector< RecNode > sample );

double getWeightedBranchLenSum( shared_ptr< TreesetMetadata > CCPdistrib );

double getWeightedBranchLenSumAux( shared_ptr< TreesetMetadata > CCPdistrib , boost::dynamic_bitset<> currentClade );


void WriteRecTreeToStream( ostream& OUT, shared_ptr< RecNode > RT, string description="");
void InitRecXMLToStream( ostream& OUT);
void FinishRecXMLToStream( ostream& OUT);

map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > getGeneCladeSpeciesMasks( shared_ptr< TreesetMetadata > geneDistribution , shared_ptr< TreesetMetadata > speciesDistribution , map< string , string > GtoSpCorr);
vector<string> getCladeLeafNames( shared_ptr< TreesetMetadata > CCPDistribution , boost::dynamic_bitset<> Cid);
boost::dynamic_bitset<> getCladeBitSet( shared_ptr< TreesetMetadata > CCPDistribution , vector<string> leafSet);

boost::dynamic_bitset<> verifyLeafCorrespondences( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GspMask);

map< boost::dynamic_bitset<>, vector< double > >  getBranchWiseRates( ParentModel * model );
void OutputBranchWiseRates(  ostream& OUT ,ParentModel * model );

void OutputCCPdistribution( string filename , shared_ptr< TreesetMetadata > CCPDistribution);

int readRatesFile(string filename, shared_ptr< TreesetMetadata > CCPDistribution, map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > &rateMap);

int guessFileFormat(string filename);


#endif