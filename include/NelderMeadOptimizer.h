#ifndef NMOPT__H_
#define NMOPT_H_

/*

This file contains utilitary functions for the nelder mead optimization algorithm 

Created the: 05-03-2015
by: Wandrille Duchemin

Last modified the: 08-03-2018
by: Wandrille Duchemin


*/

#include <math.h>
#include <map>
#include <iostream>

#include <CCPCPP/treeset_metadata.h>



using namespace std;

class NelderMeadOptimizer
{

protected:
/*
attributes:
*/

    //NelderNead specific params:

    vector< vector< long double > > Parameters;
    vector< long double > ParametersLkh;
    int bestPsetIndex;
    int worstPsetIndex;
    int secondWorstPsetIndex;

    unsigned int nbSets;
    unsigned int nbParams;


    long double step;//=1
    long double no_improve_thr;//=10e-6
    unsigned int no_improv_break;//=10
    unsigned int max_iter;//=0
    double alpha;//=1.
    double gamma;//=2.
    double rho;//=0.5
    double sigma;//=0.5

    ///counters
    unsigned int iters;
    long double no_improv_iter;
    long double prev_best;


    /// states
    int state;
    int ShrinkedIndex;

    long double reflexionLkh;
    long double expansionLkh;
    long double contractionLkh;

    vector <long double> centroid;
    vector <long double> reflexionPoint;
    vector <long double> expansionPoint;
    vector <long double> contractionPoint;


/*
methods:
*/
    vector<long double> toLogPoint( vector<long double> p )
    {
        vector<long double> np;
        np.reserve( p.size() );
        for( auto v : p )
            np.push_back( log( v ) );
        return np;
    }

    vector<long double> fromLogPoint( vector<long double> p )
    {
        vector<long double> np;
        np.reserve( p.size() );
        for( auto v : p )
            np.push_back( exp( v ) );
        return np;
    }


    //FLOAT_TYPE EvaluateParameterSet( vector< double > parameters );
    vector < long double > computeCentroid( );

    void setupParametersOrder();


    vector<long double> getReflexion(vector< long double > reference, vector<long double> point);
    vector<long double> getExpansion(vector< long double > reference, vector<long double> point);
    vector<long double> getContraction(vector< long double > reference, vector<long double> point);
    vector<long double> shrinkPoint(vector< long double > reference, vector<long double> point);
    vector<long double> getProjectedPoint(vector< long double > reference, vector<long double> point, double factor);

    void replacePoint(unsigned int index , vector<long double> newPoint);




public:

    NelderMeadOptimizer( 
                    vector< long double > startingPoint,
                    long double Step = 1,
                    long double No_improve_thr =10e-6,
                    unsigned int No_improv_break =10,
                    unsigned int Max_iter =0,
                    double Alpha =1,
                    double Gamma = 2,
                    double Rho = 0.5,
                    double Sigma =0.5
                     );

    ~NelderMeadOptimizer()
    {
        for(unsigned i = 0 ; i < nbSets ; i++)
        {
            Parameters[i].clear();
        }
        Parameters.clear();
        ParametersLkh.clear();
    }

    //void optimizationRound();

    bool checkStopConditions(); 

    unsigned int get_iters() const {return iters;}

    vector< long double > getBestParameters(bool log = false);
    long double get_BestParametersLkh();
    

    vector< long double > ReceiveLikelihoodAndGetNextParameterToTest( long double LogLkh , int verboseLevel = 0 );


};


#endif