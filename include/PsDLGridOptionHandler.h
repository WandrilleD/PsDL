#ifndef PSDLSAMPLE_OPTION_HANDLER_H
#define PSDLSAMPLE_OPTION_HANDLER_H
/*

 * This class ensures the clean starting and ending of the program for the grid algorithm of the PsDL model:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 07-03-2018
by: Wandrille Duchemin

Last modified the: 07-03-2018
by: Wandrille Duchemin


*/



#include "PsDLOptionHandler.h"


using namespace std;


class PsDLGridOptionHandler : public PsDLOptionHandler
{
protected:

    unsigned int stepNb;

    double MinDup;
    double MaxDup;
    double MinLoss;
    double MaxLoss;
    
    bool isLog;


    string outputFile;

public:
    PsDLGridOptionHandler();

    // Getters
    unsigned int get_stepNb() const;
    double get_MinDup() const;
    double get_MaxDup() const;
    double get_MinLoss() const;
    double get_MaxLoss() const;

    bool get_isLog() const;

    // Setters

    // Actions
    bool verify();

    void printUsage();

};

#endif
