#ifndef PSDLMAX_UTILS_H_
#define PSDLMAX_UTILS_H_

/*

This file defines helper functions 
for the rate expectation maximisation algorithm
of the reconciliation of a species and a gene CCP distributions

Created the: 02-03-2018
by: Wandrille Duchemin

Last modified the: 02-03-2018
by: Wandrille Duchemin

*/

#include <iostream>

#include <boost/math/special_functions/gamma.hpp>
// includes of the CCP library. local for now

#include "ReconciledTree.h"
#include "PsDLModel.h"

using namespace std;

pair<double,double> expectationMaximisationround( PsDLModel * model , double BLEN, unsigned int sampleSize, unsigned int verboseLevel=0);

void countTransitions( shared_ptr<RecNode> RT ,map < int , array<double,3>  > &transitionCount , double weight = 1.0 );

pair<double,double> estimateNewRates( double p10, double p11, double Blen, double pExtinct );

double getMeanExtremeQuantileGammaD(double mean, double variance, double k, bool max );

long double getMean(vector<long double> V);
long double getVar(vector<long double> V , long double mean);

#endif