/*

This file contains helper functions
for MPI PsDL


Created the: 24-03-2018
by: Wandrille Duchemin

Last modified the: 24-03-2018
by: Wandrille Duchemin

*/

#include "PsDLModel.h"

// includ eof the CCP library. local for now
#include <CCPCPP/treeset_metadata.h>

#include <fstream>
#include <list>

#include <bitset>
#include <boost/dynamic_bitset.hpp>

#include <boost/mpi.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/bitset.hpp>


#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>



#include <boost/program_options.hpp>

namespace mpi = boost::mpi;

using namespace std;


int bitset_To_int( boost::dynamic_bitset<> bs);
//string bitset_To_string( boost::dynamic_bitset<> bs);

vector< vector< string > > divideFilesNames( vector< string > fileNames , unsigned int n ); 


void broadcastSpeciesCCPdistrib( mpi::communicator &world, int rank , shared_ptr<TreesetMetadata> SpeciesCCPDistrib );

void broadcastRates( mpi::communicator &world, int rank , PsDLModel * model , bool branchWise );

void gatherTransitionCounts( mpi::communicator &world, int rank , map < int , array<double,3>  > &transitionCount );

void gatherOservedSplit( mpi::communicator &world, int rank , map< int, map< int , float > > &splitCountsObservedInReconciliations );




int getAleFileLastLeafset(string filename);

int findSortedIndex( int value, vector < int > & sortedList , int minIndex, int maxIndex );

int assessAleFilesSizes( string filename, vector < string > &names, vector < int > &size );
int assessAleFilesSizes( vector < string > fileNames , vector < string > &names, vector < int > &size );
int assessAleFilesSizesNoSort( vector < string > &names, vector < int > &size );


void mergeIntoSortedBySizes( vector < string > &SortedNames, vector < int > &SortedSizes, vector < string > &names, vector < int > &sizes );

vector< vector< string > > divideFilesNamesSorted( vector< string > fileNames , vector< int > FileSizes , unsigned int n ); 