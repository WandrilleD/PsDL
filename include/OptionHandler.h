#ifndef OPTION_HANDLER_H
#define OPTION_HANDLER_H
/*

 * This class ensures the clean starting and ending of the program:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 04-05-2018
by: Wandrille Duchemin

Last modified the: 17-05-2018
by: Wandrille Duchemin


*/
#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>

using namespace std;


class OptionHandler
{
protected:

    //// general options



    boost::program_options::positional_options_description pd_; // for the algorithm

    boost::program_options::options_description GenericDesc_; // generic options

        bool withTransfer_;
    
        std::string gene_input_filename_;
        std::string species_input_filename_;
        std::string separator_; //separator between species and gene name
    
        unsigned int verboseLevel_;

    boost::program_options::options_description AdvancedDesc_; // advanced options    

        std::string corr_input_filename_;
        int seed_;
        std::string extant_sampling_ ;//("extant.sampling,E",boost::program_options::value< double>(&extant_sampling),"Expected gene sampling rate among extant species ; must be between 0 (excluded) and 1. default: 1.")
        bool unrooted_sp_;         //("unrooted.species.tree,u","treat the species tree as unrooted. only used if the input is trees and not .ale files")

    boost::program_options::options_description OutputDesc_; // output options

        unsigned int outputSample_ ;
        std::string outputPrefix_ ; 

    boost::program_options::options_description HiddenDesc_; // hidden options
    
        std::string algorithm_;

    boost::program_options::options_description RateDesc_; // rate options

        double initialDupRate_;//  ("init.dup,d",boost::program_options::value< double >(&initialDupRate_), "Initial duplication rate. Default: 0.1")
        double initialLossRate_;// ("init.loss,l",boost::program_options::value< double >(&initialLossRate_),"Initial loss rate. Default: 0.1")
        double initialTransRate_;// ("init.transfer,t",boost::program_options::value< double >(&initialTransRate_),"Initial transfer rate (only used when --with.transfer/-T is used). Default: 0.1")
        bool branch_wise_ ; //("branch.wise,B", "trigger optimization of branch wise rates of duplication and loss (tree-wide parameters otherwise).")
        std::string input_rate_file_ ; //("input.rate",boost::program_options::value<string>(&input_rate_file),"file containing branch-wise duplication and loss rates. Necessitates that option --branch.wise/-B is set.")
    
    boost::program_options::options_description MaxDesc_; // max options
        bool MAX_writeIntermediary_;
        bool MAX_NoRateUpdate_;
        int MAX_SpCCPUpdatingNbRound_ ;//("nb.update.rounds,N",boost::program_options::value< unsigned int>(&SpCCPUpdatingNbRound),"number of species CCP and rate updating round ; minimum of one, put option -w at 0 to avoid species CCP update. default: 1." )
        double MAX_SpCCPUpdatingWeight_ ;//("updating.weight,W",boost::program_options::value< double>(&SpCCPUpdatingWeight),"weight of the splits observed in reconciliations in the update of the species CCP distribution ; must be between 0 and 1.\nuse a value of 0 to avoid species CCP update\ndefault: 0.5.")
        double MAX_cutOff_ ; //("cutOff.CCP,C",boost::program_options::value< double>(&cutOff),"threshold to remove splits from the the species CCP distribution during its updates; must be between 0 and 1.\ndefault: 0 (nothing is removed).")
        bool MAX_relativeCutOff_;//("relative.cutOff,R","consider --cutOff.CCP/-C to apply to the ratio of the split CCP to the CCP of the best split rather than rather than directly to the CCP.")
        int MAX_sampleSize_ ;//("update.sample.size",boost::program_options::value< unsigned int>(&MAX_sampleSize_),"Size of the reconciliation sampled per gene family to update event rates and the species CCP distribution. default 100." )

        bool MAX_force_resolve_mode_;

        float MAX_SpeciationLossWeights_ ;
        bool MAX_OneVotePerReconciledTree_ ;


    boost::program_options::options_description SampleDesc_; // sample options
        int SAMPLE_sampleSize_;
        int SAMPLE_burnin_;
        int SAMPLE_thinning_;



    boost::program_options::options_description GridDesc_; // grid options
        unsigned int GRID_stepNb_;//default 10
        double GRID_MinDup_;//default -3
        double GRID_MaxDup_;//default 1
        double GRID_MinLoss_;//default -3
        double GRID_MaxLoss_;//default 1
        double GRID_MinTrans_;//default -3
        double GRID_MaxTrans_;//default 1
        bool GRID_NoLog_; //default false 


    boost::program_options::options_description NMDesc_; // nelderMead options
            long double  NM_No_improve_thr_; // =10e-3;
            unsigned int NM_No_improv_break_; // =100;
            unsigned int NM_Max_iter_; // =0;


    boost::program_options::options_description desc_; // all options
    boost::program_options::variables_map vm_;





public:
    OptionHandler();

    void readArguments(int argc , char* argv[]);
    // Getters

    //generic
    std::string get_algorithm() const {return algorithm_;}
    bool get_withTransfer() const {return withTransfer_;}

    std::string get_gene_input_filename() const { return gene_input_filename_;}
    std::string get_species_input_filename() const { return species_input_filename_;}
    std::string get_separator() const { return separator_;}
    unsigned int get_verbose_level() const {return verboseLevel_;}

    //advanced
    std::string get_corr_input_filename() const { return corr_input_filename_;}
    int get_seed() const {return seed_;}
    bool get_unrooted_sp() const {return unrooted_sp_;}
    std::string get_extant_sampling() const{ return extant_sampling_; } ;


    //rates
    double get_initialDupRate() const{return initialDupRate_;} 
    double get_initialLossRate() const{return initialLossRate_;} 
    double get_initialTransferRate() const{return initialTransRate_;} 
    bool get_branch_wise() const {return branch_wise_ ;}
    std::string get_input_rate_file() const {return input_rate_file_ ;}

    //output
    unsigned int get_outputSample(){return outputSample_ ;}
    std::string get_outputPrefix(){return outputPrefix_ ;} 

    //Grid
    unsigned int get_GRID_stepNb() const {return GRID_stepNb_;}
    double get_GRID_MinDup() const {return GRID_MinDup_;}
    double get_GRID_MaxDup() const {return GRID_MaxDup_;}
    double get_GRID_MinLoss() const {return GRID_MinLoss_;}
    double get_GRID_MaxLoss() const {return GRID_MaxLoss_;}
    double get_GRID_MinTrans() const { return GRID_MinTrans_; }
    double get_GRID_MaxTrans() const { return GRID_MaxTrans_; }
    bool get_GRID_NoLog() const {return GRID_NoLog_;}

    //Max
    bool get_MAX_writeIntermediary() const {return MAX_writeIntermediary_;}
    bool get_MAX_NoRateUpdate() const {return MAX_NoRateUpdate_;}
    int get_MAX_SpCCPUpdatingNbRound() const {return MAX_SpCCPUpdatingNbRound_ ;}
    double get_MAX_SpCCPUpdatingWeight() const {return MAX_SpCCPUpdatingWeight_ ;}
    double get_MAX_cutOff() const {return MAX_cutOff_ ;}
    bool get_MAX_relativeCutOff() const {return MAX_relativeCutOff_;}
    int get_MAX_sampleSize() const {return MAX_sampleSize_;}
    bool get_MAX_force_resolve_mode() const {return MAX_force_resolve_mode_;}
    float get_MAX_SpeciationLossWeights() const {return MAX_SpeciationLossWeights_;}
    bool get_MAX_OneVotePerReconciledTree() const {return MAX_OneVotePerReconciledTree_;}


    // sample
    int get_SAMPLE_sampleSize() const { return SAMPLE_sampleSize_; }
    int get_SAMPLE_burnin() const { return SAMPLE_burnin_; }
    int get_SAMPLE_thinning() const { return SAMPLE_thinning_; }

    // NM
    long double  get_NM_No_improve_thr() const { return NM_No_improve_thr_; }
    unsigned int  get_NM_No_improv_break() const { return NM_No_improv_break_; }
    unsigned int  get_NM_Max_iter() const { return NM_Max_iter_; }


    // Actions
    bool verify();
    bool Sample_verify();
    bool Grid_verify();
    bool Max_verify();
    void printUsage();

};

#endif
