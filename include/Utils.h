/*

This file contains util functions to the various algorithm proposed.

Created the: 07-05-2018
by: Wandrille Duchemin

Last modified the: 18-05-2018
by: Wandrille Duchemin

*/

#include <vector>
#include <map>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include <boost/random.hpp>
#include <boost/math/distributions/normal.hpp>


#include "ReconciledTree.h"
#include "ParentModel.h"
#include "PsDLModel.h"
#include "PsDTLModel.h"
#include "PsDLMaxUtils.h"

using namespace std;
using boost::math::normal;


double Uniform0to1( );

double Normal(double mean, double sd);

int readExtantSamplingFile(string filename, shared_ptr< TreesetMetadata > CCPDistribution, map< string ,  FLOAT_TYPE > &ExtantSamplingMap);

vector < vector< long double > > GRID_generate_rateList( unsigned int step, vector < double > minimums , vector < double > maximums , bool NoLog );

void MAX_countEvents( shared_ptr<RecNode> RT ,map < int , vector<long double>  > &evtCounts , long double weight =1 );
void MAX_countTransitions( shared_ptr<RecNode> RT ,map < int , vector<long double>  > &transitionCount , long double weight=1 );
void MAX_countTransitionsAndEvents( shared_ptr<RecNode> RT ,map < int , vector<long double>  > &evtCounts , long double weight );

vector<long double> MAX_estimateNewRates_DL( ParentModel * model , boost::dynamic_bitset<> species, vector<long double> &transitionCount);

vector<long double> MAX_estimateNewRates_DTL(  long double nbD , long double nbL , long double nbT , long double nbS );

double MAX_estimateNewRates_DTL(  ParentModel * model , 
                                  map < int , vector<long double> > &RateUpdateCount , 
                                  vector < long double > &EventSums,
                                  vector < long double > RateDefaultMinimums, 
                                  vector < long double > RateDefaultMaximums );

double MAX_estimateNewRates( ParentModel * model , 
                             map < int , vector<long double> > &RateUpdateCount ,
                             vector < long double > &EventSums , 
                             bool withTransfer,
                             vector < long double > RateDefaultMinimums, 
                             vector < long double > RateDefaultMaximums);


string MAX_findEvtAfterBranchingOut( shared_ptr<RecNode> RT );
vector<long double> MAX_estimateNewRates_DTL( long double Ee , long double Ge , long double Te , long double Espe , long double Etr , long double Ptr );
double MAX_estimateNewRates_DTL(  ParentModel * model , 
                                  map < int , vector<long double> > &RateUpdateCount , 
                                  vector < long double > &EventSums,
                                  vector < long double > RateDefaultMinimums, 
                                  vector < long double > RateDefaultMaximums , unsigned int VerboseLevel);

double SAMPLE_drawNewRate( double oldRate , double sd );
double SAMPLE_drawNewRate( double oldRate , double sd , double min , double max  );
bool SAMPLE_acceptProposition(long double currentLogP , long double newLogP ); 