#ifndef PSDTL_MODEL_H_
#define PSDTL_MODEL_H_


/*

This file defines a class representing the the PsDTL model 
of evolution of a gene CCP distribution inside a species CCP distribution
with events of duplication, loss, transfers and speciation.


Created the: 20-04-2018
by: Wandrille Duchemin

Last modified the: 07-05-2018
by: Wandrille Duchemin

*/

#include <CCPCPP/treeset_metadata.h>

#include "ParentModel.h"
#include "ReconciledTree.h"

//#include "PsDLModel.h"

#include <stdlib.h>
#include <iostream>
#include <bitset>
#include <math.h>
#include <boost/dynamic_bitset.hpp>

/*
NOTE : there are a lot of similarity with PsDLModel. 
    consider making a model parent class for both.
*/

typedef long double FLOAT_TYPE ;

typedef boost::dynamic_bitset<> CCPcladeIdType ;

//typedef unsigned short LIndexType ;
//
//typedef unsigned short CIndexType ;

using namespace std;




class PsDTLModel : public ParentModel
{
protected:

//////// ATTRIBUTES ///////////////


    // rates
    vector < FLOAT_TYPE > TransferRates; //     index : species clade id ; value : associated, branch-wise, loss rate
    vector < FLOAT_TYPE > SpeciationRates; //   index : species clade id ; value : associated, branch-wise, loss rate

    vector < FLOAT_TYPE > DuplicationPropensities; //  index : species clade id ; value : associated, branch-wise, duplication propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
    vector < FLOAT_TYPE > LossPropensities; //         index : species clade id ; value : associated, branch-wise, loss propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
    vector < FLOAT_TYPE > TransferPropensities; //     index : species clade id ; value : associated, branch-wise, loss propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
    vector < FLOAT_TYPE > SpeciationPropensities; //   index : species clade id ; value : associated, branch-wise, loss propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )


    // transfer algorithm specific attributes

    FLOAT_TYPE SumTransferExtinctionProba;
    vector < FLOAT_TYPE > SumTransferProba; // 1st index : gene clade id
                                            // value sum over all sigma of all proba of a gene gamma to transfer to a species sigma. (doesn't account for incompatibilities)

    vector< vector < FLOAT_TYPE > > TransferProbas;  // 1st index : species clade id ; sigma
                                                     // 2nd index : gene clade id ; gamma
                                                     // value     : sum of the probabilities to for a gene lineage gamma to transfer from species lineage sigma (accounts for incompatibilities)


    // probability maps


    vector< FLOAT_TYPE > ProbaGeneExtinct;  // 1st index : species clade id
                                            // value     : probability for a lineage to have all its descendant go extinct before the present
                                            //             equivalent to E(s,t) or P0(t) in other models
                                            //             along branch it follows the formula Ee[t] -> ( mu + (la - mu)/(1 + (E^((la - mu) t) la (-1 + p))/(mu - la p)))/la
                                            //             where t is branch length; mu is loss rate ; la is duplication rate

    vector< vector < FLOAT_TYPE > > ReconciliationMatrix;  // 1st index : species clade id ; sigma
                                                           // 2nd index : gene clade id ; gamma
                                                           // value     : probability for a gene lineage gamma to be present in species sigma at nramch interval l

    vector< vector< FLOAT_TYPE > > GeneLeavesProbas; // 1st index : species clade id ; sigma
                                                     // 2nd index : species id; placeholder for the gene clade id of a single gene leaf
                                                     // value     : probability for a gene lineage gamma to be present in species sigma at nramch interval l


//////// METHODS ///////////////

    void computeTransferProbas(CIndexType Gid);


    FLOAT_TYPE computeProbaSpeciation(CIndexType Sid, CIndexType Gid );
    FLOAT_TYPE computeProbaDuplication(CIndexType Sid, CIndexType Gid );
    FLOAT_TYPE computeProbaTransfer(CIndexType Sid, CIndexType Gid );
    FLOAT_TYPE computeProbaTransferLoss(CIndexType Sid, CIndexType Gid );
    FLOAT_TYPE computeProbaDuplicationLoss(CIndexType Sid, CIndexType Gid );

    FLOAT_TYPE ComputerecProba( CIndexType Sid, CIndexType Gid );


    shared_ptr<RecNode> backtrackAux( CIndexType Sid, CIndexType Gid);// different interface from PsDL
    long double backtrackAuxSpeciation( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> &currentRec, long double r);
    long double backtrackAuxDuplication( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> &currentRec, long double r);
    long double backtrackAuxTransfer( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> &currentRec, long double r);
    long double backtrackAuxTransferLoss( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> &currentRec, long double r);

    shared_ptr<RecNode> createLossNode( CIndexType Sid );// different interface from PsDL
    CIndexType findTransferDestinationSpecies(CIndexType Sid, CIndexType Gid );



    FLOAT_TYPE computeExtinctionProba( CIndexType Sid, unsigned int verboseLevel=0);
    void initProbaGeneExtinct();

    int getTransferIncompatibility( CCPcladeIdType g1 , CCPcladeIdType g2 );

    void initializeRecMatrix();

    /*
      !! both lineages must be leaf lineages !!
        Takes:
            - CIndexType Sid : species clade Id
            - CIndexType Gid : gene clade Id
        Returns:
            (FLOAT_TYPE) : probability of a LeafExtremity event
    */
    FLOAT_TYPE computeProbaLeafExtremity(CIndexType Sid, CIndexType Gid )
    {
        if(geneSpeciesCorrespondence[ Gid ] == Sid )
            return SpeciationPropensities[ Sid ]; // correct species => proba is propensity to speciate.
        //else
        return 0;// proba is 0
    }


    void reset_recMat();



public:

    PsDTLModel();

    ~PsDTLModel()
    {
        //cout << "deleting PsDTLModel" << endl;
        /*
        geneCCPdistribution.reset() ; // ccp distribution of the gene
        speciesCCPdistribution.reset() ; //ccp distribution of the species

        speciesCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
        speciesCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 

        geneCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
        geneCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 

        geneSpeciesCorrespondence.clear() ; //correspondence between the gene and species leaves


        DuplicationRates.clear();
        LossRates.clear();*/
        TransferRates.clear();
        SpeciationRates.clear();

        DuplicationPropensities.clear();
        LossPropensities.clear();
        TransferPropensities.clear();
        SpeciationPropensities.clear();


        ProbaGeneExtinct.clear(); //index : species clade id ; value : vector of the length of the corresponding branch interval


        for(auto it = ListTransferIncompatibilities.begin() ; it != ListTransferIncompatibilities.end() ; ++it)
        {
            it->clear();
        }
        ListTransferIncompatibilities.clear();


        for(auto it = ReconciliationMatrix.begin() ; it != ReconciliationMatrix.end() ; ++it)
        {
            it->clear();
        }
      
        ReconciliationMatrix.clear();  

        for(auto it = GeneLeavesProbas.begin() ; it != GeneLeavesProbas.end() ; ++it)
        {
            it->clear();
        }
      
        GeneLeavesProbas.clear();  

        for(auto it = TransferProbas.begin() ; it != TransferProbas.end() ; ++it)
        {
            it->clear();
        }
      
        TransferProbas.clear();  

    }

    // actions

    void addSpeciesCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel=0);// ONLY POST TREATMENT DIFFER FROM PsDL

    void computePropensities();

    void computeTransferIncompatibilities(unsigned int verboseLevel=0);

    void computeProbaGeneExtinct(unsigned int verboseLevel=0);

    void initializeGeneLeavesProbas();

    void computeReconciliationMatrix( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask , unsigned int verboseLevel=0);

    FLOAT_TYPE getOverAllProbability();

    shared_ptr<RecNode> backtrack();

    void reset();
    void reset_GeneLeavesProbas();

    //getter
    vector<FLOAT_TYPE> get_TransferRates() const { return TransferRates; }
    vector<FLOAT_TYPE> get_SpeciationRates() const { return SpeciationRates; }

    FLOAT_TYPE get_TransferRates(CIndexType spId) const { return TransferRates[spId]; }
    FLOAT_TYPE get_SpeciationRates(CIndexType spId) const { return 1; }

    FLOAT_TYPE get_ProbaGeneExtinct(int spId) const {return ProbaGeneExtinct[spId];}// takes : species clade id; returns : probability for a lineage to have all its descendant go extinct before the present
    FLOAT_TYPE get_ProbaGeneExtinct( boost::dynamic_bitset<> sigma )
    { return ProbaGeneExtinct[ speciesCCPidToCladeIndex[ sigma ] ]; }



    FLOAT_TYPE get_ReconciliationMatrix(int spId, int gId) const {return ReconciliationMatrix[spId][gId];}// takes : species clade id ; sigma , gene clade id : gamma ; returns : probability for a gene lineage gamma to be present in species sigma 

    bool get_recMatrixComputed() const{return recMatrixComputed;}// true if the matrix has been computed and is ready to be backtracked




    //// getters for the copy constructor 
    vector<  FLOAT_TYPE  > get_ProbaGeneExtinct() const { return ProbaGeneExtinct; } 
    vector<  vector < FLOAT_TYPE > > get_ReconciliationMatrix() const { return ReconciliationMatrix; }
    vector<  vector < FLOAT_TYPE > > get_GeneLeavesProbas() const { return GeneLeavesProbas; } 

    ////setters

    //  takes : species clade id ; associated, branch-wise, duplication rate
    void set_TransferRates(CCPcladeIdType spId, FLOAT_TYPE value)
    {
        auto it = speciesCCPidToCladeIndex.find( spId );
        if(it != speciesCCPidToCladeIndex.end())
            TransferRates[it->second] = value;
    }

    //  takes : duplication rate and set it as the global duplication rate for all the species tree
    void set_TransferRates( FLOAT_TYPE value )
    {
        for(unsigned i = 0 ; i < TransferRates.size() ; i++)
            TransferRates[i]=value;
    }

    void prepareForComputation(shared_ptr<TreesetMetadata> speciesCCPdistrib , 
                                vector <  FLOAT_TYPE  > DefaultRates, 
                                map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates, 
                                map< string , FLOAT_TYPE> extantSampling , 
                                unsigned int verboseLevel=0, 
                                double deltal = 0.05 , int minNbInt = 5 );

    void resetForNewParameters( unsigned int verboseLevel=0)
    {
        reset();
        computePropensities();
        initializeGeneLeavesProbas();
        computeProbaGeneExtinct(verboseLevel);
    }

    void reMapSpeciesCCPdistribution();

    vector< vector<long double> > get_Rates();
    void set_Rates( vector< vector<long double> > rateList );
    void set_Rates( vector<long double> rateList );
    void set_Rates( CIndexType Sid,  vector<long double> rateList );
};

#endif