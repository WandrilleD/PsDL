#ifndef REC_TREE_H_
#define REC_TREE_H_

/*

This file defines classes for very lightweight reconciled trees
 * a class of reconciled node



Created the: 19-02-2018
by: Wandrille Duchemin

Last modified the: 23-02-2018
by: Wandrille Duchemin

*/

#include <memory>
#include <vector>
#include <sstream>

using namespace std;


string createIndent(int level, string indentStr = "  ");
string float2string (float number) ; /* float to string conversion */
string int2string (int number) ; 

class RecNode : public enable_shared_from_this<RecNode>
{
protected:

    int id;

    int GeneCladeId;
    int SpeciesCladeId;
    int SpeciesBranchSubdivisionId;

    float BranchLength;

    string name;
    string SpeciesName;

    string event;

    vector< shared_ptr<RecNode> > children;
    weak_ptr<RecNode> parent;

    bool Tb;

    int setPOIdAux( int current );//recursively sets ids according to post-order traversal
public:

    RecNode( int GId, int SId, int LId, int id = 0);
    ~RecNode()
    {
        children.clear();
    } // the shared pointer should take care of themselves ; the parent pointer should always have been dealt with

    shared_ptr<RecNode> getptr() {
        return shared_from_this();
    }


    //getter
    int get_id() const { return id; }
    int get_GeneCladeId() const {return GeneCladeId;}
    int get_SpeciesCladeId() const {return SpeciesCladeId;}
    int get_SpeciesBranchSubdivisionId() const {return SpeciesBranchSubdivisionId;}
    float get_BranchLength() const {return BranchLength;}
    string get_name() const {return name;}
    string get_event() const {return event;}
    string get_SpeciesName() const { return SpeciesName; }
    vector< shared_ptr<RecNode> > get_children() { return children; }
    bool is_leaf() const { return (children.size() == 0); }
    bool is_RealLeaf() const { return (event == "leaf"); }
    bool is_root() const { return parent.expired(); }
    bool is_Tb()const {return Tb;}


    //setter
    void set_GeneCladeId(int  value){ GeneCladeId = value ; }
    void set_SpeciesCladeId(int  value){ SpeciesCladeId = value ; }
    void set_SpeciesBranchSubdivisionId(int  value){ SpeciesBranchSubdivisionId = value ; }
    void set_BranchLength(float  value){ BranchLength = value ; }
    void set_name(string  value){ name = value ; }
    void set_event(string  value){ event = value ; }
    void set_SpeciesName(string value) { SpeciesName = value; }
    void set_Tb(bool value ) {Tb=value;}

    //tree manipulation

    // sets up the parent pointer of the child
    void add_child( shared_ptr<RecNode> child )
    {
        children.push_back( child );
        child->set_parent( getptr(), false);
    }
    
    
    void set_parent( shared_ptr<RecNode> newParent , bool informParent ) {parent = newParent;}

    //output
    string getRecXML( int indentLevel = 0 ); // recursive function

    pair<int,int> countEvents(); // recursive function

    void setPOId();//recursively sets ids according to post-order traversal

    vector < shared_ptr<RecNode> > getLeaves();
    vector < shared_ptr<RecNode> > getRealLeaves();
};

#endif