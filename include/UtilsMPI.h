/*

This file contains helper functions
for the MPI version 


Created the: 24-05-2018
by: Wandrille Duchemin

Last modified the: 24-05-2018
by: Wandrille Duchemin

*/



#include "ParentModel.h"

// includ eof the CCP library. local for now
#include <CCPCPP/treeset_metadata.h>


#include <bitset>
#include <boost/dynamic_bitset.hpp>

#include <boost/mpi.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>


#include "MPI_PsDLUtils.h"


namespace mpi = boost::mpi;

using namespace std;

void broadcastExtantSamplingRates( mpi::communicator &world, int rank , map< string , FLOAT_TYPE > &extantSamplingMap );
void broadcastRates( mpi::communicator &world, int rank , map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > &rateMap );

void broadcastRates( mpi::communicator &world, int rank , ParentModel * model , bool branchWise );




void gatherRateUpdateCount( mpi::communicator &world, int rank , map < int , vector< long double>  > &RateUpdateCount );