#ifndef PSDLSAMPLE_UTILS_H
#define PSDLSAMPLE_UTILS_H



/*

This file contains utilitary functions for PsDLsample 

Created the: 23-02-2015
by: Wandrille Duchemin

Last modified the: 06-03-2018
by: Wandrille Duchemin


*/

#include "PsDLModel.h"


using namespace std;

double Uniform0to1( );
double Exponential( double mean);

/*
    Draws rate scaling from one iteration to the next
        exp( random between log(1-alpha) and log(1+alpha)  )
*/
double drawRateScale( double alpha );

/*
    Draws new rate 
     oldRate * exp( random between log(1-alpha) and log(1+alpha)  )
*/
double drawNewRate( double oldRate , double alpha );

bool acceptProposition(long double currentP , long double newP, bool AreLog) ;

bool samplerStep( PsDLModel * &model , vector< shared_ptr<TreesetMetadata> > &GeneCCPDistribV, map<string, string > &GtoSpCorr, vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > &GeneCladeSpeciesMasks ,double &currentDupRate , double &currentLossRate , long double &currentP , unsigned int verboseLevel, double alpha , double  minRate, double maxRate);

void InitSampleFile( ostream& OUT);

void WriteSampleToFile( ostream& OUT , unsigned int sampleNb , unsigned int  stepNumber , double DupRate , double LossRate , double currentP);


#endif