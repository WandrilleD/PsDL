#ifndef SPCCPUPDATE_UTILS_H_
#define SPCCPUPDATE_UTILS_H_


/*

This file defines helper functions 
for the analyse of reconciled trees to infer new species CCPs

Created the: 01-03-2018
by: Wandrille Duchemin

Last modified the: 21-03-2018
by: Wandrille Duchemin

*/

#include <iostream>

#include <boost/dynamic_bitset.hpp>




// includes of the CCP library. local for now
#include <CCPCPP/treeset_metadata.h>

#include "ReconciledTree.h"


using namespace std;



//Returns the map of all split of the species tree encountered in the reconciled tree, with a value corresponding to the number of times it is seen
map< int , map < int , float > > getReconciledTreeSpeciesSplits( shared_ptr< RecNode > RT , float SLweight =0 );

void AddReconciledTreeSpeciesSplitsToMap( shared_ptr< RecNode > RT , map< int , map < int , float > > &splitCounts , float SLweight = 0, bool OneVotePerReconciledTree = true);

void UpdateCCPdistribution( shared_ptr< TreesetMetadata > CCPdistrib,  map< int , map < int , float > > &splitCounts , vector< boost::dynamic_bitset<> > &SpeciesIdToCladeId , double splitWeight =1.0 );

int CutOffCCPdistribution( shared_ptr< TreesetMetadata > CCPdistrib, double cutOff , bool relativeProba, unsigned int verboseLevel);

void MakeBipCoherentWithRootSplits( shared_ptr< TreesetMetadata > CCPdistrib , double defaultForAbsence = 1);

//vector< boost::dynamic_bitset<> > CutOffClade( shared_ptr< TreesetMetadata > CCPdistrib ,  boost::dynamic_bitset<> Cid , double cutOff , bool relativeProba);
vector <int> forceSpeciesCladeResolution( vector<int> Sids , 
                                          shared_ptr< TreesetMetadata > CCPdistrib , 
                                          map< int , map < int , float > > splitCounts , 
                                          vector< boost::dynamic_bitset<> > &SpeciesIdToCladeId , 
                                          double threshold =0 );



#endif