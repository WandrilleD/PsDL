#ifndef PSDL_MODEL_H_
#define PSDL_MODEL_H_


/*

This file defines a class representing the the PsDL model 
of evolution of a gene CCP distribution inside a species CCP distribution
with events of duplication, loss and speciation.


Created the: 14-02-2018
by: Wandrille Duchemin

Last modified the: 04-05-2018
by: Wandrille Duchemin

*/

#include <CCPCPP/treeset_metadata.h>


#include "ParentModel.h"
#include "ReconciledTree.h"

#include <stdlib.h>
#include <iostream>
#include <bitset>
#include <math.h>
#include <boost/dynamic_bitset.hpp>

typedef long double FLOAT_TYPE ;

typedef boost::dynamic_bitset<> CCPcladeIdType ;

//typedef unsigned short LIndexType ;
//
//typedef unsigned short CIndexType ;

using namespace std;


//CCPcladeIdType getSisterClade(CCPcladeIdType parent, CCPcladeIdType child){return ( ~child & parent );}

class PsDLModel : public ParentModel
{
protected:

//////// ATTRIBUTES ///////////////


    FLOAT_TYPE deltal_ ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    LIndexType minNbBLInterval_ ; // minimum number of interval for a given branch of the species tree

    vector < LIndexType > branchesDivisionNumber; // index : species clade id ; value : associated number of "branch length interval", 
                                                  //                                    also noted lsigma (0 is the part of the branch that is closest to the present, lsigma is the part that is the farthest)
                                                  //                                    defined as max( ceil( l / deltal ) , minNbBLInterval )
                                                  //                                    where l is the length of the branch 
                                                  //                                    deltal is the length of the branch length intervals)

    vector< vector < FLOAT_TYPE >  > branchesDivisionLength; //index : species clade id ; value : vector of the length of the corresponding branch interval


        /* Note : 
        as a convention, a branch will be composed of (number_of_division + 1) cells
        the first cell (of index 0) correspond to the very start of the branch (the part closer to the present, given the branch orientation), with length 0
        the other cells, of index n(>0), correspond to the point along the branch that is lengths[n-1] above the previous one.
        */


    // probability maps


    vector< vector< FLOAT_TYPE > > ProbaGeneExtinct;  // 1st index : species clade id
                                                         // 2nd index : position along the branch of the species clade
                                                         // value     : probability for a lineage to have all its descendant go extinct before the present
                                                         //             equivalent to E(s,t) or P0(t) in other models
                                                         //             along branch it follows the formula Ee[t] -> ( mu + (la - mu)/(1 + (E^((la - mu) t) la (-1 + p))/(mu - la p)))/la
                                                         //             where t is branch length; mu is loss rate ; la is duplication rate

    vector< vector< FLOAT_TYPE > > ProbaGeneOneChild;    // 1st index : species clade id
                                                         // 2nd index : position along the branch of the species clade
                                                         // value     : probability for a lineage to have all its descendant but one go extinct before the present
                                                         //             equivalent to G(s,t) or P1(t) in other models
                                                         //             along branch it follows the formula (E^((-la + mu) t) (la - mu)^2 q)/(la - E^((-la + mu) t) mu - la p +   E^((-la + mu) t) la p)^2
                                                         //             where t is branch length; mu is loss rate ; la is duplication rate ; Ee[0]=p ; Ge[0]=q=1


    vector< vector< vector < FLOAT_TYPE > > > ReconciliationMatrix;  // 1st index : species clade id ; sigma
                                                                   // 2nd index : position along the branch of the species clade ; l
                                                                   // 3rd index : gene clade id ; gamma
                                                                   // value     : probability for a gene lineage gamma to be present in species sigma at nramch interval l

    vector< vector< vector< FLOAT_TYPE > > > GeneLeavesProbas; // 1st index : species clade id ; sigma
                                                                   // 2nd index : position along the branch of the species clade ; l
                                                                   // 3rd index : species id; placeholder for the gene clade id of a single gene leaf
                                                                   // value     : probability for a gene lineage gamma to be present in species sigma at nramch interval l



//////// METHODS ///////////////

    vector <FLOAT_TYPE> divideOneBranch(FLOAT_TYPE Blen);

    FLOAT_TYPE getSpeciesBranchLength( CIndexType sigma );

    FLOAT_TYPE computeExtinctionProba( FLOAT_TYPE D, FLOAT_TYPE L , FLOAT_TYPE l , FLOAT_TYPE I );
    FLOAT_TYPE computeOneChildProba( FLOAT_TYPE D, FLOAT_TYPE L , FLOAT_TYPE l , FLOAT_TYPE I0 );//, FLOAT_TYPE I1 );

    FLOAT_TYPE computeExtinctionProba( CIndexType Sid, LIndexType Lid , unsigned int verboseLevel = 0);
    FLOAT_TYPE computeOneChildProba( CIndexType Sid, LIndexType Lid );
    FLOAT_TYPE ComputerecProba( CIndexType Sid, LIndexType Lid, CIndexType Gid );



    void initializeRecMatrix();

    /*
      !! both lineages must be leaf lineages !!
        Takes:
            - CIndexType Sid : species clade Id
            - CIndexType Gid : gene clade Id
        Returns:
            (FLOAT_TYPE) : probability of a LeafExtremity event
    */
    FLOAT_TYPE computeProbaLeafExtremity(CIndexType Sid, CIndexType Gid )
    {
        if(geneSpeciesCorrespondence[ Gid ] == Sid )
            return 1; // correct species.
        //else
        return 0;// proba is 0
    }


    //// probability computation functions:
    FLOAT_TYPE computeProbaNullIntraBranch( CIndexType Sid , LIndexType Lid , CIndexType Gid );
    FLOAT_TYPE computeProbaDupIntraBranch( CIndexType Sid , LIndexType Lid , CIndexType Gid );
    FLOAT_TYPE computeProbaIntraBranch( CIndexType Sid , LIndexType Lid , CIndexType Gid );

    FLOAT_TYPE computeProbaInterBranch( CIndexType Sid , CIndexType Gid );

    ////backtrack functions:   
    shared_ptr<RecNode> backtrackIntra( CIndexType Sid, LIndexType Lid, CIndexType Gid);
    shared_ptr<RecNode> backtrackInter( CIndexType Sid, CIndexType Gid);
    shared_ptr<RecNode> createLossNode( CIndexType Sid , LIndexType Lid );
    shared_ptr<RecNode> backtrackAux( CIndexType Sid, LIndexType Lid, CIndexType Gid);


    //// resets
    void reset_GeneLeavesProbas();
    void reset_recMat();
    void reset_PoneGene(); // calls reset_recMat as well

    /// associated to partial re-compute...
    void partial_reCompute( vector< CIndexType > toReset );// partially resets probaExtinct, probaOneGene and reconciliationMatrix
    vector< CCPcladeIdType > get_ParentClades( CCPcladeIdType sigma );
    vector< CIndexType > get_ParentClades( CIndexType Sid );



public:

    PsDLModel();
    PsDLModel( const PsDLModel& other);
    ~PsDLModel()
    {
      //cout << "deleting PsDLModel" << endl;
      /*
      geneCCPdistribution.reset() ; // ccp distribution of the gene
      speciesCCPdistribution.reset() ; //ccp distribution of the species

      speciesCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
      speciesCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 

      geneCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
      geneCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 

      geneSpeciesCorrespondence.clear() ; //correspondence between the gene and species leaves

      DuplicationRates.clear(); //  index : species clade id ; value : associated, branch-wise, duplication rate
      LossRates.clear(); //         index : species clade id ; value : associated, branch-wise, loss rate
      */
      branchesDivisionNumber.clear(); // index : species clade id ; value : associated number of "branch length interval", 

      for(auto it = branchesDivisionLength.begin() ; it != branchesDivisionLength.end() ; ++it)
        (*it).clear();
      branchesDivisionLength.clear(); //index : species clade id ; value : vector of the length of the corresponding branch interval

      for(auto it = ProbaGeneExtinct.begin() ; it != ProbaGeneExtinct.end() ; ++it)
        (*it).clear();
      ProbaGeneExtinct.clear(); //index : species clade id ; value : vector of the length of the corresponding branch interval

      for(auto it = ProbaGeneOneChild.begin() ; it != ProbaGeneOneChild.end() ; ++it)
        (*it).clear();
      ProbaGeneOneChild.clear(); //index : species clade id ; value : vector of the length of the corresponding branch interval

      for(auto it = ReconciliationMatrix.begin() ; it != ReconciliationMatrix.end() ; ++it)
      {
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2)
          it2->clear();
        it->clear();
      }
      
      ReconciliationMatrix.clear();  

      for(auto it = GeneLeavesProbas.begin() ; it != GeneLeavesProbas.end() ; ++it)
      {
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2)
          it2->clear();
        it->clear();
      }
      
      GeneLeavesProbas.clear();  

    }


    //getter

    FLOAT_TYPE get_deltal() const { return deltal_; } // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    LIndexType get_minNbBLInterval() const {return minNbBLInterval_;} // minimum number of interval for a given branch of the species tree

    LIndexType get_branchesDivisionNumber(int spId ) const {return branchesDivisionNumber[spId];}// takes : species clade id ; returns : associated number of "branch length interval", 

    FLOAT_TYPE get_branchesDivisionLength(int spId, int Lid) const {return branchesDivisionLength[spId][Lid];}//takes : species clade id , nramch interval id ; returns the length of the corresponding branch interval

    FLOAT_TYPE get_ProbaGeneExtinct(int spId, int Lid) const {return ProbaGeneExtinct[spId][Lid];}// takes : species clade id, position along the branch ; returns : probability for a lineage to have all its descendant go extinct before the present
    FLOAT_TYPE get_ProbaGeneOneChild(int spId, int Lid) const {return ProbaGeneOneChild[spId][Lid];}// takes : species clade id, position along the branch ; returns : probability for a lineage to have all its descendant but one go extinct before the present

    FLOAT_TYPE get_ProbaGeneExtinct( boost::dynamic_bitset<> sigma )
    { return ProbaGeneExtinct[ speciesCCPidToCladeIndex[ sigma ] ][0]; }

    FLOAT_TYPE get_ReconciliationMatrix(int spId, int Lid, int gId) const {return ReconciliationMatrix[spId][Lid][gId];}// takes : species clade id ; sigma , position along the branch of the species clade : l , gene clade id : gamma ; returns : probability for a gene lineage gamma to be present in species sigma at branch interval l



    //// getters for the copy constructor 
    vector < LIndexType >  get_branchesDivisionNumber() const { return branchesDivisionNumber; } 
    vector< vector < FLOAT_TYPE >  >  get_branchesDivisionLength() const { return branchesDivisionLength; } 
    vector< vector< FLOAT_TYPE > > get_ProbaGeneExtinct() const { return ProbaGeneExtinct; } 
    vector< vector< FLOAT_TYPE > > get_ProbaGeneOneChild() const { return ProbaGeneOneChild; }
    vector< vector< vector < FLOAT_TYPE > > > get_ReconciliationMatrix() const { return ReconciliationMatrix; }
    vector< vector< vector < FLOAT_TYPE > > > get_GeneLeavesProbas() const { return GeneLeavesProbas; } 


    ////setters


    void set_deltal(FLOAT_TYPE value){ deltal_ = value; } // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval") 
    void set_minNbBLInterval(LIndexType value)  { minNbBLInterval_ = value; } // minimum number of interval for a given branch of the species tree 


    //manipulation
    void divideSpeciesBranches(unsigned int verboseLevel=0);    //
    void initializeGeneLeavesProbas();                          // 

    void computeProbaGeneExtinct(unsigned int verboseLevel=0);  // 
    void computeProbaGeneOneChild(unsigned int verboseLevel=0); //

    void addSpeciesCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel=0);// makes some precomputations With ids


    void computeReconciliationMatrix( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask , unsigned int verboseLevel=0);

    FLOAT_TYPE getOverAllProbability();

    shared_ptr<RecNode> backtrack();

    void reset(); //reset probaExtinct calls reset_PoneGene as well (wchich also resets the recMatrix) 

    int changeBranchWiseParamAndRecompute( CIndexType cladeToChange , string paramName , FLOAT_TYPE newValue );
    
    void reMapSpeciesCCPdistribution();

    //void quickCheck(); /// tmp

    void prepareForComputation(shared_ptr<TreesetMetadata> speciesCCPdistrib , 
                               vector <  FLOAT_TYPE  > DefaultRates, 
                               map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates, 
                               map < string , FLOAT_TYPE>  extantSampling, 
                               unsigned int verboseLevel=0, 
                               double deltal = 0.05 , 
                               int minNbInt = 5 );

    void resetForNewParameters( unsigned int verboseLevel=0)
    {
        reset();
        computeProbaGeneExtinct(verboseLevel);
        computeProbaGeneOneChild(verboseLevel);
    }
};


#endif
