/*

This file defines helper functions 
for the reconciliation of a species and a gene CCP distributions

Created the: 13-02-2018
by: Wandrille Duchemin

Last modified the: 04-05-2018
by: Wandrille Duchemin

*/

#include "PsDLUtils.h"


/*

    Takes:
        - string filename : name of the file containing the correspondence between gene (leaves) and species (leaves) name
                            (one per line, name of the gene followed by name of the species separated by a space.)
        - map< string , string > &correspondenceMap : keys : gene name
                                                                    values : species name

    Returns:
        int :  0 if no problem
               1 if the file cannot be opened

*/
int readGeneSpeciesCorrespondenceFile(string filename, map< string , string > &correspondenceMap)
{

    ifstream CorrStream(filename.c_str());
    

    if( CorrStream.is_open() )
    {
        while( !CorrStream.eof() ) 
        {
            string gene;
            string species;
    
            CorrStream >> gene;
    
            if(CorrStream.eof()) // to avoid hanging line
                continue;
            CorrStream >> species;
    
            correspondenceMap[gene] = species;
    
        }

        CorrStream.close();
    }
    else
    {
        return 1;
    }

    
    
    return 0;
}

/*

    Takes:
        - string inputFilename : file to parse the trees data 
        - shared_ptr<TreesetMetadata> CCPdistrib : CCP distribution that will be built
        - int fileFormat  : 1 -> newick format
                            2 -> ale format
                            3 -> format has to be detected

        - bool handleAsUnrooted = true : unroot the trees or not

    Returns:
        int :  0 if no problem
               1 if the file cannot be opened
               2 if the file can't be determined to be ale or newick

This function will evolve with the ccp library
*/
int buildCCPDistributionFromFile(string inputFilename, shared_ptr<TreesetMetadata> CCPdistrib, int fileFormat , bool handleAsUnrooted )
{

    if(fileFormat == 3)
    {
        fileFormat = guessFileFormat(inputFilename);
        if(fileFormat == -1)
            return 1; // couldn't open the file

    }

    bool isAle=false;
    if(fileFormat == 2)
        isAle = true;

    if(fileFormat == 3)
        return 2; //couldn't determine format

    return buildCCPDistributionFromFile( inputFilename,  CCPdistrib, isAle ,  handleAsUnrooted );
}

/*

    Takes:
        - string inputFilename : file to parse the trees data 
        - shared_ptr<TreesetMetadata> CCPdistrib : CCP distribution that will be built
        - bool isAle = false : specifies if the trees data is in .ale format (if false it is presumed that the trees are in newick format)
        - bool handleAsUnrooted = true : unroot the trees or not

    Returns:
        int :  0 if no problem
               1 if the file cannot be opened


This function will evolve with the ccp library
*/
int buildCCPDistributionFromFile(string inputFilename, shared_ptr<TreesetMetadata> CCPdistrib, bool isAle , bool handleAsUnrooted )
{

    if(!isAle)
    {

        return buildCCPDistributionFromTreeSetFile( inputFilename, CCPdistrib, handleAsUnrooted );

    }
    else
    {
        string ale_input_line;

        // TODO: move to treesetmetadata
        string constructor_string;
        string observations;
        string bipartition_counts;
        string bipartition_branchlengths;
        string tripartition_counts;
        string last_leafset_id;
        string leaf_ids;
        string set_ids;
        // ENDTOdO

        ifstream input_file( inputFilename );
        if( input_file.is_open() )
        {

            // It shouldn't be so ugly
            // Search for C++11 STD library which can read in file to a stirng to a given position
            // http://insanecoding.blogspot.hu/2011/11/how-to-read-in-file-in-c.html


            // Get one tree from newick formatted input file, semicolon will be discarded
            while( getline(input_file, ale_input_line, ccpcpp_presets::ale_comment_marker) )
            {

                CCPdistrib->parse(ale_input_line);


            }
            input_file.close();

            CCPdistrib->process_ale();
        }
        else
        {
            cerr << "Unable to open ALE tree input file: " << inputFilename << endl;
            return 1;
        }
    }

    return 0;
}


/*

    Takes:
        - string inputFilename : file to parse the trees data 
        - shared_ptr<TreesetMetadata> CCPdistrib : CCP distribution that will be built
        - bool handleAsUnrooted = true : unroot the trees or not

    Returns:
        int :  0 if no problem
               1 if the file cannot be opened


This function will evolve with the ccp library
*/
int buildCCPDistributionFromTreeSetFile(string inputFilename, shared_ptr<TreesetMetadata> CCPdistrib, bool handleAsUnrooted )
{
    // Object which will hold all the trees
    shared_ptr<Treeset> treeset = make_shared<Treeset>(handleAsUnrooted);
    // Read in newick trees
    string newick_tree_string;
    ifstream input_file( inputFilename );
    if( input_file.is_open() )
    {
        // Get one tree from newick formatted input file, semicolon will be discarded
        while( getline(input_file, newick_tree_string, ';') )
        {
            treeset->add_tree_from_newick(newick_tree_string);
        }
        input_file.close();
    }
    else
    {
        cerr << "Unable to open newick tree input file: " << inputFilename << endl;
        return 1;
    }

    treeset->build_leaf_dictionary();


    CCPdistrib->parse(treeset);

    return 0;
}

/*
    Takes:
        - string filename : name of the file to open
        - vector < string > toFill : object to fill with gene distribution filenames

    Returns:
        (int) : 0 if no problem
                1 if the file cannot be opened
*/
int readGeneDistributionFile( string filename, vector < string > &toFill)
{

    string GeneDistribFileName;

    ifstream input_file( filename );
    if( input_file.is_open() )
    {
        // Get one tree from newick formatted input file, \n will be discarded
        while( getline(input_file, GeneDistribFileName, '\n') )
        {
            toFill.push_back( GeneDistribFileName );
        }
        input_file.close();
    }
    else
    {
        cerr << "Unable to open gene distribution input file: " << filename << endl;
        return 1;
    }


    return 0;

}

/*

    Takes:
        - string leafName
        - string separator

    Returns:
        (pair <string , string>) : leafName cut in 2 according to the first occurence of separator (which is absent from output)

*/
pair <string , string> splitLeafName(string leafName, string separator)
{
    size_t pos = leafName.find(separator);

    if(pos == leafName.npos)
    {
        return pair<string, string>( leafName, "" );
    }

    return pair<string, string>( leafName.substr(0,pos), leafName.substr(pos+separator.size()) );

}


/*
    Takes:
        - vector< RecNode > sample : sample of reconciled trees

    Returns:
        ( pair< int , int > ) : count of duplications and losses
*/
pair< int , int > getCountEvents( vector< shared_ptr< RecNode > > sample )
{
    pair<int,int> count(0,0);
    for( auto it = sample.begin() ; it != sample.end(); ++it)
    {
        pair<int,int> tmp = (*it)->countEvents();
        count.first += tmp.first;
        count.second += tmp.second;
    }

    return count;
}


/*
    Takes:
        - shared_ptr< TreesetMetadata > CCPdistrib

    Returns:
        (double) : mean branch length of the trees in the given CCP distribution

*/
double getWeightedBranchLenSum( shared_ptr< TreesetMetadata > CCPdistrib )
{

    boost::dynamic_bitset<> rootClade { CCPdistrib->get_number_of_leaves() , 0};
    rootClade.flip();

    return getWeightedBranchLenSumAux( CCPdistrib , rootClade);

}

/*
    !!recursive function!!

    Takes:
        - shared_ptr< TreesetMetadata > CCPdistrib
        - boost::dynamic_bitset<> currentClade : current clade in the recursion 
    Returns:
        (double) : mean branch length of the subtrees rooted at the given clade in the given CCP distribution

*/
double getWeightedBranchLenSumAux( shared_ptr< TreesetMetadata > CCPdistrib , boost::dynamic_bitset<> currentClade )
{

    double bip = CCPdistrib->get_count_of_bipartition( currentClade );

    double WblenSum = CCPdistrib->get_branchlength(currentClade);

    //cout << "looking at "<< currentClade << " blen: "<< WblenSum << endl;

    if( ! CCPdistrib->is_leaf( currentClade ) )
    {
        for( auto Trit = CCPdistrib->tripartition_cbegin( currentClade ) ; Trit != CCPdistrib->tripartition_cend( currentClade ) ; ++Trit)
        {
            double pCCP = Trit->second / bip;
    
            WblenSum += pCCP * ( getWeightedBranchLenSumAux( CCPdistrib ,  CCPdistrib->get_complementer(currentClade, Trit->first ) ) + getWeightedBranchLenSumAux( CCPdistrib , Trit->first ) );
    
        }
    }

    //cout << "\tsubtree:"<< WblenSum << endl;


    return WblenSum;

}



void WriteRecTreeToStream( ostream& OUT, shared_ptr< RecNode > RT , string description)
{

    OUT << "  <recGeneTree>" << endl;
    OUT << "    <phylogeny rooted=\"true\">" << endl;

    if(description!="")
    {
        OUT << "      <description>" <<  description << "</description>" << endl;
    }


    string lines = RT->getRecXML( 3 ); // recursive function
    OUT << lines;
    OUT << "    </phylogeny>" << endl;
    OUT << "  </recGeneTree>" << endl;

}

void InitRecXMLToStream( ostream& OUT)
{
    OUT << "<recPhylo>" << endl;
}

void FinishRecXMLToStream( ostream& OUT)
{
    OUT << "</recPhylo>" << endl;

}





/*
    Takes:
        - shared_ptr< TreesetMetadata > geneDistribution  : distribution of genes CCPs
        - shared_ptr< TreesetMetadata > speciesDistribution  : distribution of species CCPs

    Returns:
        ( map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > ): keys: bitset of gene clade
                                                                      values: bitset of corresponding species clade (i.e : bitset of its species )
                                                                                ** the species bitset will be empty if the species doesn't actually exists => way to verify correspondence validity??
*/
map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > getGeneCladeSpeciesMasks( shared_ptr< TreesetMetadata > geneDistribution , shared_ptr< TreesetMetadata > speciesDistribution , map< string , string > GtoSpCorr)
{
    map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GspMask ;

    for( auto it = geneDistribution->bipartition_cbegin() ; it != geneDistribution->bipartition_cend(); ++it)
    {
        if ( geneDistribution->is_leaf( it->first ) )
        {
            vector<string> GeneLeafSet = getCladeLeafNames( geneDistribution , it->first); // gene bitset to gene names
    
            vector<string> SpLeafSet;
            for(auto leafIt = GeneLeafSet.begin() ; leafIt != GeneLeafSet.end() ; ++leafIt)
                SpLeafSet.push_back( GtoSpCorr[ *leafIt ] ); //gene names to species names
    
            GspMask[ it->first ] = getCladeBitSet( speciesDistribution , SpLeafSet); // species names to species bitset
        }
        else
        { // clade sp mask == OR of the sp masks of its children
            auto firstTrip = geneDistribution->tripartition_cbegin( it->first ) ; 
            boost::dynamic_bitset<> gammaP = firstTrip->first;
            boost::dynamic_bitset<> gammaPP = geneDistribution->get_complementer( it->first , gammaP );

            auto s1 =  GspMask.find( gammaP ) ;
            auto s2 =  GspMask.find( gammaPP ) ;
            if( ( s1 == GspMask.end() ) || ( s2 == GspMask.end() ) )
            {
                cerr << "!!WARNING!! : problem while computing species masks. using long but safe procedure instead to avoid crashing." << endl;

                vector<string> GeneLeafSet = getCladeLeafNames( geneDistribution , it->first); // gene bitset to gene names
        
                vector<string> SpLeafSet;
                for(auto leafIt = GeneLeafSet.begin() ; leafIt != GeneLeafSet.end() ; ++leafIt)
                    SpLeafSet.push_back( GtoSpCorr[ *leafIt ] ); //gene names to species names
        
                GspMask[ it->first ] = getCladeBitSet( speciesDistribution , SpLeafSet); // species names to species bitset

            }
            else
            {
                GspMask[ it->first ] = GspMask[ gammaP ] | GspMask[ gammaPP ] ;            
            }

        }
    }
    return GspMask;
}

/*
    Takes:
        - shared_ptr< TreesetMetadata > CCPDistribution  : distribution of CCPs
        - boost::dynamic_bitset<> Cid : bitset of a clade
    
    Returns:
        (vector<string>) : vector of the leaf names
*/
vector<string> getCladeLeafNames( shared_ptr< TreesetMetadata > CCPDistribution , boost::dynamic_bitset<> Cid)
{
    vector<string> leafSet;
    for(boost::dynamic_bitset<>::size_type i = 0; i < Cid.size(); ++i)
    {
        if(Cid[i])
        {
            boost::dynamic_bitset<> leafBitset (Cid.size(),0);
            leafBitset[i] = 1;

            leafSet.push_back( CCPDistribution->get_name_of_leaf( leafBitset ) );
        }
    }
    return leafSet;
}


/*
    Takes:
        - shared_ptr< TreesetMetadata > CCPDistribution  : distribution of CCPs
        - vector<string> leafSet: vector of leaf names
    
    Returns:
        ( boost::dynamic_bitset<> ) : bitset of the correponding clade
*/
boost::dynamic_bitset<> getCladeBitSet( shared_ptr< TreesetMetadata > CCPDistribution , vector<string> leafSet)
{
    boost::dynamic_bitset<> result ( CCPDistribution->get_number_of_leaves() , 0 );
    for(auto leafIt = leafSet.begin() ; leafIt != leafSet.end() ; ++leafIt)
    {
        boost::dynamic_bitset<> leaf = CCPDistribution->get_bitset_of_leaf( *leafIt );
        result |= leaf; // adding the leaf
    }

    return result;
}

/*
    Takes:
        - map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > ): keys: bitset of gene clade
                                                                      values: bitset of corresponding species clade (i.e : bitset of its species )
                                                                                ** the species bitset will be empty if the species doesn't actually exists => way to verify correspondence validity??

    Returns:
        (boost::dynamic_bitset<>): bitset of the first gene clade that does not corrspond to a valid species
                                    OR
                                   empty if all gene btiset actually coreesponds to a valifd speices
*/
boost::dynamic_bitset<> verifyLeafCorrespondences( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GspMask)
{
    boost::dynamic_bitset<>::size_type counter = 0;

    for( auto it = GspMask.begin() ; it != GspMask.end() ; ++it)
    {
        if( it->first.count() == 1 ) 
        { // we only need to check the leaf clades --> clades with only one 1.

            if(it->second.count()  == 0 )
            { // error -> unknown species association
                return it->first;
            }


            counter++;
            if( it->first.size() == counter )
                break; // means that we've checked all leaf clades -> no need to go further
        }
    }

    return boost::dynamic_bitset<>(1,0); //empty bitset --> no problem
}


/*
    Takes:
        - ParentModel * model

    Returns:
        ( map< boost::dynamic_bitset<>, vector< double  > > ) : keys: bitset of species
                                                          value: vector < duplication rate,  loss rate [, transfer rate]> > 
                                                                 <- values are normalized by branch length IF there is no transfers

*/
map< boost::dynamic_bitset<>, vector< double > >  getBranchWiseRates( ParentModel * model )
{
    map< boost::dynamic_bitset<>, vector< double >  >  res;

    shared_ptr<TreesetMetadata> spCCP = model->get_speciesCCPdistribution();

    vector< vector<long double> > Rates = model->get_Rates();

    bool hasTransfer = (Rates[0].size() ==3);

    for(unsigned int Sid = 1; Sid < Rates.size() ; Sid++)
    {
        boost::dynamic_bitset<> BS = model->get_CCPindexFromSpeciesCladeIndex( Sid ) ;


        double blen = 1;
//        if( hasTransfer )
//        {
//
//            if( Sid == ( Rates.size() -1 ) )
//                blen = model->get_rootBranchLength();
//            else
//                blen = spCCP->get_branchlength( BS );
//
//            //if( blen == 0 )
//            //    blen = 1;
//        }

        res[BS] = vector< double >();
        res[BS].reserve(2 + hasTransfer );

        for(auto r : Rates[Sid])
            res[BS].push_back( r * blen );        
    }
        
    return res;
}


/*
    outputs duplication and loss rates for each clade ; normalized for Blen 1.

    Takes:
        - ostream& OUT : stream to output in
        - PsDLModel * model

*/
void OutputBranchWiseRates(  ostream& OUT ,ParentModel * model )
{
    //cout << "OutputBranchWiseRates" << endl;


    map< boost::dynamic_bitset<>, vector<double> >  rates = getBranchWiseRates( model );

    shared_ptr< TreesetMetadata > CCPDistribution  = model->get_speciesCCPdistribution();

    bool hasTransfer = (rates.begin()->second.size()==3);

    OUT << "clade duplication_rate loss_rate" ;
    if(hasTransfer)
        OUT << " transfer_rate";
    OUT << endl;

    for(auto it = rates.begin() ; it != rates.end() ; ++it)
    {
        vector<string> leaves = getCladeLeafNames( CCPDistribution, it->first );

        cout << it->first << " " <<  leaves.size()<<endl;

        OUT << "(";
        OUT << leaves[0];
        for(unsigned i = 1 ; i < leaves.size() ; i++)
        {
            OUT << "," << leaves[i];
        }
        OUT << ")";

        for(auto r : it->second)
            OUT << " "<< r;

        OUT << endl;
    }

}


/*
    Takes:
        - string filename
        - shared_ptr< TreesetMetadata > CCPDistribution

*/
void OutputCCPdistribution( string filename , shared_ptr< TreesetMetadata > CCPDistribution)
{
    ofstream ofs;
    ofs.open(filename.c_str(),ofstream::out );

    ofs << CCPDistribution->output_as_ale() << endl;

    ofs.close();    
}



/*

    Takes:
        - string filename : name of the file containing the branch-wise rates 
                            (one per line, clade specification followed by the duplication rate followed by the loss rate, and optionaly followed by the transfer rate, separated by spaces, with a header)
                            example:

clade duplication_rate loss_rate
(A) 1e-10 1e-10
(B) 7.5e-11 7.5e-11
(A,B) 2e-11 2e-11
(C) 2.02571e-16 0.405465
(B,C) 1e-11 1e-11
(A,B,C) 0.405465 6e-11
(D) 1e-10 0.182322
(A,B,C,D) 0.175891 1e-09

        - shared_ptr< TreesetMetadata > CCPDistribution : the species CCP distibution

        - map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > keys : clade bitset
                                                                values : duplication rate ; loss rate ; transfer rate (optional)

    Returns:
        int :  0 if no problem
               1 if the file cannot be opened

*/
int readRatesFile(string filename, shared_ptr< TreesetMetadata > CCPDistribution, map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > &rateMap)
{

    ifstream CorrStream(filename.c_str());
    
    bool hasTransfer=false;
    int nbF = 0;

    if( CorrStream.is_open() )
    {
        while( !CorrStream.eof() ) 
        {

            string speciesStr;
            vector <string> L;
    
            double Drate;
            double Lrate;
            double Trate;

            CorrStream >> speciesStr;
    
            if(speciesStr[0] == '(')
            { // beginning of a clade
                if(nbF >=0)
                {
                    if(nbF ==4) // means that the header has a transfer_rate field 
                        hasTransfer = true;
                    nbF = -1;
                }

                string current = "";
                for(unsigned i = 1 ; i < speciesStr.size() ; i++)
                {
                    if( speciesStr[i] == ',' )
                    {
                        L.push_back(current);
                        current = "";
                    }
                    else if(speciesStr[i] == ')')
                    {
                        L.push_back(current);                        
                        break;
                    }
                    else
                        current += speciesStr[i];
                }
            
            }
            else if(nbF >= 0)
            {
                nbF++;
                continue;
            }


            if(CorrStream.eof()) // to avoid hanging line
                continue;

            CorrStream >> Drate;

            if(CorrStream.eof()) // to avoid hanging line
                continue;

            CorrStream >> Lrate;

            if(hasTransfer)
            {
                if(CorrStream.eof()) // to avoid hanging line
                    continue;
                CorrStream >> Trate;
            }


            //for(unsigned i = 0 ; i < L.size(); i++)
            //    cout << " " << L[i] ;

            boost::dynamic_bitset<> C = CCPDistribution->get_clade_bitset( L );

            if( C.any() )
            { // existing clade
                //cout << "-> " << C << endl;
                rateMap[C] = vector < FLOAT_TYPE > (2,0);
                rateMap[C][0] = Drate;
                rateMap[C][1] = Lrate;
                if(hasTransfer)
                    rateMap[C].push_back(Trate);
             }
            //else
            //{
            //    cout << "-> XXX" << endl;
            //}

    
        }

        CorrStream.close();
    }
    else
    {
        return 1;
    }

    
    
    return 0;
}

/*
    Guess the format of the given file by looking at the first character of the first line of the file:
        '(' -> newick
        '#' -> ale
        else -> list of filenames

    Takes:
        - string filename
    Returns:
        int :  1 if the file is presumed to contain a bunch of newick trees
               2 if the file is presumed to contain CCP distribution (.ale file)
               3 if the file is presumed to contain a bunch of file names
               -1 if the file cannot be opened


*/
int guessFileFormat(string filename)
{

    ifstream IN( filename );
    if( IN.is_open() )
    {
        string line;
        getline(IN, line);
        IN.close();

        if(line[0] == '(')
            return 1;
        else if(line[0] == '#')
            return 2;
        else
            return 3;
    }
    else
    {
        return -1;
    }

    return 3;
}