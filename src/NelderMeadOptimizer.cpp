
/*

This file contains utilitary functions for the nelder mead optimization algorithm of PsDL

Created the: 05-03-2015
by: Wandrille Duchemin

Last modified the: 08-03-2018
by: Wandrille Duchemin


*/

#include "NelderMeadOptimizer.h"


/*
FLOAT_TYPE NelderMeadOptimizer::EvaluateParameterSet( vector< double > parameters )
{
    //setup parameters
    unsigned int LowerVerboseLevel = 0;

    ///// parameter "brute" value is log of parameter ////

    /// for instance:
    if(parameterization != 2)
    {
        model->set_DuplicationRates( exp( parameters[0] ) ); 
        model->set_LossRates( exp( parameters[1] ) ); 
    }
    else
    {
        cerr <<  "not implemented yet" << endl;        
        exit(3);
    }


    if(parameterization == 1)
    {

        cerr <<  "not implemented yet" << endl;*/
        /*
        //////// only if relevant in current parameterisation 
        double BLEN = getWeightedBranchLenSum( SpeciesCCPDistrib ); 
        double rootBranchRatio = 0.01;
        model->set_rootBranchLength( rootBranchRatio * BLEN ); // putting 5 percent of the total branch length of the tree at the top of the species tree
        /////////////////////////////////////////////////////  <<- maybe not this

        model->divideSpeciesBranches(LowerVerboseLevel);    //   <- adapt that
        *//*
    }

    //do precomputations 
    model->computeProbaGeneExtinct(LowerVerboseLevel);  // maybe these could move to protected when they've been tested
    model->computeProbaGeneOneChild(LowerVerboseLevel); // as they NEED to be executed only once and in this precise order


    long double currentP = 0;

    //compute the overall probability for all gene families 
    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < GeneCCPDistribV.size() ; GeneFamilyId++ )
    {
        shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];

        model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
        model->addgeneSpeciesCorrespondence( GtoSpCorr );

        model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 
        currentP += -log( model->getOverAllProbability() );

        model->prepareForNewGeneFamily();
        GeneCCPDistrib.reset();

    }

    model->reset();

    return currentP;
}*/

/*
Computes the parameter centroid of all parameter sets but the last (worst) one

    Returns:
        (vector<double>): centroid of the parameters sets

*/
vector < long double > NelderMeadOptimizer::computeCentroid( )
{
    vector < long double > centroid;

    centroid.reserve(nbParams);
    for( unsigned int j = 0 ; j < nbParams ; j++ )
    {
        centroid.push_back( 0 );
    }    

    for(unsigned int i = 0 ; i < nbSets ; i++)
    { // iterate on all parameter sets but the last (worst one)
        if( i == worstPsetIndex )
            continue;

        for( unsigned int j = 0 ; j < nbParams ; j++ )
        {
            centroid[j] +=  ( Parameters[i][j] / (nbSets - 1 ) );
        }

    }

    return centroid;
}

/*
fills 
    int bestPsetIndex;
    int worstPsetIndex;
    int secondWorstPsetIndex;
based on the likelihood of the different points 
*/
void NelderMeadOptimizer::setupParametersOrder()
{
    bestPsetIndex = 0;
    worstPsetIndex = 0;
    secondWorstPsetIndex = 0;

    for(unsigned i = 1 ; i < nbSets ; i++)
    {
        if( ParametersLkh[i] <= ParametersLkh[worstPsetIndex] )
        {
            secondWorstPsetIndex = worstPsetIndex;
            worstPsetIndex = i;
        }
        else if( worstPsetIndex == secondWorstPsetIndex )
        {
            secondWorstPsetIndex = i;
        }
        else if( ParametersLkh[i] <= ParametersLkh[secondWorstPsetIndex] )
        {
            secondWorstPsetIndex = i;
        }

        if( ParametersLkh[i] > ParametersLkh[bestPsetIndex] )
        {
            bestPsetIndex = i;
        }
    }


    //cout << " setupParametersOrder " << endl;
    //for( auto v : ParametersLkh)
    //    cout << " " << v;
    //cout << endl;
    //cout << "best :      " << bestPsetIndex << endl;
    //cout << "2nd worst : " << secondWorstPsetIndex << endl;
    //cout << "worst :     " << worstPsetIndex << endl;

    return;
}



/*
    Takes:
        - vector< double > centroid
        - vector<double> point
    
    Returns:
        (vector<double>) centroid + factor*(centroid - point)
*/
vector<long double> NelderMeadOptimizer::getReflexion(vector< long double > reference, vector< long double> point)
{ return getProjectedPoint(reference,point, - alpha); }
vector<long double> NelderMeadOptimizer::getExpansion(vector< long double > reference, vector< long double> point)
{ return getProjectedPoint(reference,point, gamma); }
vector<long double> NelderMeadOptimizer::getContraction(vector< long double > reference, vector< long double> point)
{ return getProjectedPoint(reference,point, rho); }
vector<long double> NelderMeadOptimizer::shrinkPoint(vector< long double > reference, vector< long double> point)
{ return getProjectedPoint(reference,point, sigma); }


/*
    Takes:
        - vector< double > centroid
        - vector<double> point
        - double factor
    
    Returns:
        (vector<double>) centroid + factor*(centroid - point)
*/
vector<long double> NelderMeadOptimizer::getProjectedPoint(vector< long double > reference, vector< long double> point, double factor)
{
    vector< long double> newPoint;
    newPoint.reserve( nbParams );

    for(unsigned i = 0 ; i < nbParams ; i++)
    {
        newPoint.push_back( reference[i] + factor * ( point[i] - reference[i] ) );
    }
    return newPoint;
}


/*

replaces the values of the point at the given index in Parameters
with the values of newPoint

    Takes:
        - unsigned int index 
        - vector<double> newPoint
*/
void NelderMeadOptimizer::replacePoint(unsigned int index , vector< long double> newPoint)
{
    for(unsigned i = 0 ; i < nbParams ; i++)
    {
        Parameters[index][i] = newPoint[i];
    }
}




NelderMeadOptimizer::NelderMeadOptimizer( 
                    vector< long double > startingPoint,
                    long double Step ,
                    long double No_improve_thr ,
                    unsigned int No_improv_break ,
                    unsigned int Max_iter ,
                    double Alpha ,
                    double Gamma ,
                    double Rho ,
                    double Sigma 
                     ): step(Step), no_improve_thr(No_improve_thr), no_improv_break(No_improv_break), max_iter(Max_iter), alpha(Alpha), gamma(Gamma), rho(Rho), sigma(Sigma)
{

    nbParams = startingPoint.size();
    nbSets = nbParams+1;

    /// creating the desired parameter sets.

    Parameters.reserve( nbSets );

    Parameters.push_back( toLogPoint( startingPoint ) );


    for(unsigned i = 0 ; i < nbParams ; i++)
    {
        Parameters.push_back( toLogPoint( startingPoint )  );
        Parameters.back()[i] += step;
        //for(auto v : Parameters.back() )
        //    cout << " " << v;
        //cout << endl;
    }

    /// evaluating all parameter sets
    ParametersLkh.reserve( nbParams );

    state = 0;
}

/*
    Operates on the assumption that :
        * likelihood have been computed
        * best, worst and second worst index have been set

*//*
void NelderMeadOptimizer::optimizationRound()
{

    iters++;

    vector <double> centroid = computeCentroid();


    /// computing the reflexion of the worst point on the centroid
    vector<double> reflexionPoint = getReflexion(centroid , Parameters[ worstPsetIndex ]);
    long double reflexionLkh = EvaluateParameterSet( reflexionPoint );

    if( reflexionLkh < ParametersLkh[ bestPsetIndex ] )
    { // the new point is better than any other point we've found yet

        // expansion of the reflected point
        vector<double> expansionPoint = getExpansion(centroid , reflexionPoint );
        long double expansionLkh = EvaluateParameterSet( expansionPoint );

        if( expansionLkh < reflexionLkh)
        { //the expanded reflected is better than the reflectd
            replacePoint(worstPsetIndex , expansionPoint);
            ParametersLkh[ worstPsetIndex ] = expansionLkh;
        }
        else
        {
            replacePoint(worstPsetIndex , reflexionPoint);
            ParametersLkh[ worstPsetIndex ] = reflexionLkh;
        }

        setupParametersOrder();
        return;
    }
    else if( reflexionLkh < ParametersLkh[ secondWorstPsetIndex ] )
    {
        replacePoint(worstPsetIndex , reflexionPoint);
        ParametersLkh[ worstPsetIndex ] = reflexionLkh;

        setupParametersOrder();
        return;
    }
    else
    { // here we know that reflexionLkh >= ParametersLkh[ secondWorstPsetIndex ]

        //contraction 
        vector<double> contractionPoint = getContraction(centroid , Parameters[ worstPsetIndex ] );
        long double contractionLkh = EvaluateParameterSet( contractionPoint );

        if( contractionLkh < ParametersLkh[ worstPsetIndex ] )
        {
            replacePoint(worstPsetIndex , contractionPoint);
            ParametersLkh[ worstPsetIndex ] = contractionLkh;            

            setupParametersOrder();
            return;
        }

    }

    //shrinking, if all else failed ...
     for(unsigned i = 0 ; i < nbSets ; i++)
    {
        if(i == bestPsetIndex)
            continue; // avoiding the shrink of best point, as it wouldn't move ...

        vector< double > newPoint = shrinkPoint( Parameters[ bestPsetIndex ] , Parameters[ i ] );
        replacePoint(i,newPoint);
        ParametersLkh[i] = EvaluateParameterSet(newPoint);
    }
    setupParametersOrder();

    return;
}
*/





/*returns true of the stopping conditions are met*/
bool NelderMeadOptimizer::checkStopConditions()
{

    if( max_iter != 0 )
    {
        if( iters >= max_iter )
            return true; // max number of iterations reached
    }
        

    //cout << prev_best << "-" << ParametersLkh[ bestPsetIndex ] << "->" << ( prev_best - ParametersLkh[ bestPsetIndex ]) << endl;
    if( ( prev_best < ParametersLkh[ bestPsetIndex ]) )
    {
        if( ( prev_best - ParametersLkh[ bestPsetIndex ]) <  -no_improve_thr)
        {
            //cout << "significant impr" << endl;
            no_improv_iter = 0;
        }
        prev_best = ParametersLkh[ bestPsetIndex ];
        
    }
    else
        no_improv_iter++;

    if( no_improv_iter >= no_improv_break) 
        return true;

    return false;
}


vector< long double > NelderMeadOptimizer::getBestParameters( bool log  )
{
    if(log)
        return Parameters[ bestPsetIndex ];
    return fromLogPoint( Parameters[ bestPsetIndex ] );
}

long double NelderMeadOptimizer::get_BestParametersLkh()
{
    return ParametersLkh[ bestPsetIndex ];
}





/*
Takes:
    - long double LogLkh : log likelihood corresponding to the parameter set that was just tested
                            0 if no parameter set was tested 

Returns:
    (vector<double>) : parameter set to test
                       empty vector if the stopping conditions are met
*/
vector< long double> NelderMeadOptimizer::ReceiveLikelihoodAndGetNextParameterToTest( long double LogLkh , int verboseLevel )
{
    if( verboseLevel > 1 )
        cout << "NelderMead : state " << state << endl;


    switch ( state )
    {
        case 0:
            // initialization of parameter set likelihoods
            if( verboseLevel > 1 )
                cout << "NelderMead : initialization steps" << endl;

            if( LogLkh != 0 )
                ParametersLkh.push_back( LogLkh );


            //for( auto v : Parameters[ ParametersLkh.size() ] )
            //    cout << " " << v;
            //cout << endl;
            //cout << nbSets << "<>" << ParametersLkh.size() << endl;

            if( ParametersLkh.size() < nbSets )
            {
                //cout << "plop" << endl;
                vector <long double > P = fromLogPoint( Parameters[ ParametersLkh.size() ] );
                //cout << " NM init ";
                //for(auto v : P )
                //    cout << " " << v;
                //cout << endl;
                return fromLogPoint( Parameters[ ParametersLkh.size() ] );
            }
            else // this is done. -> go to next step
            {
                state = 1 ;
                ///getting the better and the 2 worse
                setupParametersOrder();
                iters=0;
                no_improv_iter=0;
                prev_best = ParametersLkh[ bestPsetIndex ];
                //cout << "plop " << prev_best << " " << ParametersLkh[ bestPsetIndex ] << endl;
            }

        case 1:
            // begining of a new optimization round
            if( verboseLevel > 1 )
                cout << "NelderMead : new round " << endl;


            iters++;
        
            centroid = computeCentroid();
            //cout << "centroid ";
            //for( auto v : centroid )
            //    cout << v << " ";
            //cout <<endl;
        
            /// computing the reflexion of the worst point on the centroid
            reflexionPoint = getReflexion(centroid , Parameters[ worstPsetIndex ]);
            //cout << "reflexion ";
            //for( auto v : reflexionPoint )
            //    cout << v << " ";
            //cout <<endl;


            state = 2;
            return  fromLogPoint( reflexionPoint );

        case 2:
            if( verboseLevel > 1 )
                cout << "NelderMead : reflexion" << endl;


            // the given lkh is that of the reflexion point
            reflexionLkh = LogLkh;
            if( reflexionLkh > ParametersLkh[ bestPsetIndex ] )
            { // the new point is better than any other point we've found yet

                // expansion of the reflected point
                expansionPoint = getExpansion(centroid , reflexionPoint );
                //cout << "expansion ";
                //for( auto v : expansionPoint )
                //    cout << v << " ";
                //cout << endl;
                state = 3;
                return fromLogPoint( expansionPoint );
            }
            else if( reflexionLkh > ParametersLkh[ secondWorstPsetIndex ] )
            { // the reflexion point is better than the second worst one. -> drop the worst point 
                //cout << "the reflexion point is better than the second worst one. -> drop the worst point " << endl;
                replacePoint(worstPsetIndex , reflexionPoint);
                ParametersLkh[ worstPsetIndex ] = reflexionLkh;
        
                setupParametersOrder();
                
                /// end of round!
                break;    
            }
            else
            { // here we know that the reflexion point is worst than the second worst point.

                //contraction 
                contractionPoint = getContraction(centroid , Parameters[ worstPsetIndex ] );
                //cout << "contraction " ;
                //for( auto v : contractionPoint )
                //    cout << v << " ";
                //cout << endl;

                state = 4;
                return fromLogPoint( contractionPoint );
            }


        case 3:
            if( verboseLevel > 1 )
                cout << "NelderMead : expansion" << endl;

            expansionLkh = LogLkh;
            // the given lkh is that of the exapnded reflexion point
            if( expansionLkh < reflexionLkh)
            { //the expanded reflected is better than the reflectd
                replacePoint(worstPsetIndex , expansionPoint);
                ParametersLkh[ worstPsetIndex ] = expansionLkh;
            }
            else
            {
                replacePoint(worstPsetIndex , reflexionPoint);
                ParametersLkh[ worstPsetIndex ] = reflexionLkh;
            }
    
            setupParametersOrder();

            /// end of round!!
            break;
        case 4:
            if( verboseLevel > 1 )
                cout << "NelderMead : contraction" << endl;

            contractionLkh = LogLkh;

            if( contractionLkh > ParametersLkh[ worstPsetIndex ] )
            { // better than the worst point !!
                replacePoint(worstPsetIndex , contractionPoint);
                ParametersLkh[ worstPsetIndex ] = contractionLkh;            

                setupParametersOrder();

                /// end of round!!
                break;
            }
            else
            {
                ShrinkedIndex = -1;
                state = 5;
            }
        case 5:
            if( verboseLevel > 1 )
                cout << "NelderMead : shrinking no." << ShrinkedIndex+1 << endl;

            //shrinking, if all else failed ...
            if( ShrinkedIndex != -1 )
                ParametersLkh[ShrinkedIndex] = LogLkh ; 

            ShrinkedIndex++;
            if(ShrinkedIndex == bestPsetIndex)
                ShrinkedIndex++; // avoiding the shrink of best point, as it wouldn't move ...

            if( ShrinkedIndex < nbSets )
            {
                //cout << "reference point : " ;
                //for( auto v : Parameters[ bestPsetIndex ] )
                //    cout << v << " ";
                //cout <<endl;
                //cout << "projected point : " ;
                //for( auto v : Parameters[ ShrinkedIndex ] )
                //    cout << v << " ";
                //cout <<endl;
                //cout << " sigma " << sigma << endl;


                vector< long double > newPoint = shrinkPoint( Parameters[ bestPsetIndex ] , Parameters[ ShrinkedIndex ] );
                //cout << "shrinked point : " ;
                //for( auto v : newPoint )
                //    cout << v << " ";
                //cout <<endl;

                replacePoint(ShrinkedIndex,newPoint);
                return fromLogPoint( newPoint );   
            }

            setupParametersOrder();
            break;

        default:
            cerr << "ERROR : unknown nelder-mead algorithm state " << state << endl;

    }


    if( verboseLevel > 1 )
        cout << "NelderMead : end of round " << endl;

    if( checkStopConditions() )
    {
        return vector < long double  >() ;
    }

    state = 1;

    return ReceiveLikelihoodAndGetNextParameterToTest( 0 , verboseLevel ) ;
}
