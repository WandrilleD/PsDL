/*

 * This class ensures the clean starting and ending of the program:
 * - reads and validates input parameters
 * - outputs errors
 * - output usage information
 * - handles errors

Created the: 14-02-2018
by: Wandrille Duchemin

Last modified the: 27-04-2018
by: Wandrille Duchemin


*/

#include "PsDLOptionHandler.h"

using namespace std;

PsDLOptionHandler::PsDLOptionHandler(): input_is_ale_(false), 
                                        desc_{"Options"} , 
                                        corr_input_filename_(""), 
                                        separator_("_") , 
                                        verboseLevel(1), 
                                        seed(-1), 
                                        outputFile(), 
                                        initialDupRate(0.1), 
                                        initialLossRate(0.1), 
                                        outputSample(0),
                                        unrooted_species_tree(false), 
                                        multiple_gene_families(false),
                                        SpCCPUpdatingNbRound(1),
                                        SpCCPUpdatingWeight(0.5),
                                        cutOff(0),
                                        extant_sampling(1),
                                        branchWiseRates(false), 
                                        input_rate_file("")
{
    desc_.add_options()
        ("help,h", "Display this help text and usage information")
        ("gene,g", boost::program_options::value<string>(&gene_input_filename_), "Path to file containing input gene tree(s) in newick format")
        ("species,s", boost::program_options::value<string>(&species_input_filename_), "Path to file containing input species tree(s) in newick format")
        ("corr.file", boost::program_options::value<string>(&corr_input_filename_), "Path to file containing correspondence betzeen gene leaves and species leaves")
        ("separator", boost::program_options::value<string>(&separator_), "Separator between the species and the gene name in gene tree leaves. Used if a correspondence file is not given.\nExpects the species name to be BEFORE the separator. Default : \"_\". Can be multiple characters long.")
        ("ale.input,a", "treat the input file as .ale files instead of newick")
        ("verbose,v", boost::program_options::value< unsigned int>(&verboseLevel), "0 : as quiet as possible\n1 : prints some basic information to stdout (default value)\n2 : prints lot of information to stdout (intended for debug/testing)")
        ("seed", boost::program_options::value<int>(&seed), "use to fix the random seed.")
        ("tree.sample,n", boost::program_options::value< unsigned int>(&outputSample), "size of the reconciled tree sample that will be used to update the species CCP distribution and that will be outputed at the end of the program. default: 0")
        ("output.file.prefix,p", boost::program_options::value< string>(&outputFile), "Prefix of the files where output will be written.\nreconciled trees will be written in prefix+\".trees.txt\".\nspecies CCP distribution will be written in prefix+\".updated.species.ale\".\ndefault prints to stdout.")
        ("init.dup,d",boost::program_options::value< double >(&initialDupRate), "Initial duplication rate. default: 0.1")
        ("init.loss,l",boost::program_options::value< double >(&initialLossRate),"Initial loss rate. default: 0.1")
        ("unrooted.species.tree,u","treat the species tree as unrooted. only used if the input is trees and not .ale files")
        ("multiple.gene.families,m","consider the input gene tree file contains, in fact, a list of gene trees files (one per line, one for each gene family).")
        ("species.CCP.update.rounds,N",boost::program_options::value< unsigned int>(&SpCCPUpdatingNbRound),"number of species CCP updating round ; minimum of one, put option -w at 0 to avoid species CCP update. default: 1." )
        ("updating.weight,W",boost::program_options::value< double>(&SpCCPUpdatingWeight),"weight of the splits observed in reconciliations in the update of the species CCP distribution ; must be between 0 and 1.\nuse a value of 0 to avoid species CCP update\ndefault: 0.5.")
        ("cutOff.CCP,C",boost::program_options::value< double>(&cutOff),"threshold to remove splits from the the species CCP distribution during its updates; must be between 0 and 1.\ndefault: 0 (nothing is removed).")
        ("relative.cutOff,R","consider --cutOff.CCP/-C to apply to the ratio of the split CCP to the CCP of the best split rather than rather than directly to the CCP.")
        ("extant.sampling,E",boost::program_options::value< double>(&extant_sampling),"Expected gene sampling rate among extant species ; must be between 0 (excluded) and 1. default: 1.")
        ("branch.wise,B", "trigger optimization of branch wise rates of duplication and loss (tree-wide parameters otherwise).")
        ("input.rate",boost::program_options::value<string>(&input_rate_file),"file containing branch-wise duplication and loss rates. Necessitates that option --branch.wise/-B is set.")

    ;


}

void PsDLOptionHandler::readArguments(int argc , char* argv[]) 
{
    // Set and parse parameters
    try
    {

        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc_), vm_);
        boost::program_options::notify(vm_);

    }
    catch (const boost::program_options::error &exception)
    {
        cerr << endl << exception.what() << endl;
    }
}



// Getters
string PsDLOptionHandler::get_gene_input_filename() const
{
    return gene_input_filename_;
}

string PsDLOptionHandler::get_species_input_filename() const
{
    return species_input_filename_;
}

string PsDLOptionHandler::get_corr_input_filename() const
{
    return corr_input_filename_;
}

string PsDLOptionHandler::get_separator() const
{
    return separator_;
}

bool PsDLOptionHandler::get_input_is_ale() const
{
    return input_is_ale_;
}

unsigned int PsDLOptionHandler::get_verbose_level() const
{
    return verboseLevel;
}

int PsDLOptionHandler::get_seed() const
{
    return seed; 
}

unsigned int PsDLOptionHandler::get_outputSample() const
{
    return outputSample;
}

std::string PsDLOptionHandler::get_outputFile() const
{
    return outputFile;
}


double PsDLOptionHandler::get_initialDupRate() const
{
    return initialDupRate;
}

double PsDLOptionHandler::get_initialLossRate() const
{
    return initialLossRate;
}

bool PsDLOptionHandler::get_unrooted_species_tree() const
{
    return unrooted_species_tree;
}

bool PsDLOptionHandler::get_multiple_gene_families() const
{
    return multiple_gene_families;
}


unsigned int PsDLOptionHandler::get_SpCCPUpdatingNbRound() const
{
    return SpCCPUpdatingNbRound;
}

double PsDLOptionHandler::get_SpCCPUpdatingWeight() const
{
    return SpCCPUpdatingWeight;
}


double PsDLOptionHandler::get_cutOff() const
{
    return cutOff;
}

bool PsDLOptionHandler::get_isRelativeCutoff() const
{
    return isRelativeCutoff;
}

double PsDLOptionHandler::get_extant_sampling() const
{
    return extant_sampling;
}


bool PsDLOptionHandler::get_branchWiseRates() const
{
    return branchWiseRates;
}
string PsDLOptionHandler::get_input_rate_file() const
{
    return input_rate_file;
}

// Actions
bool PsDLOptionHandler::verify()
{
    if (vm_.count("help"))
    {
        printUsage();

        cout << desc_ << endl << endl;

        return false;
    }

    if(vm_.count("ale.input"))
    {
        input_is_ale_ = true;
    }

    if(vm_.count("unrooted.species.tree"))
    {
        unrooted_species_tree = true;
    }

    if(vm_.count("multiple.gene.families"))
    {
        multiple_gene_families = true;
    }

    if( vm_.count("relative.cutOff") )
    {
        isRelativeCutoff = true;
    }

    if(!vm_.count("gene"))
    {
        cerr << "ERROR: No input gene trees file was specified!" << endl;
        printUsage();
        cerr << desc_ << endl << endl;
        return false;
    }

    if(!vm_.count("species"))
    {
        cerr << "ERROR: No input species trees file was specified!" << endl;
        printUsage();
        cerr << desc_ << endl << endl;
        return false;
    }

    if( ( SpCCPUpdatingWeight<0) || ( SpCCPUpdatingWeight>1) )
    {
        cerr << "ERROR: --updating.weight/-W must be a value between 0 and 1 !"<< endl;
        printUsage();
        cerr << desc_ << endl << endl;
        return false;
    }

    if( ( SpCCPUpdatingWeight==0 ) && (SpCCPUpdatingNbRound != 1 ) )
    {
        cerr << "Warning: --updating.weight/-W option equals to 0. Adjusting --species.CCP.update.rounds/-N to 1 to avoid un-necessary computations."<<endl;
        SpCCPUpdatingNbRound = 1;
    }

    if( SpCCPUpdatingNbRound < 1)
    {
        cerr << "Warning: --species.CCP.update.rounds/-N set to less than 1. Setting it to 1, its minimal value (set --updating.weight/-W to 0 to avoid species CCP updating rounds)." << endl;
        SpCCPUpdatingNbRound = 1;
    }


    if( ( cutOff<0) || ( cutOff>1) )
    {
        cerr << "ERROR: --cutOff.CCP/-C must be a value between 0 and 1 !"<< endl;
        printUsage();
        cerr << desc_ << endl << endl;
        return false;
    }

    if( ( extant_sampling <= 0 ) || ( extant_sampling > 1) )
    {
        cerr << "ERROR: --extant.sampling/-E must be a value between 0 (excluded) and 1 !" << endl;
        printUsage();
        cerr << desc_ << endl << endl;
        return false;
    }


    if(vm_.count("branch.wise"))
    {
        branchWiseRates = true;
    }

    if( get_input_rate_file() != "" )
    {
        if( !get_branchWiseRates() )
        {
            cerr << "ERROR: option --input.rate was given while option --branch.wise/-B is not set." << endl;
            return false;
        }
    }


    //if( cutOff != 0)
    //{
    //    cerr << "WARNING : --cutOff.CCP/-C currently not working. putting back to 0 to avoid problems..." << endl;
    //    cutOff = 0;
    //}


    return true;

}

void PsDLOptionHandler::printUsage()
{
    cout << "PsDL preliminary work" << endl;
    cout << "This program is intended to do the DL reconciliation of a gene CCP distribution and a species CCP distribution" << endl;
    cout << "usage : PsDL -g <gene tree distribution file>  -s <species tree distribution file>"  << endl;

}