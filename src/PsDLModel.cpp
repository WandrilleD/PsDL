/*

This file defines a class representing the the PsDL model 
of evolution of a gene CCP distribution inside a species CCP distribution
with events of duplication, loss and speciation.


Created the: 14-02-2018
by: Wandrille Duchemin

Last modified the: 25-05-2018
by: Wandrille Duchemin

*/

#include "PsDLModel.h"

// protected methods

/*
    divides a branch in several intervals 
    using both deltal and minNbBLInterval 

    Takes:
        - FLOAT_TYPE Blen : length of branch to divide


    Returns:
        ( vector <FLOAT_TYPE> ): vector of instervals to fill up


*/
vector <FLOAT_TYPE> PsDLModel::divideOneBranch(FLOAT_TYPE Blen)
{
    vector <FLOAT_TYPE> subdivisions = vector <FLOAT_TYPE> ();

    FLOAT_TYPE divSize = min( deltal_ , Blen / minNbBLInterval_ ) ;

    //cout << "PsDLModel::divideOneBranch " << Blen << ' ' << divSize << " ("<< deltal_ << ',' <<  Blen / minNbBLInterval_ << ")"<< endl;

    while( Blen > divSize )
    {
        subdivisions.push_back(divSize);
        Blen -= divSize;
    }

    if(Blen>0) // handle the case of a last chunk that is smaller than deltal
        subdivisions.push_back(Blen);

    return subdivisions;
}

/*

!!DUMMY IMPLEMENTATION!!

    Takes: 
        - CIndexType sigma : id of a species clade


    Returns:
        (FLOAT_TYPE) : length of branch to divide

*/
FLOAT_TYPE PsDLModel::getSpeciesBranchLength( CIndexType sigma )
{
    //cerr << "Warning : PsDLModel::getSpeciesBranchLength relies on unimplemented features of the ccp.cpp libary" << endl;
    if( speciesCladeIndexToCCPid[sigma].count() == speciesCCPdistribution->get_number_of_leaves() )
        return rootBranchLength;

    return speciesCCPdistribution->get_branchlength( speciesCladeIndexToCCPid[sigma] );
}

/*
    based on resolution of probability to yield no children after time t in birth death process of birth rate la and death rate mu.
    Ee' = mu - (mu + la) Ee + la Ee**2 ; Ee[0] = p
    Ee[t] -> ( mu + (la - mu)/(1 + (E^((la - mu) t) la (-1 + p))/(mu - la p)))/la

    Takes:
        - FLOAT_TYPE D : duplication (birth) rate
        - FLOAT_TYPE L : loss (death) rate
        - FLOAT_TYPE l : length of branch to consider
        - FLOAT_TYPE I : probability of extinction when l=0

    Returns:
        (FLOAT_TYPE) : probability to go extinct before present

*/
FLOAT_TYPE PsDLModel::computeExtinctionProba( FLOAT_TYPE D, FLOAT_TYPE L , FLOAT_TYPE l , FLOAT_TYPE I )
{


    if(D == L)
    { // when the duplication rate is equal to the lss rate then we use a different formula
        FLOAT_TYPE X = L * (I - 1) * l - 1;
        return 1 + (1 - I)/ X;
    }


    FLOAT_TYPE A = exp((D-L)*l) ;
    FLOAT_TYPE B = D * (I - 1) ;
    FLOAT_TYPE C = L - D*I ;

    //cout << " -> " << (L + (D-L)/(1 + A*B/C))/D  << endl;
    return (L + (D-L)/(1 + A*B/C))/D ;

}

/*
    based on resolution of probability to yield 1 children betzeen time t0 and time t1 in birth death process of birth rate la and death rate mu
    t = t1 - t0
    Ge'[t] = - (mu + la) Ge[t] + la Ee[t0+t] Ge[t] ; Ee[0] = p ; Ge[0] = q = 1
    (E^((-la + mu) t) (la - mu)^2 q)/(la - E^((-la + mu) t) mu - la p +   E^((-la + mu) t) la p)^2


    Takes:
        - FLOAT_TYPE D : duplication (birth) rate
        - FLOAT_TYPE L : loss (death) rate
        - FLOAT_TYPE l : length of branch to consider
        - FLOAT_TYPE I0 : probability of extinction at l0
        //////////- FLOAT_TYPE I1 : probability of having 1 child when l=0 /// oldest unused version

    Returns:
        (FLOAT_TYPE) : probability to go have exactly 1 child at 0

*/
FLOAT_TYPE PsDLModel::computeOneChildProba( FLOAT_TYPE D, FLOAT_TYPE L , FLOAT_TYPE l , FLOAT_TYPE I0) ///, FLOAT_TYPE I1 )
{



    if(D == L)
    { // when the duplication rate is equal to the lss rate then we use a different formula
        FLOAT_TYPE X = L * (I0 - 1) * l - 1;
        return 1 / pow( X , 2 ); 
    }

    FLOAT_TYPE I1 = 1;

    FLOAT_TYPE X = exp((L-D)*l);
    FLOAT_TYPE A = X * pow(D-L,2) * I1;
    FLOAT_TYPE B = D - X * L ;
    FLOAT_TYPE C = (X - 1) * D * I0 ;
    return A/pow((B+C),2);

}


/*
    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch 


    Returns:
        (FLOAT_TYPE): probability of becoming extinct there

*/
FLOAT_TYPE PsDLModel::computeExtinctionProba( CIndexType Sid, LIndexType Lid , unsigned int verboseLevel)
{
    if(verboseLevel>1)
        cout << "PsDLModel::computeExtinctionProba "<<  Sid << " "<< speciesCladeIndexToCCPid[ Sid ] << " " << Lid << endl;
    
    FLOAT_TYPE P = 0;

    if(Lid == 0)
    {//first interval of the branch 

        if( ! is_SpeciesLeaf( Sid ) ) // we are among the leaf clades
        {
            //use the probability in the children clades
            CCPcladeIdType sigma = speciesCladeIndexToCCPid[ Sid ];

            FLOAT_TYPE bip = speciesCCPdistribution->get_count_of_bipartition( sigma );

            auto END = speciesCCPdistribution->tripartition_cend( sigma );
            for(auto sigmaPpointer = speciesCCPdistribution->tripartition_cbegin( sigma ); sigmaPpointer != END ; ++sigmaPpointer )
            {
                if(verboseLevel>1)
                    cout << "   " << Sid << "->" << sigma << ":" << sigmaPpointer->first;
                
                FLOAT_TYPE trip = sigmaPpointer->second;

                
                FLOAT_TYPE pCCP = trip/bip;
                
                CCPcladeIdType sigmaPP = speciesCCPdistribution->get_complementer(sigma, sigmaPpointer->first); //getSisterClade(sigma, *sigmaPpointer); // 
                
                if(verboseLevel>1)
                    cout << "," << sigmaPP ;
                
                CIndexType CIP  = speciesCCPidToCladeIndex[sigmaPpointer->first];
                CIndexType CIPP = speciesCCPidToCladeIndex[sigmaPP];
                
                if(verboseLevel>1)
                {
                    cout << "->" << CIP << "," << CIPP << " -> " << pCCP;
                    cout << endl;   
                }
                
                P += pCCP * ProbaGeneExtinct[CIP][ branchesDivisionNumber[CIP] ] * ProbaGeneExtinct[CIPP][ branchesDivisionNumber[CIPP] ] ; 
                // probability of this split times to probability to go extinct from the top of both children
            }
        }
        else
        {
            P = 1 - get_probaExtantGeneSampled(Sid);
        }
    }
    else
    {//not the first interval of the branch
        FLOAT_TYPE dl = branchesDivisionLength[Sid][Lid-1];
        
        P = computeExtinctionProba( DuplicationRates[Sid], LossRates[Sid] , dl , ProbaGeneExtinct[Sid][Lid-1] ) ;
        //along branch it follows the formula Ee[t] -> ( mu + (la - mu)/(1 + (E^((la - mu) t) la (-1 + p))/(mu - la p)))/la
    }


    return P;
}


/*
    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch 


    Returns:
        (FLOAT_TYPE): probability of having exactly 1 child there

*/
FLOAT_TYPE PsDLModel::computeOneChildProba( CIndexType Sid, LIndexType Lid )
{
    FLOAT_TYPE P = 0;

    if(Lid == 0)
    {//first interval of the branch 
        
        if( is_SpeciesLeaf( Sid ) ) // we are among the leaf clades
        {
            P = 1; 
            
            // the probability to have one child before the end of a leaf branch when you are at the end of a leaf branch is 1
        }
        else
        {
            P = 1; 
            // the probability to have one child before the end of a branch when you are at the end of said branch is always 1
        }
    }
    else
    {//not the first interval of the branch
        FLOAT_TYPE dl = branchesDivisionLength[Sid][Lid-1];
        P = computeOneChildProba( DuplicationRates[Sid], LossRates[Sid] , dl , ProbaGeneExtinct[Sid][Lid-1] );//, ProbaGeneOneChild[CI][LI-1] ) );
        //along branch it follows the formula     (E^((-la + mu) t) (la - mu)^2 q)/(la - E^((-la + mu) t) mu - la p +   E^((-la + mu) t) la p)^2
    }


    return P;
}




FLOAT_TYPE PsDLModel::ComputerecProba( CIndexType Sid, LIndexType Lid, CIndexType Gid )
{

    FLOAT_TYPE P = 0;

    
    CIndexType mask = 0;

    bool Gleaf = is_GeneLeaf(Gid);

    if( Gleaf )
    { // special procedure to avoid re-computing probebility vectors of single gene lineages
        mask = geneSpeciesCorrespondence[ Gid ] ;

        if( GeneLeavesProbas[Sid][Lid][mask] != -1 ) // this probability has already been computed
            return GeneLeavesProbas[Sid][Lid][mask] ;
        //else we'll compute the value and add it
    }

    if(Lid == 0)
    {
        if( ( is_SpeciesLeaf(Sid) ) && ( Gleaf ) )
        { // both leaves at length 0 -> leaf function
            P = computeProbaLeafExtremity(  Sid ,  Gid );
        }
        else
        { //using speciation function
            P = computeProbaInterBranch( Sid , Gid );
        }
    }
    else
    {
        P = computeProbaIntraBranch( Sid , Lid , Gid );
    }

    if(mask != 0)
        GeneLeavesProbas[Sid][Lid][mask] = P ; 
    

    return P;
}



void PsDLModel::initializeRecMatrix()
{
    assert( geneCladeIndexToCCPid.size() > 0) ;//checks that the gene ccp has been set
    assert( branchesDivisionLength.size() > 0); //checks that the species clade branches have been subdivided

    int nbSpClades = speciesCladeIndexToCCPid.size();
    int nbGeneClades = geneCladeIndexToCCPid.size();

    //checking if the matrix already has been initialized
    if( ReconciliationMatrix.size() > 0 )
    { //crude check 
        return;
    }

    ReconciliationMatrix.reserve(nbSpClades);

    ReconciliationMatrix.push_back( vector < vector<FLOAT_TYPE> > () ); // first is dummy vector
    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1

        ReconciliationMatrix.push_back( vector <  vector<FLOAT_TYPE> > () );

        LIndexType nbDiv = branchesDivisionNumber[Sid];

        ReconciliationMatrix[Sid].reserve(nbDiv+1);

        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) // there is (number of subdivision + 1) cells    
        { 
            ReconciliationMatrix[Sid].push_back( vector<FLOAT_TYPE>( nbGeneClades, 0 ) );
        }
    }


    //for( CIndexType Sid = 0 ; Sid < ReconciliationMatrix.size() ; Sid++ ) 
    //{ // the clade with index 0 is a dummy clade, so we start at 1
    //    for(LIndexType Lid = 0 ; Lid < ReconciliationMatrix[Sid].size(); Lid++) // there is (number of subdivision + 1) cells    
    //    {
    //        cout << Sid << "," << Lid;
    //        for( CIndexType Gid = 0 ; Gid < ReconciliationMatrix[Sid][Lid].size() ; Gid++ )
    //            cout << " " << Gid ;
    //        cout << endl;
    //    }
    //}


}


void PsDLModel::initializeGeneLeavesProbas()
{
    assert( branchesDivisionLength.size() > 0); //checks that the species clade branches have been subdivided
    unsigned nbClades = speciesCladeIndexToCCPid.size();

    /// initialization of GeneLeavesProbas
    GeneLeavesProbas.reserve(nbClades);

    unsigned nbSpLeaves = speciesCCPdistribution->get_number_of_leaves();

    GeneLeavesProbas.push_back( vector < vector<FLOAT_TYPE> > () ); // first is dummy vector

    for( CIndexType Sid = 1 ; Sid < nbClades ; Sid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1

        GeneLeavesProbas.push_back( vector <  vector<FLOAT_TYPE> > () );

        LIndexType nbDiv = branchesDivisionNumber[Sid];

        GeneLeavesProbas[Sid].reserve(nbDiv+1);

        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) // there is (number of subdivision + 1) cells    
        { 
            GeneLeavesProbas[Sid].push_back( vector<FLOAT_TYPE>( nbSpLeaves+1, -1 ) );
        }
    }
}
//// probability computation functions:

/*

    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch !!MUST BE >0!!
        - CIndexType Gid : id of the gene clade

    Returns:
        (FLOAT_TYPE): probability of a Null event here
*/
FLOAT_TYPE PsDLModel::computeProbaNullIntraBranch( CIndexType Sid , LIndexType Lid , CIndexType Gid )
{
    //cout << "PsDLModel::computeProbaNullIntraBranch(" << Sid << "," << Lid << "," << Gid << ") ";
    //cout <<  ProbaGeneOneChild[Sid][Lid] * ReconciliationMatrix[Sid][Lid-1][Gid] << "=" << ProbaGeneOneChild[Sid][Lid] <<"*"<< ReconciliationMatrix[Sid][Lid-1][Gid]<<endl;

    return ProbaGeneOneChild[Sid][Lid] * ReconciliationMatrix[Sid][Lid-1][Gid];
}

/*

    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch !!MUST BE >0!!
        - CIndexType Gid : id of the gene clade

    Returns:
        (FLOAT_TYPE): probability of a duplication event here across all possible splits of Gid
*/
FLOAT_TYPE PsDLModel::computeProbaDupIntraBranch( CIndexType Sid , LIndexType Lid , CIndexType Gid )
{
    //cout << "PsDLModel::computeProbaDupIntraBranch(" << Sid << "," << Lid << "," << Gid << ") " << endl;

    FLOAT_TYPE Pd =0; 


    if(is_GeneLeaf(Gid))
        return Pd; // no need for further computqtion if the gene clade cannot split

    /*
    CCPcladeIdType gamma = geneCladeIndexToCCPid[ Gid ];


    FLOAT_TYPE bip = geneCCPdistribution->get_count_of_bipartition( gamma );

    auto END = geneCCPdistribution->tripartition_cend( gamma );
    for(auto gammaPpointer = geneCCPdistribution->tripartition_cbegin( gamma ); gammaPpointer != END ; ++gammaPpointer )
    {
        FLOAT_TYPE trip = gammaPpointer->second;
           
        FLOAT_TYPE pCCP = trip/bip; //conditional probability of this split

        CCPcladeIdType gammaPP = geneCCPdistribution->get_complementer(gamma, gammaPpointer->first); //getSisterClade(gamma, *gammaPpointer); //
        
       //cout << "  " << gamma <<"|"<< gammaPpointer->first<<","<<gammaPP << "->"<< pCCP;
        
        CIndexType GidP  = geneCCPidToCladeIndex[gammaPpointer->first];
        CIndexType GidPP = geneCCPidToCladeIndex[gammaPP];
        
        //out << "*" << ReconciliationMatrix[Sid][Lid-1][GidP] <<"*" << ReconciliationMatrix[Sid][Lid-1][GidPP] <<endl;
        
        Pd += pCCP * ReconciliationMatrix[Sid][Lid-1][GidP] * ReconciliationMatrix[Sid][Lid-1][GidPP] ; // pCCP times the proba to have both children in the same species at the same time
        
    }*/
    for( unsigned g = 0 ; g < GeneProbaV[ Gid ].size() ; g++ )
    {
        Pd += GeneProbaV[ Gid ][g] * ReconciliationMatrix[Sid][Lid-1][ GeneIndexV[ Gid ][g].first  ] * ReconciliationMatrix[Sid][Lid-1][ GeneIndexV[ Gid ][g].second ];
    }

    Pd *= DuplicationRates[Sid] * branchesDivisionLength[Sid][Lid - 1] ;
    //Pd *= 2*DuplicationRates[Sid] * branchesDivisionLength[Sid][Lid - 1] ;

    //cout << " * " << DuplicationRates[Sid] << " -> " << Pd <<endl;
    return Pd;
}

/*
    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch !!MUST BE >0!!
        - CIndexType Gid : id of the gene clade

    Returns:
        (FLOAT_TYPE): probability of this gene lineage in that species lineage at that time 
*/
FLOAT_TYPE PsDLModel::computeProbaIntraBranch( CIndexType Sid , LIndexType Lid , CIndexType Gid )
{
    return computeProbaDupIntraBranch(Sid,Lid,Gid) + computeProbaNullIntraBranch(Sid,Lid,Gid);
}


/*

    Takes:
        - CIndexType Sid : id of the species clade
        - CIndexType Gid : id of the gene clade

    Returns:
        (FLOAT_TYPE): probability of this gene lineage in that species lineage at the beginning of the species branch
*/
FLOAT_TYPE PsDLModel::computeProbaInterBranch( CIndexType Sid , CIndexType Gid )
{

    //cout << "PsDLModel::computeProbaInterBranch( "<<Sid<<" , "<< Gid<<" ) "<<endl;

    FLOAT_TYPE Ps = 0;

    //vector< pair< CIndexType, CIndexType > > SpeciesIndexV ; 
    //vector< FLOAT_TYPE > SpeciesProbaV ;
    //fillVccp( Sid , SpeciesIndexV , SpeciesProbaV , true );
    //vector< pair< CIndexType, CIndexType > > GeneIndexV ; 
    //vector< FLOAT_TYPE > GeneProbaV ;
    //fillVccp( Gid , GeneIndexV , GeneProbaV , false );

    for( unsigned s = 0 ; s < SpeciesProbaV[ Sid ].size() ; s++ )
    {
        CIndexType CIP = SpeciesIndexV[ Sid ][s].first;
        LIndexType L_CIP = branchesDivisionNumber[CIP];
        CIndexType CIPP = SpeciesIndexV[ Sid ][s].second;
        LIndexType L_CIPP = branchesDivisionNumber[CIPP];

        FLOAT_TYPE tmpPs = 0;

        //speciation and Loss cases
        tmpPs += ReconciliationMatrix[ CIP  ][ L_CIP  ][Gid] * ProbaGeneExtinct[ CIPP ][ L_CIPP ];
        tmpPs += ReconciliationMatrix[ CIPP ][ L_CIPP ][Gid] * ProbaGeneExtinct[ CIP  ][ L_CIP  ];

        //cout << "  SL("<<CIP<<","<<CIPP<<"): "<< ReconciliationMatrix[ CIP  ][ L_CIP  ][Gid] * ProbaGeneExtinct[ CIPP ][ L_CIPP ]<< " = " << ReconciliationMatrix[ CIP  ][ L_CIP  ][Gid] << " * " << ProbaGeneExtinct[ CIPP ][ L_CIPP ] << endl;
        //cout << "  SL("<<CIPP<<","<<CIP<<"): "<< ReconciliationMatrix[ CIPP ][ L_CIPP ][Gid] * ProbaGeneExtinct[ CIP  ][ L_CIP  ]<< " = " << ReconciliationMatrix[ CIPP ][ L_CIPP ][Gid] << " * " << ProbaGeneExtinct[ CIP  ][ L_CIP  ] << endl;


        for( unsigned g = 0 ; g < GeneProbaV[ Gid ].size() ; g++ )
        {
            //speciation cases
            tmpPs += GeneProbaV[ Gid ][g] * ReconciliationMatrix[ CIPP ][ L_CIPP ][ GeneIndexV[ Gid ][g].first  ] * ReconciliationMatrix[ CIP ][ L_CIP ][ GeneIndexV[ Gid ][g].second ];
            tmpPs += GeneProbaV[ Gid ][g] * ReconciliationMatrix[ CIPP ][ L_CIPP ][ GeneIndexV[ Gid ][g].second ] * ReconciliationMatrix[ CIP ][ L_CIP ][ GeneIndexV[ Gid ][g].first  ];

            //cout << "  S("<<CIPP<<","<<CIP<<"):("<< GeneIndexV[g].first<<","<<GeneIndexV[g].second<<"): ";
            //cout << GeneProbaV[g] * ReconciliationMatrix[ CIPP ][ L_CIPP ][ GeneIndexV[g].first ] * ReconciliationMatrix[ CIP ][ L_CIP ][ GeneIndexV[g].second  ];
            //cout << " = "<< GeneProbaV[g]<<" * "<<ReconciliationMatrix[ CIPP ][ L_CIPP ][ GeneIndexV[g].first ]<<" * "<<ReconciliationMatrix[ CIP ][ L_CIP ][ GeneIndexV[g].second  ]<< endl;

            //cout << "  S("<<CIPP<<","<<CIP<<"):("<< GeneIndexV[g].second<<","<<GeneIndexV[g].first<<"): ";
            //cout << GeneProbaV[g] * ReconciliationMatrix[ CIPP ][ L_CIPP ][ GeneIndexV[g].second ] * ReconciliationMatrix[ CIP ][ L_CIP ][ GeneIndexV[g].first  ];
            //cout << " = "<< GeneProbaV[g]<<" * "<<ReconciliationMatrix[ CIPP ][ L_CIPP ][ GeneIndexV[g].second ]<<" * "<<ReconciliationMatrix[ CIP ][ L_CIP ][ GeneIndexV[g].first  ]<< endl;


        }

        //cout << "  * " <<SpeciesProbaV[s] << endl;

        Ps += ( tmpPs * SpeciesProbaV[ Sid ][s]);
    }
    return Ps;
}


/*
!!recursive function!!

    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch !!MUST BE >0!!
        - CIndexType Gid : id of the gene clade

    Returns:
        ( shared_ptr<RecNode> ): backtracked reconciled subtree
*/
shared_ptr<RecNode> PsDLModel::backtrackIntra( CIndexType Sid, LIndexType Lid, CIndexType Gid)
{
    if( is_GeneLeaf(Gid) ) // special gene leaf hack -> they cannot do duplications
        return backtrackAux(Sid , Lid-1, Gid); // recursion to lower on the branch



    FLOAT_TYPE total = ReconciliationMatrix[Sid][Lid][Gid];

    FLOAT_TYPE r = ((double) rand()/ RAND_MAX) * total; //random result

    //probability that no event let a trace in the gene tree
    FLOAT_TYPE probaNull = computeProbaNullIntraBranch(Sid,Lid,Gid);
    
    //cout << "proba to Null "<< computeProbaNullIntraBranch(Sid,Lid,Gid)<< endl;

    r -= probaNull;
    
    if(r<0)
    {
        return backtrackAux(Sid , Lid-1, Gid); // recursion to lower on the branch
    }

    //duplication cases
    CCPcladeIdType gamma = geneCladeIndexToCCPid[Gid];
    //creqting the node
    shared_ptr<RecNode> dupNode ( new RecNode(Gid, Sid,Lid) );
    dupNode->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );
    dupNode->set_event("duplication");

    pair< CIndexType, CIndexType > chosenSplit;

    FLOAT_TYPE bip = geneCCPdistribution->get_count_of_bipartition( gamma );
    auto END = geneCCPdistribution->tripartition_cend( gamma );
    for(auto it = geneCCPdistribution->tripartition_cbegin( gamma ); it != END ; ++it )
    {
        FLOAT_TYPE trip = it->second;
           
        FLOAT_TYPE pCCP = trip/bip; //conditional probability of this split
        CCPcladeIdType gammaP = it->first;
        CCPcladeIdType gammaPP = geneCCPdistribution->get_complementer(gamma, gammaP); //getSisterClade(gamma, *gammaPpointer); //
        
        //cout << "  " << gamma <<"|"<< gammaP <<","<<gammaPP << "->"<< pCCP;
        
        CIndexType GidP  = geneCCPidToCladeIndex[gammaP];
        CIndexType GidPP = geneCCPidToCladeIndex[gammaPP];
        
        //cout << "*" << ReconciliationMatrix[Sid][Lid-1][GidP] <<"*" << ReconciliationMatrix[Sid][Lid-1][GidPP] <<endl;
        r -= DuplicationRates[Sid] * pCCP * ReconciliationMatrix[Sid][Lid-1][GidP] * ReconciliationMatrix[Sid][Lid-1][GidPP] ; // pCCP times the proba to have both children in the same species at the same time
        //r -= 2*DuplicationRates[Sid] * pCCP * ReconciliationMatrix[Sid][Lid-1][GidP] * ReconciliationMatrix[Sid][Lid-1][GidPP] ; // pCCP times the proba to have both children in the same species at the same time
        
        if(r<0)
        { //found the duplication we want
            chosenSplit= pair< CIndexType, CIndexType >(GidP,GidPP);
            break;
        }
    }

    if(r>=0)
        cerr << "PsDLModel::backtrackIntra " << Sid<<","<< Lid<<","<< Gid<< " : can't backtrack a proper solution..."<<endl;

    //recursion of the tzo children of the duplicqtion
    dupNode->add_child( backtrackAux( Sid , Lid-1 , chosenSplit.first ) );
    dupNode->add_child( backtrackAux( Sid , Lid-1 , chosenSplit.second ) );
    return dupNode;

}

/*
!!recursive function!!

    Takes:
        - CIndexType Sid : id of the species clade
        - CIndexType Gid : id of the gene clade

    Returns:
        ( shared_ptr<RecNode> ): backtracked reconciled subtree
*/
shared_ptr<RecNode> PsDLModel::backtrackInter( CIndexType Sid, CIndexType Gid)
{
    bool speciesLeaf = is_SpeciesLeaf(Sid);
    bool geneLeaf = is_GeneLeaf(Gid);

    CCPcladeIdType sigma = speciesCladeIndexToCCPid[Sid];
    CCPcladeIdType gamma = geneCladeIndexToCCPid[Gid];

    if( (speciesLeaf)&&(geneLeaf) ) // should actually occur iif speciesLeaf
    {
        shared_ptr<RecNode> leafNode ( new RecNode(Gid, Sid,0) );
        leafNode->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );
        leafNode->set_event("leaf");
        return leafNode; //end of the recursion
    }


    FLOAT_TYPE total = ReconciliationMatrix[Sid][0][Gid];

    FLOAT_TYPE r = ((double) rand()/ RAND_MAX) * total; //random result

    //cout << " " << r << "/" <<  total << endl;

    FLOAT_TYPE bipS = speciesCCPdistribution->get_count_of_bipartition( sigma );

    CIndexType Sid1 = 0;
    CIndexType Sid2 = 0;
    string event = "";
    CIndexType Gid1 = 0;
    CIndexType Gid2 = 0;
    LIndexType Lid1 = 0;
    LIndexType Lid2 = 0;

    auto END_S = speciesCCPdistribution->tripartition_cend(sigma);
    for(auto Sit = speciesCCPdistribution->tripartition_cbegin(sigma) ; Sit != END_S; ++Sit)
    {

        FLOAT_TYPE pCCP_S = Sit->second / bipS;

        CCPcladeIdType sigmaP = Sit->first;
        CIndexType SidP = speciesCCPidToCladeIndex[ sigmaP ];

        CCPcladeIdType sigmaPP = speciesCCPdistribution->get_complementer( sigma, sigmaP );
        CIndexType SidPP = speciesCCPidToCladeIndex[ sigmaPP ];

        LIndexType L_SidP = branchesDivisionNumber[SidP];
        LIndexType L_SidPP = branchesDivisionNumber[SidPP];

        //speciation and Loss cases
        //cout << " ("<<SidP<<","<<Gid<<")("<<SidPP<<",X) " << r << "-" <<  pCCP_S * ReconciliationMatrix[ SidP  ][ L_SidP  ][Gid] * ProbaGeneExtinct[ SidPP ][ L_SidPP ] << endl;
        r -= pCCP_S * ReconciliationMatrix[ SidP  ][ L_SidP  ][Gid] * ProbaGeneExtinct[ SidPP ][ L_SidPP ];
        if(r<0)
        { 
            Sid1 = SidP;
            Lid1 = L_SidP;
            Sid2 = SidPP;
            Lid2 = L_SidPP;
            Gid1 = Gid;
            event = "SL";
            break;
        }

        //cout << " ("<<SidPP<<","<<Gid<<")("<<SidP<<",X) " << r << "-" << pCCP_S * ReconciliationMatrix[ SidPP ][ L_SidPP ][Gid] * ProbaGeneExtinct[ SidP  ][ L_SidP  ] << endl;
        r -= pCCP_S * ReconciliationMatrix[ SidPP ][ L_SidPP ][Gid] * ProbaGeneExtinct[ SidP  ][ L_SidP  ];
        if(r<0)
        {
            Sid1 = SidPP;
            Lid1 = L_SidPP;
            Sid2 = SidP;
            Lid2 = L_SidP;
            Gid1 = Gid;
            event = "SL";
            break;
        }

        //speciation cases
        if(geneLeaf) // no speciation cases for gene leaf clades
            continue;

        FLOAT_TYPE bipG = geneCCPdistribution->get_count_of_bipartition( gamma );
        auto END_G = geneCCPdistribution->tripartition_cend(gamma);
        for(auto Git = geneCCPdistribution->tripartition_cbegin(gamma) ; Git != END_G ; ++Git)
        {

            FLOAT_TYPE pCCP_G = Git->second / bipG;

            CCPcladeIdType gammaP = Git->first;
            CIndexType GidP = geneCCPidToCladeIndex[ gammaP ];

            CCPcladeIdType gammaPP = geneCCPdistribution->get_complementer( gamma, gammaP );
            CIndexType GidPP = geneCCPidToCladeIndex[ gammaPP ];

            //cout << " ("<<SidP<<","<<GidP<<")("<<SidPP<<","<<GidPP<<") " << r << "-" << pCCP_S * pCCP_G * ReconciliationMatrix[ SidP ][ L_SidP ][ GidP ] * ReconciliationMatrix[ SidPP ][ L_SidPP ][ GidPP ] << endl;
            r -= pCCP_S * pCCP_G * ReconciliationMatrix[ SidP ][ L_SidP ][ GidP ] * ReconciliationMatrix[ SidPP ][ L_SidPP ][ GidPP ];
            if(r<0)
            {
                Sid1 = SidP;
                Lid1 = L_SidP;
                Sid2 = SidPP;
                Lid2 = L_SidPP;
                Gid1 = GidP;
                Gid2 = GidPP;
                event = "S";
                break;
            }

            //cout << " ("<<SidP<<","<<GidPP<<")("<<SidPP<<","<<GidP<<") " << r << "-" << pCCP_S * pCCP_G * ReconciliationMatrix[ SidP ][ L_SidP ][ GidPP ] * ReconciliationMatrix[ SidPP ][ L_SidPP ][ GidP ] << endl;
            r -= pCCP_S * pCCP_G * ReconciliationMatrix[ SidP ][ L_SidP ][ GidPP ] * ReconciliationMatrix[ SidPP ][ L_SidPP ][ GidP ];
            if(r<0)
            {
                Sid1 = SidP;
                Lid1 = L_SidP;
                Sid2 = SidPP;
                Lid2 = L_SidPP;
                Gid1 = GidPP;
                Gid2 = GidP;
                event = "S";
                break;
            }

        }

        if(r<0)
            break;
    }


    ////creation of return Node
    shared_ptr<RecNode> speNode ( new RecNode(Gid, Sid,0) );
    speNode->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );
    speNode->set_event("speciation");

    speNode->add_child( backtrackAux( Sid1, Lid1, Gid1 ) ); //always

    if(event == "SL")
    { // in case of a loss
        speNode->add_child( createLossNode( Sid2 , Lid2 ) );
    }
    else
    {
        speNode->add_child( backtrackAux( Sid2, Lid2, Gid2 ) ); //always
    }


    return speNode; //end of the recursion

}

/*
    Returns:
        ( shared_ptr<RecNode> ): pointer to a single node with a loss event and gene clade -1

*/
shared_ptr<RecNode> PsDLModel::createLossNode( CIndexType Sid , LIndexType Lid )
{
    shared_ptr<RecNode> lossNode ( new RecNode(-1, Sid,0) );
    //cout << "add BL later"<<endl;//dupNode->set_BranchLength( geneCCPdistribution->get_branch_length(gamma) );
    lossNode->set_event("loss");
    return lossNode;
}


/*
!!recursive function!!

    Takes:
        - CIndexType Sid : id of the species clade
        - LIndexType Lid : id of the branch interval along the species clade branch !!MUST BE >0!!
        - CIndexType Gid : id of the gene clade

    Returns:
        ( shared_ptr<RecNode> ): backtracked reconciled subtree
*/
shared_ptr<RecNode> PsDLModel::backtrackAux( CIndexType Sid, LIndexType Lid, CIndexType Gid)
{
    //cout << "backtrackAux:" << Sid << "," << Lid << "," << Gid << endl;

    if(Lid == 0)
        return backtrackInter(Sid,Gid);
    return backtrackIntra(Sid,Lid,Gid);
}

//// resets

/*puts all values at -1*/
void PsDLModel::reset_GeneLeavesProbas()
{
    unsigned nbClades = speciesCladeIndexToCCPid.size();

    unsigned nbSpLeaves = speciesCCPdistribution->get_number_of_leaves();

    
    for( CIndexType Sid = 1 ; Sid < nbClades ; Sid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1

        LIndexType nbDiv = branchesDivisionNumber[Sid];

        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) // there is (number of subdivision + 1) cells    
        { 
            for( LIndexType Sid2 = 0 ; Sid2 < nbSpLeaves+1 ; Sid2++ )
                GeneLeavesProbas[Sid][Lid][Sid2] = -1 ;
        }
    }

}

/*empties the reconciliation matrix*/
void PsDLModel::reset_recMat()
{
    for(auto it = ReconciliationMatrix.begin() ; it != ReconciliationMatrix.end() ; ++it)
    {
      for(auto it2 = it->begin() ; it2 != it->end() ; ++it2)
        it2->clear();
      it->clear();
    }
    
    ReconciliationMatrix.clear();  
    recMatrixComputed = false;
}


/* clear ProbaGeneOneChild  and calls reset_recMat as well */
void PsDLModel::reset_PoneGene()
{
    for(auto it = ProbaGeneOneChild.begin() ; it != ProbaGeneOneChild.end() ; ++it)
      (*it).clear();
    ProbaGeneOneChild.clear(); //index : species clade id ; value : vector of the length of the corresponding branch interval
    
    reset_GeneLeavesProbas();
    reset_recMat();
}

/*
    partially recomputes, probaExtinct probaOneGene and reconciliationMatrix

    Takes:
        - vector< CIndexType > toReset : species clade to reset
*/
void PsDLModel::partial_reCompute( vector< CIndexType > toReset )
{    
    int nbGeneClades = geneCladeIndexToCCPid.size();
    for(auto it = toReset.begin() ; it != toReset.end() ; ++it)
    { // iterate over all the species clade to reset
        CIndexType Sid = *it;

        LIndexType nbDiv = branchesDivisionNumber[Sid];
        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++)  // iterate over all their subdivisions
        {
            ProbaGeneExtinct[Sid][Lid] = computeExtinctionProba( Sid , Lid ); // replace the proba extinc
            ProbaGeneOneChild[Sid][Lid] = computeOneChildProba( Sid , Lid ); // replace the proba one child
            
            // we are now ready to compute the proba of reconciliation over all gene clades.
            for( CIndexType Gid = 1 ; Gid < nbGeneClades ; Gid++ ) 
            {
                ReconciliationMatrix[Sid][Lid][Gid] = ComputerecProba( Sid, Lid, Gid );
            }
        }
    }
    recMatrixComputed = true;
    return;
}


/*
    *recursive function*

    Takes:
        - CCPcladeIdType sigma : a species clade

    Returns:
        vector< CCPcladeIdType >: vector of all species clades that are ancestors of sigma , including sigma
                                    ORDERED from sigma to the biggest clade !
*/
vector< CCPcladeIdType > PsDLModel::get_ParentClades( CCPcladeIdType sigma )
{

    vector< CCPcladeIdType > res ; 
    res.push_back(sigma);

    if( speciesCCPdistribution->size(sigma) == speciesCCPdistribution->get_number_of_leaves() )
        return res;// root clade -> empty set of ancestor

    map< CCPcladeIdType , int > mapAncestor ;

    vector < CCPcladeIdType > Vclades = speciesCCPdistribution->get_bipartitions();

    for(auto it = Vclades.begin(); it != Vclades.end(); ++it)
    {
        if( speciesCCPdistribution->get_count_of_tripartition(*it,sigma) >0)
        {
            // we've found a parent of sigma!!

            //recursively look for ancestors of the ancestor
            vector< CCPcladeIdType > tmp = get_ParentClades( *it );
            for( auto tmpit = tmp.begin() ; tmpit != tmp.end() ; ++tmpit )
            {
                mapAncestor[*tmpit]= speciesCCPdistribution->size( *tmpit ); // we keep their size around to facilitate the sorting
                
            }
        }

    }


    //the ancestors are the keys of the map
    map< int, vector< CCPcladeIdType > > AncestorBySize;

    //now we want to ensore that the ancestors are ordered, and we will use their size for that 

    for(auto it = mapAncestor.begin() ; it != mapAncestor.end(); ++it)
    {
        if( AncestorBySize.find( it->second ) == AncestorBySize.end() )//new key
            AncestorBySize[it->second] = vector< CCPcladeIdType >();
        AncestorBySize[it->second].push_back( it->first );
    }


    unsigned from = speciesCCPdistribution->size(sigma) +1 ;
    unsigned to = speciesCCPdistribution->get_number_of_leaves() ;

    for(unsigned i = from ; i<=to ; i++)
    {
        if( AncestorBySize.find( i ) != AncestorBySize.end() )//there are ancestors of that size
        {
            res.insert( res.end() ,  AncestorBySize[i].begin() , AncestorBySize[i].end() );
        }
    }

    return res;
}

/*
    *recursive function*

    Takes:
        - CIndexType Sid : a species clade

    Returns:
        vector< CIndexType >: vector of all species clades that are ancestors of Sid, incliuding Sid 
*/
vector< CIndexType > PsDLModel::get_ParentClades( CIndexType Sid )
{
    vector< CCPcladeIdType > tmp = PsDLModel::get_ParentClades( speciesCladeIndexToCCPid[Sid] );
    vector< CIndexType > res;

    for(auto it = tmp.begin() ; it != tmp.end(); ++it)
        res.push_back(  speciesCCPidToCladeIndex[ *it ] );

    return res;
}













// public methods

PsDLModel::PsDLModel(): ParentModel(),
                        deltal_(0), // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
                        minNbBLInterval_(0), // minimum number of interval for a given branch of the species tree
                        branchesDivisionNumber(), // index : species clade id ; value : associated number of "branch length interval", 
                        branchesDivisionLength(), //index : species clade id ; value : vector of the length of the corresponding branch interval
                        ProbaGeneExtinct(),  // 1st index : species clade id
                        ReconciliationMatrix(),  // 1st index : species clade id ; sigma
                        GeneLeavesProbas() // keeps the likelihoods of single genes lineages
{
    //cout << "PsDLModel::PsDLModel" << endl;
}


PsDLModel::PsDLModel( const PsDLModel& other) // copy constructor
{

    addSpeciesCCPdistribution( other.get_speciesCCPdistribution());// makes some precomputations With ids

    if( other.get_geneCCPdistribution()  )
        addGeneCCPdistribution( other.get_geneCCPdistribution() );// makes some precomputations With ids

    const vector < CIndexType > Corr = other.get_geneSpeciesCorrespondence();
    for(auto it = Corr.begin() ; it != Corr.end() ; ++it)
        geneSpeciesCorrespondence.push_back(*it);

    for(unsigned i = 0 ; i < DuplicationRates.size(); i++)
        set_DuplicationRates( i, DuplicationRates[i] ); 
    for(unsigned i = 0 ; i < LossRates.size(); i++)
        set_LossRates( i, LossRates[i] ); 

    set_deltal( deltal_ ) ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    set_minNbBLInterval( minNbBLInterval_ ) ; // minimum number of interval for a given branch of the species tree

    //// branch divisions
    vector < LIndexType > BDN = other.get_branchesDivisionNumber();
    for(auto it = BDN.begin() ; it != BDN.end() ; ++it)
        branchesDivisionNumber.push_back(*it);
   

    vector< vector < FLOAT_TYPE >  > BDL = other.get_branchesDivisionLength() ;
    unsigned i= 0;
    for(auto it = BDL.begin() ; it != BDL.end() ; ++it)
    {
        branchesDivisionLength.push_back( vector < FLOAT_TYPE >() );
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2 )
            branchesDivisionLength[i].push_back(*it2);
        i++;
    }


    vector< vector< FLOAT_TYPE > > PGE = other.get_ProbaGeneExtinct() ;
    i= 0;
    for(auto it = PGE.begin() ; it != PGE.end() ; ++it)
    {
        ProbaGeneExtinct.push_back( vector < FLOAT_TYPE >() );
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2 )
            ProbaGeneExtinct[i].push_back(*it2);
        i++;
    }


    vector< vector< FLOAT_TYPE > > POC = other.get_ProbaGeneOneChild();
    i= 0;
    for(auto it = POC.begin() ; it != POC.end() ; ++it)
    {
        ProbaGeneOneChild.push_back( vector < FLOAT_TYPE >() );
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2 )
            ProbaGeneOneChild[i].push_back(*it2);
        i++;
    }

    vector< vector< vector < FLOAT_TYPE > > > RM = other.get_ReconciliationMatrix();  
    i= 0;
    for(auto it = RM.begin() ; it != RM.end() ; ++it)
    {
        ReconciliationMatrix.push_back( vector< vector < FLOAT_TYPE > >() );
        unsigned j = 0;
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2 )
        {
            ReconciliationMatrix[i].push_back( vector < FLOAT_TYPE >() );
            unsigned k = 0;
            for(auto it3 = it2->begin() ; it3 != it2->end() ; ++it3 )
            {
                ReconciliationMatrix[i][j].push_back(*it3);
                //cout << i << ','<<j<<','<<k<<';'<<ReconciliationMatrix[i][j][k]<<endl;
                k++;
            }

            j++;
        }
        i++;
    }


    GeneLeavesProbas.clear();
    vector< vector< vector < FLOAT_TYPE > > > GLP = other.get_GeneLeavesProbas();  
    i= 0;
    for(auto it = GLP.begin() ; it != GLP.end() ; ++it)
    {
        GeneLeavesProbas.push_back( vector< vector < FLOAT_TYPE > >() );
        unsigned j = 0;
        for(auto it2 = it->begin() ; it2 != it->end() ; ++it2 )
        {
            GeneLeavesProbas[i].push_back( vector < FLOAT_TYPE >() );
            unsigned k = 0;
            for(auto it3 = it2->begin() ; it3 != it2->end() ; ++it3 )
            {
                GeneLeavesProbas[i][j].push_back(*it3);
                //cout << i << ','<<j<<','<<k<<';'<<ReconciliationMatrix[i][j][k]<<endl;
                k++;
            }
            j++;
        }
        i++;
    }



    rootBranchLength = other.get_rootBranchLength();

    set_probaExtantGeneSampled(other.get_probaExtantGeneSampled() );

    recMatrixComputed = other.get_recMatrixComputed();


}



/*

    Takes:
        - unsigned int verboseLevel : 0 : quiet (default)
                                      1 : prints some information to stdout
*/
void PsDLModel::divideSpeciesBranches(unsigned int verboseLevel)
{
    assert(speciesCladeIndexToCCPid.size()>0); //checks that the species ccp has been set
    assert(deltal_>0); // checks that the deltal has been set
    assert(minNbBLInterval_>0); // checks that the minimum number of interval per branch has been set

    //cerr << "Warning : function PsDLModel::divideSpeciesBranches uses default branch length while the full CCP library is being developped." << endl;

    branchesDivisionLength.reserve(speciesCladeIndexToCCPid.size());
    branchesDivisionNumber.reserve(speciesCladeIndexToCCPid.size());

    for(CIndexType i = 0 ; i < speciesCladeIndexToCCPid.size(); i++)
    {
        if( i == 0 )
        {//empty first element does not correspond to any meaningful clade
            branchesDivisionLength.push_back( vector< FLOAT_TYPE >()  ); 
            branchesDivisionNumber.push_back(0); 
            continue; 
        }

        //using default value for the moment 
        FLOAT_TYPE L = getSpeciesBranchLength( i );

        if(L == 0 )
        {
            cerr << "ERROR : detected a species clade (" << speciesCladeIndexToCCPid[i] << ") with branch length 0. abort."<<endl;
            cerr << "You can use the script : scripts/AddBranchLengthToAle.py , distributed with this software, to add some branch lengths to clades that have none in a CCP distribution."<<endl;
            exit(4);
        }

        branchesDivisionLength.push_back( divideOneBranch( L ) );

        branchesDivisionNumber.push_back(branchesDivisionLength[i].size());

        /* Note : 
        as a convention, a branch will be composed of (number_of_division + 1) cells
        the first cell (of index 0) correspond to the very start of the branch (the part closer to the present, given the branch orientation), with length 0
        the other cells, of index n(>0), correspond to the point along the branch that is lengths[n-1] above the previous one.
        */

        if(verboseLevel>1)
        {
            cout << "divided clade "<< speciesCladeIndexToCCPid[i] << ",blen:"<< getSpeciesBranchLength( i ) <<" in " << branchesDivisionLength[i].size() << " : " << endl;
            if(verboseLevel>2)
            {
                for(unsigned j = 0; j < branchesDivisionLength[i].size(); j++)
                    cout << " " << branchesDivisionLength[i][j] ;
                cout << endl;
                
            }
        }

    }


    return;
}

/*

    Takes:
        - unsigned int verboseLevel : 0 : quiet (default)
                                      1 : prints some information to stdout

*/
void PsDLModel::computeProbaGeneExtinct(unsigned int verboseLevel)
{

    assert( DuplicationRates.size() > 0); //checks that the species ccp has been set and duplication rates have been assigned
    //assert( DuplicationRates[0] > 0); //lazy check that the duplication rate is > 0
    assert( LossRates.size() > 0); //checks that the species ccp has been set and loss rates have been assigned
    //assert( LossRates[0] > 0); //lazy check that the loss rate is > 0
    assert( branchesDivisionLength.size() > 0); //checks that the species clade branches have been subdivided

    //if(verboseLevel>1)
    //{
    //    //cout << "PsDLModel::computeProbaGeneExtinct " <<endl;
    //    //cout << "branchesDivisionNumber "<< branchesDivisionNumber.size() <<endl;
    //}

    /*
    boost::dynamic_bitset<> root { speciesCCPdistribution->get_number_of_leaves() , 0 };
    root.flip();
    for( auto it = speciesCCPdistribution->tripartition_cbegin( root ) ; it != speciesCCPdistribution->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = speciesCCPdistribution->get_complementer(root, it->first);
        int id1 = speciesCCPdistribution->get_bitset_id( it->first );
        int id2 = speciesCCPdistribution->get_bitset_id( sigmaPP );

        if((id1 == -1) || (id2 == -1))
        {
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << it->second << endl;
            exit(1);
        }

        int id1bis = speciesCCPidToCladeIndex[ it->first ];
        int id2bis = speciesCCPidToCladeIndex[ sigmaPP ];

        if((id1bis == 0) || (id2bis == 0))
        {
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << id1bis << "," << id2bis;
            cout << " -> " << it->second << endl;
            exit(1);
        }
    }
    cout << " species distrib checks off" << endl;
    */

    ProbaGeneExtinct.reserve(speciesCladeIndexToCCPid.size());

    for(CIndexType CI = 0 ; CI < speciesCladeIndexToCCPid.size(); CI++)
    { //for each species clade
        ProbaGeneExtinct.push_back( vector<FLOAT_TYPE>() );
        if( CI == 0 )
        {//empty first element does not correspond to any meaningful clade
            continue; 
        }

        //if(verboseLevel>1)
        //    cout << " " << CI << " >" << branchesDivisionNumber[CI] << " . " << ProbaGeneExtinct.size() << endl;


        ProbaGeneExtinct[CI].reserve( branchesDivisionNumber[CI] );
        

        for(LIndexType LI = 0 ; LI <= branchesDivisionNumber[CI]; LI++) // there is number of subdivision + 1 cells
        { // for each subdivision of the branch of this species clade

            //if(verboseLevel>1)
            //{
            //    cout << " " << CI << "-" << LI << endl;
            //    cout << "             " << computeExtinctionProba( CI, LI , verboseLevel ) <<endl;                
            //}
            ProbaGeneExtinct[CI].push_back( computeExtinctionProba( CI, LI ) );

        }

    }


    if(verboseLevel>1)
    {
        cout << "probability of having no child at present"<<endl;

        for(CIndexType CI = 0 ; CI < speciesCladeIndexToCCPid.size(); CI++)
        { //for each species clade
            if( CI == 0 )
            {//empty first element does not correspond to any meaningful clade
                continue; 
            }
            
            bool spLeaf = is_SpeciesLeaf(CI);

            for(LIndexType LI = 0 ; LI <= branchesDivisionNumber[CI]; LI++) // there is number of subdivision + 1 cells
            { // for each subdivision of the branch of this species clade
    
                cout << CI <<" " << LI << ' ' << ProbaGeneExtinct[CI][LI] << " " ;
    

                if(LI == 0)
                {                    

                    if( spLeaf ) // we are among the leaf clades
                    {
                        cout << "present";
                        // the probability to become extinct before the end of a leaf branch when you are at the end of a leaf branch is 0
                    }
                    else
                    {

                        //use the probability in the children clades
                        CCPcladeIdType sigma = speciesCladeIndexToCCPid[ CI ];
    
                        //vector< CCPcladeIdType > splits = speciesCCPdistribution->get_tripartitions(sigma);
                        FLOAT_TYPE bip = speciesCCPdistribution->get_count_of_bipartition(sigma);
                        auto END = speciesCCPdistribution->tripartition_cend(sigma);
                        for(auto sigmaPpointer = speciesCCPdistribution->tripartition_cbegin( sigma ) ; sigmaPpointer != END ; ++sigmaPpointer )
                        {    
                            FLOAT_TYPE trip = sigmaPpointer->second;
                            
                            FLOAT_TYPE pCCP = trip/bip;
    
                            CCPcladeIdType sigmaPP = speciesCCPdistribution->get_complementer(sigma, sigmaPpointer->first); //getSisterClade(sigma, *sigmaPpointer);//
    
                            CIndexType CIP  = speciesCCPidToCladeIndex[sigmaPpointer->first];
                            CIndexType CIPP = speciesCCPidToCladeIndex[sigmaPP];
    
                            cout << "\n\t" << pCCP << '*' << ProbaGeneExtinct[CIP][ branchesDivisionNumber[CIP] ] << '*' << ProbaGeneExtinct[CIPP][ branchesDivisionNumber[CIPP] ] ; 
                            // probability of this split times to probability to go extinct from the top of both children
                        }
                    }

                }
                else
                {//not the first interval of the branch
    
                    cout << "D:" << DuplicationRates[CI] << " ";
                    cout << "L:" << LossRates[CI]  << " ";
                    cout << "dl:" << branchesDivisionLength[CI][LI-1] << " ";
                    cout << "E:" << ProbaGeneExtinct[CI][LI-1] << " ";
    
                }
                cout <<endl;
            }
        }
    }

}



/*
    ProbaGeneExtinct must already have been computed

    Takes:
        - unsigned int verboseLevel : 0 : quiet (default)
                                      1 : prints some information to stdout

*/
void PsDLModel::computeProbaGeneOneChild(unsigned int verboseLevel)
{
    assert( ProbaGeneExtinct.size() > 0) ;// checks that gene extinction probabilities have been computed. Basically also ensures that species ccp have been set, DL have been set and branches subdivided

    ProbaGeneOneChild.reserve(speciesCladeIndexToCCPid.size());

    for(CIndexType CI = 0 ; CI < speciesCladeIndexToCCPid.size(); CI++)
    { //for each species clade
        ProbaGeneOneChild.push_back( vector<FLOAT_TYPE>() );
        if( CI == 0 )
        {//empty first element does not correspond to any meaningful clade
            continue; 
        }


        ProbaGeneOneChild[CI].reserve( branchesDivisionNumber[CI] );
        for(LIndexType LI = 0 ; LI <= branchesDivisionNumber[CI]; LI++) // there is number of subdivision + 1 cells
        { // for each subdivision of the branch of this species clade
            ProbaGeneOneChild[CI].push_back( computeOneChildProba( CI , LI ) );
        /*if(LI == 0)
            {//first interval of the branch 

                ProbaGeneOneChild[CI].push_back( 0 ); //initialize
                if( is_SpeciesLeaf( CI ) ) // we are among the leaf clades
                {
                    ProbaGeneOneChild[CI][LI] = 1; 
                    
                    // the probability to have one child before the end of a leaf branch when you are at the end of a leaf branch is 1
                }
                else
                {
                    ProbaGeneOneChild[CI][LI] = 1; 
                    // the probability to have one child before the end of a branch when you are at the end of said branch is always 1
                }
            }
            else
            {//not the first interval of the branch

                FLOAT_TYPE dl = branchesDivisionLength[CI][LI-1];

                ProbaGeneOneChild[CI].push_back( computeOneChildProba( DuplicationRates[CI], LossRates[CI] , dl , ProbaGeneExtinct[CI][LI-1]));//, ProbaGeneOneChild[CI][LI-1] ) );
                //along branch it follows the formula     (E^((-la + mu) t) (la - mu)^2 q)/(la - E^((-la + mu) t) mu - la p +   E^((-la + mu) t) la p)^2

            }*/

        }

    }

    if(verboseLevel>1)
    {
        cout << "probability of having exactly 1 child"<<endl;

        for(CIndexType CI = 0 ; CI < speciesCladeIndexToCCPid.size(); CI++)
        { //for each species clade
            if( CI == 0 )
            {//empty first element does not correspond to any meaningful clade
                continue; 
            }
            
            for(LIndexType LI = 0 ; LI <= branchesDivisionNumber[CI]; LI++) // there is number of subdivision + 1 cells
            { // for each subdivision of the branch of this species clade
    
                cout << CI <<" " << LI << ' ' << ProbaGeneOneChild[CI][LI] << " " ;
    
                if(LI > 0)
                {//not the first interval of the branch
    
                    cout << "D:" << DuplicationRates[CI] << " ";
                    cout << "L:" << LossRates[CI]  << " ";
                    cout << "dl:" << branchesDivisionLength[CI][LI-1] << " ";
                    cout << "E:" << ProbaGeneExtinct[CI][LI-1] << " ";
    
                }
                cout <<endl;
            }
        }
    }

}

// makes some precomputations With ids
// and duplication and loss rates
void PsDLModel::addSpeciesCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel)
{
    speciesCCPdistribution = CCPdistribution;

    CCPcladeIdType emptyClade(speciesCCPdistribution->get_number_of_leaves(),0ul);

    speciesCladeIndexToCCPid.push_back(emptyClade);
    speciesCCPidToCladeIndex[emptyClade] = 0;
    SpeciesIndexV.push_back( vector< pair< CIndexType, CIndexType > >() );
    SpeciesProbaV.push_back( vector< FLOAT_TYPE >() );

    //vector< CCPcladeIdType > clades = speciesCCPdistribution->get_bipartitions();

    auto END = speciesCCPdistribution->bipartition_cend();
    for(auto gamma_it = speciesCCPdistribution->bipartition_cbegin() ; gamma_it != END ; ++gamma_it)
    {
        /*
        speciesCCPidToCladeIndex[*gamma_it] = speciesCladeIndexToCCPid.size(); // from the bitset that are used inside the CCP object to the integer type id 
        if(verboseLevel>1)
        {
            cout << "species : " << *gamma_it << " " << speciesCCPidToCladeIndex[*gamma_it] << " " << speciesCCPdistribution->get_branchlength( *gamma_it ) ;
            if(speciesCCPdistribution->is_leaf( speciesCCPidToCladeIndex[*gamma_it] ) )
                cout << " " << speciesCCPdistribution->get_name_of_leaf( speciesCCPidToCladeIndex[*gamma_it] );
            cout <<endl;
        }
        speciesCladeIndexToCCPid.push_back(*gamma_it); // from integer type id to the bitset that are used inside the CCP object
        */
        speciesCCPidToCladeIndex[ gamma_it->first ] = speciesCladeIndexToCCPid.size(); // from the bitset that are used inside the CCP object to the integer type id 
        if(verboseLevel>1)
        {
            cout << "species : " << gamma_it->first << " " << speciesCCPidToCladeIndex[gamma_it->first] << " " << speciesCCPdistribution->get_branchlength( gamma_it->first ) ;
            if(speciesCCPdistribution->is_leaf( speciesCCPidToCladeIndex[gamma_it->first] ) )
                cout << " " << speciesCCPdistribution->get_name_of_leaf( speciesCCPidToCladeIndex[gamma_it->first] );
            cout <<endl;
        }
        speciesCladeIndexToCCPid.push_back(gamma_it->first); // from integer type id to the bitset that are used inside the CCP object


        vector< pair< CIndexType, CIndexType > > IV ; 
        vector< FLOAT_TYPE > PV ;

        fillVccp( speciesCladeIndexToCCPid.size()-1 , IV , PV , true );
        SpeciesIndexV.push_back( IV ) ;
        SpeciesProbaV.push_back( PV ) ;
        //cout << "OPT " << gamma_it->first <<endl;
        //for(auto p : SpeciesIndexV.back() )
        //    cout << " " << p.first << "|" << p.second;
        //cout << endl;
        //for(auto p : SpeciesProbaV.back() )
        //    cout << " " << p;
        //cout << endl;


    }

    /*
    boost::dynamic_bitset<> root { speciesCCPdistribution->get_number_of_leaves() , 0 };
    root.flip();
    for( auto it = speciesCCPdistribution->tripartition_cbegin( root ) ; it != speciesCCPdistribution->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = speciesCCPdistribution->get_complementer(root, it->first);
        int id1 = speciesCCPdistribution->get_bitset_id( it->first );
        int id2 = speciesCCPdistribution->get_bitset_id( sigmaPP );

        if((id1 == -1) || (id2 == -1))
        {
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << it->second << endl;
            exit(1);
        }

        int id1bis = speciesCCPidToCladeIndex[ it->first ];
        int id2bis = speciesCCPidToCladeIndex[ sigmaPP ];

        if((id1bis == 0) || (id2bis == 0))
        {
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << id1bis << "," << id2bis;
            cout << " -> " << it->second << endl;

            vector<string> V = speciesCCPdistribution->get_clade_leaf_names( it->first ); 
            for(unsigned i = 0 ; i < V.size() ; i++)
                cout << V[i] << " ";
            cout << endl;

            exit(1);
        }
    }
    cout << " species distrib checks off after adding it " << endl;
    */

    /// additional steps

    /// filling default event rates to 0
    unsigned nbClades = speciesCladeIndexToCCPid.size();
    DuplicationRates.reserve(nbClades);
    LossRates.reserve(nbClades);
    for(unsigned i = 0 ; i < nbClades;i++)
    {
        DuplicationRates.push_back(0);
        LossRates.push_back(0);
    }

 
}



/*
    co;pute the reconciliqtion probqbilitiews for all cells of the reconciliation matrix (|Gccp|*|Sccp'|)

    ProbaGeneOneChild must already have been computed

    Takes:
        - const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask
                                    -> mask from gene bit sets to species bitset to speed-up computing of "impossible" scenarios in DL reconciliation
        - unsigned int verboseLevel : 0 : quiet (default)
                                      1 : prints some information to stdout


*/
void PsDLModel::computeReconciliationMatrix( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask , unsigned int verboseLevel)
{
    assert( geneCladeIndexToCCPid.size() > 0) ;//checks that the gene ccp has been set
    assert( ProbaGeneOneChild.size() > 0) ;// checks that gene transmission probabilities have been computed. Basically also ensures that species ccp have been set, DL have been set and branches subdivided

    int nbSpClades = speciesCladeIndexToCCPid.size();
    int nbGeneClades = geneCladeIndexToCCPid.size();

    initializeRecMatrix();

    if(verboseLevel>0)
        cout << "Reconciliation matrix initialization done." << endl;


    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1

        LIndexType nbDiv = branchesDivisionNumber[Sid];

        bool SpeciesLeaf = is_SpeciesLeaf( Sid );

        for( CIndexType Gid = 1 ; Gid < nbGeneClades ; Gid++ ) 
        { // the clade with index 0 is a dummy clade, so we start at 1

            bool computeCase = true;

            auto SpMask =  GSpMask.find( geneCladeIndexToCCPid[ Gid ] );
            if( SpMask != GSpMask.end() )
            { // the map is not empty
                if( ! SpMask->second.is_subset_of( speciesCladeIndexToCCPid[ Sid ] ) )
                { // the mask is not a subset of the current species --> the scenario is impossible
                    computeCase =false;
                }
            }


            for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) // there is (number of subdivision + 1) cells    
            { 
                if(computeCase)
                    ReconciliationMatrix[Sid][Lid][Gid] = ComputerecProba( Sid, Lid, Gid );
                else // we can bypass these computations
                    ReconciliationMatrix[Sid][Lid][Gid] = 0;

                if(verboseLevel>1)
                    cout << "P( "<< Sid << " , " << Lid << " , " << Gid << " ) = " << ReconciliationMatrix[Sid][Lid][Gid] << endl;
            }
        }
    }

    recMatrixComputed = true;

    if(verboseLevel>0)
        cout << "Reconciliation matrix computed." << endl;

}



/*

    Returns:
        (FLOAT_TYPE) : probability of the reconciliations across all possible position for the root of the gene tree (in the species tree)
                            across all gene and species topologies represented in theoir respoective CCP distributions

                OR
                    -1 if the reconciliation matrix has not been computed
*/
FLOAT_TYPE PsDLModel::getOverAllProbability()
{
    //check if the matrix was computed
    if(!recMatrixComputed)
    {
        return -1;
    }


    FLOAT_TYPE Psum = 0;
    FLOAT_TYPE normalizator = 0;

    CIndexType rootGeneClade = geneCCPidToCladeIndex.size()-1; // the root clade is the last one

    int nbSpClades = speciesCladeIndexToCCPid.size();

    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { 
        LIndexType nbDiv = branchesDivisionNumber[Sid];
        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) 
        { 
            Psum += ReconciliationMatrix[Sid][Lid][ rootGeneClade ];
            normalizator +=  1 - ProbaGeneExtinct[Sid][Lid] ;
        }
    }

    //cout << Psum << '/' << normalizator << endl;

    return Psum / (normalizator);
}


/*
    Returns:
        ( shared_ptr<RecNode> ): backtracked reconciled tree
                            OR
                                empty pointer if the matrix is not ready to be backtracked
*/
shared_ptr<RecNode> PsDLModel::backtrack()
{
    //check if the matrix was computed
    if(!recMatrixComputed)
    {
        return  shared_ptr<RecNode>() ;
    }


    CIndexType rootGeneClade = geneCCPidToCladeIndex.size()-1; // the root clade is the last one

    CIndexType chosenSid = 0;
    LIndexType chosenLid = 0;

    FLOAT_TYPE  Psum = 0; // we use the unnormalized sum

    int nbSpClades = speciesCladeIndexToCCPid.size();

    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { 
        LIndexType nbDiv = branchesDivisionNumber[Sid];
        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) 
        { 
            Psum += ReconciliationMatrix[Sid][Lid][ rootGeneClade ] ;
            //cout << "  BT: "<< ReconciliationMatrix[Sid][Lid][ rootGeneClade ] <<endl;
        }
    }

    if( Psum == 0 )
    {
        cerr << "!ERROR! : the overall probability of the reconciliation is 0. possible underflow?" << endl;

        exit(3);
    }


    FLOAT_TYPE r = ((double) rand()/ RAND_MAX) * Psum; //random result
    
    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { 
        LIndexType nbDiv = branchesDivisionNumber[Sid];
        for(LIndexType Lid = 0 ; Lid <= nbDiv; Lid++) 
        {             
            
            r -= ReconciliationMatrix[Sid][Lid][ rootGeneClade ] ;
            
            if(r<0)
            {
                chosenSid = Sid;
                chosenLid = Lid;
                break;
            }
        }
        if(r<0)
            break;
    }

    //cout << "backtrack:" << chosenSid << "," << chosenLid << "," << rootGeneClade << endl;

    return backtrackAux(  chosenSid, chosenLid, rootGeneClade );
}


void PsDLModel::reset()
{
    for(auto it = ProbaGeneExtinct.begin() ; it != ProbaGeneExtinct.end() ; ++it)
      (*it).clear();
    ProbaGeneExtinct.clear(); //index : species clade id ; value : vector of the length of the corresponding branch interval

    fillSpeciesIndexAndProbaCCP();

    reset_PoneGene();
}

/*
    Changes a branch wise parameter and recompute all that needs to be.

    Takes:
        - CIndexType cladeToChange : id of a pseices clade whose specific parameter will be changed
        - string paramName : name of the parameter.
                                currently supported: "D": duplication rate
                                                     "L": loss rate
                                TODO : branch length
        - FLOAT_TYPE newValue : new value for the parameter

    Returns:
        (int) : 0 : everything went ok
                1 : the new value is invalid (typically <= 0)
                2 : the parameter to change is unknown ...
                3 : the reconciliation matrix weren't computed before this was called
*/
int PsDLModel::changeBranchWiseParamAndRecompute( CIndexType cladeToChange , string paramName , FLOAT_TYPE newValue )
{
    if( ! recMatrixComputed )
    {
        //cerr << "Error: called PsDLModel::changeBranchWiseParamAndRecompute but matrix was never computed to begin with. does nothing!"
        return 3;
    }

    if( newValue <= 0 )
    { // maybe also define a max??
        return 1;
    }

    // changing the paraneter
    if( paramName == "D" )
    {
        set_DuplicationRates( cladeToChange , newValue );
    }
    else if( paramName == "L" )
    {
        set_LossRates( cladeToChange , newValue );
    }
    else// unknown parameter 
        return 2; 

    // getting the clades to reset
    vector < CIndexType > toReset = get_ParentClades( cladeToChange );

    //cout << "reset of " << cladeToChange<< ":";
    //for(auto it = toReset.begin() ; it != toReset.end() ; ++it)
    //{
    //    cout << " " << *it;
    //}
    //cout << endl;

    partial_reCompute(toReset);

    return 0;
}


/*
    adapts to deletions of bipiartitions in the ccp distribution

    MUST be followed by a reset if probabilities were already computed (as they won't correspond to the updated ccp distribution)
*/
void PsDLModel::reMapSpeciesCCPdistribution()
{
    assert( branchesDivisionLength.size() > 0); //checks that the species clade branches have been subdivided

    vector < CCPcladeIdType > New_speciesCladeIndexToCCPid; // from integer type id to the bitset that are used inside the CCP object
    map < CCPcladeIdType , CIndexType > New_speciesCCPidToCladeIndex; // from the bitset that are used inside the CCP object to the integer type id 

    // rates
    vector < FLOAT_TYPE > New_DuplicationRates; //  index : species clade id ; value : associated, branch-wise, duplication rate
    vector < FLOAT_TYPE > New_LossRates; //         index : species clade id ; value : associated, branch-wise, loss rate
    
    // species clades branch length division
    vector < LIndexType > New_branchesDivisionNumber; // index : species clade id ; value : associated number of "branch length interval", 

    vector< vector < FLOAT_TYPE >  > New_branchesDivisionLength; //index : species clade id ; value : vector of the length of the corresponding branch interval



    /// setup the new indexes 

    CCPcladeIdType emptyClade(speciesCCPdistribution->get_number_of_leaves(),0ul);

    New_speciesCladeIndexToCCPid.push_back(emptyClade);
    New_speciesCCPidToCladeIndex[emptyClade] = 0;


    auto END = speciesCCPdistribution->bipartition_cend();
    for(auto gamma_it = speciesCCPdistribution->bipartition_cbegin() ; gamma_it != END ; ++gamma_it)
    {
        New_speciesCCPidToCladeIndex[ gamma_it->first ] = New_speciesCladeIndexToCCPid.size(); // from the bitset that are used inside the CCP object to the integer type id 
        New_speciesCladeIndexToCCPid.push_back( gamma_it->first ); // from integer type id to the bitset that are used inside the CCP object

    }

    unsigned nbClades = New_speciesCladeIndexToCCPid.size();

    New_DuplicationRates.reserve(nbClades);
    New_LossRates.reserve(nbClades);
    New_branchesDivisionNumber.reserve( nbClades );
    New_branchesDivisionLength.reserve( nbClades );

    for(unsigned i = 0 ; i < nbClades;i++)
    {
        CCPcladeIdType gamma = New_speciesCladeIndexToCCPid[i];
        CIndexType oldIndex = speciesCCPidToCladeIndex[ gamma ];

        New_DuplicationRates.push_back( DuplicationRates[ oldIndex ] );
        New_LossRates.push_back( LossRates[ oldIndex ] );

        New_branchesDivisionNumber.push_back( branchesDivisionNumber[ oldIndex ] );
        New_branchesDivisionLength.push_back( branchesDivisionLength[ oldIndex ] );

    }

    ///checks
    //cout << "new dup  rate: " << New_DuplicationRates.size() << endl;
    //cout << "new loss rate: " << New_LossRates.size() << endl;
    //cout << "new brDivNum : " << New_branchesDivisionNumber.size() << endl;
    //cout << "new brDivLen : " << New_branchesDivisionLength.size() << endl;
    //for(unsigned i = 0 ; i < New_branchesDivisionLength.size() ; i++ )
    //    cout << " " << New_branchesDivisionLength[i].size(); 
    //cout << endl;

    /// replacing the old with the new

    speciesCladeIndexToCCPid.clear();
    speciesCCPidToCladeIndex.clear();


    DuplicationRates.clear();
    LossRates.clear();
    branchesDivisionNumber.clear();
    for(unsigned i = 0 ; i < New_branchesDivisionLength.size() ; i++ )
        branchesDivisionLength[i].clear(); 
    branchesDivisionLength.clear();


    speciesCladeIndexToCCPid = New_speciesCladeIndexToCCPid;
    speciesCCPidToCladeIndex = New_speciesCCPidToCladeIndex;

    DuplicationRates = New_DuplicationRates;
    LossRates = New_LossRates;
    branchesDivisionNumber = New_branchesDivisionNumber;
    branchesDivisionLength = New_branchesDivisionLength;


    fillSpeciesIndexAndProbaCCP();

    for(auto it = GeneLeavesProbas.begin() ; it != GeneLeavesProbas.end() ; ++it)
    {
      for(auto it2 = it->begin() ; it2 != it->end() ; ++it2)
        it2->clear();
      it->clear();
    }
      
    GeneLeavesProbas.clear();
    initializeGeneLeavesProbas();
}
/*
void PsDLModel::quickCheck()
{
    ///checks
    cout << "PsDLModel::quickCheck" << endl;
    cout << "    dup  rate: " << DuplicationRates.size() << endl;
    cout << "    loss rate: " << LossRates.size() << endl;
    cout << "    brDivNum : " << branchesDivisionNumber.size() << endl;
    cout << "    brDivLen : " << branchesDivisionLength.size() << endl;
    for(unsigned i = 0 ; i < branchesDivisionLength.size() ; i++ )
        cout << " " << branchesDivisionLength[i].size(); 
    cout << endl;
}*/


/*
    Takes:
        - shared_ptr<TreesetMetadata> speciesCCPdistrib 
        - vector <  FLOAT_TYPE  > DefaultRates : first element : duplication rates ; second element : loss rates
        - map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates : keys-> species bitset; value-> first element : duplication rates ; second element : loss rates 
        - map<string , double > extantSampling 
        - unsigned int verboseLevel=0
        - double deltal = 0.05 
        - int minNbInt = 5 
*/
void PsDLModel::prepareForComputation(shared_ptr<TreesetMetadata> speciesCCPdistrib , 
                                        vector <  FLOAT_TYPE  > DefaultRates, 
                                        map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates, 
                                        map< string , FLOAT_TYPE >  extantSampling , 
                                        unsigned int verboseLevel, 
                                        double deltal  , int minNbInt  )
{

    addSpeciesCCPdistribution(speciesCCPdistrib);// makes some precomputations With ids
    
    set_DuplicationRates(DefaultRates[0]);
    set_LossRates(DefaultRates[1]);

    for( auto it : Rates )
    {
        set_DuplicationRates( it.first , it.second[0] );
        set_LossRates( it.first , it.second[1] );
    }
    
    set_deltal( deltal ) ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    set_minNbBLInterval( minNbInt ) ; // minimum number of interval for a given branch of the species tree
    set_rootBranchLength( deltal*minNbInt*5 ); 

    divideSpeciesBranches(verboseLevel);    //   
    initializeGeneLeavesProbas();

    set_probaExtantGeneSampled(extantSampling);
            
    computeProbaGeneExtinct(verboseLevel);  // maybe these could move to protected when they've been tested

    computeProbaGeneOneChild(verboseLevel); // as they NEED to be executed only once and in this precise order

}