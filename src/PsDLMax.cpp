/*

This file contains a small executable to
perform an expatation maximisation opti;isation of the parameter of the PsDL model


Created the: 21-02-2018
by: Wandrille Duchemin

Last modified the: 04-05-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>

#include "PsDLUtils.h"
#include "PsDLMaxOptionHandler.h"
#include "PsDLModel.h"
#include "PsDLMaxUtils.h"
#include "speciesCCPupdateUtils.h"

#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>
#include <limits>

using namespace std;





/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    shared_ptr<PsDLMaxOptionHandler> optionHandler = make_shared<PsDLMaxOptionHandler>();
 
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        return -99;
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;

    // initializing some objects

    clock_t begin;
    clock_t end;
    double elapsed_secs1;
    double elapsed_secs2;


    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( VerboseLevel > 0 )
        cout << "using random seed : "<< seed << endl;

    srand(seed);

    if( VerboseLevel > 0 )
        begin = clock();

    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names


    // **************
    // 1 reading data
    // **************

    
    // 1A reading tree data
    // **************


    
    // 1A reading tree data
    // **************

    // 1A.1 gene file names and files
    // **************


    vector <string> geneDistribFileNames;

    if( optionHandler->get_multiple_gene_families() )
    {
        pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
    }
    else
        geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );


    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
        exit(pb);
    }


    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , optionHandler->get_input_is_ale() , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "read gene distribution(s)."<<endl;

    // 1A.2 species tree data
    // **************


    pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, optionHandler->get_input_is_ale() , optionHandler->get_unrooted_species_tree() ); 

    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
        exit(pb);
    }

    if(optionHandler->get_verbose_level()>1)
        cout << "read species distribution"<<endl;




    // 1B reading gene species correspondence data
    // **************

    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }
    if(VerboseLevel > 2)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }

    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        GeneCladeSpeciesMasks.push_back( getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }


    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks[GeneFamilyId] );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }

    
    // 1D eventually reading branch-wise input rates
    // ************************
    map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > rateMap;
    if(optionHandler->get_input_rate_file() != "")
    {

        int pb = readRatesFile(optionHandler->get_input_rate_file() , SpeciesCCPDistrib , rateMap);
        if(pb != 0)
        {
            if(pb == 1)
                cerr << "Unable to open the rate input file: " << optionHandler->get_input_rate_file() << endl ;
            exit(pb);

        }
    }


    if( VerboseLevel > 0 )
    {
        end = clock();
        elapsed_secs1 = double(end - begin) / CLOCKS_PER_SEC;
        cout << " time elapsed to read input data: "<< elapsed_secs1 << endl;
        elapsed_secs1 = 0;
        elapsed_secs2 = 0;
    }



    // 2 instanciating the model
    // **************

    // instanciation that does not rely of the gene trees

    bool BranchWiseRates = optionHandler->get_branchWiseRates();


    double InitialDupRate = optionHandler->get_initialDupRate();
    double InitialLossRate = optionHandler->get_initialLossRate();


    //working with the assumption of an underlying Gamma distribution for rates. 
    //used to get some "default" values for extreme observations of p10 and p11 when re-computing rates
    double DupRateMean = InitialDupRate;
    double DupRateVar = DupRateMean;

    double LossRateMean = InitialLossRate;
    double LossRateVar = LossRateMean;

    double nbRateCategories = 10;
    double MinMinRate = 0.000000001; // minimum possible rate. to avoid underflows because of very low duplication of loss numbers 

    // cout << 2.8944 << " " << getMeanExtremeQuantileGammaD(1, 2, 4,  true)<<endl; // exemples of extremum values 
    // cout << 0.0334 << " " << getMeanExtremeQuantileGammaD(1, 2, 4,  false)<<endl; // exemples of extremum values 


    PsDLModel * model = new PsDLModel();

    vector<FLOAT_TYPE> DefaultRates( 3 , optionHandler->get_initialDupRate() );
    DefaultRates[1] = optionHandler->get_initialLossRate();


    model->prepareForComputation( SpeciesCCPDistrib , 
                                  DefaultRates, 
                                  rateMap,
                                  LowerVerboseLevel, optionHandler->get_extant_sampling() , 0.05 , 5 );
    

    /// initiation of the data for species CCP distribtuion update
    

    unsigned int SpCCPupdateNbIteration = optionHandler->get_SpCCPUpdatingNbRound();
    double splitWeight = optionHandler->get_SpCCPUpdatingWeight();

    double speciesCCPcutOff = optionHandler->get_cutOff();
    bool isRelativeCutoff = optionHandler->get_isRelativeCutoff();



    // rates expectation maximisation process
    pair<double,double> currentRates(InitialLossRate , InitialDupRate );
    long double logP = 0;

    //presuming fixed branch lengths for now

    unsigned int sampleSize = optionHandler->get_outputSample();


    unsigned int MaxNumberIteration = SpCCPupdateNbIteration; // optionHandler->get_maxIteration(); //<- should become some parameter

    //double epsilon = 0.001;

    unsigned int NbGeneDistrib = geneDistribFileNames.size();


    for(unsigned int RatesOptimRound = 0 ; RatesOptimRound < MaxNumberIteration ; RatesOptimRound++)
    {
        if(VerboseLevel>0)
            cout << "event rates optimization round " << RatesOptimRound+1 << "/" << MaxNumberIteration << endl;;

        pair<double,double> newRates(0,0); // RATE OPT
        logP = 0;

        ////// RATE OPT NEW //////
        map < int , array<double,3>  > transitionCount;// RATE OPT NEW
        double DefaultMinDup  = getMeanExtremeQuantileGammaD( DupRateMean , DupRateVar , nbRateCategories, false);
        double DefaultMaxDup  = getMeanExtremeQuantileGammaD( DupRateMean , DupRateVar , nbRateCategories, true);
        double DefaultMinLoss = getMeanExtremeQuantileGammaD( LossRateMean , LossRateVar , nbRateCategories, false);
        double DefaultMaxLoss = getMeanExtremeQuantileGammaD( LossRateMean , LossRateVar , nbRateCategories, true);

        /// enacting a lower limit for rates to avoid some underflows ( generation of impossible reconciliations )
        if(DefaultMinDup < MinMinRate)
        {
            DefaultMinDup = MinMinRate;
            if(DefaultMinDup > DefaultMaxDup)
                DefaultMaxDup = DefaultMinDup;
        }

        if(DefaultMinLoss < MinMinRate)
        {
            DefaultMinLoss = MinMinRate;
            if(DefaultMinLoss > DefaultMaxLoss)
                DefaultMaxLoss = DefaultMinLoss;
        }


        if(VerboseLevel>1)
        {
            cout << "  default dup "<< DefaultMinDup << " "<< DefaultMaxDup << endl;
            cout << "  default loss "<< DefaultMinLoss << " "<< DefaultMaxLoss << endl;
        }
        //////////////////////////

        map< int, map< int , int > > splitCountsObservedInReconciliations; // SP CCP UPDATE
        //// filling the clade id map
        vector< boost::dynamic_bitset<> > SpeciesIdToCladeId;
        unsigned int Sid = 0;
        SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
        Sid++;
        SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
    
        while( SpeciesIdToCladeId.back().count() != 0 )
        {
            Sid++;
            SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );        
        }

        /////////////////////////////////////////////////////////

        /// computing reconciliations and backtracking fro all families
        for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        {


            if( VerboseLevel > 0 )
            {
                begin = clock();
            }

            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];

            ///matrix computation
    
            model->addGeneCCPdistribution(GeneCCPDistrib, LowerVerboseLevel);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );
    
            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 


            if( VerboseLevel > 0 )
            {
                end = clock();
                elapsed_secs1 += double(end - begin) / CLOCKS_PER_SEC;
                begin = clock();
            }

            /// doing stuff
            if(VerboseLevel>0)
            {
                logP += log( model->getOverAllProbability() );
            }


            // backtracking over all gene families
        
            for(unsigned i = 0 ; i < sampleSize  ; i++)
            {
                shared_ptr<RecNode> RT = model->backtrack();
                //cout << "BT : \n"<< RT->getRecXML() <<endl;
        
                ////// RATE OPT NEW //////
                countTransitions( RT ,transitionCount , 1.0 );
                //////////////////////////

                ////// SP CCP UPDATE /////
                AddReconciledTreeSpeciesSplitsToMap(  RT , splitCountsObservedInReconciliations);
                //////////////////////////    
            }


            if( VerboseLevel > 0 )
            {
                end = clock();
                elapsed_secs2 += double(end - begin) / CLOCKS_PER_SEC;
            }


            ////resetting matrix
            if(VerboseLevel>1)
                    cout << "\r finished backtrack "<< RatesOptimRound+1 << "/" << MaxNumberIteration << " of family " << GeneFamilyId << "/" << NbGeneDistrib-1 ;
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
        }

        if(VerboseLevel>1)
            cout << endl;

        ////// RATE OPT NEW //////
        if( VerboseLevel > 0 )
        {
            begin = clock();
        }

        if( BranchWiseRates )
        {
            model->set_DuplicationRates( DefaultMinDup );
            model->set_LossRates( DefaultMinLoss );
        }

        // NB : only useful if not branch wide ... 
        double SumWeightedDupRates = 0; // BW duplication rates weighted by the number of observation they have
        double SumWeightedLossRates = 0; // BW loss rates weighted by the number of observation they have
        double sumObservations = 0;
        


        for(auto it = transitionCount.begin() ; it != transitionCount.end() ; ++it)
        {
            double nbObservations = it->second[0] + it->second[1] + it->second[2] ;
            double p10 = it->second[0] / nbObservations;
            double p11 = it->second[1] / nbObservations;
            //cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << "|" << it->second[0] <<","<< it->second[1] <<","<< it->second[2] << endl;
            double pExtinct = model->get_ProbaGeneExtinct(it->first,0);

            boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );
            double Blen = model->get_speciesCCPdistribution()->get_branchlength(sigma);
            if( model->get_speciesCCPdistribution()->get_number_of_leaves() == sigma.count() )
            { // special root procedure
                Blen  = model->get_rootBranchLength();
            }

            pair < double, double > rates = estimateNewRates( p10, p11, Blen, pExtinct );


            ///handling particular cases 
            if( rates.first == -1 )
                rates.first = DefaultMaxDup;
            else if( rates.first == -2 )
                rates.first = DefaultMinDup;

            if( rates.second == -1 )
                rates.second = DefaultMaxLoss;
            else if( rates.second == -2 )
                rates.second = DefaultMinLoss;

            if(VerboseLevel>1)
                cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << " l:"<< Blen << " p:" << pExtinct << " -> D:" << rates.first << " L:"<< rates.second<<endl;

            
            model->set_DuplicationRates( it->first , rates.first );
            model->set_LossRates( it->first , rates.second );
            
            if( !BranchWiseRates )
            {
                //if(VerboseLevel>1)
                //{
                //    cout << "  +"<<  nbObservations << "*" << rates.first << endl;
                //    cout << "  +"<<  nbObservations << "*" << rates.second << endl;
                //}

                SumWeightedDupRates  += nbObservations * rates.first ;
                SumWeightedLossRates += nbObservations * rates.second;
                sumObservations += nbObservations;
            }


        }
        //////////////////////////


        ////// RATE OPT //////////

        /// update of rates mean and variance
        vector < long double> Drates( model->get_DuplicationRates() );
        DupRateMean = getMean(Drates);
        DupRateVar =  getVar( Drates , DupRateMean);

        vector < long double> Lrates(  model->get_LossRates() );
        LossRateMean = getMean(Lrates);
        LossRateVar =  getVar( Lrates , LossRateMean);


        if(VerboseLevel>1)
        {
            for(unsigned i = 0 ; i < Drates.size();i++)
                cout << Drates[i] << " : " << Lrates[i] << endl;

        }

        //updates of bigger rates if need be
        if(! BranchWiseRates )
        {
            /*
            Here I use the rates weighted by the number of time a branch of the species tree were observed. 
            This is especially important as we use a species CCP distributions, and we want to avoid scenarii where, 
            for instance a lot of branches where nothing happens are observed once each, 
            and they influence the overall rate a lot while the branch where a loss happened in every reconciliations is only once once
            */
            newRates.first = SumWeightedDupRates / sumObservations;
            newRates.second = SumWeightedLossRates / sumObservations;

            model->set_DuplicationRates( newRates.first ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
            model->set_LossRates( newRates.second ); //  takes : loss rate and set it as the global duplication rate for all the species tree
        }
        //////////////////////////


        ////// SP CCP UPDATE /////
        UpdateCCPdistribution( SpeciesCCPDistrib,  splitCountsObservedInReconciliations , SpeciesIdToCladeId , splitWeight);

        ///applying cutoff
        if(speciesCCPcutOff > 0)
        {
            
            int nbDeleted = CutOffCCPdistribution( SpeciesCCPDistrib, speciesCCPcutOff , isRelativeCutoff , LowerVerboseLevel);

            if(nbDeleted>0)
            { // re-map the species CCP inside the model.
                if(VerboseLevel>0)
                {
                    cout << " deleted " << nbDeleted << " clades in the CCP distribution (cut-off:" << speciesCCPcutOff << ")."<<endl;
                }
                
                model->reMapSpeciesCCPdistribution();
                
            }            
        }
        //////////////////////////

        if(VerboseLevel>0)
        {
            end = clock();
            elapsed_secs2 += double(end - begin) / CLOCKS_PER_SEC;
        }

        if(VerboseLevel>0)
        {
            cout << "****************************************"<< endl;
            if(!BranchWiseRates)
            {
                cout << "new duplication rate: " << newRates.first;
                cout << "\tnew loss rate: " << newRates.second ;
            }
            else
            {
                cout << "mean Duplication rate : " << DupRateMean << " variance : "<< DupRateVar << endl;
                cout << "mean Loss rate : " << LossRateMean<< " variance : "<< LossRateVar << endl;
            }

            cout << "\tnew log(P): "<< logP;
            cout << endl;
            cout << "****************************************"<< endl;

        }


        if((optionHandler->get_writeIntermediary() ) && ( RatesOptimRound+1 != RatesOptimRound ))
        {
            //if((splitWeight+speciesCCPcutOff) >0)
            //    MakeBipCoherentWithRootSplits(SpeciesCCPDistrib);

            string fileName = optionHandler->get_outputFile() + ".round" + int2string( RatesOptimRound+1 ) + ".updated.species.ale";
            OutputCCPdistribution(fileName, SpeciesCCPDistrib);
        }


        /// if there is some updating of the ccp (splitWeight>0 or speciesCCPcutOff>0) AND this is not the last round (SpCCPupdateIter+1 != SpCCPupdateNbIteration)
        if( ( (splitWeight+speciesCCPcutOff) >0) && (RatesOptimRound+1 != RatesOptimRound))
        { // then we need to re-compute the probability of gene extinction and gene transmission as the CCP distribution changed
            model->reset();
            model->computeProbaGeneExtinct();
            model->computeProbaGeneOneChild();
        }

        //// setup the process better in my head in order to knoz if I keep this or not
        if(! BranchWiseRates)
        {
            //double Ddup =  abs(currentRates.first - newRates.first) / currentRates.first ;
            //double Dloss =  abs(currentRates.second - newRates.second) / currentRates.second ;
    
            currentRates.first = newRates.first;
            currentRates.second = newRates.second;
            
    
            //if( ( Ddup < epsilon ) && ( Dloss < epsilon ) )
            //    break;
        }

        for( auto it = splitCountsObservedInReconciliations.begin(); it != splitCountsObservedInReconciliations.end() ; ++it)
        {
            it->second.clear();
        }
        splitCountsObservedInReconciliations.clear();

        //// filling the clade id map
        SpeciesIdToCladeId.clear();


    }




    streambuf * buf;
    ofstream of;

    FILE *fp;


    string filename = optionHandler->get_outputFile();
    if(filename!="")
    {
        filename += ".updated.species.ale";
        of.open(filename.c_str() );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream OUT(buf);

    //if((splitWeight+speciesCCPcutOff) >0)
    //    MakeBipCoherentWithRootSplits(SpeciesCCPDistrib);

    OUT << SpeciesCCPDistrib->output_as_ale() << endl;

    if(filename!="")
    {        
        of.close();
    }

    if(BranchWiseRates)
    {
        filename = optionHandler->get_outputFile();
        if(filename!="")
        {
            filename += ".rates.txt";
            of.open(filename.c_str() );
            buf = of.rdbuf();
        }
        else
            buf = cout.rdbuf();
    
        ostream OUT(buf);
    
        
        OutputBranchWiseRates(  OUT , model );
    
        if(filename!="")
        {        
            of.close();
        }

    }

    if(VerboseLevel>0)
    {
        cout << "****************************************"<< endl;
        cout << "time spent computing reconciliation probabilities     : " << elapsed_secs1<< "s."<<endl;
        cout << "time spent backtracking and analyzing reconciliations : " << elapsed_secs2<< "s."<<endl;
        

        if(!BranchWiseRates)
        {
            cout << "****************************************"<< endl;
            cout << "final duplication rate: " << currentRates.first<< endl;
            cout << "final loss rate: " << currentRates.second << endl;
            cout << "final log(P): "<< logP << endl;
        }

        cout << "****************************************"<< endl;

        pair<Tree,double> res = SpeciesCCPDistrib->get_posterior_proba_tree(false);
        cout << "max species posterior probability:       " << res.second << endl;
        cout << "max species posterior probability tree : " << res.first.output_as_newick() << endl;
    
        cout << "****************************************"<< endl;
    }

    // output later
    /*
    // preparing output
    string filename = optionHandler->get_outputFile();


    streambuf * buf;
    ofstream of;

    FILE *fp;
    if(filename!="")
    {
        filename += ".trees.txt";
        of.open(filename.c_str() );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream OUT(buf);

    InitRecXMLToStream( OUT );

    FinishRecXMLToStream( OUT );

    if(filename!="")
    {
        of.close()
    }
    */

    return 0;

}
