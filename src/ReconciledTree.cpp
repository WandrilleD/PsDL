/*


Created the: 19-02-2018
by: Wandrille Duchemin

Last modified the: 23-02-2018
by: Wandrille Duchemin

*/

#include "ReconciledTree.h"


///helper functions

/*
    Takes:
        - int level : desired number of indentations
        - string indentStr = "  " : string for 1 level of indentation

    Returns:
        (string): level * indentStr
*/
string createIndent(int level, string indentStr )
{
    string s = "";

    for(unsigned i = 0 ; i < level ; i++)
        s += indentStr;
    return s;
}



/* float to string conversion */
string float2string (float number)
{
    ostringstream buff;
    buff<<number;
    return buff.str();   
}

/* int to string conversion */
string int2string (int number)
{
    ostringstream buff;
    buff<<number;
    return buff.str();   
}

/*
recursively sets ids according to post-order traversal
    
    takes:
        - int current: minimum unused id in the rest of the tree

    returns:
        (int): id of the node+1 -> new minimum unused id

*/
int RecNode::setPOIdAux( int current )
{

    for(auto it = children.begin(); it != children.end(); ++it )
        current = (*it)->setPOIdAux( current );

    id = current;
    return id+1;
}



///class function

RecNode::RecNode( int GId, int SId, int LId, int id ):GeneCladeId(GId),
                                                        SpeciesCladeId(SId),
                                                        SpeciesBranchSubdivisionId(LId),
                                                        BranchLength(0),
                                                        id(id),
                                                        parent(),
                                                        SpeciesName(""),
                                                        name(""),
                                                        Tb(false)
{

    children = vector<shared_ptr<RecNode> >();
}





//output

/*
    !! This does not give the full phyloRecXML representation of a tree; just that of a subtree (ie. you still need a recGeneTree and phylogeny object encapsulating this).
    !! recursive function

    Takes:
        - int indentLevel = 0 : current level of indentation


    Returns:
        (string): phyloRecXML representation of the subtree, rooted on the current node <clade> object
*/
string RecNode::getRecXML( int indentLevel  )
{
    string indentStr = "  ";

    string prefix = createIndent(indentLevel,  indentStr);

    string r = prefix + "<clade>\n";

    r += prefix + indentStr + "<name>" ;

    if( get_name() == "")
    {
        r += int2string(id);
    }
    else
        r += get_name();

    r += "</name>\n";// get a more intelligent way to deduce names later

    if(BranchLength != 0)
        r += prefix + indentStr + "<branch_length>" + float2string( BranchLength ) + "</branch_length>\n";

    r += prefix + indentStr + "<eventsRec>\n" ;

    if(Tb)
    {
        r += prefix + indentStr + indentStr + "<" + "transferBack" + " destinationSpecies=\"" ;
            if( SpeciesName == "")
        {
            r += int2string(SpeciesCladeId);
        }
        else
            r += SpeciesName;

        r += "\"></transferBack>\n" ; 
    }

    r += prefix + indentStr + indentStr + "<" + event ;
    r += " speciesLocation=\"" ;

    if( SpeciesName == "")
    {
        r += int2string(SpeciesCladeId);
    }
    else
        r += SpeciesName;

    r += "\"></" + event +">\n" ; 
    
    r += prefix + indentStr + "</eventsRec>\n" ; 

    for(auto it = children.begin(); it != children.end(); ++it )
        r += (*it)->getRecXML( indentLevel + 1 );

    r += prefix + "</clade>\n";

    return r;
}

/*
 recursive function
    Returns:
        ( pair< int , int > ) : count of duplications and losses
*/
pair<int,int> RecNode::countEvents()
{
    pair<int,int> count(0,0);

    if(event == "duplication")
        count.first++;
    else if(event == "loss")
        count.second++;

    for(auto it = children.begin(); it != children.end(); ++it )
    {
        pair<int,int> tmp = (*it)->countEvents();
        count.first += tmp.first;
        count.second += tmp.second;
    }

    return count;
}   

//recursively sets ids according to post-order traversal
void RecNode::setPOId()
{
    setPOIdAux(0);
}


/*

    Returns:
        vector < shared_ptr<RecNode> > : vector of the leaves 
*/
vector < shared_ptr<RecNode> > RecNode::getLeaves()
{
    vector < shared_ptr<RecNode> > res;

    if(is_leaf())
        res.push_back( getptr() );

    for(auto it = children.begin(); it != children.end(); ++it )
    {
        vector < shared_ptr<RecNode> > tmp = (*it)->getLeaves();
        res.insert( res.end() ,  tmp.begin() , tmp.end() );
    }

    return res;

}

/*

    Returns:
        vector < shared_ptr<RecNode> > : vector of the leaves 
*/
vector < shared_ptr<RecNode> > RecNode::getRealLeaves()
{
    vector < shared_ptr<RecNode> > res;

    if(is_RealLeaf())
        res.push_back( getptr() );

    for(auto it = children.begin(); it != children.end(); ++it )
    {
        vector < shared_ptr<RecNode> > tmp = (*it)->getRealLeaves();
        res.insert( res.end() ,  tmp.begin() , tmp.end() );
    }

    return res;

}