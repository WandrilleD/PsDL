
/*

This file defines helper functions 
for the rate expectation maximisation algorithm
of the reconciliation of a species and a gene CCP distributions

Created the: 02-03-2018
by: Wandrille Duchemin

Last modified the: 06-03-2018
by: Wandrille Duchemin

*/


#include "PsDLMaxUtils.h"

/*
    Takes:
        - PsDLModel model : the PsDL model, where it is presumed that the reconciliation matrix has already been computed for the current rates of duplication and loss
        - double BLEN : weighted sum of the branch length of the species CCP distribution
        - unsigned int sampleSize : size of the sample to estimate the new rates
        - unsigned int verboseLevel : 0 : quiet (default)
                                      1 : prints some information to stdout

    Returns:
        pair<double,double> : new rates of duplication and loss

*/
pair<double,double> expectationMaximisationround( PsDLModel * model , double BLEN, unsigned int sampleSize, unsigned int verboseLevel)
{
    //cout << "expectationMaximisationround " <<  BLEN << " " << sampleSize << " " << verboseLevel << endl;

    pair<double,double> newRates(0,0);

    pair<int,int> counts(0,0); // count of event

    //cout << "expectationMaximisationround1" <<endl;

    map < int , array<double,3>  > transitionCount;


    // backtracking over all gene families

    for(unsigned i = 0 ; i < sampleSize  ; i++)
    {
        shared_ptr<RecNode> RT = model->backtrack();
        //cout << "BT : \n"<< RT->getRecXML() <<endl;
        pair<int,int> tmp = RT->countEvents();
        counts.first += tmp.first;
        counts.second += tmp.second;

        countTransitions( RT ,transitionCount , 1.0 );

        cout << RT->getRecXML() <<endl;

    }


    for(auto it = transitionCount.begin() ; it != transitionCount.end() ; ++it)
    {
        double nbObservations = it->second[0] + it->second[1] + it->second[2] ;
        double p10 = it->second[0] / nbObservations;
        double p11 = it->second[1] / nbObservations;

        cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << "|" << it->second[0] <<","<< it->second[1] <<","<< it->second[2] << endl;

        double pExtinct = model->get_ProbaGeneExtinct(it->first,0);
        double Blen = model->get_speciesCCPdistribution()->get_branchlength(model->get_CCPindexFromSpeciesCladeIndex( it->first ));

        pair < double, double > rates = estimateNewRates( p10, p11, Blen, pExtinct );
        
        cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << " l:"<< Blen << " p:" << pExtinct << " -> D:" << rates.first << " L:"<< rates.second<<endl;

    }

    //cout << "expectationMaximisationround2" <<endl;

    newRates.first = counts.first / (sampleSize * BLEN ) ;
    newRates.second = counts.second / (sampleSize * BLEN ) ;



    if(verboseLevel>0)
    {
        cout << "expected duplications : " << model->get_DuplicationRates(1) * BLEN * sampleSize << " observed: " << counts.first << endl;
        cout << "expected loss : " << model->get_LossRates(1) * BLEN * sampleSize << " observed: " << counts.second << endl;
        cout << "new rates of duplications :" << newRates.first << endl;
        cout << "new rates of loss :" << newRates.second << endl;
    }

    return newRates;
}

/*
    *recursive function*

    Takes:
        - shared_ptr<RecNode> RT :  reconciled tree to analyse
        - map < int , double[]  > &transitionCount : count the transition of gene lineages, zill be updated in this function
                                                      key:   id of the species clade
                                                      value: array of three values, the count of respectively the transition from 1 lineage to 0m fro; 1 lineage to 1, from 1 lineage to many
        - double weight =1 : value used to increment to counts 

*/
void countTransitions( shared_ptr<RecNode> RT ,map < int , array<double,3>  > &transitionCount , double weight )
{

    // special root treatment
    if(RT->is_root())
    {
        int sp = RT->get_SpeciesCladeId();

        if( transitionCount.find( sp ) == transitionCount.end() )
        {
            transitionCount[ sp ] = array<double,3>();
            transitionCount[ sp ].fill(0);
        }

        if( RT->get_event() == "duplication" ) // transition from 1 to many
            transitionCount[ sp ][ 2 ] += weight;
        else // speciation or leaf (I discount the possiblity of having a loss as the root node) -> transition from 1 to 1
            transitionCount[ sp ][ 1 ] += weight; 

    }

    vector < shared_ptr<RecNode> > children = RT->get_children();


    if( RT->get_event() == "speciation" )
    { // counting transitions

        for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
        { // recursion loop . 
            
            string evt = (*ChIt)->get_event();
            int sp = (*ChIt)->get_SpeciesCladeId();

            if( transitionCount.find( sp ) == transitionCount.end() )
            {
                transitionCount[ sp ] = array<double,3>();
                transitionCount[ sp ].fill(0);
            }

            if( evt == "duplication" )
            { // from 1 to many
                transitionCount[ sp ][ 2 ] += weight;
            }   
            else if(evt == "loss")
            { // from 1 to 0
                transitionCount[ sp ][ 0 ] += weight;
            }
            else
            { // includes speciation and leaf -> from 1 to 1
                transitionCount[ sp ][ 1 ] += weight;
            }

        }
    }
    

    for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
    { // recursion loop . 
        countTransitions(*ChIt , transitionCount);
    }


    return;
}


/*
Estimates branch wise new duplication and loss rates based on observed transition frequencies, branch length and probability of a lineage going extinct after the branch

    Takes:
        - double p10 : frequency of observed transitions from 1 to 0 lineages
        - double p11 : frequency of observed transitions from 1 to 1 lineages
        - double Blen : branch length
        - double pExtinct : probability to go extinct from the BOTTOM of the branch

    Returns:
        (pair<double,double>) : DupRate, LossRate
                                -1 if +inf
                                -2 if 0
*/
pair<double,double> estimateNewRates( double p10, double p11, double Blen, double pExtinct )
{
    //cout <<  " p10:" << p10 << " p11:" << p11 << " Blen:" << Blen << " pExtinct:" << pExtinct <<endl;
    double LossRate;
    double DupRate;

    if(p11 == 1) //"border" cases
    {   
        LossRate = -2;
        DupRate = -2;
    }
    else if( p10 == 0 ) 
    {
        LossRate = -2;
        if(p11 == 0)
        {
            DupRate = -1;
        }
        else if(p11 < 1)
        {
            DupRate = - log( p11 ) / Blen; // presuming a loss rate at 0
        }
        else
        {
            DupRate = -2;
        }   

    }
    else if(p10 == 1)
    {
        LossRate = -1;
        DupRate = -2;
    }
    else if(p11 == 0)
    {   // case where p10>0 AND p11 ==0
        LossRate = -1;
        DupRate = -1;
    }
    else
    { // "normal" procedure

        if( p10 < pExtinct )
        { // small correction in that case; this leads to better approximation than defaulting to some extreme values. 
            pExtinct = p10;
            //cout << "corr" << p10 << " " << pExtinct << endl;

            
        }
        double x = (-1 + p10 + p11);
        double y = ((-1 + p10)*p10 + pExtinct*p11);
        //cout << "x " << x << " y" << y << endl;  
        if( abs(y) > 1e-15 )
        {
        
            double ratioDupOverLoss = x/y;
    
            double E =  log(pow(-1 + p10,2)/(p11 - pExtinct*p11)); // log(pow(-1 + p10,2)/(pow(1-pExtinct , 2)*p11));
            LossRate = E / ( Blen * (ratioDupOverLoss -1 ) );
            DupRate = ratioDupOverLoss * LossRate;
    
            //special case where p10 < p 
            if(LossRate <= 0)
                LossRate = -2;
    
            if(DupRate <= 0)
                DupRate = -2;
        }
        else
        {
            LossRate = -2;
            DupRate = -2;
        }

    }


    //cout << "\t\t--> " << DupRate << " " << LossRate << endl;
    //old implementation
    //double A = p10 * E;
    //double B = pow(p10,12)* E;
    //double C = pExtinct * p11 * E;
    //double D = pow( pExtinct ,2) * p11 * E;
    //double Y = ( A - B - C + D );

    //double LossRate = Y/X;
    //double DupRate = (E/Blen) + LossRate;

    return pair<double,double>(DupRate,LossRate);
}


/*
using method from 
"Maximum Likelihood Phylogenetic Estimation from DNA Sequences with Variable Rates over Sites: Approximate Methods",Yang 2003

test with:

mean = 1 , variance = 2 (alpha = 0.5, beta = 0.5)
k = 4

max == true:
    2.8944
else:
    0.0334

    Takes:
        `- double mean : mean of the gamma distribution
         - double variance : variance of the gamma distribution
         - double k = number of equal cut in the distribution, such that a quantile is 1/k:
         - bool max = true : if true,  compute the mean betweem the quantile and 1
                             if false, compute the mean betweem 0 and the quantile

    Returns:
        (double): compute the mean betweem the quantile and 1
                    in the desired gamma distribution
*/
double getMeanExtremeQuantileGammaD(double mean, double variance, double k, bool max )
{
    //cout << "getMeanExtremeQuantileGammaD " << mean << " " << variance << endl; 

    if(variance == 0)
        return mean;
    if(variance < 1e-9)
        variance = 1e-9;

    //re-parameterize
    double beta = mean/variance;
    double alpha = beta * mean;

    double q = 1 - (1/k) ;
    if(!max) // first quantile
        q = 1/k ;

    double a = (1/beta) * boost::math::gamma_p_inv( alpha, q );

    double HiValue = 1;
    double LoValue = 0;

    double tmp = boost::math::gamma_p( alpha+1 , a*beta);
    if(!max)
        HiValue = tmp;
    else
        LoValue = tmp;
    return (alpha/beta) * (HiValue - LoValue) / (1/k);

}

long double getMean(vector<long double> V)
{
    double s = 0;
    for(unsigned i = 0 ; i < V.size() ; i++)
        s += V[i];
    return  s / V.size() ;
}


long double getVar(vector<long double> V , long double mean)
{
    double s = 0;
    for(unsigned i = 0 ; i < V.size() ; i++)
        s += pow( V[i] - mean ,2);
    return  s / V.size() ;

}

