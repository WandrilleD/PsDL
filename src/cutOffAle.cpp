/*

Created the: 21-03-2018
by: Wandrille Duchemin

Last modified the: 21-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>


#include "PsDLUtils.h"
#include "speciesCCPupdateUtils.h"


#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/dynamic_bitset.hpp>

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    if(argc < 3)
    {
        cerr << " usage: ./cutOffAle ale cutOff output "<< endl;
        cerr << " will remove all splits whose CCP is < to cutoff." << endl;
        exit(1);
    }


    string f1 = argv[1];
    double cutOff = stod( argv[2] );
    string fo = argv[3];


    shared_ptr<TreesetMetadata> ale1 = make_shared<TreesetMetadata>();


    int pb = 0;

    pb = buildCCPDistributionFromFile( f1 ,  ale1, true , false ); 
    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the ale input file: " << f1 << endl ;
        exit(pb);
    }

    /*
    cout << " gamma "<< ale1->get_gamma() << endl;
    
    boost::dynamic_bitset<> root { ale1->get_number_of_leaves() , 0 };
    root.flip();

    cout << " root " << root << "  " << ale1->get_count_of_bipartition(root) << endl;
    for( auto it = ale1->tripartition_cbegin( root ) ; it != ale1->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = ale1->get_complementer(root, it->first);
        int id1 = ale1->get_bitset_id( it->first );
        int id2 = ale1->get_bitset_id( sigmaPP );

        cout << it->first << "|" << sigmaPP;
        cout << " -> " << id1 << "," << id2;
        cout << " -> " << it->second << endl;
    }

    */

    CutOffCCPdistribution( ale1, cutOff , false, 1);

    /*
    cout << "after bipartition"<<endl;

    cout << " root " << root << "  " << ale1->get_count_of_bipartition(root) << endl;
    for( auto it = ale1->tripartition_cbegin( root ) ; it != ale1->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = ale1->get_complementer(root, it->first);
        int id1 = ale1->get_bitset_id( it->first );
        int id2 = ale1->get_bitset_id( sigmaPP );

        cout << it->first << "|" << sigmaPP;
        cout << " -> " << id1 << "," << id2;
        cout << " -> " << it->second << endl;
    }

    MakeBipCoherentWithRootSplits(ale1);
    */
    streambuf * buf;
    ofstream of;

    FILE *fp;
    
    of.open(fo.c_str() );
    buf = of.rdbuf();
   

    ostream OUT(buf);
   
    OUT << ale1->output_as_ale() << endl;

    of.close();

    return 0;


}