/*

This file defines helper functions 
for the analyse of reconciled trees to infer new species CCPs

Created the: 01-03-2018
by: Wandrille Duchemin

Last modified the: 22-03-2018
by: Wandrille Duchemin

*/

#include "speciesCCPupdateUtils.h"


/*
 !!recursive!!

    Takes:
        - shared_ptr< RecNode > RT
        - float SLweight [default = 0] : how much weight to give to Speciations followed by a loss 

    Returns:
        (map< int , map < int , float > >) the map of all split of the species tree encountered in the reconciled tree, with a value corresponding to the number of times it is seen

*/
map< int , map < int , float > > getReconciledTreeSpeciesSplits( shared_ptr< RecNode > RT , float SLweight )
{

    map< int , map < int , float > > SplitCounts;

    vector < shared_ptr< RecNode > > children = RT->get_children();


    if(RT->get_event() == "speciation")
    {

        int parent = RT->get_SpeciesCladeId();
        //presuming exactly two child for the speciation
        int child = min( children[0]->get_SpeciesCladeId() , children[1]->get_SpeciesCladeId());

        float weight = 1;

        if( SLweight != 1)
        {
            if( (children[0]->get_event() =="loss") || (children[1]->get_event() =="loss") )
            {
                weight = SLweight;
            }
            //else 
            //    cout << "Not ";
            //cout << " corrected a  SL in " << parent << "(" << children[0]->get_event() << "||"<< children[1]->get_event() << ")"<< endl ; 
        }

        if(weight!=0)
        {
            SplitCounts[ parent ] = map< int,float>();
            SplitCounts[ parent ][ child ]  = 1;
        }

    }

    //recursion loop
    for( auto it = children.begin() ; it!= children.end() ; ++it)
    {
        map< int , map < int , float > > tmp = getReconciledTreeSpeciesSplits( *it , SLweight );

        for(auto it2 = tmp.begin() ; it2 != tmp.end() ; ++it2)
        {
            if( SplitCounts.find( it2->first ) == SplitCounts.end() )
                SplitCounts[ it2->first ] = map<int,float>();

            for(auto it3 = it2->second.begin() ; it3 != it2->second.end() ; ++it3)
            {
                if( SplitCounts[ it2->first ].find( it3->first ) == SplitCounts[ it2->first ].end() )
                    SplitCounts[ it2->first ][ it3->first ] = 0;

                SplitCounts[ it2->first ][ it3->first ]  += it3->second;
            }

        }
    }

    return SplitCounts;
}


/*


    Takes:
        - shared_ptr< RecNode > RT
        - map< int , map < int , float > > &splitCounts : the map of all split of the species tree encountered in the reconciled tree, with a value corresponding to the number of times it is seen
                                                            will be updated
        - float SLweight [default = 0] : how much weight to give to Speciations followed by a loss 
        - bool OneVotePerReconciledTree [default = 1 ] : if true, the counts different gene lineage in the same reconciled gene tree will be normalized so that any gene family contributes at most 1 count to each clade splits

    Returns:
        void

*/
void AddReconciledTreeSpeciesSplitsToMap( shared_ptr< RecNode > RT , map< int , map < int , float > > &splitCounts , float SLweight , bool OneVotePerReconciledTree )
{
    map< int , map< int , float > > tmp = getReconciledTreeSpeciesSplits( RT );

    if(OneVotePerReconciledTree)
    {
        //cout << "normalizing" << endl;
        // normalization of split counts for each observed clade 
        for(auto it = tmp.begin() ; it != tmp.end() ; ++it)
        {
            float S = 0;
            for(auto it2 = it->second.begin() ; it2 != it->second.end() ; ++it2)
            {
                S += it2->second;
            }

            for(auto it2 = it->second.begin() ; it2 != it->second.end() ; ++it2)
            {
                //cout << " " << it->first << " " << it2->first << " : " << it2->second;
                tmp[ it->first  ][ it2->first ] /= S ;
                //cout << " -> " << tmp[ it->first  ][ it2->first ] << endl;
            }
        }
    }

    for(auto it2 = tmp.begin() ; it2 != tmp.end() ; ++it2)
    {
        if( splitCounts.find( it2->first ) == splitCounts.end() )
            splitCounts[ it2->first ] = map<int,float>();
        for(auto it3 = it2->second.begin() ; it3 != it2->second.end() ; ++it3)
        {
            if( splitCounts[ it2->first ].find( it3->first ) == splitCounts[ it2->first ].end() )
                splitCounts[ it2->first ][ it3->first ] = 0;
            splitCounts[ it2->first ][ it3->first ]  += it3->second;
        }
    }
}


/*




    Takes:
        - shared_ptr< TreesetMetadata > CCPdistrib : ccp distribution that will be updated 
        - map< int , map < int , float > > &splitCounts : the map of all split of the species tree encountered in the reconciled tree, with a value corresponding to the number of times it is seen
        - vector< boost::dynamic_bitset<> > &SpeciesIdToCladeId : acts as a map from the ids that are in splitCountMap and the ids that are in the ccp distribution
        - double splitWeight = 1.0 : between 0 and 1 ; frqction of the new observed splits that will replace the old (current) ones
                                            0 --> no change to te ccp distribution
                                            1 --> erase the old number and replace them by the new obeservations

    Returns:
        void
*/
void UpdateCCPdistribution( shared_ptr< TreesetMetadata > CCPdistrib,  map< int , map < int , float > > &splitCounts , vector< boost::dynamic_bitset<> > &SpeciesIdToCladeId , double splitWeight )
{



    for(auto it = splitCounts.begin() ; it != splitCounts.end() ; ++it)
    {
        boost::dynamic_bitset<> parent = SpeciesIdToCladeId[ it->first ];
        
        //cout << "before update, clade "<< parent << " " << CCPdistrib->get_count_of_bipartition( parent ) << endl;
        //for( auto tripIt = CCPdistrib->tripartition_cbegin( parent ) ; tripIt != CCPdistrib->tripartition_cend( parent ) ; ++tripIt )
        //    cout << "   " <<  tripIt->first << " "<< tripIt->second<<endl;


        float countNew = 0 ;
        
        for(auto it2 = it->second.begin() ; it2 != it->second.end() ; ++it2)
        {
            countNew += it2->second;
        }

        double countOld = CCPdistrib->get_count_of_bipartition( parent );

        // we want it so that the new bipartition and tripartition count is composed at 
        //          splitWeight of countNew
        //          1-splitWeight of countOld
        //
        // lets have new total count = OldCount

        double ratioNew = splitWeight * ((double) countOld  /  countNew);

        //cout << " old:"<<countOld << " new:"<<countNew << " ratioNew:"<<ratioNew << endl;

        //updating the old counts
        for( auto tripIt = CCPdistrib->tripartition_cbegin( parent ) ; tripIt != CCPdistrib->tripartition_cend( parent ) ; ++tripIt )
        {
            double currentCount = tripIt->second;
            double desiredCount = currentCount*( 1-splitWeight );
            CCPdistrib->increment_tripartition( parent , tripIt->first, desiredCount - currentCount  ); // putting them at desired count
        }



        for(auto it2 = it->second.begin() ; it2 != it->second.end() ; ++it2)
        {
            boost::dynamic_bitset<> child = SpeciesIdToCladeId[ it2->first ];

            double Increment = ratioNew * it2->second;

            CCPdistrib->increment_tripartition( parent , child, Increment  ); // putting them at desired count
        }

        //cout << "after update, clade "<< parent<< endl;
        //for( auto tripIt = CCPdistrib->tripartition_cbegin( parent ) ; tripIt != CCPdistrib->tripartition_cend( parent ) ; ++tripIt )
        //    cout << "   " <<  tripIt->first << " "<< tripIt->second<<endl;        

    }


}


/*
    Takes:
        - shared_ptr< TreesetMetadata > CCPdistrib : pointer to the CCP distribution that will be updated
        - double cutOff : probability under which a split is considered impossible 
        - bool relativeProba : consider cutOff to apply to the ratio of the split proba over the proba of the split with max proba (rather than directly to the proba) 
        - unsigned int verboseLevel

    Returns:
        (int) : number of clades that were deleted from the ccp distribution
*/
int CutOffCCPdistribution( shared_ptr< TreesetMetadata > CCPdistrib, double cutOff , bool relativeProba , unsigned int verboseLevel)
{
    bool verbose = false;
    if(verboseLevel > 0)
        verbose = true;
    return CCPdistrib->apply_cut_off(  cutOff ,  relativeProba , verbose);
}

/*
modify bip counts to make them reflect their CCP of the root split.

    Takes:
        - shared_ptr< TreesetMetadata > CCPdistrib : pointer to the CCP distribution that will be updated
        - double defaultForAbsence = 1 : default bip count to appear if the root doesn't split in this clades.

*/
void MakeBipCoherentWithRootSplits( shared_ptr< TreesetMetadata > CCPdistrib , double defaultForAbsence )
{
    assert( defaultForAbsence > 0); //<- will result in invalid ccp distribuution otherwise

    if(CCPdistrib->get_number_of_leaves() < 3)
        return;

    boost::dynamic_bitset<> root { CCPdistrib->get_number_of_leaves() , 0 };
    root.flip();

    //cout << " root " << root << "  " << CCPdistrib->get_count_of_bipartition(root) << endl;

    for(auto it1 = CCPdistrib->bipartition_cbegin() ; it1 != CCPdistrib->bipartition_cend() ; ++it1)
    {
        double newCount = CCPdistrib->get_count_of_tripartition( root , it1->first );

        if(newCount == 0)
            newCount = defaultForAbsence ;

        if(newCount != it1->second )
        {
            double ratioNew = ((double)  newCount /  it1->second);
            
            double BL = CCPdistrib->get_branchlength( it1->first );
            CCPdistrib->increment_branchlength( it1->first , BL * (newCount - it1->second) );            

            CCPdistrib->increment_bipartition( it1->first , (newCount - it1->second) );

            //updating the old counts
            for( auto tripIt = CCPdistrib->tripartition_cbegin( it1->first ) ; tripIt != CCPdistrib->tripartition_cend( it1->first ) ; ++tripIt )
            {
                double desiredCount = ratioNew * tripIt->second;
                CCPdistrib->increment_tripartition( it1->first , tripIt->first, desiredCount - tripIt->second  ); // putting them at desired count
            }


        }
    }



}


/*
    Takes:
        - vector<int> Sids : clades to force the resolution of
        - shared_ptr< TreesetMetadata > CCPdistrib : ccp distribution that will be updated 
        - map< int , map < int , float > > &splitCounts : the map of all split of the species tree encountered in the reconciled tree, with a value corresponding to the number of times it is seen
        - vector< boost::dynamic_bitset<> > &SpeciesIdToCladeId : acts as a map from the ids that are in splitCountMap and the ids that are in the ccp distribution

    Returns:
        (vector<int>) : children of the resulved clades (for the next iteration)
*/
vector <int> forceSpeciesCladeResolution( vector<int> Sids , 
                                          shared_ptr< TreesetMetadata > CCPdistrib , 
                                          map< int , map < int , float > > splitCounts , 
                                          vector< boost::dynamic_bitset<> > &SpeciesIdToCladeId , 
                                          double threshold )
{

    vector < boost::dynamic_bitset<> > resolvedCladesChildren;

    map< boost::dynamic_bitset<> , int> CladeIdToSpeciesId;
    for(int i = 0 ; i < SpeciesIdToCladeId.size() ; i++)
        CladeIdToSpeciesId[ SpeciesIdToCladeId[i] ] = i;

    map< boost::dynamic_bitset<> , bool > CladeToResolve;
    for(auto sid : Sids)
        CladeToResolve[ SpeciesIdToCladeId[sid] ] = true ; 


    map< boost::dynamic_bitset<> , bool > conserved;

    vector< boost::dynamic_bitset<> >  BIPS = CCPdistrib->get_bipartitions();

    //akways conserve the root clade!
    boost::dynamic_bitset<> Rclade( CCPdistrib->get_number_of_leaves());
    Rclade.flip();
    conserved[Rclade] = true;

    // 1. go over all bip and choose which to cut-off
    for( auto bipIt  = BIPS.rbegin() ; bipIt != BIPS.rend() ; ++bipIt )
    {
        if( conserved.find( *bipIt ) == conserved.end() )
        { // this bipartition have not been conserved in any splits of a bigger clade --> we'll have to delete it AND not consider its splits to look for conserved bipartitions
            conserved[ *bipIt ] = false;
            continue;
        }

        vector< boost::dynamic_bitset<> > kept;

        if( CladeToResolve.find( *bipIt ) != CladeToResolve.end() )
        { // resolve this clade
            //cout << "forcing resolution of clade " << *bipIt << endl;

            map< boost::dynamic_bitset<> , double > probas ;
            double total = 0;
            if( splitCounts.find( CladeIdToSpeciesId[ *bipIt ] ) != splitCounts.end() )
            { // found in split count
                //cout << "found in split count" << endl;
                for( auto counts : splitCounts[ CladeIdToSpeciesId[ *bipIt ] ]  )
                {
                    total += (double) counts.second;
                    probas[ SpeciesIdToCladeId[ counts.first ] ] = (double) counts.second;
                }
            }
            else
            {
                //cout << "not found in split count" << endl;
                for( auto tripIt = CCPdistrib->tripartition_cbegin( *bipIt ) ; tripIt != CCPdistrib->tripartition_cend( *bipIt ) ; ++tripIt )
                {
                    probas[ tripIt->first ] = (double) tripIt->second;
                    total += (double) tripIt->second;
                }
            } 

            boost::dynamic_bitset<> MAX;
            double maxP = 0;
            bool aboveThreshold=false;

            for(auto it : probas )
            {
                double p = it.second / total;
                if( p > maxP)
                {
                    if(p > threshold)
                    {
                        aboveThreshold = true;
                        maxP = p;
                        MAX = it.first;
                    }
                }
            }

            if(aboveThreshold)
            {
                //cout << "  " << MAX << endl;
                kept.push_back( MAX );
                kept.push_back( CCPdistrib->get_complementer( *bipIt , MAX ) );
                //cout << "keeping " << MAX << " <> " << CCPdistrib->get_complementer( *bipIt , MAX ) << endl;

                for( auto tripIt = CCPdistrib->tripartition_cbegin( *bipIt  ) ; tripIt != CCPdistrib->tripartition_cend( *bipIt ) ; ++tripIt )
                {
                    if( tripIt->first != MAX )
                    {
                        //cout << " removing trip " << *bipIt << " , "<<  tripIt->first << endl;
                        CCPdistrib->remove_tripartition( *bipIt , tripIt->first );
                    }
                }
                CCPdistrib->increment_tripartition( *bipIt , MAX , CCPdistrib->get_count_of_bipartition( *bipIt ) - CCPdistrib->get_count_of_tripartition( *bipIt , MAX )  );

            }
            else
            { // nothing above the threshold -> keep all sampled ones
                for( auto tripIt = CCPdistrib->tripartition_cbegin( *bipIt  ) ; tripIt != CCPdistrib->tripartition_cend( *bipIt ) ; ++tripIt )
                {
                    if( probas.find(tripIt->first) != probas.end() )
                    {
                        kept.push_back( tripIt->first );
                        kept.push_back( CCPdistrib->get_complementer( *bipIt , tripIt->first ) );
                        //cout << "keeping " << tripIt->first << " <> " << CCPdistrib->get_complementer( *bipIt , tripIt->first ) << endl;
                    }
                    else
                    {
                        CCPdistrib->remove_tripartition( *bipIt , tripIt->first );
                    }
                }
            }

            for(auto k : kept)
            {
                if(k.count() > 2) // otherwise these are trivial cases
                {
                    resolvedCladesChildren.push_back( k );
                }
            }

        }
        else
        {
            for( auto tripIt = CCPdistrib->tripartition_cbegin( *bipIt  ) ; tripIt != CCPdistrib->tripartition_cend( *bipIt ) ; ++tripIt )
            {
                kept.push_back( tripIt->first );
                kept.push_back( CCPdistrib->get_complementer( *bipIt , tripIt->first ) );
            }
        }


        for(unsigned i = 0 ; i < kept.size(); i++)
        {
            conserved[ kept[i] ] = true;
        }
    }

    int nbDeletedClades = 0;

    // 2. go a second time over bips to delete the ones that cannot be observed anymore
    for( auto bipIt  = conserved.begin() ; bipIt != conserved.end() ; ++bipIt )
    {
        if(! bipIt->second)
        { //this clade is not part of any valid split anymore --> delete it!!
            //cout << "deleting clade "<< bipIt->first <<endl;

            nbDeletedClades++;

            CCPdistrib->remove_bipartition( bipIt->first );
        }
    }


    vector < int > toReturn;
    for(auto gamma : resolvedCladesChildren )
        toReturn.push_back( CladeIdToSpeciesId[gamma] );

    return toReturn;

}