/*

 * This class ensures the clean starting and ending of the program:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 04-05-2018
by: Wandrille Duchemin

Last modified the: 04-05-2018
by: Wandrille Duchemin


*/


#include "OptionHandler.h"

OptionHandler::OptionHandler() : GenericDesc_{"Generic options"},
                                 OutputDesc_{"Output options"},
                                 AdvancedDesc_{"Advanced options"},
                                 HiddenDesc_{"Hidden options"},
                                 RateDesc_{"Rate options"},
                                 MaxDesc_{"Max algorithm options"},
                                 SampleDesc_{"Sample algorithm options"},
                                 GridDesc_{"Grid algorithm options"},
                                 algorithm_(""),
                                 withTransfer_(false),
                                 corr_input_filename_(""), 
                                 separator_("_") , 
                                 verboseLevel_(1), 
                                 seed_(-1),
                                 outputSample_(0),
                                 outputPrefix_(""),
                                 input_rate_file_(""),
                                 branch_wise_(false),
                                 extant_sampling_(""),
                                 initialDupRate_(0.1),
                                 initialLossRate_(0.1),
                                 initialTransRate_(0.1),
                                 unrooted_sp_(false),
                                 GRID_stepNb_(10),
                                 GRID_MinDup_(-3),
                                 GRID_MaxDup_(1),
                                 GRID_MinLoss_(-3),
                                 GRID_MaxLoss_(1),
                                 GRID_MinTrans_(-3),
                                 GRID_MaxTrans_(1),
                                 GRID_NoLog_(false),
                                 MAX_writeIntermediary_(false),
                                 MAX_NoRateUpdate_(false),
                                 MAX_SpCCPUpdatingNbRound_(5),
                                 MAX_SpCCPUpdatingWeight_(0.5),
                                 MAX_cutOff_(0),
                                 MAX_relativeCutOff_(false),
                                 MAX_sampleSize_(100),
                                 MAX_force_resolve_mode_(false),
                                 MAX_SpeciationLossWeights_(0.),
                                 MAX_OneVotePerReconciledTree_(true),
                                 SAMPLE_burnin_(100),
                                 SAMPLE_thinning_(1),
                                 SAMPLE_sampleSize_(100),
                                 NM_No_improve_thr_(10e-3),
                                 NM_No_improv_break_(100),
                                 NM_Max_iter_(0)
{
    pd_.add("algorithm", 1);

    GenericDesc_.add_options()
        ("help,h", "Display this help text and usage information")
        ("with.transfer,T","authorize horizontal gene transfers (by default only duplications and losses are considered).")
        ("parameter.file,P", boost::program_options::value<string>(), "Path to file containing parameters")
        ("gene,g", boost::program_options::value<string>(&gene_input_filename_), "Path to file containing input gene tree(s) in newick format")
        ("species,s", boost::program_options::value<string>(&species_input_filename_), "Path to file containing input species tree(s) in newick format")
        ("separator", boost::program_options::value<string>(&separator_), "Separator between the species and the gene name in gene tree leaves. Used if a correspondence file is not given.\nExpects the species name to be BEFORE the separator. Default : \"_\". Can be multiple characters long.")
        ("verbose,v", boost::program_options::value< unsigned int>(&verboseLevel_), "0 : as quiet as possible\n1 : prints some basic information to stdout (default value)\n2 : prints lot of information to stdout (intended for debug/testing)")
        ;

    AdvancedDesc_.add_options()
        ("seed", boost::program_options::value<int>(&seed_), "use to fix the random seed.")
        ("corr.file", boost::program_options::value<string>(&corr_input_filename_), "Path to file containing correspondence betzeen gene leaves and species leaves")
        ("extant.sampling,E",boost::program_options::value< string >(&extant_sampling_),"Expected gene sampling rate among extant species ; must be between 0 (excluded) and 1. default: 1.")
        ("unrooted.species.tree,u","treat the species tree as unrooted. Only used if the input is trees and not .ale files")
        ;

    HiddenDesc_.add_options()
        ("algorithm",boost::program_options::value<string>(&algorithm_),"algorithm to apply. Must be one of : max , sample , grid .")
        ;

    OutputDesc_.add_options()
        ("output.sample,n", boost::program_options::value< unsigned int>(&outputSample_), "size of the reconciled tree sample that will be outputed at the end of the program. default: 0")
        ("output.file.prefix,p", boost::program_options::value< string>(&outputPrefix_), "Prefix of the files where output will be written.\nsampled reconciled trees will be written in prefix+\".trees.txt\".\nspecies CCP distribution will be written in prefix+\".updated.species.ale\".\n branch specific rates (if option --branch.wise/-B was used) +\".rates.txt\".\ndefault prints to stdout.")
        ;

    RateDesc_.add_options()
        ("init.dup,d",boost::program_options::value< double >(&initialDupRate_), "Initial duplication rate. Default: 0.1")
        ("init.loss,l",boost::program_options::value< double >(&initialLossRate_),"Initial loss rate. Default: 0.1")
        ("init.transfer,t",boost::program_options::value< double >(&initialTransRate_),"Initial transfer rate (only used when --with.transfer/-T is used). Default: 0.1")
        ("branch.wise,B", "trigger optimization of branch wise rates of duplication and loss (tree-wide parameters otherwise).")
        ("input.rate",boost::program_options::value<string>(&input_rate_file_),"file containing branch-wise duplication and loss rates. Necessitates that option --branch.wise/-B is set.")
        ;

    GridDesc_.add_options()
        ("steps", boost::program_options::value< unsigned int > (&GRID_stepNb_), "number of steps in the space of each 2 parameters (duplication and loss rates). default: 10.")
        ("min.dup", boost::program_options::value< double > (&GRID_MinDup_), "Minimum duplication rate (log10 value unless --NoLog log is used). default: -3.")
        ("max.dup", boost::program_options::value< double > (&GRID_MaxDup_), "Maximum duplication rate (log10 value unless --NoLog log is used). default:  1.")
        ("min.loss", boost::program_options::value< double > (&GRID_MinLoss_), "Mininmum loss rate (log10 value unless --NoLog log is used). default: -3.")
        ("max.loss", boost::program_options::value< double > (&GRID_MaxLoss_), "Maximum loss rate (log10 value unless --NoLog log is used). default:  1.")
        ("min.transfer", boost::program_options::value< double > (&GRID_MinTrans_), "Minimum transfer rate (log10 value unless --NoLog log is used). default: -3.")
        ("max.transfer", boost::program_options::value< double > (&GRID_MaxTrans_), "Maximum transfer rate (log10 value unless --NoLog log is used). default:  1.")
        ("NoLog", "steps are taken in directly using values rather than their log10.")
        ;

    MaxDesc_.add_options()
        ("write.intermediary,I", "writes an updated species ccp distribution file for each optimization round. Necessitates that option --output.file.prefix/-p is set.")
        ("no.rate.update", "stops estimation of rates (only the updating of the species CCP distribution occurs).")
        ("nb.update.rounds,N",boost::program_options::value<int>(&MAX_SpCCPUpdatingNbRound_),"number of species CCP and rate updating round ; minimum of 0, put option -w at 0 to avoid species CCP update. default: 5." )
        ("updating.weight,W",boost::program_options::value< double>(&MAX_SpCCPUpdatingWeight_),"weight of the splits observed in reconciliations in the update of the species CCP distribution ; must be between 0 and 1.\nuse a value of 0 to avoid species CCP update\ndefault: 0.5.")
        ("cutOff.CCP,C",boost::program_options::value< double>(&MAX_cutOff_),"threshold to remove splits from the the species CCP distribution during its updates; must be between 0 and 1.\ndefault: 0 (nothing is removed).")
        ("relative.cutOff,R","consider --cutOff.CCP/-C to apply to the ratio of the split CCP to the CCP of the best split rather than rather than directly to the CCP.")
        ("update.sample.size",boost::program_options::value<int>(&MAX_sampleSize_),"Size of the reconciliation sampled per gene family to update event rates and the species CCP distribution. default 100." )
        ("force.species.resolution.mode","forces iterative resolution of the species tree. \nOverride the -N option. We recommend this option is only used after several species CCP distribution optimization rounds have been performed.\nclades whose best split is not superior to a threshold (given through the -C parameter) wil not be solved.")
        ("SL.weight",boost::program_options::value< float >(&MAX_SpeciationLossWeights_),"Weight to give to speciation losses when sampling splits for species CCP update. default : 0., maximum : 1.")
        ("N.vote.per.fam","allows the different gene lineages of the same gene family to be counted independently when sampling splits for species CCP update.")
        ;

    SampleDesc_.add_options()
        ("sample.size", boost::program_options::value<int>(&SAMPLE_sampleSize_), "desired number of sampled rates (in an output prefix was specified, they will be written in prefix+\".sampled.rates.txt\"). default:100")
        ("burnin", boost::program_options::value< int >(&SAMPLE_burnin_), "size of the burnin phase. default:100")
        ("thin", boost::program_options::value< int >(&SAMPLE_thinning_), "thinning rate. default:1")
        ;

    NMDesc_.add_options()
        ("improve.threshold", boost::program_options::value<long double>(&NM_No_improve_thr_), "threshold above wich a likelihood improvement is taken into account for the algorithm stopping condition. default:10e-3")
        ("no.improve.break", boost::program_options::value<unsigned int>(&NM_No_improv_break_), "number of round without improvement before stopping. default:100")
        ("max.iter", boost::program_options::value<unsigned int>(&NM_Max_iter_), "maximum number of iteration before stopping. Put in 0 for not limit. default:0")
        ;
    
    desc_.add(GenericDesc_).add(AdvancedDesc_).add(HiddenDesc_).add(RateDesc_).add(OutputDesc_).add(MaxDesc_).add(SampleDesc_).add(GridDesc_).add(NMDesc_);

}




void OptionHandler::readArguments(int argc , char* argv[])
{
    // Set and parse parameters
    try
    {
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).positional(pd_).options(desc_).run(), vm_);
        if(vm_.count("parameter.file"))
        {
            //cout << "parsing file " << vm_["parameter.file"].as<string>() << endl; 
            ifstream ifs( vm_["parameter.file"].as<string>() );
            boost::program_options::store(boost::program_options::parse_config_file(ifs, desc_), vm_);
        }
        boost::program_options::notify(vm_);
    }
    catch (const boost::program_options::error &exception)
    {
        cerr << endl << exception.what() << endl;
    }

}

bool OptionHandler::verify()
{
    if (vm_.count("help"))
    {
        return false;
    }

    if(!vm_.count("gene"))
    {
        cerr << "ERROR: No input gene trees file was specified!" << endl;
        return false;
    }

    if(!vm_.count("species"))
    {
        cerr << "ERROR: No input species trees file was specified!" << endl;
        return false;
    }

    if(algorithm_ == "")
    {
        cerr << "ERROR: No algorithm was specified! (must be one of max, sample or grid)" << endl;
        return false;        
    }
    else
    {
        string authorizedAlgo[] {"max","sample","grid","nelderMead"};
        bool ok = false;
        for(auto a : authorizedAlgo)
        {
            if(a == algorithm_)
            {
                ok=true;
                break;
            }
        }
        if(!ok)
        {
            cerr << "ERROR: the specified algorithm ("<<algorithm_<<") is not valid! (must be one of max, sample, nelderMead or grid)" << endl;
            return false;
        }
    } 

    if(vm_.count("with.transfer"))
    {
        withTransfer_=true;

        if(initialTransRate_ == 0)
        {
            cerr << "WARNING: --with.transfer/-T option specified but transfer rate set to 0 (-t). Reverting back to the duplication/loss only algorithm." << endl;
            withTransfer_=false;
        }
    }

    if(vm_.count("branch.wise"))
        branch_wise_=true;

    if( get_input_rate_file() != "" )
    {
        if( !get_branch_wise() )
        {
            cerr << "ERROR: option --input.rate was given while option --branch.wise/-B is not set." << endl;
            return false;
        }
    }


    if( initialDupRate_ <= 0 ) 
    {
        cerr << "ERROR: --init.dup/-d must be > 0!" << endl;
        return false;
    }
    if( initialLossRate_ <= 0 ) 
    {
        cerr << "ERROR: --init.loss/-l must be > 0!" << endl;
        return false;
    }
    if(( initialTransRate_ <= 0 ) && (withTransfer_))
    {
        cerr << "ERROR: --init.transfer/-t must be > 0!" << endl;
        return false;
    }

    if(vm_.count("unrooted.species.tree"))
        unrooted_sp_=true;


    /// algorithm specific treatment.
    bool pb = true;

    if(algorithm_ == "grid")
    {
        pb = Grid_verify();
    }
    else if(algorithm_ == "max")
    {
        pb = Max_verify();
    }
    else if(algorithm_ == "sample")
    {
        pb = Sample_verify();
    }
    else if(algorithm_ == "nelderMead")
    {
        if(get_branch_wise())
        {
            branch_wise_=false;
            cerr << "WARNING : nelderMead and branch-wise rates is not implemented. Desactivating branch-wise rate optimization." << endl;
        }
    }

    return pb;
}


bool OptionHandler::Sample_verify()
{
    if( SAMPLE_sampleSize_ <= 0)
    {
        cerr << "ERROR: parameter ";
        cerr << "--sample.size";
        cerr << " must be >0."<<endl;
        return false;
    }

    if( SAMPLE_thinning_ <= 0)
    {
        cerr << "ERROR: parameter ";
        cerr << "--thin";
        cerr << " must be >0."<<endl;
        return false;
    }

    if( SAMPLE_burnin_ < 0)
    {
        cerr << "ERROR: parameter ";
        cerr << "--burnin";
        cerr << " must be  at least 0."<<endl;
        return false;
    }

    return true;
}


bool OptionHandler::Grid_verify()
{
    if(vm_.count("NoLog"))
    {
        GRID_NoLog_ = true;
    }

    if(GRID_NoLog_)
    {
        if(GRID_MinLoss_ < 0)
        {
            cerr << "ERROR: parameter --min.loss must be >0 when --NoLog is used."<<endl;
            return false;
        }
        if(GRID_MaxLoss_ < 0)
        {
            cerr << "ERROR: parameter --max.loss must be >0 when --NoLog is used."<<endl;
            return false;
        }

        if(GRID_MinDup_ < 0)
        {
            cerr << "ERROR: parameter --min.dup must be >0 when --NoLog is used."<<endl;
            return false;
        }
        if(GRID_MaxDup_ < 0)
        {
            cerr << "ERROR: parameter --max.dup must be >0 when --NoLog is used."<<endl;
            return false;
        }

        if(withTransfer_)
        {
            if(GRID_MinTrans_ < 0)
            {
                cerr << "ERROR: parameter --min.transfer must be >0 when --NoLog is used."<<endl;
                return false;
            }
            if(GRID_MaxTrans_ < 0)
            {
                cerr << "ERROR: parameter --max.transfer must be >0 when --NoLog is used."<<endl;
                return false;
            }            
        }

    }

    if( GRID_MaxDup_ < GRID_MinDup_ )
    {
        cerr << "Warning: --max.dup < --min.dup : switching them to avoid problems" << endl;
        double tmp = GRID_MaxDup_;
        GRID_MaxDup_ = GRID_MinDup_;
        GRID_MinDup_ = tmp;
    }

    if( GRID_MaxLoss_ < GRID_MinLoss_ )
    {
        cerr << "Warning: --max.loss < --min.loss : switching them to avoid problems" << endl;
        double tmp = GRID_MaxLoss_;
        GRID_MaxLoss_ = GRID_MinLoss_;
        GRID_MinLoss_ = tmp;
    }

    if(withTransfer_)
    {
        if( GRID_MaxTrans_ < GRID_MinTrans_ )
        {
            cerr << "Warning: --max.transfer < --min.transfer : switching them to avoid problems" << endl;
            double tmp = GRID_MaxTrans_;
            GRID_MaxTrans_ = GRID_MinTrans_;
            GRID_MinTrans_ = tmp;
        }    
    }

    if(branch_wise_)
    {
        branch_wise_ = false;
        cerr << "Warning : --branch.wise/-B option incompatible with the grid algorithm. Reverting to tree-wide parameterization." << endl;  
    }

    return true;
}

bool OptionHandler::Max_verify()
{

    if(vm_.count("relative.cutOff"))
        MAX_relativeCutOff_ = true;

    if(vm_.count("no.rate.update"))
        MAX_NoRateUpdate_ = true;

    if(vm_.count("write.intermediary"))
    {
        if( get_outputPrefix() == "" )
        {
            cerr << "ERROR: option --write.intermediary/-I was given while option --output.file.prefix/-p is not set" << endl;
            return false;
        }
        MAX_writeIntermediary_ = true;
    }

    if( ( MAX_SpCCPUpdatingWeight_<0) || ( MAX_SpCCPUpdatingWeight_>1) )
    {
        cerr << "ERROR: --updating.weight/-W must be a value between 0 and 1 !"<< endl;
        printUsage();
        cerr << desc_ << endl << endl;
        return false;
    }

    if( ( MAX_cutOff_<0) || ( MAX_cutOff_>1) )
    {
        cerr << "ERROR: --cutOff.CCP/-C must be a value between 0 and 1 !"<< endl;
        return false;
    }

    if(MAX_sampleSize_ <= 0)
    {
        cerr << "ERROR: --update.sample.size must be >0!" << endl;
        return false;
    }

    if(vm_.count("N.vote.per.fam"))
        MAX_OneVotePerReconciledTree_ = false;

    if( MAX_SpeciationLossWeights_ < 0 )
    {
        cerr << "Warning : asked for speciation loss weight (--SL.weight) < 0. Adjusting at 0." << endl;
        MAX_SpeciationLossWeights_ = 0;
    }
    else if( MAX_SpeciationLossWeights_ > 1 )
    {
        cerr << "Warning : asked for speciation loss weight (--SL.weight) > 1. Adjusting at 1." << endl;
        MAX_SpeciationLossWeights_ = 1;
    }

    if( ( MAX_SpCCPUpdatingNbRound_>0 ) && ( MAX_SpCCPUpdatingWeight_ == 0 ) && ( MAX_NoRateUpdate_ ) && ( MAX_cutOff_ == 0 ) )
    {
        cerr << "Warning: asked for updating round (--nb.update.rounds/-N > 0) while disabling rate estimation (--no.rate.update); ";
        cerr << "and species CCP distribution update (--updating.weight/-W and --cutOff.CCP/-C set to 0). ";
        cerr << "Adjusting the number of updating rounds to 0 to avoid un-necessary computations."<<endl;
        MAX_SpCCPUpdatingNbRound_ = 0;
    }

    if( MAX_SpCCPUpdatingNbRound_ < 0)
    {
        cerr << "Warning: --nb.update.rounds/-N set to less than 0. Setting it to 0, its minimal value." << endl;
        MAX_SpCCPUpdatingNbRound_ = 0;
    }

    if( vm_.count("force.species.resolution.mode") )
    {
        MAX_force_resolve_mode_ = true;
    }




    return true;
}


void OptionHandler::printUsage()
{
    cout << "asgRd 0.1" << endl;
    cout << "usage : " << "asgRd" << " [max|sample|grid|nelderMead] -g geneTree -s speciesTree " << endl;

    cout << "for more information, type : " << "asgRd" << " --help " << endl;
    cout << "for more information on a specific algorithm, type : " << "asgRd" << " [max|sample|grid|all|nelderMead] --help " << endl;


   if (vm_.count("help"))
    {
        cout << endl;

        cout << endl;

        cout << GenericDesc_ << endl;
        cout << OutputDesc_ << endl;
        cout << AdvancedDesc_ << endl;
        cout << RateDesc_ << endl;
        
        if(algorithm_ == "max")
        {
            cout << "max algorithm : " ;
            cout << "\toptimization of the model likelihood via a maximum expactation method. This algorithm can also be applied on the species tree distribution (by gradually centering it on the most sampled speciations in the reconciliations)." << endl;
            cout << "\tNote : at the moment this option is not the one yielding the best likelihoods when there is transfers ; maybe prefer the nelderMead algorithm to find the best rates." << endl;

            cout << MaxDesc_ << endl;
        }
        else if(algorithm_ == "sample")
        {
            cout << "sample algorithm : " ;
            cout << "\tsampling of duplication, loss and transfer rates according to their likelihood." << endl;

            cout << SampleDesc_ << endl;
        }
        else if(algorithm_ == "grid")
        {
            cout << "grid algorithm : " ;
            cout << "\tcomputation of the model likelihood along a grid of (tree-wise) duplication, loss (and transfer if applicable) rates." <<endl;

            cout << GridDesc_ << endl;
        }
        else if(algorithm_ == "nelderMead")
        {
            cout << "nelderMead algorithm : " ;
            cout << "\toptimization of rates via the Nelder-Mead method (aka. downhill simplex method)." << endl;
            cout << "\tNote : at the moment this option is the one yielding the best likelihoods when there is transfers" << endl;
            cout << NMDesc_ << endl;

        }
        else if(algorithm_=="all")
        {

            cout << "grid algorithm : " ;
            cout << "\tcomputation of the model likelihood along a grid of (tree-wise) duplication, loss (and transfer if applicable) rates." <<endl;
    
            cout << "max algorithm : " ;
            cout << "\toptimization of the model likelihood via a maximum expactation method. This algorithm can also be applied on the species tree distribution (by gradually centering it on the most sampled speciations in the reconciliations)." << endl;
            cout << "\tNote : at the moment this option is not the one yielding the best likelihoods when there is transfers ; maybe prefer the nelderMead algorithm to find the best rates." << endl;
    
            cout << "sample algorithm : " ;
            cout << "\tsampling of duplication, loss and transfer rates according to their likelihood." << endl;
            
            cout << "nelderMead algorithm : " ;
            cout << "\toptimization of rates via the Nelder-Mead method (aka. downhill simplex method)." << endl;
            cout << "\tNote : at the moment this option is the one yielding the best likelihoods when there is transfers" << endl;


            cout << MaxDesc_ << endl;
            cout << SampleDesc_ << endl;
            cout << GridDesc_ << endl;
            cout << NMDesc_ << endl;
        }
    }
}
