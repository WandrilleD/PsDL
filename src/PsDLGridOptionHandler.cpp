/*

 * This class ensures the clean starting and ending of the program for the grid algorithm of the PsDL model:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 07-03-2015
by: Wandrille Duchemin

Last modified the: 09-03-2018
by: Wandrille Duchemin


*/


#include "PsDLGridOptionHandler.h"

using namespace std;

PsDLGridOptionHandler::PsDLGridOptionHandler() : PsDLOptionHandler() , stepNb(10), MinDup(-3) , MaxDup(1) , MinLoss(-3) , MaxLoss(1) , isLog(true)
{ 

    desc_.add_options()
        ("steps", boost::program_options::value< unsigned int > (&stepNb), "number of steps in the space of each 2 parameters (duplication and loss rates). default: 10")
        ("min.dup", boost::program_options::value< double > (&MinDup), "MinRate (log10 value unless --NoLog log is used). default: -3.")
        ("max.dup", boost::program_options::value< double > (&MaxDup), "MaxRate (log10 value unless --NoLog log is used). default:  1.")
        ("min.loss", boost::program_options::value< double > (&MinLoss), "MinRate (log10 value unless --NoLog log is used). default: -3.")
        ("max.loss", boost::program_options::value< double > (&MaxLoss), "MaxRate (log10 value unless --NoLog log is used). default:  1.")

        ("NoLog", "steps are taken in directly using values and their log10.")
        ;

}

// Getters

unsigned int PsDLGridOptionHandler::get_stepNb() const
{ return stepNb; }

double PsDLGridOptionHandler::get_MinDup() const
{ return MinDup; }
double PsDLGridOptionHandler::get_MaxDup() const
{ return MaxDup; }
double PsDLGridOptionHandler::get_MinLoss() const
{ return MinLoss; }
double PsDLGridOptionHandler::get_MaxLoss() const
{ return MaxLoss; }

bool PsDLGridOptionHandler::get_isLog() const
{ return isLog; }

bool PsDLGridOptionHandler::verify()
{
    bool pb = PsDLOptionHandler::verify();


    if(vm_.count("NoLog"))
    {
        isLog = false;
    }

    if(isLog)
    {
        if(MinLoss < 0)
        {
            cerr << "error: parameter --min.loss must be >0 when --NoLog is used."<<endl;
            return true;
        }
        if(MaxLoss < 0)
        {
            cerr << "error: parameter --max.loss must be >0 when --NoLog is used."<<endl;
            return true;
        }

        if(MinDup < 0)
        {
            cerr << "error: parameter --min.dup must be >0 when --NoLog is used."<<endl;
            return true;
        }
        if(MaxDup < 0)
        {
            cerr << "error: parameter --max.dup must be >0 when --NoLog is used."<<endl;
            return true;
        }

    }

    if( MaxDup < MinDup )
    {
        cerr << "Warning: --max.dup < --min.dup : switching them to avoid problems" << endl;
        double tmp = MaxDup;
        MaxDup = MinDup;
        MinDup = tmp;
    }

    if( MaxLoss < MinLoss )
    {
        cerr << "Warning: --max.loss < --min.loss : switching them to avoid problems" << endl;
        double tmp = MaxLoss;
        MaxLoss = MinLoss;
        MinLoss = tmp;
    }

    return pb;
}

// Actions
void PsDLGridOptionHandler::printUsage()
{
    cout << "grid of rates on the PsDL model" << endl;
    cout << "This program is intended to do the DL reconciliation of a gene CCP distribution and a species CCP distribution" << endl;
    cout << "usage : PsDLGrid -g <gene tree distribution file>  -s <species tree distribution file>"  << endl;

}

