/*

Created the: 21-03-2018
by: Wandrille Duchemin

Last modified the: 21-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>


#include "PsDLUtils.h"
#include "speciesCCPupdateUtils.h"


#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/dynamic_bitset.hpp>

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    if(argc < 3)
    {
        cerr << " usage: ./flattenAle ale output "<< endl;
        cerr << "Flattens the CCP distribution, which means a modification of the pre-existing clade splits probabilities "<< endl;
        cerr << "so that the probability of a given split is no ore dpendent on the observations but rather on what would be expected in a distribution where each tree topology is seen exactly once (the equivalent of a uniform distribution, but on trees and splits)." << endl;
        exit(1);
    }


    string f1 = argv[1];
    string fo = argv[2];


    shared_ptr<TreesetMetadata> ale1 = make_shared<TreesetMetadata>();


    int pb = 0;

    pb = buildCCPDistributionFromFile( f1 ,  ale1, true , false ); 
    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the ale input file: " << f1 << endl ;
        exit(pb);
    }


    ale1->flattenDistribution();


    //MakeBipCoherentWithRootSplits(ale1);
    
    streambuf * buf;
    ofstream of;

    FILE *fp;
    
    of.open(fo.c_str() );
    buf = of.rdbuf();
   

    ostream OUT(buf);
   
    OUT << ale1->output_as_ale() << endl;

    of.close();

    return 0;


}