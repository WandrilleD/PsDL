/*

Created the: 12-03-2018
by: Wandrille Duchemin

Last modified the: 12-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>


#include "PsDLUtils.h"



#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    if(argc < 3)
    {
        cerr << " usage: ./addAle ale1 ale2 output "<< endl;
        exit(1);
    }


    string f1 = argv[1];
    string f2 = argv[2];
    string fo = argv[3];


    shared_ptr<TreesetMetadata> ale1 = make_shared<TreesetMetadata>();
    shared_ptr<TreesetMetadata> ale2 = make_shared<TreesetMetadata>();

    int pb = 0;

    pb = buildCCPDistributionFromFile( f1 ,  ale1, true , false ); 
    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the ale input file: " << f1 << endl ;
        exit(pb);
    }


    pb = buildCCPDistributionFromFile( f2 ,  ale2, true , false ); 
    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the ale input file: " << f2 << endl ;
        exit(pb);
    }


    ale1->addition( *ale2 );




    streambuf * buf;
    ofstream of;

    FILE *fp;
    
    of.open(fo.c_str() );
    buf = of.rdbuf();
   

    ostream OUT(buf);
   
    OUT << ale1->output_as_ale() << endl;

    of.close();

    return 0;


}