/*

Created the: 21-03-2018
by: Wandrille Duchemin

Last modified the: 21-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>


#include "PsDLUtils.h"
#include "speciesCCPupdateUtils.h"


#include <CCPCPP/treeset_metadata.h>
#include <CCPCPP/treeset.h>
#include <CCPCPP/tree.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/dynamic_bitset.hpp>

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    if(argc < 3)
    {
        cerr << " usage: ./getTreeCCPproba ale treeFile output rooted (0 or 1. default is 0)"<< endl;
        cerr << " exemple : ./getTreeCCPproba myale.ale mytree.txt output.f 1" << endl;
        cerr << "!! only works on rooted trees." << endl;
        exit(1);
    }


    string f1 = argv[1];
    string f2 = argv[2];
    string fo = argv[3];

    bool rooted = false;
    if(argc > 4)
    {
        if(strcmp( argv[4] , "1") == 0)
        {
            cout << "rooted mode: on." << endl;
            rooted = true;
        }
    }

    shared_ptr<TreesetMetadata> ale1 = make_shared<TreesetMetadata>();


    int pb = 0;

    pb = buildCCPDistributionFromFile( f1 ,  ale1, true , false ); 
    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the ale input file: " << f1 << endl ;
        exit(pb);
    }
    cout << "ale input read" << endl;

    // Object which will hold all the trees
    shared_ptr<Treeset> treeset = make_shared<Treeset>(!rooted);
    // Read in newick trees
    string newick_tree_string;
    ifstream input_file( f2 );
    if( input_file.is_open() )
    {
        // Get one tree from newick formatted input file, semicolon will be discarded
        while( getline(input_file, newick_tree_string, ';') )
        {
            treeset->add_tree_from_newick(newick_tree_string);
        }
        input_file.close();
    }
    else
    {
        cerr << "Unable to open newick tree input file: " << f2 << endl;
        return 1;
    }

    treeset->build_leaf_dictionary();

    cout << "tree set input read" << endl;


    streambuf * buf;
    ofstream of;

    FILE *fp;
    
    of.open(fo.c_str() );
    buf = of.rdbuf();
   

    ostream OUT(buf);

    vector<std::shared_ptr<Tree>> trees = treeset->get_trees();
    for(auto tree : trees)
    {
        double P = ale1->get_ccp_score_of(tree, !rooted);
        if(P<0)
            cerr << "Warning : at least 1 of the leaf in the given tree does not exists in the given CCP distribution or vice-versa (noted as -1 in output)" << endl;
        OUT << P << endl;
    }   

    of.close();

    return 0;


}