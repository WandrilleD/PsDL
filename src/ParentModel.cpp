/*

This file defines a parent class for the PsDL and PsDTL model

Created the: 02-05-2018
by: Wandrille Duchemin

Last modified the: 07-05-2018
by: Wandrille Duchemin

*/

#include "ParentModel.h"


void ParentModel::fillSpeciesIndexAndProbaCCP()
{
    for( unsigned i = 0 ; i < SpeciesIndexV.size() ; i++ )
    {
        SpeciesIndexV[i].clear() ; 
        SpeciesProbaV[i].clear() ;

    }
    SpeciesIndexV.clear() ;
    SpeciesProbaV.clear() ;

    SpeciesIndexV.push_back( vector< pair< CIndexType, CIndexType > >() );
    SpeciesProbaV.push_back( vector< FLOAT_TYPE >() );


    for(unsigned int i = 1 ; i < speciesCladeIndexToCCPid.size() ; i++)
    {
        vector< pair< CIndexType, CIndexType > > IV ; 
        vector< FLOAT_TYPE > PV ;

        fillVccp( i , IV , PV , true );
        SpeciesIndexV.push_back( IV ) ;
        SpeciesProbaV.push_back( PV ) ;
        //cout << "OPT " << i <<endl;
        //for(auto p : SpeciesIndexV.back() )
        //    cout << " " << p.first << "|" << p.second;
        //cout << endl;
        //for(auto p : SpeciesProbaV.back() )
        //    cout << " " << p;
        //cout << endl;
    }

}

/*

    Takes:
         - CIndexType Cid : id of a clade
         - vector< pair< CIndexType, CIndexType > >& IndexV : vector to put pair of subclades ids
         - vector< FLOAT_TYPE >& ProbaV : vector to put conditional proba of each split
         - bool isSpecies) : whether the given clade is a species clade

    Returns:
       (void) 

*/
void ParentModel::fillVccp(CIndexType Cid , vector< pair< CIndexType, CIndexType > >& IndexV , vector< FLOAT_TYPE >& ProbaV , bool isSpecies)
{
    //switch CCP distribution for the right one
    shared_ptr<TreesetMetadata> CCPdistrib = speciesCCPdistribution;
    vector < CCPcladeIdType > * CladeIndexToCCPid = &speciesCladeIndexToCCPid;
    map < CCPcladeIdType , CIndexType > * CCPidToCladeIndex = &speciesCCPidToCladeIndex;

    if(!isSpecies)
    {
        CCPdistrib = geneCCPdistribution;
        CladeIndexToCCPid = &geneCladeIndexToCCPid;
        CCPidToCladeIndex = &geneCCPidToCladeIndex;
        if ( geneCCPdistribution->is_leaf( geneCladeIndexToCCPid[Cid] ) )//NB: shouldn't be needed as we only call this function when we are in a speciation. but serves as a sanity check nonetheless
            return; /// leave the vector empty
    }
    else
    {
        if ( speciesCCPdistribution->is_leaf( speciesCladeIndexToCCPid[Cid] ) )//NB: shouldn't be needed as we only call this function when we are in a speciation. but serves as a sanity check nonetheless
            return;
    }

    CCPcladeIdType sigma = CladeIndexToCCPid->at( Cid );

    unsigned int NbSplits = CCPdistrib->get_number_of_tripartitions( sigma); 

    IndexV.reserve( NbSplits );
    ProbaV.reserve( NbSplits );

    FLOAT_TYPE bip = CCPdistrib->get_count_of_bipartition( sigma );

    auto END = CCPdistrib->tripartition_cend( sigma );
    for(auto sigmaPpointer = CCPdistrib->tripartition_cbegin( sigma ); sigmaPpointer != END ; ++sigmaPpointer )
    {
        FLOAT_TYPE trip = sigmaPpointer->second;

        CCPcladeIdType sigmaPP =  CCPdistrib->get_complementer(sigma, sigmaPpointer->first); //getSisterClade(sigma, *sigmaPpointer); //

        IndexV.push_back( pair< CIndexType, CIndexType >( CCPidToCladeIndex->at( sigmaPpointer->first ), CCPidToCladeIndex->at( sigmaPP ) ) );
        ProbaV.push_back( trip/bip ) ;

    }
}



/*
    * add gene names to leaves
    * add species names when relevant

    Takes:
        - shared_ptr<RecNode> RT : root node of the tree to annotate

*/
void ParentModel::annotateReconciledTreeAux(shared_ptr<RecNode> RT)
{

    //cout << "\tannotate node " << RT->get_id() << endl;

    int Gid = RT->get_GeneCladeId();
    CCPcladeIdType gamma;
    if(Gid != -1) // -1 -> loss node -> non valid clade
        gamma = geneCladeIndexToCCPid[ Gid ];

    CCPcladeIdType sigma = speciesCladeIndexToCCPid[ RT->get_SpeciesCladeId() ];

    //cout << "\t\t" << "leaf:"<< RT->is_RealLeaf() << endl;
    //cout << "\t\t" << gamma << endl;
    //cout << "\t\t" << sigma << endl;

    if( RT->is_RealLeaf() )//geneCCPdistribution->is_leaf(gamma) )
        RT->set_name( geneCCPdistribution->get_name_of_leaf( gamma ) );

    string spName = to_string( speciesCCPdistribution->get_bitset_id(sigma) );

    if( speciesCCPdistribution->is_leaf(sigma) )
        spName = speciesCCPdistribution->get_name_of_leaf( sigma ) ;

    RT->set_SpeciesName( spName );


    vector< shared_ptr<RecNode> > children = RT->get_children();
    for(auto it = children.begin() ; it != children.end(); ++it )
        annotateReconciledTreeAux( *it );

}

// makes some precomputations With ids
void ParentModel::addGeneCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel)
{
    geneCCPdistribution = CCPdistribution;

    CCPcladeIdType emptyClade(geneCCPdistribution->get_number_of_leaves(),0ul);

    geneCladeIndexToCCPid.push_back(emptyClade);
    geneCCPidToCladeIndex[emptyClade] = 0;
    GeneIndexV.push_back( vector< pair< CIndexType, CIndexType > >() );
    GeneProbaV.push_back( vector< FLOAT_TYPE >() );

    vector< CCPcladeIdType > clades = geneCCPdistribution->get_bipartitions();

    auto END = geneCCPdistribution->bipartition_cend();
    for(auto gamma_it = geneCCPdistribution->bipartition_cbegin() ; gamma_it != END ; ++gamma_it)
    {
        geneCCPidToCladeIndex[gamma_it->first] = geneCladeIndexToCCPid.size(); // from the bitset that are used inside the CCP object to the integer type id         
        if(verboseLevel>1)
        {
            cout << "gene : " << gamma_it->first << " " << geneCCPidToCladeIndex[gamma_it->first] ;
            if(geneCCPdistribution->is_leaf( geneCCPidToCladeIndex[gamma_it->first] ) )
                cout << " " << geneCCPdistribution->get_name_of_leaf( geneCCPidToCladeIndex[gamma_it->first] );
            cout <<endl;
        }
        geneCladeIndexToCCPid.push_back(gamma_it->first); // from integer type id to the bitset that are used inside the CCP object
        

        vector< pair< CIndexType, CIndexType > > IV ; 
        vector< FLOAT_TYPE > PV ;

        fillVccp( geneCladeIndexToCCPid.size()-1 , IV , PV , false );
        //cout << "OPT " << gamma_it->first ;
        //for(auto p : IV )
        //    cout << " " << p.first << "|" << p.second;
        //cout << endl;
        //for(auto p : PV )
        //    cout << " " << p;
        //cout << endl;
        GeneIndexV.push_back( IV ) ;
        GeneProbaV.push_back( PV ) ;

    }

}


/*
    Takes:
        - map <string,string> CorrMap : key: gene leaf name
                                        value: species leaf name

    Returns:
        void
*/
void ParentModel::addgeneSpeciesCorrespondence(map <string,string> &CorrMap)
{
    assert( speciesCladeIndexToCCPid.size() > 0); //checks that the species ccp has been set
    assert( geneCladeIndexToCCPid.size() > 0) ;//checks that the gene ccp has been set


    geneSpeciesCorrespondence.reserve( geneCCPdistribution->get_number_of_leaves() + 1 ); //+1 because first element is dummy element;

    //initialize
    for( unsigned i = 0 ; i < (geneCCPdistribution->get_number_of_leaves() + 1) ; i++ )
        geneSpeciesCorrespondence.push_back(0);

    //fill
    for(auto it = CorrMap.begin() ; it != CorrMap.end() ; ++it)
    {
        CCPcladeIdType LeafId =  geneCCPdistribution->get_bitset_of_leaf( it->first ) ;

        if(LeafId.count() == 0)
            continue; //case where the leaf does not correspond to any leaf in the gene distribution

        geneSpeciesCorrespondence[ geneCCPidToCladeIndex[ LeafId ] ] = speciesCCPidToCladeIndex[ speciesCCPdistribution->get_bitset_of_leaf( it->second ) ] ;
    }

    //for( unsigned i = 0 ; i < (geneCCPdistribution->get_number_of_leaves() + 1) ; i++ )
    //    cout << "corr:" << i << "-" << geneSpeciesCorrespondence[i] << endl;

    //we could do some check now, but it should have been done earlier

}

/*
    Re-initialize 
        * the reconciliation matrix
        * the correspondance map
        * the gene family distribution

*/
void ParentModel::prepareForNewGeneFamily()
{
    geneCCPdistribution.reset() ; // ccp distribution of the gene
    //geneCCPdistribution = nullptr;
    
    geneCladeIndexToCCPid.clear(); // from integer type id to the bitset that are used inside the CCP object
    geneCCPidToCladeIndex.clear(); // from the bitset that are used inside the CCP object to the integer type id 


    geneSpeciesCorrespondence.clear() ; //correspondence between the gene and species leaves

    for( unsigned i = 0 ; i < GeneIndexV.size() ; i++ )
    {
        GeneIndexV[i].clear() ; 
        GeneProbaV[i].clear() ;

    }
    GeneIndexV.clear() ;
    GeneProbaV.clear() ;
     

    reset_recMat();

}

/*
    * sets the id of nodes
    * add gene names to leaves
    * add species names when relevant

    Takes:
        - shared_ptr<RecNode> RT : root node of the tree to annotate

*/
void ParentModel::annotateReconciledTree(shared_ptr<RecNode> RT)
{

    RT->setPOId(); // setting post-order ids
    //cout << "ID assigned "<<endl;

    annotateReconciledTreeAux(RT);

}



ParentModel::ParentModel(): geneCCPdistribution() , // ccp distribution of the gene
                        speciesCCPdistribution() , //ccp distribution of the species
                        speciesCladeIndexToCCPid(), // from integer type id to the bitset that are used inside the CCP object
                        speciesCCPidToCladeIndex(), // from the bitset that are used inside the CCP object to the integer type id 
                        geneCladeIndexToCCPid(), // from integer type id to the bitset that are used inside the CCP object
                        geneCCPidToCladeIndex(), // from the bitset that are used inside the CCP object to the integer type id 
                        geneSpeciesCorrespondence(), //correspondence between the gene and species leaves
                        SpeciesIndexV(),
                        SpeciesProbaV(),
                        GeneIndexV(),
                        GeneProbaV(),
                        ListTransferIncompatibilities(),
                        DuplicationRates(), //  index : species clade id ; value : associated, branch-wise, duplication rate
                        LossRates(), //         index : species clade id ; value : associated, branch-wise, loss rate
                        recMatrixComputed(false), // true if the matrix has been computed and is ready to be backtracked
                        rootBranchLength(0.1),
                        probaExtantGeneSampled()
{
    //cout << "ParentModel::ParentModel" << endl;
}


vector< vector<long double> > ParentModel::get_Rates()
{
    vector< vector<long double> > Rates;
    Rates.reserve(DuplicationRates.size());
    for(unsigned i = 0 ; i < DuplicationRates.size() ; i++)
    {
        Rates.push_back( vector<long double>(2,0) );
        Rates[i][0] = DuplicationRates[i];
        Rates[i][1] = LossRates[i];
    }
    return Rates;
}

void ParentModel::set_Rates( vector< vector<long double> > rateList )
{
    for( unsigned int i = 0 ; i < rateList.size() ; i++ )
    {
        DuplicationRates[i] = rateList[i][0] ;
        LossRates[i] = rateList[i][1] ;
    }
}

void ParentModel::set_Rates( vector<long double> rateList )
{
    set_DuplicationRates( rateList[0] );
    set_LossRates( rateList[1] );
}

void ParentModel::set_Rates( CIndexType Sid,  vector<long double> rateList )
{
    DuplicationRates[Sid] = rateList[0] ;
    LossRates[Sid] = rateList[1] ;

}