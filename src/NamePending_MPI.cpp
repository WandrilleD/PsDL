/*

This file contains a small executable to play with reconciliation of gene qnd species CCPs

Created the: 24-05-2018
by: Wandrille Duchemin

Last modified the: 28-02-2019
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>
#include "Utils.h"
#include "PsDLMaxUtils.h"
#include "PsDLUtils.h"

#include "OptionHandler.h"

#include "PsDLModel.h"
#include "PsDTLModel.h"
#include "speciesCCPupdateUtils.h"

#include "MPI_PsDLUtils.h"

#include "UtilsMPI.h"

#include "NelderMeadOptimizer.h"


#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>

using namespace std;


#include <boost/mpi.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

namespace mpi = boost::mpi;



/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    mpi::environment env;
    mpi::communicator world;


    // reading and checking options

    shared_ptr<OptionHandler> optionHandler = make_shared<OptionHandler>();
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        if(world.rank() == 0)
        {
            optionHandler->printUsage();
            return -99;
        }
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;

    unsigned int VerboseLevelMaster = VerboseLevel;
    unsigned int LowerVerboseLevelMaster = LowerVerboseLevel; 
    if( world.rank() != 0 )
    {
        VerboseLevelMaster = 0;
        LowerVerboseLevelMaster = 0;
    }


    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( VerboseLevel > 1 )
        cout << "process " << world.rank() << " using random seed : "<< seed << endl;

    srand(seed);
 
    string algorithm = optionHandler->get_algorithm() ;

    bool WithTransfer = optionHandler->get_withTransfer();

    if( VerboseLevel > 0 )
    {
        cout << "Algorithm : "<< algorithm;    
        cout << " - ";
        if(!WithTransfer)
            cout << "no ";
        cout << "transfers." << endl;
    }
    


    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names

    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    vector <string> geneDistribFileNames;
    vector <int> geneDistribSizes;
    
    int fileFormat = 3;

    time_t timerStart;
    double seconds;

    if (world.rank() == 0)
    {


        // **************
        // 1 reading data
        // **************
    
        
        // 1A reading tree data
        // **************
    
        // 1A.1 gene file names and files
        // **************
    
    
    
    
        fileFormat = guessFileFormat( optionHandler->get_gene_input_filename() );
        if(fileFormat == -1)
        {
            cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
            exit(1);
        }
        else if( fileFormat == 3 )
        {
            timerStart = time(NULL);
            //1. read file names

            pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
            if(pb != 0)
            {
                exit(pb);
            }

            //2. assign them to child processes to assess sizes 
            vector< vector< string > > dividedFileNames;
            vector< vector< int > > dividedFileSizes;

            dividedFileNames = divideFilesNames( geneDistribFileNames , world.size() );
 
            geneDistribFileNames.clear();
    
            for(unsigned i = 1 ; i < world.size() ; i++ )
            {
                //cout << "trying to send" <<endl;
                world.send(i, 0, dividedFileNames[i] );
            }
        
            seconds = difftime( time(NULL) , timerStart );        
            if(VerboseLevel>1)
                cout << "distribution file names sent to all "<< world.size() << " processes in " << seconds << "s."<<endl;
            timerStart = time(NULL);

            //3. assess file size

            //pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
            pb = assessAleFilesSizes( dividedFileNames[0] , geneDistribFileNames , geneDistribSizes );
            if(VerboseLevel>1)
                cout << "process "<< world.rank() << " assessed distrib size in " << difftime( time(NULL) , timerStart ) << "s."<<endl;

            //4. receive file size from child processes
            dividedFileSizes.push_back( vector< int > () ) ; //dummy first
            for(unsigned i = 1 ; i < world.size() ; i++ )
            {
                dividedFileSizes.push_back( vector< int > () ) ;
                if( dividedFileNames[i].size() >0 )
                {
                    world.recv(i, i , dividedFileSizes[i] );
                    if( dividedFileNames[i].size() != dividedFileSizes[i].size() )
                    {
                        cerr << "ERROR: process "<< i << " did not return the nexpected number of file sizes." << endl;
                        exit(1);
                    }
                }
            }

            //5. merge file sizes and names in a sorted fashion
            for(unsigned i = 1 ; i < world.size() ; i++ )
            {
                mergeIntoSortedBySizes( geneDistribFileNames , geneDistribSizes , dividedFileNames[i] , dividedFileSizes[i] );
            }



            if(VerboseLevel>1)
            {
                seconds += difftime( time(NULL) , timerStart );        
                cout << "ale file size assessed in " << seconds << "s."<<endl;
                if(VerboseLevel>3)
                {
                    for(unsigned i = 0 ; i < geneDistribSizes.size() ; i++ )
                    {
                        cout << geneDistribFileNames[i] << " " << geneDistribSizes[i] << endl ;
                    }
                }
            }


            if(pb == -1)
            {
                geneDistribFileNames.clear();
                pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
            }

        }
        else
        {
            geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );
            geneDistribSizes.push_back( 1 );
            for(unsigned i = 1 ; i < world.size() ; i++ )
                world.send(i, 0, vector<string>() );
        }
    
        if(VerboseLevel>1)
        {
            cout << "Detected gene file format : " ;
            if(fileFormat == 1)
                cout << "newick" << endl;
            if(fileFormat == 2)
                cout << "ale" << endl;
            if(fileFormat == 3)
                cout << "multiple files" << endl;           
        }
    
    
        // 1A.2 species tree data
        // **************
    
        timerStart = time(NULL);

        pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, 3 , optionHandler->get_unrooted_sp() ); // detects format
    
        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
            if(pb == 2)
                cerr << "Species input file: \"" << optionHandler->get_species_input_filename() << "\" was not in a valid format (newick or ale)." << endl ;
            exit(pb);
        }

        seconds = difftime( time(NULL) , timerStart );
        
        if(VerboseLevel>1)
            cout << "read species distribution in " << seconds << "s."<<endl;
        
    
    }
    else
    {
        //cout << "trying to receive" << endl;
        world.recv(0,0, geneDistribFileNames );
        if( geneDistribFileNames.size() > 0 ) // if empty -> no need to send back
        {
            if(VerboseLevel>2)
                cout << "process " << world.rank() << " received " << geneDistribFileNames.size() << "file names" << endl;
            
            timerStart = time(NULL);

            //3. assess file size

            //pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
            pb = assessAleFilesSizesNoSort( geneDistribFileNames , geneDistribSizes );
            if(VerboseLevel>1)
                cout << "process "<< world.rank() << " assessed distrib size in " << difftime( time(NULL) , timerStart ) << "s."<<endl;

            //4. send the file sizes
            world.send(0, world.rank() , geneDistribSizes );
            geneDistribFileNames.clear();
            geneDistribSizes.clear();
        }
    }



    ////
    //// the server reads all gene file names 
    //// and divides them between the different processes (including itelf)

    if( world.rank() == 0 )
    {

        timerStart = time(NULL);

        vector< vector< string > > dividedFileNames;

        //timerStart = time(NULL);

        if( geneDistribSizes.size() == geneDistribFileNames.size() )
            dividedFileNames = divideFilesNamesSorted( geneDistribFileNames , geneDistribSizes , world.size() );
        else
            dividedFileNames = divideFilesNames( geneDistribFileNames , world.size() );

        //seconds += difftime( time(NULL) , timerStart );
        
        seconds = difftime( time(NULL) , timerStart );
        
        if(VerboseLevel>1)
            cout << "divided gene distribution between "<< world.size() << " processes in " << seconds << "s."<<endl;
        timerStart = time(NULL);        
    

        geneDistribFileNames.clear();
        geneDistribFileNames = dividedFileNames[0];

        for(unsigned i = 1 ; i < world.size() ; i++ )
        {
            //cout << "trying to send" <<endl;
            world.send(i, 0, dividedFileNames[i] );
        }

        seconds = difftime( time(NULL) , timerStart );        
        if(VerboseLevel>1)
            cout << "gene distribution sent to all "<< world.size() << " processes in " << seconds << "s."<<endl;


    }
    else
    {
        //cout << "trying to receive" << endl;
        world.recv(0,0, geneDistribFileNames );

        if(VerboseLevel>2)
            cout << "process " << world.rank() << " received " << geneDistribFileNames.size() << "file names" << endl;
    }


    ////
    //// processes read and build their respective gene families

    timerStart = time(NULL);

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        if(VerboseLevel>2)
            cout << "process "<< world.rank() <<" is reading : "<< geneDistribFileNames[ GeneFamilyId ]<<endl;

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , fileFormat , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            if(pb == 2)
                cerr << "Gene input file: \"" << geneDistribFileNames[ GeneFamilyId ] << "\" was not in a valid format (newick or ale)." << endl ;

            exit(pb);
        }


    }

    seconds = difftime( time(NULL) , timerStart );        
    if(VerboseLevel>1)
        cout << "process "<< world.rank() << " has read gene distribution(s) in " << seconds << "s."<<endl;
    

    // 1B reading gene species correspondence data
    // **************

    broadcastSpeciesCCPdistrib(world, world.rank() , SpeciesCCPDistrib );
    if(VerboseLevelMaster>1)
        cout << "species distribution successfully broadcasted."<<endl;


    timerStart = time(NULL);

    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }

    seconds = difftime( time(NULL) , timerStart );        
    if(VerboseLevel>1)
        cout << "process "<< world.rank() << " has associated gene and species leaves in " << seconds << "s."<<endl;
    timerStart = time(NULL);


    if(VerboseLevel > 3)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }

    // equivalence between gene and species bitsets
    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > x;
        GeneCladeSpeciesMasks.push_back( x );//getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }


    seconds = difftime( time(NULL) , timerStart );        
    if(VerboseLevel>1)
        cout << "process "<< world.rank() << " has built gene to species mask in " << seconds << "s."<<endl;
    timerStart = time(NULL);



    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > GeneCladeSpeciesMasks = getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) ;
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }
    

    seconds = difftime( time(NULL) , timerStart );        
    if(VerboseLevel>1)
        cout << "process "<< world.rank() << " has checked gene and species leaves correspondances in " << seconds << "s."<<endl;
    timerStart = time(NULL);


    map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > rateMap;
    map< string ,  FLOAT_TYPE > ExtantSamplingMap;

    if( world.rank() == 0 )
    {

        // 1D eventually reading branch-wise input rates
        // ************************
        if(optionHandler->get_input_rate_file() != "")
        {
    
            int pb = readRatesFile(optionHandler->get_input_rate_file() , SpeciesCCPDistrib , rateMap);
            if(pb != 0)
            {
                if(pb == 1)
                    cerr << "Unable to open the rate input file: " << optionHandler->get_input_rate_file() << endl ;
                exit(pb);
    
            }
    
        }


        // 1E eventually reading extant genes sampling rates
        // ************************
        if( optionHandler->get_extant_sampling() != "" ) 
        {
            int pb = readExtantSamplingFile(optionHandler->get_extant_sampling() , SpeciesCCPDistrib, ExtantSamplingMap );
            if(pb != 0)
            {
                if(pb == 1)
                    cerr << "Unable to open the extant sampling input file: " << optionHandler->get_extant_sampling() << endl ;
                exit(pb);
    
            }
            
        }




    }





    // 1E preparing output
    // ************************

    string Prefix = optionHandler->get_outputPrefix();
    unsigned int nbOutputSamples = optionHandler->get_outputSample();


    streambuf * buf;
    ofstream of;

    FILE *fp;




    streambuf * bufSAMPLE;
    ofstream ofSAMPLE;

    FILE *fpSAMPLE;
    ostream *OUTSAMPLE;

    if(world.rank() == 0 )
    {
        if((Prefix!="") && (algorithm == "sample"))
        {
            ofSAMPLE.open( Prefix + ".sampled.rates.txt" );
            bufSAMPLE = ofSAMPLE.rdbuf();
        }
        else
            bufSAMPLE = cout.rdbuf();
    
        OUTSAMPLE = new ostream(bufSAMPLE);
    }



    streambuf * bufGRID;
    ofstream ofGRID;

    FILE *fpGRID;
    ostream *OUTGRID;

    if(world.rank() == 0 )
    {
        if((Prefix!="") && (algorithm == "grid"))
        {
            ofGRID.open( Prefix + ".sampled.rates.txt" );
            bufGRID = ofGRID.rdbuf();
        }
        else
            bufGRID = cout.rdbuf();
    
        OUTGRID = new ostream(bufGRID);
    }






    /// initiation of the data for species CCP distribtuion update


    // **************
    // 2A instanciating some variables
    // **************



    //generic parameters
    bool STOP = false;
    int roundNumber = 1;

    int roundNumberToDo = 0; 

    int NbGeneDistrib = geneDistribFileNames.size();

    bool BranchWiseRates = optionHandler->get_branch_wise();


    vector<FLOAT_TYPE> DefaultRates( 2 , optionHandler->get_initialDupRate() );
    DefaultRates[1] = optionHandler->get_initialLossRate();
    if(WithTransfer)
        DefaultRates.push_back( optionHandler->get_initialTransferRate() );


    // MAX algorithm specific parameters 
    bool MAX_writeIntermediary = false;
    bool MAX_NoRateUpdate = false;
    double MAX_SpCCPUpdatingWeight = 0;
    double MAX_cutOff = 0;
    bool MAX_relativeCutOff = false;
    int MAX_sampleSize = 0;
    bool MAX_reMapSpeciesCCP = false;

    bool MAX_force_resolve_mode_ = false;
    vector< int > MAX_speciesCladeToForceResolution; 


    map < int , vector< long double>  > RateUpdateCount;
    vector < long double > RateDefaultMinimums;
    vector < long double > RateDefaultMaximums;
    vector < long double > RateMeans;
    vector < long double > RateVars;
    vector < long double > CurrentRate = DefaultRates;

    double nbRateCategories = 10;
    double MinMinRate = 0.000000001; // minimum possible rate. to avoid underflows because of very low duplication of loss numbers 
    
    map< int, map< int , float > > splitCountsObservedInReconciliations; // SP CCP UPDATE
    float SpeciationLossWeights = optionHandler->get_MAX_SpeciationLossWeights();
    bool OneVotePerReconciledTree = optionHandler->get_MAX_OneVotePerReconciledTree();



    vector< boost::dynamic_bitset<> > SpeciesIdToCladeId;

    if(algorithm == "max")
    {
        MAX_writeIntermediary = optionHandler->get_MAX_writeIntermediary();
        MAX_NoRateUpdate = optionHandler->get_MAX_NoRateUpdate();
        
        roundNumberToDo = optionHandler->get_MAX_SpCCPUpdatingNbRound();
        
        MAX_SpCCPUpdatingWeight = optionHandler->get_MAX_SpCCPUpdatingWeight();
        MAX_cutOff = optionHandler->get_MAX_cutOff();
        MAX_relativeCutOff = optionHandler->get_MAX_relativeCutOff();
        MAX_sampleSize = optionHandler->get_MAX_sampleSize();


        MAX_force_resolve_mode_ = optionHandler->get_MAX_force_resolve_mode();
        if( MAX_force_resolve_mode_ )
        {
            roundNumberToDo = -1 ; // we change the stopping condition.
            
            MAX_SpCCPUpdatingWeight = 0;
        }


        /// filling some containers used to frame the new rates computations.
        if(world.rank() == 0)
        {
            RateMeans.reserve(2 + WithTransfer);
            RateVars.reserve(2 + WithTransfer);
    
            RateMeans.push_back( DefaultRates[0] );
            RateMeans.push_back( DefaultRates[1] );
            if(WithTransfer)
                RateMeans.push_back( DefaultRates[2] );
            RateVars.push_back( DefaultRates[0] );
            RateVars.push_back( DefaultRates[1] );
            if(WithTransfer)
                RateVars.push_back( DefaultRates[2] );
            
            if(optionHandler->get_input_rate_file() != "")
            { // use real mean and var instead.
    
    
                vector< vector<long double> > ZipRates( 2+WithTransfer , vector<long double>(  rateMap.size() ) );
    
                unsigned i = 0;
    
                for(auto it : rateMap)
                {
                    for(unsigned j = 0 ; j < it.second.size() ; j++)
                    {
                        ZipRates[j][i] = it.second[j];
                    }
                    i++;
                }
                
                for(unsigned i = 0 ; i < ZipRates.size() ; i++)
                {
                    RateMeans[i] = getMean( ZipRates[i] );
                    RateVars[i] = getVar( ZipRates[i] , RateMeans[i] );
                    if(RateVars[i] == 0) // a variance to 0 would cause problems. -> use the mean, as when we use a single rate as input
                        RateVars[i] = RateMeans[i]; 
                }
    
            }
    
            RateDefaultMinimums.reserve(2+WithTransfer);
            RateDefaultMaximums.reserve(2+WithTransfer);
            RateDefaultMinimums.push_back(0);RateDefaultMinimums.push_back(0);
            RateDefaultMaximums.push_back(0);RateDefaultMaximums.push_back(0);
            if(WithTransfer)
            {
                RateDefaultMaximums.push_back(0); RateDefaultMinimums.push_back(0);
            }
        }
    }


    // GRID algorithm specific parameters 
    vector < vector< long double > > GRID_rateLists;

    if( algorithm == "grid" )
    {

        vector < double > minimums ;
        vector < double > maximums ;

        minimums.push_back( optionHandler->get_GRID_MinDup() );
        maximums.push_back( optionHandler->get_GRID_MaxDup() );
        minimums.push_back( optionHandler->get_GRID_MinLoss() );
        maximums.push_back( optionHandler->get_GRID_MaxLoss() );

        if(WithTransfer)
        {
            minimums.push_back( optionHandler->get_GRID_MinTrans() );
            maximums.push_back( optionHandler->get_GRID_MaxTrans() );
        }

        GRID_rateLists = GRID_generate_rateList( optionHandler->get_GRID_stepNb(), 
                                                 minimums , 
                                                 maximums , 
                                                 optionHandler->get_GRID_NoLog() );

        roundNumberToDo = GRID_rateLists.size();


        //setting starting rates.
        for(unsigned i = 0 ; i < GRID_rateLists[ roundNumber-1 ].size() ; i++ )
            DefaultRates[i] = GRID_rateLists[ roundNumber-1 ][i];
        
        for(auto it = rateMap.begin() ; it!=rateMap.end();++it)
            it->second.clear();
        rateMap.clear(); // clearing in case of an impromptu input rate file. 
    }

    // SAMPLE algorithm specific parameter
    int SAMPLE_sampleSize = optionHandler->get_SAMPLE_sampleSize();
    int SAMPLE_burnin = optionHandler->get_SAMPLE_burnin();
    int SAMPLE_thinning = optionHandler->get_SAMPLE_thinning();
    double CurrentLogP = 0;
    double MaxRate = 1000; 

    double accepted = 0; 

    double SD = 1;

    if(algorithm == "sample")
    {
        if(world.rank() == 0)
        {
            //setting starting rates.
            for(unsigned i = 0 ; i < DefaultRates.size() ; i++ )
            {
                DefaultRates[i] = SAMPLE_drawNewRate( DefaultRates[i], SD*2, MinMinRate,MaxRate);
                CurrentRate[i] = DefaultRates[i];
            }
            
            for(auto it = rateMap.begin() ; it!=rateMap.end();++it)
                it->second.clear();
            rateMap.clear(); // clearing in case of an impromptu input rate file.
        }
        broadcast(world , DefaultRates , 0 ); // sharing the default rates
        roundNumberToDo = SAMPLE_burnin + SAMPLE_thinning*SAMPLE_sampleSize;
    }



    // NELDERMEAD algorithm specific parameters
    NelderMeadOptimizer * NMoptimizer;

    if(algorithm == "nelderMead")
    {
        if( world.rank() == 0 )
        {
        
            unsigned int parameterization = 0;
            long double Step = 3; // this is the difference between the different points during the optimization, in log units. ; Here, a value of 3 means a div/mult. by a factor ~20 (exp(3))
            long double No_improve_thr = optionHandler->get_NM_No_improve_thr();
            unsigned int No_improv_break = optionHandler->get_NM_No_improv_break();
            unsigned int Max_iter = optionHandler->get_NM_Max_iter();
            double Alpha =1;
            double Gamma = 2;
            double Rho = 0.5;
            double Sigma =0.5;

            NMoptimizer = new NelderMeadOptimizer(
                                    DefaultRates,
                                    Step,
                                    No_improve_thr,
                                    No_improv_break,
                                    Max_iter,
                                    Alpha,
                                    Gamma,
                                    Rho,
                                    Sigma
                                    );
            DefaultRates = NMoptimizer->ReceiveLikelihoodAndGetNextParameterToTest( 0, VerboseLevel );




            //cout << "NM init done !!" << endl;
    
        }
        roundNumberToDo = -1;
        
        broadcast(world , DefaultRates , 0 ); // sharing the default rates



        for(auto it = rateMap.begin() ; it!=rateMap.end();++it)
            it->second.clear();
        rateMap.clear(); // clearing in case of an impromptu input rate file.



    }

    // **************
    // 2B instanciating the model
    // **************

    // instanciation that does not rely of the gene trees
    ParentModel * model;

    if( WithTransfer )
    {
        model = new PsDTLModel();
    }
    else
        model = new PsDLModel();

    /// sharing rates with all instances
    if(BranchWiseRates)
    {

        broadcastRates( world, world.rank() , rateMap );
        if(VerboseLevelMaster > 1)
            cout << "rates successfully broadcasted." << endl;
    }


    if( optionHandler->get_extant_sampling() != "" )
    {
        broadcastExtantSamplingRates( world, world.rank() , ExtantSamplingMap );
        if(VerboseLevelMaster > 1)
            cout << "extant gene sampling rates successfully broadcasted." << endl;        
    }

    // default rates have already been filled for all instances.

    model->prepareForComputation( SpeciesCCPDistrib , 
                                  DefaultRates, 
                                  rateMap,
                                  ExtantSamplingMap,
                                  LowerVerboseLevelMaster );

    if( ( MAX_force_resolve_mode_ ) && world.rank() == 0 )
    {
        MAX_speciesCladeToForceResolution.push_back( model->get_CCPindexFromSpeciesCladeIndex( SpeciesCCPDistrib->get_gamma() ) );
    }


    seconds = difftime( time(NULL) , timerStart );        
    if(VerboseLevel>1)
        cout << "process "<< world.rank() << " model initialized in " << seconds << "s."<<endl;
    
    if(VerboseLevel > 2)
        cout << "process " << world.rank() << " model initialized and ready to start." << endl;


    // **************
    // 3 main loop
    // **************

    //cout << "number of rounds to do"  << roundNumberToDo << endl;

    //main loop
    if(roundNumberToDo!=-1) // -1 means infinite until some other stop condition
    {
        STOP = (roundNumberToDo<roundNumber);
    }


    while(!STOP)
    {
        timerStart = time(NULL);
        if(algorithm == "max")
        {
            MAX_reMapSpeciesCCP = false;
            if(world.rank() == 0)
            {
                if(VerboseLevel>0)
                    cout << "Round " << roundNumber << endl; 
                RateDefaultMinimums[0] = getMeanExtremeQuantileGammaD( RateMeans[0] , RateVars[0] , nbRateCategories, false);
                RateDefaultMaximums[0] = getMeanExtremeQuantileGammaD( RateMeans[0] , RateVars[0] , nbRateCategories, true);
                RateDefaultMinimums[1] = getMeanExtremeQuantileGammaD( RateMeans[1] , RateVars[1] , nbRateCategories, false);
                RateDefaultMaximums[1] = getMeanExtremeQuantileGammaD( RateMeans[1] , RateVars[1] , nbRateCategories, true);
                if(WithTransfer)
                {
                    RateDefaultMinimums[2] = getMeanExtremeQuantileGammaD( RateMeans[2] , RateVars[2] , nbRateCategories, false);
                    RateDefaultMaximums[2] = getMeanExtremeQuantileGammaD( RateMeans[2] , RateVars[2] , nbRateCategories, true);
                }
    
    
                for(unsigned int i = 0 ; i < RateDefaultMinimums.size() ; i++)
                {
                    if( RateDefaultMinimums[i] < MinMinRate )
                    {
                        RateDefaultMinimums[i] = MinMinRate;
                        if(RateDefaultMinimums[i] > RateDefaultMaximums[i])
                            RateDefaultMaximums[i] = RateDefaultMinimums[i];
                    }
                }    
        
                if(VerboseLevel>1)
                {
                    cout << "  default dup      "<< RateDefaultMinimums[0] << " " << RateDefaultMaximums[0] << endl;
                    cout << "  default loss     "<< RateDefaultMinimums[1] << " " << RateDefaultMaximums[1] << endl;
                    if(WithTransfer)
                        cout << "  default transfer "<< RateDefaultMinimums[2] << " " << RateDefaultMaximums[2] << endl;
                }
                //////////////////////////
        
                //// filling the clade id map
                SpeciesIdToCladeId.clear();
    
                unsigned int Sid = 0;
                SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
                Sid++;
                SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
            
                while( SpeciesIdToCladeId.back().count() != 0 )
                {
                    Sid++;
                    SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );        
                }

            }

            for(auto it : splitCountsObservedInReconciliations)
                it.second.clear();
            splitCountsObservedInReconciliations.clear();


            for(auto it : RateUpdateCount)
                it.second.clear();
            RateUpdateCount.clear();
        }

        long double logP = 0;


        /// computing reconciliations and backtracking  all families
        for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        {
            if(VerboseLevel>1)
            {
                cout << " process " << world.rank();
                cout << " starting computations of family "<< geneDistribFileNames[GeneFamilyId] << endl;
            }


            shared_ptr<TreesetMetadata> GeneCCPDistrib = GeneCCPDistribV[ GeneFamilyId ];
    
            model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );

            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 
            
            logP += log( model->getOverAllProbability() );
            
            if(algorithm == "max")
            {
                for(unsigned i = 0 ; i < MAX_sampleSize; i++ )
                {
                    shared_ptr< RecNode > RT= model->backtrack();

                    ////// RATE OPT //////
                    if(WithTransfer)
                    {
                        MAX_countTransitionsAndEvents( RT , RateUpdateCount , 1.0 );
                        //MAX_countEvents( RT , RateUpdateCount , 1.0 );
                    }
                    else
                        MAX_countTransitions( RT , RateUpdateCount , 1.0 );
                    //////////////////////////
    
                    ////// SP CCP UPDATE /////
                    AddReconciledTreeSpeciesSplitsToMap(  RT , splitCountsObservedInReconciliations , SpeciationLossWeights , OneVotePerReconciledTree );
                    //////////////////////////    

                }

            }

            if(VerboseLevel>1)
            {
                cout << " process " << world.rank();
                cout << " Finished computations "<< roundNumber << "/" << roundNumberToDo << " of family "<< geneDistribFileNames[GeneFamilyId] << "(" << GeneFamilyId << "/" << NbGeneDistrib-1 << ")";
                cout << " (overall log likelihood : " << log( model->getOverAllProbability() ) << ")"<< endl;
            }
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
        }

        //cout << "process " << world.rank() << " P : " <<  logP << endl;  

        if( VerboseLevel > 1 )
            cout << "process " << world.rank() << " finished likelihood computations in " << difftime( time(NULL) , timerStart ) << "s."<<endl;
        seconds = difftime( time(NULL) , timerStart );
        timerStart = time(NULL);

        ////////////////// Gathering data ////////////////////////////
        if(algorithm == "max")
        {
            gatherRateUpdateCount( world, world.rank() , RateUpdateCount ); // transition count is updated with the data of all processes

            if( MAX_SpCCPUpdatingWeight > 0 )
                gatherOservedSplit( world, world.rank() , splitCountsObservedInReconciliations );

            if( VerboseLevel > 1 )
                cout << "process " << world.rank() << " waited for data gathering for " << difftime( time(NULL) , timerStart ) << "s."<<endl;
            seconds += difftime( time(NULL) , timerStart );
            timerStart = time(NULL);

        }

        if (world.rank() == 0) 
        {
            long double totallogP = 0;

            vector< long double > logPs ;
            gather(world, logP, logPs,  0);

            for(unsigned i = 0 ; i < logPs.size() ; i++ )
                totallogP += logPs[i];
            logP = totallogP;
            //cout << "The totallogP value is " << totallogP << std::endl;    
        }
        else
            gather(world, logP, 0);
        //////////////////////////////////////////////


        //stop conditions
        roundNumber++;

        //if(world.rank() == 0)
        //{
            if(roundNumberToDo!=-1) // -1 means infinite until some other stop condition
            {
                STOP = (roundNumberToDo<roundNumber);
            }
        //}
        //cout << world.rank() << " stop:" << STOP << endl;





        //preparation of next round
        if( algorithm == "grid" )
        {
            if( world.rank() == 0 )
            {
                for(unsigned int j = 0 ; j < GRID_rateLists[ roundNumber-2 ].size() ; j++ )
                    *(OUTGRID) << GRID_rateLists[ roundNumber-2 ][j] << " ";
                *(OUTGRID) <<  logP << endl;
            }

            if(!STOP)
                model->set_Rates( GRID_rateLists[ roundNumber-1 ] );  // this can be done globally.
        }
        else if( ( algorithm == "sample" ) && (world.rank() == 0) )
        {
            bool accept = SAMPLE_acceptProposition( CurrentLogP , logP );
            if( roundNumber-1 == 1 )
                accept = true; // always accepting on the first round.

            if(VerboseLevel>2)
                cout << "round number " << roundNumber-1  << "likelihood ratio : " <<   exp( logP - CurrentLogP ) << " accepted : " << accept  << " acceptance ratio " <<  (accepted+accept) / (roundNumber-1)    << endl;

            if(accept)
            {
                accepted += 1;
                CurrentLogP = logP ;

                for(unsigned i = 0 ; i < CurrentRate.size() ; i++ )
                    DefaultRates[i] = CurrentRate[i];
            }

            if(VerboseLevel>3)
                cout << "sampled rates : ";
            // drawing new solution
            for(unsigned i = 0 ; i < CurrentRate.size() ; i++ )
            {
                CurrentRate[i] = SAMPLE_drawNewRate( DefaultRates[i], SD, MinMinRate,MaxRate);
                if(VerboseLevel>3)
                    cout <<  CurrentRate[i] << " ";
            }
            if(VerboseLevel>3)
                cout <<  endl;


            model->set_Rates( CurrentRate );

            // output acceptedRates -> in DefaultRates
            if(VerboseLevel>0)
            {
                if( ( roundNumber-1 == SAMPLE_burnin ) )
                    cout << "burnin finished." << endl;
                else if( roundNumber-1 < SAMPLE_burnin )
                    cout << "burnin : " << roundNumber-1 << "/" << SAMPLE_burnin << "\r";
                else
                    cout << "sampling : "<< roundNumber-SAMPLE_burnin << "/" << SAMPLE_thinning*SAMPLE_sampleSize << "\r";
            }
            
            if( roundNumber-1 > SAMPLE_burnin )
            {
                //cout << "sample " << roundNumber-SAMPLE_burnin << "(" << (roundNumber-SAMPLE_burnin)%SAMPLE_thinning <<")" << endl;

                if( (roundNumber-SAMPLE_burnin)%SAMPLE_thinning == 0 )
                {
                    //output rates.
                    for(unsigned i = 0 ; i < DefaultRates.size() ; i++ )
                    {
                        *(OUTSAMPLE) << DefaultRates[i] << " ";
                    }
                    *(OUTSAMPLE) << endl;
                }
            }
            else if( (roundNumber)%30 == 0 )
            { // every 30 round, test the acceptance ratio.

                double acceptanceRatio  = (accepted) / 30;
                //cout << acceptanceRatio << endl;
                double Target = 0.234; // according to https://projecteuclid.org/download/pdf_1/euclid.aoap/1034625254

                SD *= min( 2.0 , max( 0.5 , acceptanceRatio/Target) );

                accepted = 0;
            }
        }
        else if( ( algorithm == "nelderMead" ) && (world.rank() == 0) )
        {

            if(VerboseLevel>0)
                cout << " --> total log P of tested parameters " << logP << endl; 


            vector < long double > newParams = NMoptimizer->ReceiveLikelihoodAndGetNextParameterToTest( logP, VerboseLevel );

            if( newParams.size() == 0)
            {
                STOP = true; 
                newParams = NMoptimizer->getBestParameters();

                //if(VerboseLevel>0)
                //{
                    cout << "Nelder-Mead optimization found ML rates after "<< NMoptimizer->get_iters() <<" iterations:" << endl;
                    cout << "d : "  << newParams[0] << endl;
                    cout << "l : "  << newParams[1] << endl;
                    if(WithTransfer)
                        cout << "t : "  << newParams[2] << endl;
                    cout << "logP : " << NMoptimizer->get_BestParametersLkh() << endl;
                //}
            }
            else
            {
                if(VerboseLevel>0)
                {
                    cout << "Nelder-Mead optimization testing parameters:" << endl;
                    cout << "d : "  << newParams[0] << endl;
                    cout << "l : "  << newParams[1] << endl;
                    if(WithTransfer)
                        cout << "t : "  << newParams[2] << endl;
                }
            }
            model->set_Rates( newParams );
        }
        else if( (algorithm == "max") && (world.rank() == 0 ) )
        {
            if( BranchWiseRates )
                model->set_Rates( RateDefaultMinimums );

            // NB : container only useful if not branch wide ... 
            vector < long double > EventSums(2 + 1*WithTransfer,0);

            double sumObservations = MAX_estimateNewRates( model , 
                                                           RateUpdateCount ,
                                                           EventSums , 
                                                           WithTransfer,
                                                           RateDefaultMinimums, 
                                                           RateDefaultMaximums);

            ////// RATE OPT //////////
    
            vector< vector<long double> > Rates = model->get_Rates();

            if(VerboseLevel>2)
            {
                cout << "inferred rates : " <<  "d : l : t" << endl;
                for(unsigned i = 0 ; i < Rates.size();i++)
                {
                    for(unsigned j = 0 ; j < Rates[i].size() ; j++)
                        cout << Rates[i][j] << " : " ;
                    cout << endl;
                }
            }
    
            vector < long double >newRates;
            //updates of bigger rates if need be
            if(! BranchWiseRates )
            {
                /*
                Here I use the rates weighted by the number of time a branch of the species tree were observed. 
                This is especially important as we use a species CCP distributions, and we want to avoid scenarii where, 
                for instance a lot of branches where nothing happens are observed once each, 
                and they influence the overall rate a lot while the branch where a loss happened in every reconciliations is only once once
                */

                if(WithTransfer)
                {
                    for(unsigned int i = 0 ; i < 3 ; i++ )
                        newRates.push_back( EventSums[i] / sumObservations );
                }
                else
                {
                    for(unsigned int i = 0 ; i < 2 ; i++ )
                        newRates.push_back( EventSums[i] / sumObservations );
                }

                for(unsigned int i = 0 ; i < 2+WithTransfer; i++ )
                {
                    if( newRates[i] == -1 )
                        newRates[i] = RateDefaultMaximums[i];
                    else if( newRates[i] == -2 )
                        newRates[i] = RateDefaultMinimums[i];
                }

                model->set_Rates( newRates );
            }
            //////////////////////////
            ////// SP CCP UPDATE /////
            if(MAX_SpCCPUpdatingWeight>0)
                UpdateCCPdistribution( SpeciesCCPDistrib,  splitCountsObservedInReconciliations , SpeciesIdToCladeId , MAX_SpCCPUpdatingWeight);
            ///applying cutoff
            if( (MAX_cutOff > 0)&&( ! MAX_force_resolve_mode_ ) )
            {            
                int nbDeleted = CutOffCCPdistribution( SpeciesCCPDistrib, MAX_cutOff , MAX_relativeCutOff , LowerVerboseLevel);
                if(nbDeleted>0)
                { // re-map the species CCP inside the model.
                    if(VerboseLevel>0)
                        cout << " deleted " << nbDeleted << " clades in the CCP distribution (cut-off:" << MAX_cutOff << ")."<<endl;
                    MAX_reMapSpeciesCCP = true;
                    //model->reMapSpeciesCCPdistribution();
                }            
            }

            if( MAX_force_resolve_mode_ )
            {
                MAX_speciesCladeToForceResolution = forceSpeciesCladeResolution( MAX_speciesCladeToForceResolution, 
                                                                                 SpeciesCCPDistrib ,
                                                                                 splitCountsObservedInReconciliations , 
                                                                                 SpeciesIdToCladeId , 
                                                                                 MAX_cutOff );
                MAX_reMapSpeciesCCP = true;

                if(MAX_speciesCladeToForceResolution.size() == 0)
                    STOP = true;
            }

            //////////////////////////

            if(VerboseLevel>0)
            {
                cout << "****************************************"<< endl;

                    if(!BranchWiseRates)
                    {
                        cout << "Duplication rate: " << CurrentRate[0];
                        cout << "\tLoss rate: " << CurrentRate[1] ;
                        if(WithTransfer)
                            cout << "\tTransfer rate: " << CurrentRate[2] ;
                    }
                    else
                    {
                        cout << "mean Duplication rate : " << RateMeans[0] << " variance : "<< RateVars[0] << endl;
                        cout << "mean Loss rate : " << RateMeans[1] << " variance : "<< RateVars[1] << endl;
                        if(WithTransfer)
                            cout << "mean Transfer rate : " << RateMeans[2] << " variance : "<< RateVars[2] << endl;
                    }
    

                    cout << "\tlog(P) "<< logP;
                    cout << "\ttime: " << seconds + difftime( time(NULL) , timerStart ) <<"s";
                cout << endl;
            }

            /// update of rates mean and variance
            vector< vector<long double> > ZipRates( 2+WithTransfer , vector<long double>(  Rates.size() ) );

            for(unsigned i = 0 ; i < Rates.size() ; i++)
            {
                for(unsigned j = 0 ; j < Rates[i].size() ; j++)
                {
                    ZipRates[j][i] = Rates[i][j];
                }
            }
            
            for(unsigned i = 0 ; i < ZipRates.size() ; i++)
            {
                RateMeans[i] = getMean( ZipRates[i] );
                RateVars[i] = getVar( ZipRates[i] , RateMeans[i] );
            }
        
            if(VerboseLevel>0)
            {
                if(!BranchWiseRates)
                {
                    cout << "new Duplication rate: " << newRates[0];
                    cout << "\tnew Loss rate: " << newRates[1] ;
                    if(WithTransfer)
                        cout << "\tnew Transfer rate: " << newRates[2] ;
                    cout << endl;
                }
                else
                {
                    cout << "mean Duplication rate : " << RateMeans[0] << " variance : "<< RateVars[0] << endl;
                    cout << "mean Loss rate : " << RateMeans[1] << " variance : "<< RateVars[1] << endl;
                    if(WithTransfer)
                        cout << "mean Transfer rate : " << RateMeans[2] << " variance : "<< RateVars[2] << endl;
                }
                cout << "****************************************"<< endl;
            }
    
            CurrentRate = newRates;


            if((MAX_writeIntermediary ) && ( !STOP ))
            {
                string fileName = Prefix + ".round" + int2string( roundNumber ) + ".updated.species.ale";
                OutputCCPdistribution(fileName, SpeciesCCPDistrib);


                if(BranchWiseRates)
                {
                    if(Prefix!="")
                    {
                        of.open(Prefix + ".round" + int2string( roundNumber ) + ".rates.txt" );
                        buf = of.rdbuf();
                    }
                    else
                        buf = cout.rdbuf();
                
                    ostream OUT(buf);
                
                    
                    OutputBranchWiseRates(  OUT , model );
                
                    if(Prefix!="")
                    {        
                        of.close();
                    }
            
                }

            }

        }


        /// now, preparing next execution and broadcasting new rates and species ccp distribution when necessary.

        if( ( algorithm == "max" ) || ( algorithm == "sample" ) || ( algorithm == "nelderMead" ) )
        {
            if( algorithm == "max" )
            {
                broadcast( world , MAX_reMapSpeciesCCP , 0 );
                if( ( ( MAX_SpCCPUpdatingWeight + MAX_cutOff ) > 0 ) || (MAX_reMapSpeciesCCP) )
                {
                    broadcastSpeciesCCPdistrib( world , world.rank() , SpeciesCCPDistrib );
                } 
                if( MAX_reMapSpeciesCCP )
                    model->reMapSpeciesCCPdistribution();
    
            }
            /// sharing rates with all instances
            broadcastRates( world, world.rank() , model , BranchWiseRates );
        }

        broadcast(world , STOP , 0 ); // sharing the stop condition


        if((!STOP)||(nbOutputSamples>0))
            model->resetForNewParameters( LowerVerboseLevel );

    }







    if(nbOutputSamples>0)
    {

        ostream *OUTTREE;
        //if(world.rank() == 0 )
        //{
            if((Prefix!="") && (nbOutputSamples>0))
            {
                of.open( Prefix + "." + int2string( world.rank() ) + ".trees.xml" );
                buf = of.rdbuf();
            }
            else
                buf = cout.rdbuf();
        
            OUTTREE = new ostream(buf);
        
            if(nbOutputSamples>0)
                InitRecXMLToStream( *OUTTREE );    
        //}


        if(VerboseLevel>0)
            cout << "process "<< world.rank() << " : reconciliation output round." << endl; 

        long double logP = 0;


        /// computing reconciliations and backtracking fro all families
        for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        {
    
            shared_ptr<TreesetMetadata> GeneCCPDistrib = GeneCCPDistribV[ GeneFamilyId ];
    
            model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );
    
            
            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 
        
    
            for(unsigned i = 0 ; i < nbOutputSamples; i++ )
            {
                shared_ptr< RecNode > RT= model->backtrack();
                model->annotateReconciledTree( RT );
    
                WriteRecTreeToStream( *OUTTREE , RT , "gene family " + geneDistribFileNames[GeneFamilyId]  + " , sample " + int2string(i+1) );
    
            }

            if(VerboseLevel>1)
            {
                cout << " Finished "<< nbOutputSamples <<" backtracks of family "<< geneDistribFileNames[GeneFamilyId] << " " << GeneFamilyId << "/" << NbGeneDistrib-1 ;
                cout << "(overall log likelihood : " << log( model->getOverAllProbability() ) << ")"<< endl;
            }
        
    
    		logP += log( model->getOverAllProbability() );
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
        }

        FinishRecXMLToStream( *OUTTREE );


        if (world.rank() == 0) 
        {
            long double totallogP = 0;

            vector< long double > logPs ;
            gather(world, logP, logPs,  0);

            for(unsigned i = 0 ; i < logPs.size() ; i++ )
                totallogP += logPs[i];
            logP = totallogP;
            //
        }
        else
            gather(world, logP, 0);

        if((VerboseLevelMaster> 0))
        	cout << "Total log P : " << logP << endl;    



        if(Prefix!="")
        {
            of.close();
        }
        delete OUTTREE;
    }



    if(world.rank() == 0)
    {

	    if(Prefix!="")
	    {
	        of.open(Prefix + ".updated.species.ale");
	        buf = of.rdbuf();
	    }
	    else
	        buf = cout.rdbuf();
	
	    ostream spOUT(buf);
	
	
	    spOUT << SpeciesCCPDistrib->output_as_ale() << endl;
	
	    if(Prefix!="")
	    {        
	        of.close();
	    }






        if(BranchWiseRates)
        {
            if(Prefix!="")
            {
                of.open(Prefix + ".rates.txt" );
                buf = of.rdbuf();
            }
            else
                buf = cout.rdbuf();
        
            ostream OUT(buf);
        
            
            OutputBranchWiseRates(  OUT , model );
        
            if(Prefix!="")
            {        
                of.close();
            }
    
        }
    }




    if( world.rank() == 0 )
    {
        delete OUTSAMPLE;
        delete OUTGRID;
    }

    if( VerboseLevel > 0 )
        cout << "process "<< world.rank() << " : program end." << endl;
    return 0;
}