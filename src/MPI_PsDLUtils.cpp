/*

This file contains helper functions
for MPI PsDL


Created the: 24-03-2018
by: Wandrille Duchemin

Last modified the: 24-03-2018
by: Wandrille Duchemin

*/


#include "MPI_PsDLUtils.h"



int bitset_To_int( boost::dynamic_bitset<> bs)
{
    int res = 0;

    int s = bs.size();

    for(int i = s-1 ; i >=0 ; i--)
        res += bs[i] * pow(2,i);
    return res;
}

/*
string bitset_To_string( boost::dynamic_bitset<> bs)
{
    string res = "";

    int s = bs.size();

    for(int i = 0 ; i < s ; i++)
    {
        string c = "0";
        if(bs[i])
            c="1";
        res += c ;
    }
    return res;
}
*/

vector< vector< string > > divideFilesNames( vector< string > fileNames , unsigned int n )
{
    vector< vector< string > > res ; 
    for(unsigned int i = 0 ; i < n ; i++)
        res.push_back( vector< string > () );

    //cout << "divideFilesNames " << n << endl;

    unsigned int c = 0;
    for(auto it = fileNames.begin() ; it != fileNames.end() ; ++it)
    {
        res[c].push_back( *it );
        c++;
        if( c%n==0 )
            c = 0;
    } 
    return res;
}


void broadcastSpeciesCCPdistrib( mpi::communicator &world, int rank , shared_ptr<TreesetMetadata> SpeciesCCPDistrib )
{
    map<int,string> id_int_to_leaf_names;

    map<int, boost::dynamic_bitset<> > id_int_to_id_bitset;
    map<int, string > id_int_to_id_bitset_int_form;
    map< int , double> bipartitions;
    map< int, double > branchlengths;
    map<  int , map< int , double > > tripartitions;

    int last_id;
    string firstTree;
    int nbTrees;

    // save data to archive
    if(rank == 0)
    {

        int nbLeaves = SpeciesCCPDistrib->get_number_of_leaves();
    
    
        for(int i = 1 ; i <= nbLeaves ; i++ )
            id_int_to_leaf_names[ i ] = SpeciesCCPDistrib->get_name_of_leaf( i );
    
        for( auto it = SpeciesCCPDistrib->bipartition_cbegin() ; it != SpeciesCCPDistrib->bipartition_cend() ; ++it )
        {
            int id = SpeciesCCPDistrib->get_bitset_id( it->first );
            id_int_to_id_bitset[ id ] = it->first ;
            id_int_to_id_bitset_int_form[ id ] = bitset_To_string( it->first ) ;
            bipartitions[ id ] = it->second;
            branchlengths[ id ] = SpeciesCCPDistrib->get_branchlength( it->first ) * it->second ;
    
            tripartitions[ id ] = map< int, double > ();
            for(auto it2 = SpeciesCCPDistrib->tripartition_cbegin( it->first ) ; it2 != SpeciesCCPDistrib->tripartition_cend( it->first ) ; ++it2 )
            {
                int id2 =  SpeciesCCPDistrib->get_bitset_id( it2->first );
                tripartitions[ id ][ id2 ] = it2->second;
            }
    
        }
    
    
        last_id = SpeciesCCPDistrib->get_last_id();
        firstTree = SpeciesCCPDistrib->get_first_tree_in_newick_format();
        nbTrees = SpeciesCCPDistrib->get_number_of_observed_trees();


        // write class instance to archive
        // archive and stream closed when destructors are called

        //cout << "sending ccp distrib which has " << SpeciesCCPDistrib->get_last_id() << " clades." << endl ;
    }

    broadcast( world , id_int_to_leaf_names , 0 );
    broadcast( world , id_int_to_id_bitset_int_form , 0 );
    broadcast( world , last_id , 0 );
    broadcast( world , bipartitions , 0 );
    broadcast( world , tripartitions , 0 );
    broadcast( world , branchlengths , 0 );
    broadcast( world , firstTree , 0 );
    broadcast( world , nbTrees , 0 );
    


    if(rank != 0)
    {
        //And to receive:
    
        SpeciesCCPDistrib->make_from_data(  id_int_to_leaf_names,
                              id_int_to_id_bitset_int_form,
                              last_id,
                              bipartitions,
                              tripartitions,
                              branchlengths,
                              firstTree,
                              nbTrees );

        //cout << rank << " received ccp distrib which has " << SpeciesCCPDistrib->get_last_id() << " clades." << endl ;        
    }

}


void broadcastRates( mpi::communicator &world, int rank , PsDLModel * model , bool branchWise )
{


    vector< long double > Drates;
    vector< long double > Lrates;

    if(rank == 0)
    {
        if(branchWise)
        {
            Drates = model->get_DuplicationRates() ;
            Lrates = model->get_LossRates() ;
        }
        else
        {
    
            Drates.push_back( model->get_DuplicationRates(0) );
            Lrates.push_back( model->get_LossRates(0) );
        }
        //cout << "sending rate " << Drates[0] << " " << Lrates[0] << endl;
    }

    broadcast(world , Drates , 0);
    broadcast(world , Lrates , 0);

    if(rank != 0)
    {
        //cout << "received rate " << Drates[0] << " " << Lrates[0] << endl;

        if(branchWise)
        {
            for(int i = 0 ; i <  Drates.size() ; i++)
            {
                model->set_DuplicationRates( i , Drates[i] );
                model->set_LossRates( i , Lrates[i] );
            }
        }
        else
        {
            model->set_DuplicationRates( Drates[0] );
            model->set_LossRates( Lrates[0] );
        }
    }

}



/*
    Takes:
        - mpi::communicator &world : mpi communicator
        - int rank  :rank of the process
        - map < int , array<double,3>  > &transitionCount : count the transition of gene lineages, zill be updated in this function
                                                            key:   id of the species clade
                                                            value: array of three values, the count of respectively the transition from 1 lineage to 0m fro; 1 lineage to 1, from 1 lineage to many
                                                            !! will be updated with the values of all processes if rank == 0 !!
*/
void gatherTransitionCounts( mpi::communicator &world, int rank , map < int , array<double,3>  > &transitionCount )
{

    vector< int > TcountsKeys;
    vector< double > Tcounts0;
    vector< double > Tcounts1;
    vector< double > Tcounts2;
    
    for(auto it = transitionCount.begin() ; it != transitionCount.end() ; ++it)
    {
        TcountsKeys.push_back( it->first );
        Tcounts0.push_back( it->second[0] );
        Tcounts1.push_back( it->second[1] );
        Tcounts2.push_back( it->second[2] );
    }

    if(rank == 0)
    {
        vector< vector< int > > TotalTcountsKeys;
        vector< vector< double > > TotalTcounts0;
        vector< vector< double > > TotalTcounts1;
        vector< vector< double > > TotalTcounts2;


        gather(world , TcountsKeys , TotalTcountsKeys , 0 );
        gather(world , Tcounts0 , TotalTcounts0 , 0 );
        gather(world , Tcounts1 , TotalTcounts1 , 0 );
        gather(world , Tcounts2 , TotalTcounts2 , 0 );

        //zipping the info; skipping the 1st (because this is the one we're updating)
        for(unsigned i = 1 ; i < TotalTcountsKeys.size() ; i++)
        {
            for(unsigned j = 0 ; j < TotalTcountsKeys[i].size() ; j++)
            {
                int sp = TotalTcountsKeys[i][j];
                if( transitionCount.find( sp ) == transitionCount.end() )
                {
                    transitionCount[ sp ] = array<double,3>();
                    transitionCount[ sp ].fill(0);
                }
                transitionCount[ sp ][ 0 ] += TotalTcounts0[i][j];
                transitionCount[ sp ][ 1 ] += TotalTcounts1[i][j]; 
                transitionCount[ sp ][ 2 ] += TotalTcounts2[i][j]; 
            }
        }
    }
    else
    {

        gather(world , TcountsKeys , 0 );
        gather(world , Tcounts0 , 0 );
        gather(world , Tcounts1 , 0 );
        gather(world , Tcounts2 , 0 );
    }
}


/*
    Takes:
        - mpi::communicator &world : mpi communicator
        - int rank  :rank of the process
        - map< int, map< int , float > > &splitCountsObservedInReconciliations : the map of all split of the species tree encountered in the reconciled tree, with a value corresponding to the number of times it is seen
                                                                               !! will be updated with the values of all processes if rank == 0 !!
*/
void gatherOservedSplit( mpi::communicator &world, int rank , map< int, map< int , float > > &splitCountsObservedInReconciliations )
{

    if(rank != 0)
    {
        gather(world, splitCountsObservedInReconciliations , 0);
    }
    else
    { // root process
        vector< map< int, map< int , float > > > TOTAL ;
        gather(world, splitCountsObservedInReconciliations , TOTAL ,0);

        //gathering in a single structure
        for(unsigned i = 1 ; i < TOTAL.size() ; i++)
        {
            for(auto it1 = TOTAL[i].begin() ; it1 != TOTAL[i].end() ; ++it1)
            {
                int parent = it1->first;
                if( splitCountsObservedInReconciliations.find( parent ) == splitCountsObservedInReconciliations.end()  )
                    splitCountsObservedInReconciliations[ parent ] = map<int,float>();

                for(auto it2 = it1->second.begin() ; it2 != it1->second.end() ; ++it2)
                {
                    if( splitCountsObservedInReconciliations[parent].find( it2->first ) == splitCountsObservedInReconciliations[parent].end() )
                    {
                        splitCountsObservedInReconciliations[parent][ it2->first ] = 0 ;
                    }
                    splitCountsObservedInReconciliations[parent][ it2->first ] += it2->second ;
                }
            }
        }

    }


}






/*
    
 
    Returns:
        int : last leafset

*/
int getAleFileLastLeafset(string filename)
{
    string substr = "#last_leafset_id"; 
    int l = substr.size();

    ifstream IN( filename );
    if( IN.is_open() )
    {

        string line;
 
        while( getline(IN, line) )
        {
            if(line.compare( 0 , l , substr ) == 0 )
            {
                //cout << filename << " ";
                //cout << line << " ";
                getline(IN, line);
                //cout << line<< endl;
                //cout << line.substr(l);
    
                int lastId = stod( line ); 
                IN.close();
                return lastId;
            }
        }

    }


    return -1;

}


/*
For a descending order 
*/
int findSortedIndex( int value, vector < int > & sortedList , int minIndex , int maxIndex )
{

    if ( minIndex == maxIndex )
        return minIndex;

    int I = (minIndex + maxIndex)/2;

    //cout << minIndex << "-" << maxIndex << endl;

    if( sortedList[I] == value )
        return I;
    if( sortedList[I] < value )
        return findSortedIndex( value, sortedList , minIndex , I );
    else
        return findSortedIndex( value, sortedList , I+1 , maxIndex );


}

/*
NB : fills <names> and <size> such that they stay sorted by size

    Takes:
        - string filename : name of the file to open
        - vector < string > &names : will be filled with file names 
        - vector < int > &size : will be filled with number of clades of the file

    Returns:
        (int) : 0 if no problem
                1 if the file cannot be opened
*/
int assessAleFilesSizes( string filename, vector < string > &names, vector < int > &size )
{

    string GeneDistribFileName;

    ifstream input_file( filename );
    if( input_file.is_open() )
    {
        // Get a file name from formatted input file, \n will be discarded
        while( getline(input_file, GeneDistribFileName, '\n') )
        {
            int s = getAleFileLastLeafset(GeneDistribFileName);
            //cout << GeneDistribFileName << " -> " << s << endl;
            if(s == -1)
            {
                cerr << "Warning while reading " << GeneDistribFileName << ". No valid #last_leafset_id found." << endl;
                input_file.close();
                return -1;
            }

            if( size.size() == 0 )
            {
                size.push_back( s );
                names.push_back( GeneDistribFileName );
            }
            else if( ( s < size.back() )  )
            {
                size.push_back( s );
                names.push_back( GeneDistribFileName );
            }
            else
            {
                int InsertIndex;
                if( s > size.front() )
                    InsertIndex = 0;
                else 
                    InsertIndex = findSortedIndex( s, size , 0 , size.size() );
                
                size.insert( size.begin() + InsertIndex , s );
                names.insert( names.begin() + InsertIndex , GeneDistribFileName );
            }

            
        }
        input_file.close();
    }
    else
    {
        cerr << "Unable to open gene distribution input file: " << filename << endl;
        return 1;
    }


    return 0;

}


/*
NB : fills <names> and <size> such that they stay sorted by size

    Takes:
        - vector < string > fileNames : name of the files to assess
        - vector < string > &names : will be filled with file names 
        - vector < int > &size : will be filled with number of clades of the file

    Returns:
        (int) : 0 if no problem
                1 if the file cannot be opened
*/
int assessAleFilesSizes( vector < string > fileNames , vector < string > &names, vector < int > &size )
{

    string GeneDistribFileName;

    for(unsigned int i = 0 ; i < fileNames.size() ; i++)
    {
        GeneDistribFileName = fileNames[i];
        int s = getAleFileLastLeafset(GeneDistribFileName);
        //cout << GeneDistribFileName << " -> " << s << endl;
        if(s == -1)
        {
            cerr << "Warning while reading " << GeneDistribFileName << ". No valid #last_leafset_id found." << endl;
            return -1;
        }
        if( size.size() == 0 )
        {
            size.push_back( s );
            names.push_back( GeneDistribFileName );
        }
        else if( ( s < size.back() )  )
        {
            size.push_back( s );
            names.push_back( GeneDistribFileName );
        }
        else
        {
            int InsertIndex;
            if( s > size.front() )
                InsertIndex = 0;
            else 
                InsertIndex = findSortedIndex( s, size , 0 , size.size() );
            
            size.insert( size.begin() + InsertIndex , s );
            names.insert( names.begin() + InsertIndex , GeneDistribFileName );
        }

    }
    return 0;
}

/*
NB : the file  sizes will be in the order they are found in the names vector

    Takes:
        - vector < string > &names : file names 
        - vector < int > &size : will be filled with number of clades of the file

    Returns:
        (int) : 0 if no problem
                1 if the file cannot be opened
*/
int assessAleFilesSizesNoSort( vector < string > &names, vector < int > &size )
{

    string GeneDistribFileName;

    for(unsigned int i = 0 ; i < names.size() ; i++)
    {
        GeneDistribFileName = names[i];
        int s = getAleFileLastLeafset(GeneDistribFileName);
        //cout << GeneDistribFileName << " -> " << s << endl;
        if(s == -1)
        {
            cerr << "Warning while reading " << GeneDistribFileName << ". No valid #last_leafset_id found." << endl;
            return -1;
        }
        size.push_back( s );
    }



    return 0;

}

/*
this function adds file names and file sizes to <SortedNames> and <SortedSizes> so that they stay sorted

Takes:
    * vector < string > &SortedNames : file names, sorted by descending size (ie. nb of clades)
    * vector < int > &SortedSizes : file names, sorted by descending size (ie. nb of clades)
    * vector < string > &names : file names
    * vector < int > &sizes : file sizes

*/
void mergeIntoSortedBySizes( vector < string > &SortedNames, vector < int > &SortedSizes, vector < string > &names, vector < int > &sizes )
{

    for(unsigned int i = 0 ; i < names.size() ; i++)
    {

        int s = sizes[i];
        string GeneDistribFileName = names[i];


        if( SortedSizes.size() == 0 )
        {
            SortedSizes.push_back( s );
            SortedNames.push_back( GeneDistribFileName );
        }
        else if( ( s < SortedSizes.back() )  )
        {
            SortedSizes.push_back( s );
            SortedNames.push_back( GeneDistribFileName );
        }
        else
        {
            int InsertIndex;
            if( s > SortedSizes.front() )
                InsertIndex = 0;
            else 
                InsertIndex = findSortedIndex( s, SortedSizes , 0 , SortedSizes.size() );
            
            SortedSizes.insert( SortedSizes.begin() + InsertIndex , s );
            SortedNames.insert( SortedNames.begin() + InsertIndex , GeneDistribFileName );
        }
    }

}


/*

Takes:
    vector< string > fileNames , 
    vector< int > FileSizes  : size of file (last id), ordered by 
    unsigned int n  : number of bins

Returns:
vector< vector< string > > 
*/
vector< vector< string > > divideFilesNamesSorted( vector< string > fileNames , vector< int > FileSizes , unsigned int n )
{
    //cout << "divideFilesNamesSorted" << endl;

    vector< vector< string > > Bins;
    vector < int > BinSize;
    list < int > BinOrder;
    
    Bins.reserve( n );
    BinSize.reserve( n );
    

    int nbFile = FileSizes.size();

    for( unsigned i = 0 ; i < n ; i ++)
    {
        Bins.push_back( vector< string >() );
        BinSize.push_back( 0 );
        BinOrder.push_back( i );
    }

    int i = 0;
    while( i < nbFile)
    {
        int bIndex = BinOrder.front();
        Bins[ bIndex ].push_back( fileNames[i] );
        BinSize[ bIndex ] += FileSizes[i];

        int s = BinSize[ bIndex ];


        BinOrder.pop_front();
        for( auto it = BinOrder.begin() ; it != BinOrder.end() ; ++it )
        {
            if( BinSize[ *it ] > s )
            {
                BinOrder.insert( it , bIndex );

                break;
            }
        }
        if( BinOrder.size() != n )
            BinOrder.push_back( bIndex );

        i++;
    }

    //for( unsigned i = 0 ; i < n ; i ++)
    //{
    //    cout << i << " nf:" << Bins[i].size() << " size: " << BinSize[i] << endl;
    //}

    return Bins;
}