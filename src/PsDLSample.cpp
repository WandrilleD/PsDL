/*

This file contains a small executable to
perform a mcmc sampling of rates of duplication and loss 


Created the: 23-02-2018
by: Wandrille Duchemin

Last modified the: 06-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>

#include "PsDLSampleOptionHandler.h"
#include "PsDLSampleUtils.h"
#include "PsDLUtils.h"
#include "PsDLModel.h"


#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>
#include <limits>

using namespace std;





/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{
   
    shared_ptr<PsDLSampleOptionHandler> optionHandler = make_shared<PsDLSampleOptionHandler>();
    
    optionHandler->readArguments(argc, argv);
    int pb = 0;

    if(!optionHandler->verify())
    {
        return -99;
    }


    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( optionHandler->get_verbose_level() > 0 )
        cout << "using random seed : "<< seed << endl;

    srand(seed);

    shared_ptr<TreesetMetadata> GeneCCPDistrib = make_shared<TreesetMetadata>();
    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names


    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;

    // **************
    // 1 reading data
    // **************

    
    // 1A reading tree data
    // **************

    // 1A.1 gene file names
    // **************


    vector <string> geneDistribFileNames;

    if( optionHandler->get_multiple_gene_families() )
    {
        pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
    }
    else
        geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );


    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
        exit(pb);
    }


    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , optionHandler->get_input_is_ale() , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "read gene distribution(s)."<<endl;

    // 1A.2 species tree data
    // **************

    pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, optionHandler->get_input_is_ale() , optionHandler->get_unrooted_species_tree() ); 

    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
        exit(pb);
    }

    if(VerboseLevel>1)
        cout << "read species distribution"<<endl;


    // 1B reading gene species correspondence data
    // **************


    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }
    if(VerboseLevel > 1)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }


    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        GeneCladeSpeciesMasks.push_back( getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }


    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks[GeneFamilyId] );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }



    // 2 instanciating the model
    // **************
   
    double currentDupRate = optionHandler->get_initialDupRate();
    double currentLossRate = optionHandler->get_initialLossRate();

    double alpha = 0.2; // alpha serves as a scaler parameter when drawing new values for the loss and duplication rates
    double minRate = pow(10,-6);
    double maxRate = pow(10,6);


    PsDLModel * model = new PsDLModel();

    model->addSpeciesCCPdistribution(SpeciesCCPDistrib);// makes some precomputations With ids

    model->set_DuplicationRates( currentDupRate ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
    model->set_LossRates( currentLossRate ); //  takes : loss rate and set it as the global duplication rate for all the species tree

    model->set_deltal( 0.05 ) ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    model->set_minNbBLInterval( 5 ) ; // minimum number of interval for a given branch of the species tree


    double BLEN = getWeightedBranchLenSum( SpeciesCCPDistrib ); 
    double rootBranchRatio = 0.05;

    model->set_rootBranchLength( rootBranchRatio * BLEN ); // putting 5 percent of the total branch length of the tree at the top of the species tree

    model->divideSpeciesBranches( LowerVerboseLevel );    //   

    double extantSampling = optionHandler->get_extant_sampling();
    model->set_probaExtantGeneSampled(extantSampling);

    
    model->computeProbaGeneExtinct( LowerVerboseLevel );  // maybe these could move to protected when they've been tested
    model->computeProbaGeneOneChild( LowerVerboseLevel ); // as they NEED to be executed only once and in this precise order
    model->initializeGeneLeavesProbas();
    long double currentP = 0;

    //compute the overall probability for all gene families 
    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];

        model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
        model->addgeneSpeciesCorrespondence( GtoSpCorr );

        model->computeReconciliationMatrix(  GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 
        currentP += log( model->getOverAllProbability() );

        model->prepareForNewGeneFamily();
        GeneCCPDistrib.reset();

    }

    if(VerboseLevel>0)
    {
        cout << "Initial Round: log(P)="<< currentP<<endl;
    }
    // sampling process
    // 


    //initialisation of output
    string FilePrefix = optionHandler->get_outputFile();

    streambuf * buf_rates;
    ofstream of_rates;

    if(FilePrefix!="")
    {
        string RateFilename = FilePrefix + ".rates.txt"  ;      
        of_rates.open(RateFilename.c_str());
        buf_rates = of_rates.rdbuf();

        if(VerboseLevel>0)
        {
            cout << "writing rates in "<< RateFilename << endl;
        }
    }
    else
    {
        buf_rates = cout.rdbuf();
    }

    
    ostream OUT_rate(buf_rates);
    //

    InitSampleFile( OUT_rate );



    /////
    // burnin phase

    ///////////////////////////////////////////////////////////////////


    unsigned int Progress = 0;
    unsigned int burnin = optionHandler->get_burnin();

    while(Progress < burnin)
    {

        bool accepted  = samplerStep( model, GeneCCPDistribV , GtoSpCorr , GeneCladeSpeciesMasks, currentDupRate , currentLossRate , currentP , LowerVerboseLevel , alpha, minRate, maxRate);

        //if(accepted)
        Progress++;
    }



    if(VerboseLevel>0)
        cout << "burnin finished."<<endl;

    Progress = 0;
    unsigned int totalsampleSize = optionHandler->get_sampleSize() * optionHandler->get_thinning();
    unsigned int thin = optionHandler->get_thinning();

    while(Progress < totalsampleSize)
    {

        bool accepted  = samplerStep( model, GeneCCPDistribV , GtoSpCorr , GeneCladeSpeciesMasks, currentDupRate , currentLossRate , currentP , LowerVerboseLevel , alpha, minRate, maxRate);

        //if(accepted)
            Progress++;

        if(Progress%thin == 0)
        {
            WriteSampleToFile( OUT_rate , Progress/thin , Progress , currentDupRate , currentLossRate , currentP);
        }
    }


    if(FilePrefix!="")
    {        
        of_rates.close();
    }


    //Output of gene reconciliations
    unsigned int TreeSampleSize = optionHandler->get_outputSample();
    if(  TreeSampleSize > 0 )
    {
       
        //initialisation of output
        
        streambuf * buf_tree;
        ofstream of_tree;
    
    
        if(FilePrefix!="")
        {
            string TreeFilename = FilePrefix + ".trees.txt" ;
            of_tree.open(TreeFilename.c_str());
            buf_tree = of_tree.rdbuf();
       
            if(VerboseLevel>0)
            {
                cout << "writing trees in "<< TreeFilename << endl;
            }
        }
        else
        {
            buf_tree = cout.rdbuf();
        }


        ostream OUT_tree(buf_tree);

        InitRecXMLToStream( OUT_tree );

        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
    
            model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );
    
            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel); 
            
            for(unsigned i = 0 ; i < TreeSampleSize; i++ )
            {
                shared_ptr< RecNode > RT= model->backtrack();
                model->annotateReconciledTree( RT );
                string description = "gene family " + int2string(GeneFamilyId) + " , sample " + int2string(i+1);
                WriteRecTreeToStream( OUT_tree , RT , description);
            }
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
    
        }
    
    
        FinishRecXMLToStream( OUT_tree );

        if(FilePrefix!="")
            of_tree.close();
    }

    /*
    TODO:
        add some decoration to tree output file:
            --> comment header to say which soft produced
            --> tree description for the rate

    */


    if(VerboseLevel>0)
        cout << "sampling finished."<<endl;


    delete model;
    GeneCCPDistrib.reset();
    SpeciesCCPDistrib.reset();




    return 0;

}