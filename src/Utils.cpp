/*

This file contains util functions to the various algorithm proposed.

Created the: 07-05-2018
by: Wandrille Duchemin

Last modified the: 17-05-2018
by: Wandrille Duchemin

*/

#include "Utils.h"


double Uniform0to1( )
{
    return   ((double) rand()/ RAND_MAX) ;
}


double Normal(double mean, double sd)
{
    normal s(mean,sd);
    return quantile(s, Uniform0to1() );
}




/*

    Takes:
        - string filename : name of the file containing the extant gene sampling rate
            one per line, format : <species name> <rate>
            example:
                A 0.9
                B 0.75
                C 1
            
            Omitted leaves are presumed to have a sampling rate of 1.
        - shared_ptr< TreesetMetadata > CCPDistribution : species CCP distribution, to check that leave do, in fact, exists
        - map< string , double > &ExtantSamplingMap : to be filled ; keys are leaf names, values are sampling rates.


    Returns:
        int :  0 if no problem
               1 if the file cannot be opened

*/
int readExtantSamplingFile(string filename, shared_ptr< TreesetMetadata > CCPDistribution, map< string ,  FLOAT_TYPE > &ExtantSamplingMap)
{

    ifstream FStream(filename.c_str());
    
    bool hasTransfer=false;
    int nbF = 0;

    if( FStream.is_open() )
    {
        while( !FStream.eof() ) 
        {

            string speciesStr;

    
            FLOAT_TYPE rate;

            FStream >> speciesStr;
    
            if(FStream.eof()) // to avoid hanging line
                continue;

            FStream >> rate;

            //for(unsigned i = 0 ; i < L.size(); i++)
            //    cout << " " << L[i] ;
            //cout << speciesStr ;

            boost::dynamic_bitset<> C = CCPDistribution->get_bitset_of_leaf( speciesStr );


            if( C.any() )
            { // existing clade
                //cout << "-> " << C << endl;
                if( (rate >1) || (rate <=0 ) )
                {
                    cerr << "Warning : found an extant sampling rate either >1 or <=0, which is not possible, for leaf " << speciesStr <<". Ignoring." << endl;
                    rate = 1;
                }
                ExtantSamplingMap[ speciesStr ] = rate;
            }
            else
            {
                //cout << "-> XXX" << endl;
                cerr << "Warning : unknown leaf " << speciesStr << " encountered while parsing extant sampling rate file. Ignoring." << endl;
            }

    
        }

        FStream.close();
    }
    else
    {
        return 1;
    }
    
    return 0;
}











/*
    Takes:
        - unsigned int step
        - vector < double > minimums
        - vector < double > maximums 

    Returns:
        vector< vector< long double > >  : 1st index : round id
                                           2nd index : duplication, loss (and transfer) rates

*/
vector < vector< long double > > GRID_generate_rateList( unsigned int step, vector < double > minimums , vector < double > maximums , bool NoLog )
{

    vector< vector< long double > > rates;

    rates.reserve( pow( step,minimums.size() ) );


    for(unsigned i = 0 ; i < step ; i++)
    {
        long double DupRate = (( maximums[0] - minimums[0] )/(step-1)) * i + minimums[0] ;
        //cout << "  "<<  MaxDupRate<< " " << MinDupRate << " " << step << " " << i <<  " >> " << DupRate << endl;
        if( ! NoLog )
        {
            DupRate = pow(10,  DupRate );
            //cout << "     >> "<< DupRate << endl;
        }
        for(unsigned j = 0 ; j < step ; j++)
        {
            long double LossRate = (( maximums[1] - minimums[1] )/(step-1)) * j + minimums[1] ;
            if( ! NoLog )
                LossRate = pow(10, LossRate );

            
            if(minimums.size()>2)
            {
                for(unsigned k = 0 ; k < step ; k++)
                {

                    long double TransferRate = (( maximums[2] - minimums[2] )/(step-1)) * k + minimums[2] ;
                    if( ! NoLog )
                        TransferRate = pow(10, TransferRate );

                    vector< long double> R (3,0);
                    R[0] = DupRate;
                    R[1] = LossRate;
                    R[2] = TransferRate;
                    rates.push_back( R );
                }
            }
            else
            {
                vector<long double> R (2,0);
                R[0] = DupRate;
                R[1] = LossRate;
                rates.push_back( R );
            }
        }
    }
    return rates;
}




/*
    *recursive function*

    Takes:
        - shared_ptr<RecNode> RT :  reconciled tree to analyse
        - map < int , vector< long double >  > &evtCounts : count the events in species lineages, will be updated in this function
                                                      key:   id of the species clade
                                                      value: vector of 4 values, the count of respectively the duplications, the losses, the transfer receptions, the speciations
        - long double weight =1 : value used to increment to counts 

*/
void MAX_countEvents( shared_ptr<RecNode> RT ,map < int , vector<long double>  > &evtCounts , long double weight )
{
    int sp = RT->get_SpeciesCladeId();

    if( evtCounts.find( sp ) == evtCounts.end() )
        evtCounts[ sp ] = vector<long double>(4,0);

    if( RT->get_event() == "duplication" ) // transition from 1 to many
        evtCounts[ sp ][ 0 ] += weight;
    else if(RT->get_event() == "loss")
        evtCounts[ sp ][ 1 ] += weight; 
    else if( (RT->get_event() == "speciation")||(RT->get_event() == "leaf") )
        evtCounts[ sp ][ 3 ] += weight;

    if(RT->is_Tb())
        evtCounts[ sp ][ 2 ] += weight; 

    vector < shared_ptr<RecNode> > children = RT->get_children();
    
    for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
    { // recursion loop . 
        MAX_countEvents(*ChIt , evtCounts, weight);
    }


    return;
}

/*
    *recursive function*

    Takes:
        - shared_ptr<RecNode> RT :  reconciled tree to analyse
        - map < int , vector<long double> > &transitionCount : count the transition of gene lineages, zill be updated in this function
                                                      key:   id of the species clade
                                                      value: vector of three values, the count of respectively the transition from 1 lineage to 0m fro; 1 lineage to 1, from 1 lineage to many
        - long double weight =1 : value used to increment to counts 

*/
void MAX_countTransitions( shared_ptr<RecNode> RT ,map < int , vector<long double>  > &transitionCount , long double weight )
{

    // special root treatment
    if(RT->is_root())
    {
        int sp = RT->get_SpeciesCladeId();

        if( transitionCount.find( sp ) == transitionCount.end() )
        {
            transitionCount[ sp ] = vector<long double>(3,0);
        }

        if( RT->get_event() == "duplication" ) // transition from 1 to many
            transitionCount[ sp ][ 2 ] += weight;
        else // speciation or leaf (I discount the possiblity of having a loss as the root node) -> transition from 1 to 1
            transitionCount[ sp ][ 1 ] += weight; 

    }

    vector < shared_ptr<RecNode> > children = RT->get_children();


    if( RT->get_event() == "speciation" )
    { // counting transitions

        for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
        { // recursion loop . 
            
            string evt = (*ChIt)->get_event();
            int sp = (*ChIt)->get_SpeciesCladeId();

            if( transitionCount.find( sp ) == transitionCount.end() )
            {
                transitionCount[ sp ] = vector< long double>(3,0);
            }

            if( evt == "duplication" )
            { // from 1 to many
                transitionCount[ sp ][ 2 ] += weight;
            }   
            else if(evt == "loss")
            { // from 1 to 0
                transitionCount[ sp ][ 0 ] += weight;
            }
            else
            { // includes speciation and leaf -> from 1 to 1
                transitionCount[ sp ][ 1 ] += weight;
            }

        }
    }
    

    for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
    { // recursion loop . 
        MAX_countTransitions(*ChIt , transitionCount, weight);
    }


    return;
}



/*
    *recursive function*

    Takes:
        - shared_ptr<RecNode> RT :  reconciled tree to analyse
        - map < int , vector< long double >  > &evtCounts : count the events in species lineages, will be updated in this function
                                                      key:   id of the species clade
                                                      value: vector of 8 values, the count of respectively 
                                                                        the transition from :
                                                                                            1 lineage to 0
                                                                                            1 lineage to 1
                                                                                            1 lineage to many
                                                                                            0 lineage to >0
                                                                        the counts of :
                                                                                            duplications
                                                                                            losses
                                                                                            transfer receptions
                                                                                            speciations
        - long double weight =1 : value used to increment to counts 

*/
void MAX_countTransitionsAndEvents( shared_ptr<RecNode> RT ,map < int , vector<long double>  > &evtCounts , long double weight )
{
    int sp = RT->get_SpeciesCladeId();

    if( evtCounts.find( sp ) == evtCounts.end() )
        evtCounts[ sp ] = vector<long double>(8,0);

    string evt = RT->get_event();

    //cout << sp << " " << evt ;

    if( evt == "branchingOut" )
    {
        evt = MAX_findEvtAfterBranchingOut(RT);
        //cout << "->" << evt;
    }

    //cout << endl;

    if( evt == "duplication" ) // transition from 1 to many
        evtCounts[ sp ][ 4 ] += weight; // counting the duplication as an event
    else if( evt == "loss")
        evtCounts[ sp ][ 5 ] += weight; // counting the loss as an event
    else if( (evt == "speciation" ) || ( evt == "leaf") )
        evtCounts[ sp ][ 7 ] += weight; // counting the speciation as an event

    if(RT->is_Tb())
    {
        //cout << " -> is Tb" << endl;
        evtCounts[ sp ][ 6 ] += weight; // counting the transfer reception as an event
        evtCounts[ sp ][ 3 ] += weight; // counting the transfer reception as a transition from 0 to x
    }



    // special root treatment
    if(RT->is_root())
    {

        if( evtCounts.find( sp ) == evtCounts.end() )
        {
            evtCounts[ sp ] = vector<long double>(8,0);
        }
        string evt = RT->get_event();
        if( evt == "branchingOut" )
            evt = MAX_findEvtAfterBranchingOut( RT );

        //cout << " -> root : " << sp << " " << evt << endl;

        if( evt == "duplication" )
        { // from 1 to many
            evtCounts[ sp ][ 2 ] += weight;
        }   
        else if(evt == "loss")
        { // from 1 to 0
            evtCounts[ sp ][ 0 ] += weight;
        }
        else
        { // includes speciation and leaf -> from 1 to 1
            evtCounts[ sp ][ 1 ] += weight;
        }
    }


    vector < shared_ptr<RecNode> > children = RT->get_children();

    if( RT->get_event() == "speciation" )
    { // counting transitions

        for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
        { // recursion loop . 
            
            string evt = (*ChIt)->get_event();
            sp = (*ChIt)->get_SpeciesCladeId();

            if( evtCounts.find( sp ) == evtCounts.end() )
            {
                evtCounts[ sp ] = vector< long double>(8,0);
            }

            if( evt == "branchingOut" )
                evt = MAX_findEvtAfterBranchingOut( *ChIt );

            //cout << " transition -> "<< sp << " " << evt << endl;

            if( evt == "duplication" )
            { // from 1 to many
                evtCounts[ sp ][ 2 ] += weight;
            }   
            else if(evt == "loss")
            { // from 1 to 0
                evtCounts[ sp ][ 0 ] += weight;
            }
            else
            { // includes speciation and leaf -> from 1 to 1
                evtCounts[ sp ][ 1 ] += weight;
            }
        }
    }
        
    for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
    { // recursion loop . 
        MAX_countTransitionsAndEvents(*ChIt , evtCounts, weight);
    }

    return;
}

/*
Takes:
    - shared_ptr<RecNode> RT : a branchingOut node

Returns:
    (string): first event in the same species which is not a branchingOut
*/
string MAX_findEvtAfterBranchingOut( shared_ptr<RecNode> RT )
{
    int sp = RT->get_SpeciesCladeId();
    vector < shared_ptr<RecNode> > children = RT->get_children();
    for(auto ChIt = children.begin() ; ChIt != children.end() ; ++ChIt)
    { // recursion loop . 
        int spC = (*ChIt)->get_SpeciesCladeId();
        if( spC == sp )
        {
            string evt = (*ChIt)->get_event();
            if( evt == "branchingOut")
                evt = MAX_findEvtAfterBranchingOut( *ChIt );
            
            return evt;
        }
    }

    cerr << "ERROR : in MAX_findEvtAfterBranchingOut() : did not find a non branchingOut child after a branchingOut. Problem during backtrack ? " << endl;
    return "";

}

vector<long double> MAX_estimateNewRates_DL( ParentModel * model , boost::dynamic_bitset<> species, vector<long double> &transitionCount)
{
    double nbObservations = transitionCount[0] + transitionCount[1] + transitionCount[2] ;
    double p10 = transitionCount[0] / nbObservations;
    double p11 = transitionCount[1] / nbObservations;

    //cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << "|" << it->second[0] <<","<< it->second[1] <<","<< it->second[2] << endl;

    double pExtinct = model->get_ProbaGeneExtinct(species);
    double Blen = model->get_speciesCCPdistribution()->get_branchlength(species);

    if( model->get_speciesCCPdistribution()->get_number_of_leaves() == species.count() )
    { // special root procedure
        Blen  = model->get_rootBranchLength();
    }

    pair < double, double > rates = estimateNewRates( p10, p11, Blen, pExtinct );
    vector<long double> Vrates(2,0);
    Vrates[0] = rates.first;
    Vrates[1] = rates.second;
    return Vrates;
}

/*
    Takes:
        - long double nbD : (corrected) number of Duplication
        - long double nbL : (corrected) number of Losses
        - long double nbT : (corrected) number of Transfer receptions 
        - long double nbS : (corrected) number of Speciation

    Returns:
        (vector<long double>): vector of rate of duplication, loss and transfer    
*/
vector<long double> MAX_estimateNewRates_DTL(  long double nbD , long double nbL , long double nbT , long double nbS )
{
    //cout << "MAX_estimateNewRates_DTL" << nbD << " " << nbL << " " << nbT << " " << nbS << endl;

    long double SumCount = nbD + nbL + nbT + nbS;
    long double D = nbD/SumCount;
    long double L = nbL/SumCount;
    long double T = nbT/SumCount;


    vector<long double> Vrates(3,0);

    // special case 
    // (D+L+T) == 1
    // L == 1

    long double X = (-1+D+L+T) ;

    if( nbS != 0 ) // equivalent to X == 0, but with less roundup errors.
    {
        Vrates[0] = -D/X ;
        Vrates[1] = -L/X ;
        Vrates[2] = -T/X ;

        for( unsigned i = 0; i < 3; i++)
        {
            if(Vrates[i] < 0)//sanity check
            {
                cerr << "BUG : estimated a rate < 0 . " << i <<" " << Vrates[i] << endl;
                cerr << " nbD " <<  nbD ;
                cerr << " nbL " <<  nbL ;
                cerr << " nbT " <<  nbT ;
                cerr << " nbS " <<  nbS ;
                cerr << endl;
                Vrates[i] = -2;
            }
        }
    }
    else
    {
        // happens iif S == 0
        // dirty fix

        Vrates[0] = D ;
        
        Vrates[1] = L ;
        
        Vrates[2] = T ;

    }

    //cout << "  -> ";
    for( unsigned i = 0; i < 3; i++)
    {
        if(Vrates[i] == 0)// avoiding 0s as they'll cause problems
            Vrates[i] = -2;
        //cout << " " << Vrates[i] ;
    }
    //cout << endl;

    return Vrates;
} // deprecated



/*
    Takes:
        - ParentModel * model : will be updated
        - map < int , vector<long double> > &RateUpdateCount : 8 counts
                                                               the transition from :
                                                                                   1 lineage to 0
                                                                                   1 lineage to 1
                                                                                   1 lineage to many
                                                                                   0 lineage to >0
                                                               the counts of :
                                                                                   duplications
                                                                                   losses
                                                                                   transfer receptions
                                                                                   speciations        
        - vector < long double > &EventSums : will get filled 
        - vector < long double > RateDefaultMinimums
        - vector < long double > RateDefaultMaximums

    Returns:
        (double) : total number of observations

*/
double MAX_estimateNewRates_DTL(  ParentModel * model , 
                                  map < int , vector<long double> > &RateUpdateCount , 
                                  vector < long double > &EventSums,
                                  vector < long double > RateDefaultMinimums, 
                                  vector < long double > RateDefaultMaximums , unsigned int VerboseLevel)
{

    shared_ptr< TreesetMetadata > SpeciesCCPDistrib = model->get_speciesCCPdistribution();

    map < int , long double > ExtinctInDescendants;

    map < int , vector < long double > > Pxxs;

    map< int , vector < long double > > Rates;

    //1. computing p10, p11 and p0x for all species
    for( auto it  = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it )
    {
        Pxxs[it->first] = vector< long double > (3,0); // p10, p11, p0x
        long double total  = it->second[0] + it->second[1] + it->second[2] + it->second[3] ;
        Pxxs[it->first][0] = it->second[0] / total; // p10
        Pxxs[it->first][1] = it->second[1] / total; // p11
        Pxxs[it->first][2] = it->second[3] / total; // p1x

        //cout << it->first << " total " << total << " ";
        //cout << " "  << it->second[0] ;
        //cout << " "  << it->second[1] ;
        //cout << " "  << it->second[2] ;
        //cout << " "  << it->second[3] ;
        //cout << " "  << it->second[4] ;
        //cout << " "  << it->second[5] ;
        //cout << " "  << it->second[6] ;
        //cout << " "  << it->second[7] << endl;
        //cout << "\t-> " ;
        //cout << " " << Pxxs[it->first][0];
        //cout << " " << Pxxs[it->first][1];
        //cout << " " << Pxxs[it->first][2] << endl;

        // also initialization of rates
        Rates[ it->first ] = vector< long double >(3,0);
        for(unsigned i = 0 ; i < RateDefaultMinimums.size() ; i++)
            Rates[ it->first ][i] = RateDefaultMinimums[i];

    }

    for( auto it  = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it )
    {

        ExtinctInDescendants[ it->first ] = 0;

        boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );

        long double bip = SpeciesCCPDistrib->get_count_of_bipartition( sigma );
        auto END = SpeciesCCPDistrib->tripartition_cend( sigma ) ;
        for( auto TripIt = SpeciesCCPDistrib->tripartition_cbegin( sigma ) ; TripIt != END ; ++TripIt )
        {
            int Sid1 = model->get_CCPindexFromSpeciesCladeIndex( TripIt->first );
            int Sid2 = model->get_CCPindexFromSpeciesCladeIndex( SpeciesCCPDistrib->get_complementer( sigma , TripIt->first ) );
            auto p1 = Pxxs.find( Sid1 );
            auto p2 = Pxxs.find( Sid2 );
            if( ( p1 == Pxxs.end() ) || ( p2 == Pxxs.end() ) )
                continue; //-> one child has no counts. -> p10 is 0 -> no contribution
            ExtinctInDescendants[ it->first ] += ( TripIt->second / bip ) * p1->second[ 0 ] * p2->second[ 0 ]; 
        }
    }

    bool STOP = false;
    unsigned int NbTurnTMP = 0;

    long double nbElt = SpeciesCCPDistrib->get_last_id()+1; // 0 is used, so there are last_id + 1 clades

    while(!STOP)
    {
        //initializations
        long double SumPtr = 0;
        long double SumEtr = 0;

        for( unsigned i = 1 ; i < nbElt+1 ; i++ )
        { // clade id in the model go from 1 to number of clades+1

            auto p1 = Pxxs.find( i );
            if( p1 == Pxxs.end() )
            { // Ei = 0 ; PTi = minRate
                SumPtr += RateDefaultMinimums[2];
            }
            else
            {
                //cout << i << " " << p1->second[0] << " " << Rates[i][2] << endl; 
                SumEtr +=  p1->second[0] * Rates[i][2]; // if i is present in Pxxs, we know it is present in Rates
                SumPtr +=  Rates[i][2];
            }
        }



        long double SumDiffs = 0;

        for( auto it  = Pxxs.begin() ; it != Pxxs.end() ; ++it )
        {
            //correction of the sums of transfer reception rates and loss after a transfer reception
            long double Et = SumEtr;
            long double Pt = SumPtr;

            long double DivFac = nbElt;

            //cout << " sp : " << it->first << endl;

            vector < CIndexType >  TrIncomp = model->get_ListTransferIncompatibilities( it->first );
            for( auto incomp : TrIncomp )
            {
                //cout << "\tincomp " << incomp << endl;
                auto p1 = Pxxs.find( incomp );
                if( p1 == Pxxs.end() )
                { // Ei = 0 ; PTi = minRate
                    Pt -= RateDefaultMinimums[2];
                }
                else
                {
                    Et -=  p1->second[0] * Rates[ incomp ][2]; // if incomp is present in Pxxs, we know it is present in Rates
                    Pt -=  Rates[ incomp ][2];
                }             
                DivFac -= 1;   
            }
            //cout << "Et " << Et << " " << "Pt " << Pt << " DivFac " << DivFac <<  endl;            
            if( DivFac != 0 )
            {
                Et /= DivFac;
                Pt /= DivFac;
            }
            else
            {
                Et = 0;
                Pt = 0;                
            }

            //cout << "Et " << Et << " " << "Pt " << Pt << endl;
            //computing new rates
            vector<long double> localRates = MAX_estimateNewRates_DTL( it->second[0] , it->second[1] , it->second[2] , ExtinctInDescendants[ it->first ] , Et, Pt );

            //replacing the old rates by the new ones.
            for(unsigned i = 0 ; i < 3 ; i++)
            {
                long double r = localRates[ i ];
                if( ( r == -1 ) || ( r > RateDefaultMaximums[i] ) )
                    r = RateDefaultMaximums[i];
                else if( ( r == -2 ) || ( r <  RateDefaultMinimums[i] ) )
                    r = RateDefaultMinimums[i];

                long double oldr = Rates[it->first][i];

                SumDiffs = fabs( oldr - r ) / min( r , oldr ) ; 

                Rates[it->first][i] = r;
            }

        }

        //cout << "sum of diffs " << SumDiffs << endl;
        NbTurnTMP++;
        if( (NbTurnTMP > 10) || ( SumDiffs < 0.000001 ) )
            STOP = true;
    }

    double nbObservations = 0;

    for( auto it  = Rates.begin() ; it != Rates.end() ; ++it )
    {
        if(VerboseLevel)
            cout << "\tfinal rates " << it->first << " " << it->second[0] << "," << it->second[1] << "," << it->second[2] << endl;
        model->set_Rates(it->first, it->second);

        double nbObs = RateUpdateCount[it->first][0] + RateUpdateCount[it->first][1] + RateUpdateCount[it->first][2] + RateUpdateCount[it->first][3] ;

        for(unsigned i = 0 ; i < 3 ; i++)
            EventSums[i] += it->second[i] * nbObs ;
        nbObservations += nbObs;

    }

    if(VerboseLevel)
    {
        cout << "\tEventSums ";
        for(auto i : EventSums)
            cout << i << " ";
        cout << endl;
        cout << "\tnbobs " << nbObservations << endl;
    }
    return nbObservations;
}


/*
Formulas in mathematica:

Solve[{ Ee == Los + (1-(Dup+Tra+Los)) * Espe + Dup * Ee^2 + Etr * Ee , 
        Ge == (1-(Dup+Tra+Los)) * (1-Espe) + 2 * Dup * Ee * Ge + Ptr * Ge ,
        Te == Tra * (1-Ee) },{Dup,Los,Tra},Reals]//FullSimplify

           
                   (-1 + Ee) (1 + Ee (-1 + Etr) + Ge (-1 + Ptr)) + Te
Out[9]= {{Dup -> -(--------------------------------------------------), Los -> 
                                               2
                             (-1 + Ee) (-1 + Ee  + 2 Ee Ge)
 
                                                               2
       (-1 + Ee) (Ee (-1 + Espe) (-1 + Etr) + 2 Ee Espe Ge + Ee  (-1 + Espe + Ge (-1 + 2 Etr - Ptr)) + Espe Ge (-1 + Ptr)) + Ee (Ee (-1 + Espe) + 2 Espe Ge) Te             Te
>      --------------------------------------------------------------------------------------------------------------------------------------------------------, Tra -> -(-------)}}
                                                                                            2                                                                             -1 + Ee
                                                              (-1 + Ee) (-1 + Espe) (-1 + Ee  + 2 Ee Ge)




Solve[{0== (-1 + Ee) (-1 + Espe) (-1 + Ee^2  + 2 Ee Ge) },{Ee,Ge,Espe},Reals]//FullSimplify

--> Ee == 1
--> Espe == 1
--> Solve[{0== (-1 + Ee^2  + 2 Ee Ge) },{Ee,Ge},Reals]//FullSimplify




Pb conditions:
    Ee == 1 :
        T = 0
        L = 1 --> l = inf 
        D = 0

    Ee == 0 :
            Solve[{ 0 == Los + (1-(Dup+Tra+Los)) * Espe , 
                    Ge == (1-(Dup+Tra+Los)) * (1-Espe) +  Ptr * Ge ,
                    Te == Tra * (1) },{Dup,Los,Tra},Reals]//FullSimplify

        /// /// Dup ->  ( -1 + Ge - Ge Ptr - Te + Espe (1 + Ge - Ge Ptr + Te) ) / ( Espe - 1)
        /// /// Los -> Espe Ge (-1 + Ptr) / ( 1-Espe ) 
        /// /// Tra -> Te

        //further force Los = 0
                    Solve[{ Ge == (1-(Dup+Tra)) * (1-Espe) +  Ptr * Ge},{Dup},Reals]//FullSimplify

                    Los = 0
                    Dup = ( 1 - Espe - Ge + Ge Ptr ) / ( 1 - Espe ) - Te
                    Tra = Te

    Ge == 0:

            Solve[{ Ee == Los + (1-(Dup+Tra+Los)) * Espe + Dup * Ee^2 + Etr * Ee , 
                    0 == (1-(Dup+Tra+Los)) * (1-Espe) ,
                    Te == Tra * (1-Ee) },{Dup,Los,Tra},Reals]//FullSimplify
            
            
                      1 + Ee (-2 + Ee + Etr - Ee Etr) - Te         Ee ((-1 + Ee) (-1 + Ee + Etr) + Ee Te)             Te
            {{Dup -> ------------------------------------, Los -> --------------------------------------, Tra -> -(-------)}}
                                      2                                             2                              -1 + Ee
                             (-1 + Ee)  (1 + Ee)                           (-1 + Ee)  (1 + Ee)

            ----> careful, that might have D+T+L == 1 ? 

    Ge == 0 && Ee == 0:
            Solve[{ 0 == (1-(Dup+Tra)) * (1-Espe)},{Dup},Reals]//FullSimplify

            Dup = 1 - Te
            Tra = Te

            --> !! D+T+L == 1

                


////    Espe == 1 does not necesastily implies that Ge == 0 in case of a TL in each children !!
////        --> do as if Ge == 0



Solve[{ Los == (l/(1+d+t+l)) , 
        Dup == (d/(1+d+t+l)) ,
        Tra == (t/(1+d+t+l)) } , {d,l,t} , Reals]//FullSimplify



                 Dup                           Los                           Tra
 {{d -> -(--------------------), l -> -(--------------------), t -> -(--------------------)}}
          -1 + Dup + Los + Tra          -1 + Dup + Los + Tra          -1 + Dup + Los + Tra


Takes:
    - long double Ee : probability of going exting (p10)
    - long double Ge : probability of simple transmission (p11)
    - long double Te : probability of apparition (p0x)
    - long double Espe : probability of going exting in descendants
    - long double Etr : probability that a transfer is received in another species and subsequently goes extinct
    - long double Ptr: probability that a transfer is received in another species

Returns:
    vector<long double> : rates of duplication, loss and transfer
                            -1 -> assign max value
                            -2 -> assign min value 
*/
vector<long double> MAX_estimateNewRates_DTL( long double Ee , long double Ge , long double Te , long double Espe , long double Etr , long double Ptr )
{
    //cout << "\t\tEe " << Ee ; 
    //cout << "\tGe " << Ge ; 
    //cout << "\tTe " << Te ; 
    //cout << "\tEspe " << Espe ; 
    //cout << "\tEtr " << Etr ; 
    //cout << "\tPtr " << Ptr << endl; 



    vector<long double> newRates(3,0);

    long double Dup;
    long double Los;
    long double Tra;

    if( Ge == 1 )
    {
        //T = 0 --> t = 0
        //L = 1 --> l = 0 
        //D = 0 --> d = 0
        //cout << "\t\t--> special case Ge == 1" << endl;
        newRates[0] = -2;
        newRates[1] = -2;
        newRates[2] = -2;
        return newRates;
    }
    else if( ( Espe == 1 ) && ( Ge != 0 ) )
    { // see function description for this case ; force Ge to 0 for this case.
        long double newTotal = 1 - Ge;
        Ee /= newTotal;
        Te /= newTotal;
        Ge = 0;
    }


    if( Ee == 1 )
    {
        //T = 0 --> t = 0
        //L = 1 --> l = inf 
        //D = 0 --> d = 0
        //cout << "\t\t--> special case Ee == 1" << endl;
        newRates[0] = -2;
        newRates[1] = -1;
        newRates[2] = -2;
        return newRates;
    }

    if( Ee == 0 )
    {
        //cout << "\t\t--> special case Ee == 0" << endl;
        Los = 0;
        if( Ge != 0)
            Dup = ( 1 - Espe - Ge + Ge * Ptr ) / ( 1 - Espe ) - Te;
        else
            Dup = 1 - Te;
        Tra = Te;

    }
    else if( Ge == 0)
    {
        //cout << "\t\t--> special case Ge == 0" << endl;

        Dup = ( 1 + Ee * (-2 + Ee + Etr - Ee * Etr) - Te ) / ( pow( (-1 + Ee) , 2 ) *  (1 + Ee) );
        Los = (Ee * ((-1 + Ee) * (-1 + Ee + Etr) + Ee * Te)) / ( pow( (-1 + Ee) , 2 ) *  (1 + Ee) );
        Tra = Te / ( 1 - Ee ) ;        

    }
    else
    {
        //cout << "\t\t--> normal case " << endl;

        long double div = (-1 + Ee) * (-1 + pow(Ee,2) + 2 * Ee * Ge);

        Dup = - ((-1 + Ee) *  (1 + Ee * (-1 + Etr) + Ge * (-1 + Ptr)) + Te) / (div ) ;

        Los = ((-1 + Ee) * (Ee * (-1 + Espe) * (-1 + Etr) + 2 * Ee * Espe * Ge + pow(Ee,2) * (-1 + Espe + Ge * (-1 + 2 * Etr - Ptr)) + Espe * Ge * (-1 + Ptr)) + Ee * (Ee * (-1 + Espe) + 2 * Espe * Ge) * Te) / (div * ( Espe - 1 ) ) ;


        Tra = Te / ( 1 - Ee ) ;
    }

    //cout << "\t\tDup " << Dup ;
    //cout << "\tLos " << Los ;
    //cout << "\tTra " << Tra << endl;

    long double Tot = ( 1 - ( Dup + Los + Tra ) );

    if( Tot != 0 )
    {
        newRates[0] = Dup / Tot;
        newRates[1] = Los / Tot;
        newRates[2] = Tra / Tot;
    }
    else
    {
        //cout << "\t\t--> special case D+T+L == 1 " << endl;
        newRates[0] = ( Dup <= 0 ? -2 : -1 );
        newRates[1] = ( Los <= 0 ? -2 : -1 );
        newRates[2] = ( Tra <= 0 ? -2 : -1 );
    }

    //cout << "\t\t\t-->d " << newRates[0] ;
    //cout << "\t-->l " << newRates[1] ;
    //cout << "\t-->t " << newRates[2] << endl;

    for(unsigned i = 0 ; i < 3 ; i++)
    {
        if( newRates[i] < 0 )
            newRates[i] = -2;
    }

    return newRates;
}

/*
    Takes:
        - ParentModel * model : will be updated
        - map < int , vector<long double> > &RateUpdateCount : 8 counts
                                                               the transition from :
                                                                                   1 lineage to 0
                                                                                   1 lineage to 1
                                                                                   1 lineage to many
                                                                                   0 lineage to >0
                                                               the counts of :
                                                                                   duplications
                                                                                   losses
                                                                                   transfer receptions
                                                                                   speciations        
        - vector < long double > &EventSums : will get filled 
        - vector < long double > RateDefaultMinimums
        - vector < long double > RateDefaultMaximums

    Returns:
        (double) : total number of observations

*/
double MAX_estimateNewRates_DTL(  ParentModel * model , 
                                  map < int , vector<long double> > &RateUpdateCount , 
                                  vector < long double > &EventSums,
                                  vector < long double > RateDefaultMinimums, 
                                  vector < long double > RateDefaultMaximums )
{
    // we want to correct the counts that don't account for events that have been lost
    // for instance, a duplication that has had one of its children loss is not sampled as such, and so not counted.
    // the present correction is useful but not perfect as it only consider "first order" losses.

    // here, p10 serves as the estimator or E, the probability for a lineage to be lost

    map < boost::dynamic_bitset<> , long double > EstimatedExtinctionProbas  ; 

    for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
    {
        boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );
        long double nbObservations = it->second[0] + it->second[1] + it->second[2] + it->second[3] ;

        //for(auto i : it->second)
        //    cout << i << " ";
        //cout << endl;
        if(nbObservations>0)
            EstimatedExtinctionProbas[sigma] = it->second[0] / nbObservations ;
        else
            EstimatedExtinctionProbas[sigma] = 0; // bit of a firty fix here
    }


    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = model->get_speciesCCPdistribution();
    map < int , vector<long double> > correctedCounts ;

    for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
    {
        correctedCounts[ it->first ] = vector<long double>(4,0) ;
        correctedCounts[ it->first ][0] = it->second[4];//D
        correctedCounts[ it->first ][1] = it->second[5];//L
        correctedCounts[ it->first ][2] = it->second[6];//T
        correctedCounts[ it->first ][3] = it->second[7];//S

        boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );

        long double p10 = EstimatedExtinctionProbas[ sigma ];

        if(p10 == 1 )
            continue; //otherwise we'll produce nans. and if p10 == 1 -> we'd go from 0 to 0 anyway

        //cout << "p10 " << p10 << endl;

        //cout << " correcting count of dup " << correctedCounts[ it->first ][0] ;

        correctedCounts[ it->first ][0] /= ( pow( (1-p10) , 2 ) );
        // or, to put it another way, #sampled dups = #real dups  * (1-E)^2 (ie, we sample the duplications only if neither children have been directly lost )

        //cout << " -> " << correctedCounts[ it->first ][0] << endl;

        //cout << " correcting count of tr " << correctedCounts[ it->first ][2] ;

        correctedCounts[ it->first ][2] /= ( (1-p10) );        
        // A transfer isn't sampled iif the copy that stays in this species is lost.

        //cout << " -> " << correctedCounts[ it->first ][2] <<endl;

        //correction of losses:
        correctedCounts[ it->first ][1] += correctedCounts[ it->first ][0] * ( p10 * (1-p10)*2 + 2 * p10*p10);
        //we add losses for each duplications which weren't sampled (1 per lost child)
        correctedCounts[ it->first ][1] += correctedCounts[ it->first ][2] * p10 ;
        //we add losses for each transfer reception which weren't sampled

        // A speciation isn't sampled iif it is lost in both children.
        long double doubleExtinctionProbas = 0;

        auto END = SpeciesCCPDistrib->tripartition_cend( sigma );
        for(auto sigmaPpointer = SpeciesCCPDistrib->tripartition_cbegin( sigma ); sigmaPpointer != END ; ++sigmaPpointer )
        {
            long double e0 = 0;
            long double e1 = 0;
            
            auto ch0 = EstimatedExtinctionProbas.find(sigmaPpointer->first);
            auto ch1 = EstimatedExtinctionProbas.find( SpeciesCCPDistrib->get_complementer( sigma , sigmaPpointer->first ) );
            
            if( ch0 != EstimatedExtinctionProbas.end() )
                e0 = ch0->second;
            if( ch1 != EstimatedExtinctionProbas.end() )
                e1 = ch1->second;

            doubleExtinctionProbas += e0*e1*sigmaPpointer->second;


        }

        //cout << " correcting count of spe " << correctedCounts[ it->first ][3] ;
        correctedCounts[ it->first ][3] /= (1-doubleExtinctionProbas);
        //cout << " -> " << correctedCounts[ it->first ][3] << endl;
    }

    // second round to correct the loss count in the children of unsampled speciations
    for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
    {
        boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );

        auto END = SpeciesCCPDistrib->tripartition_cend( sigma );
        for(auto sigmaPpointer = SpeciesCCPDistrib->tripartition_cbegin( sigma ); sigmaPpointer != END ; ++sigmaPpointer )
        {
            long double e0 = 0;
            long double e1 = 0;
            
            auto ch0 = EstimatedExtinctionProbas.find(sigmaPpointer->first);
            auto ch1 = EstimatedExtinctionProbas.find( SpeciesCCPDistrib->get_complementer( sigma , sigmaPpointer->first ) );
            
            if( ch0 != EstimatedExtinctionProbas.end() )
                e0 = ch0->second;
            if( ch1 != EstimatedExtinctionProbas.end() )
                e1 = ch1->second;

            long double correction = e0*e1*sigmaPpointer->second * correctedCounts[ it->first ][3];

            if( correction != 0 )
            { // it is possible to be lost in both children
                correctedCounts[ model->get_CCPindexFromSpeciesCladeIndex( ch0->first ) ][1] += correction; 
                correctedCounts[ model->get_CCPindexFromSpeciesCladeIndex( ch1->first ) ][1] += correction; 
            }
        }
    }

    double sumObservations = 0;
    // now that we have corrected counts of events, we can infer rates
    for(auto it = correctedCounts.begin() ; it != correctedCounts.end() ; ++it)
    {

        vector< long double > rates = MAX_estimateNewRates_DTL( it->second[0] , it->second[1] , it->second[2] , it->second[3] );
        
        for(unsigned int i = 0 ; i < rates.size(); i++ )
        {
            if( rates[i] == -1 )
                rates[i] = RateDefaultMaximums[i];
            else if( rates[i] == -2 )
                rates[i] = RateDefaultMinimums[i];
        }
        model->set_Rates( it->first , rates );
            
        double nbObs = (it->second[0] + it->second[1] + it->second[2] + it->second[3] );
        for(unsigned int i = 0 ; i < EventSums.size(); i++ )
        {
            EventSums[i] += it->second[i];
        }
        sumObservations += nbObs;
    }
    return sumObservations;
} // deprecated

/*
    Takes:
        - ParentModel * model : will be updated
        - map < int , vector<long double> > &RateUpdateCount
        - vector < long double > &EventSums : will get filled 
        - bool withTransfer 

    Returns:
        (double) : total number of observations
*/
double MAX_estimateNewRates( ParentModel * model , 
                             map < int , vector<long double> > &RateUpdateCount ,
                             vector < long double > &EventSums , 
                             bool withTransfer,
                             vector < long double > RateDefaultMinimums, 
                             vector < long double > RateDefaultMaximums)
{
    double sumObservations = 0;

    if(withTransfer)
    {
        //sumObservations = MAX_estimateNewRates_DTL( model, RateUpdateCount , EventSums , RateDefaultMinimums, RateDefaultMaximums);// <-- old way to estimate rates
        sumObservations = MAX_estimateNewRates_DTL( model , RateUpdateCount , EventSums ,RateDefaultMinimums , RateDefaultMaximums ,0);

    }
    else
    {

        for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
        {
    
            boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );
            vector< long double > rates;
    
            rates = MAX_estimateNewRates_DL( model , sigma, it->second );
            ///handling particular cases 
            for(unsigned int i = 0 ; i < rates.size(); i++ )
            {
                if( rates[i] == -1 )
                    rates[i] = RateDefaultMaximums[i];
                else if( rates[i] == -2 )
                    rates[i] = RateDefaultMinimums[i];
            }
            /*
            if((VerboseLevel>1)&&(BranchWiseRates))
            {
                cout << " sp: "<< it->first << " -> D:" << rates[0] << " L:"<< rates[1] ;
                if(WithTransfer)
                    cout << " T:" << rates[2];
                cout <<endl;
            }
            */
            model->set_Rates( it->first , rates );
        
            double nbObs = (it->second[0] + it->second[1] + it->second[2]);
            for(unsigned int i = 0 ; i < EventSums.size(); i++ )
            {
                EventSums[i] += nbObs * rates[i];
            }
            sumObservations += nbObs;
    
        }
    }
    return sumObservations;
}




/*
    Draws new rate using a normal law 
    uses log(<oldRate>) as mean and returns the exponential of sampled value.  
*/
double SAMPLE_drawNewRate( double oldRate , double sd )
{
    return exp( Normal( log(oldRate) ,sd ) );
}

/*
    Draws new rate using a normal law of mean <oldRate> and standard deviation <sd>.
    If useLog is true, use as log(<oldRate>) mean and returns the exponential of sampled value.  
*/
double SAMPLE_drawNewRate( double oldRate , double sd , double min , double max  )
{
    assert(max>min);
    double r = SAMPLE_drawNewRate(oldRate,sd);
    while( (r<min) || (r>max) )
        r = SAMPLE_drawNewRate(oldRate,sd);
    return r;

}

/* proba to accept new = min ( newP/currentP , 1 )*/
bool SAMPLE_acceptProposition(long double currentLogP , long double newLogP )
{
    long double ratio = exp( newLogP - currentLogP )  ;

    if(ratio >1)
        return true;

    return (  Uniform0to1() < (double) ratio );
}