/*

This file defines a class representing the the PsDTL model 
of evolution of a gene CCP distribution inside a species CCP distribution
with events of duplication, transfer, loss and speciation.


Created the: 20-02-2018
by: Wandrille Duchemin

Last modified the: 07-05-2018
by: Wandrille Duchemin

*/

#include "PsDTLModel.h"

// protected methods


/*
    Update the TransferProbas attribute

    Takes:
        - CIndexType Gid : id of a gene clade
*/
void PsDTLModel::computeTransferProbas(CIndexType Gid)
{
    FLOAT_TYPE base = SumTransferProba[Gid]; // this is the non normalized sum
    int nbSpClades = speciesCladeIndexToCCPid.size();

    for(unsigned Sid = 1 ; Sid < nbSpClades; Sid++) // 0 is a dummy clade
    {
        //NEWNORM
        FLOAT_TYPE tmp = base;
        int DivFac = nbSpClades - ListTransferIncompatibilities[ Sid ].size();
        for( const CIndexType& it : ListTransferIncompatibilities[ Sid ] )
        {
            tmp -= ReconciliationMatrix[it][Gid] * TransferPropensities[it] ;
            //if(verboseLevel>2)
            //    cout << Sid << " accounting for incomp with " << it << " -> -" << correction << endl; 
        }
        if( DivFac != 0 )
            TransferProbas[Sid][Gid] = tmp/DivFac ;
        else
            TransferProbas[Sid][Gid] = 0 ;
        //cout << "TransferProbas(" << Sid << "," << Gid << ")-> " << TransferProbas[Sid][Gid] << endl;
    }
}




/*
    Takes:
        - CIndexType Sid : species clade Id
        - CIndexType Gid : gene clade Id
    Returns:
        (FLOAT_TYPE) : probability of a Speciation event
*/
FLOAT_TYPE PsDTLModel::computeProbaSpeciation(CIndexType Sid, CIndexType Gid )
{
    FLOAT_TYPE Ps = 0;

    //vector< pair< CIndexType, CIndexType > > SpeciesIndexV ; 
    //vector< FLOAT_TYPE > SpeciesProbaV ;
    //fillVccp( Sid , SpeciesIndexV , SpeciesProbaV , true );
    //
    //vector< pair< CIndexType, CIndexType > > GeneIndexV ; 
    //vector< FLOAT_TYPE > GeneProbaV ;
    //fillVccp( Gid , GeneIndexV , GeneProbaV , false );

    for( unsigned s = 0 ; s < SpeciesProbaV[ Sid ].size() ; s++ )
    {
        CIndexType CIP = SpeciesIndexV[ Sid ][s].first;
        CIndexType CIPP = SpeciesIndexV[ Sid ][s].second;
        
        FLOAT_TYPE tmpPs = 0;

        //speciation and Loss cases
        tmpPs += ReconciliationMatrix[ CIP  ][Gid] * ProbaGeneExtinct[ CIPP ];
        tmpPs += ReconciliationMatrix[ CIPP ][Gid] * ProbaGeneExtinct[ CIP  ];

        for( unsigned g = 0 ; g < GeneProbaV[ Gid ].size() ; g++ )
        {
            //speciation cases
            tmpPs += GeneProbaV[ Gid ][g] * ReconciliationMatrix[ CIPP ][ GeneIndexV[ Gid ][g].first  ] * ReconciliationMatrix[ CIP ][ GeneIndexV[ Gid ][g].second ];
            tmpPs += GeneProbaV[ Gid ][g] * ReconciliationMatrix[ CIPP ][ GeneIndexV[ Gid ][g].second ] * ReconciliationMatrix[ CIP ][ GeneIndexV[ Gid ][g].first  ];

        }

        Ps += ( tmpPs * SpeciesProbaV[ Sid ][s]);
    }
    return Ps * SpeciationPropensities[Sid];
}
/*
    Takes:
        - CIndexType Sid : species clade Id
        - CIndexType Gid : gene clade Id
    Returns:
        (FLOAT_TYPE) : probability of a Duplication event
*/
FLOAT_TYPE PsDTLModel::computeProbaDuplication(CIndexType Sid, CIndexType Gid )
{
    FLOAT_TYPE Pd =0; 
    if(is_GeneLeaf(Gid))
        return Pd; // no need for further computation if the gene clade cannot split

    /*
    CCPcladeIdType gamma = geneCladeIndexToCCPid[ Gid ];

    FLOAT_TYPE bip = geneCCPdistribution->get_count_of_bipartition( gamma );

    auto END = geneCCPdistribution->tripartition_cend( gamma );
    for(auto gammaPpointer = geneCCPdistribution->tripartition_cbegin( gamma ); gammaPpointer != END ; ++gammaPpointer )
    {      
        FLOAT_TYPE pCCP = gammaPpointer->second/bip; //conditional probability of this split

        CCPcladeIdType gammaPP = geneCCPdistribution->get_complementer(gamma, gammaPpointer->first); //getSisterClade(gamma, *gammaPpointer); //
                
        CIndexType GidP  = geneCCPidToCladeIndex[gammaPpointer->first];
        CIndexType GidPP = geneCCPidToCladeIndex[gammaPP];
        
        Pd += pCCP * ReconciliationMatrix[Sid][GidP] * ReconciliationMatrix[Sid][GidPP] ; // pCCP times the proba to have both children in the same species at the same time
    }*/
    for( unsigned g = 0 ; g < GeneProbaV[ Gid ].size() ; g++ )
    {
        Pd += GeneProbaV[ Gid ][g] * ReconciliationMatrix[ Sid ][ GeneIndexV[ Gid ][g].first  ] * ReconciliationMatrix[ Sid ][ GeneIndexV[ Gid ][g].second ];
    }

    Pd *= DuplicationPropensities[Sid]  ;

    return Pd;
}

FLOAT_TYPE PsDTLModel::computeProbaDuplicationLoss(CIndexType Sid, CIndexType Gid )
{
    return DuplicationPropensities[Sid] * ProbaGeneExtinct[ Sid ] * ReconciliationMatrix[Sid][Gid];  // dependence on self, which warrants that we perform several iterations
}


/*
    Takes:
        - CIndexType Sid : species clade Id
        - CIndexType Gid : gene clade Id
    Returns:
        (FLOAT_TYPE) : probability of a Transfer event
*/
FLOAT_TYPE PsDTLModel::computeProbaTransfer(CIndexType Sid, CIndexType Gid )
{
    FLOAT_TYPE Pd =0; 
    if(is_GeneLeaf(Gid))
        return Pd; // no need for further computation if the gene clade cannot split
    /*
    CCPcladeIdType gamma = geneCladeIndexToCCPid[ Gid ];

    FLOAT_TYPE bip = geneCCPdistribution->get_count_of_bipartition( gamma );

    auto END = geneCCPdistribution->tripartition_cend( gamma );
    for(auto gammaPpointer = geneCCPdistribution->tripartition_cbegin( gamma ); gammaPpointer != END ; ++gammaPpointer )
    {      
        FLOAT_TYPE pCCP = gammaPpointer->second/bip; //conditional probability of this split

        CCPcladeIdType gammaPP = geneCCPdistribution->get_complementer(gamma, gammaPpointer->first); //getSisterClade(gamma, *gammaPpointer); //
                
        CIndexType GidP  = geneCCPidToCladeIndex[gammaPpointer->first];
        CIndexType GidPP = geneCCPidToCladeIndex[gammaPP];
        
    }*/
    for( unsigned g = 0 ; g < GeneProbaV[ Gid ].size() ; g++ )
    {
        Pd += GeneProbaV[ Gid ][g] * ReconciliationMatrix[Sid][ GeneIndexV[ Gid ][g].first ]  * TransferProbas[Sid][ GeneIndexV[ Gid ][g].second ]  ;
        Pd += GeneProbaV[ Gid ][g] * ReconciliationMatrix[Sid][ GeneIndexV[ Gid ][g].second ] * TransferProbas[Sid][ GeneIndexV[ Gid ][g].first ]   ;
    }
    //cout << "probatransfer " << Sid << "," << Gid << "->" << Pd;

    return Pd;
}
/*
    Takes:
        - CIndexType Sid : species clade Id
        - CIndexType Gid : gene clade Id
    Returns:
        (FLOAT_TYPE) : probability of a TransferLoss event
*/
FLOAT_TYPE PsDTLModel::computeProbaTransferLoss(CIndexType Sid, CIndexType Gid )
{
    //event in transfer receiver

    FLOAT_TYPE ProbaLT = SumTransferExtinctionProba;    // loss in the transfer receiver
    FLOAT_TYPE ProbaTL = SumTransferProba[Gid];         // loss in self ; dependence on self, which warrants that we perform several iterations
    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^ these are non normalized sums //NEWNORM


    int nbSpClades = speciesCladeIndexToCCPid.size() - ListTransferIncompatibilities[ Sid ].size();

    // accounting for transfer incompatibilities between species
    FLOAT_TYPE correctionTL = 0;
    FLOAT_TYPE correctionLT = 0;
    for( const CIndexType& it : ListTransferIncompatibilities[ Sid ] )
    {
        correctionLT -= ProbaGeneExtinct[it] * TransferPropensities[it] ;
        correctionTL -= ReconciliationMatrix[it][Gid] * TransferPropensities[it] ;
    }

    if(nbSpClades>0)
    {
        ProbaLT -= correctionLT;
        ProbaLT /= nbSpClades;
        ProbaTL -= correctionTL;
        ProbaTL /= nbSpClades;   
    }
    else
    { // no species to receive a transfer -> proba Tl + LT == 0
        return 0;
    }

    // event in Sid

    ProbaLT *= ReconciliationMatrix[Sid][Gid];  // dependence on self, which warrants that we perform several iterations
    ProbaTL *= ProbaGeneExtinct[Sid]; // loss


    return ProbaTL + ProbaLT;
}


FLOAT_TYPE PsDTLModel::ComputerecProba( CIndexType Sid, CIndexType Gid )
{
    FLOAT_TYPE P = 0;

    if( ( is_SpeciesLeaf(Sid) ) && (is_GeneLeaf(Gid)) )
    { // both leaves at length 0 -> leaf function
        P += computeProbaLeafExtremity(  Sid ,  Gid );
    }
    else
    {
        P += computeProbaSpeciation( Sid , Gid );
    }

    P += computeProbaDuplication( Sid , Gid );
    P += computeProbaTransfer( Sid , Gid );

    P += computeProbaTransferLoss( Sid , Gid );
    P += computeProbaDuplicationLoss( Sid , Gid );
    
    return P;
}


/*
!!recursive function!!

    Takes:
        - CIndexType Sid : id of the species clade
        - CIndexType Gid : id of the gene clade

    Returns:
        ( shared_ptr<RecNode> ): backtracked reconciled subtree

*/
shared_ptr<RecNode> PsDTLModel::backtrackAux( CIndexType Sid, CIndexType Gid)
{
    shared_ptr<RecNode> currentRec;

    FLOAT_TYPE total = ReconciliationMatrix[Sid][Gid];

    FLOAT_TYPE r = ((long double) rand()/ RAND_MAX) * total; //random result

    //cout << "BT " << Sid << "," << Gid;
    //cout << " : " <<  total ;
    //cout <<" -> " << r << endl;

    FLOAT_TYPE Remind = r ; // to remember the original r that was chosen.

    r = backtrackAuxSpeciation( Sid, Gid,  currentRec, r);
    if(r<0)
    {
        //cout << "\t spe" << r << "/" << total << endl;
        return currentRec;
    }


    r = backtrackAuxDuplication( Sid, Gid,  currentRec, r);
    if(r<0)
    {
        //cout << "\t dup" << r << "/" << total << endl;
        return currentRec;        
    }


        //cout << "\tno dup" << r << "/" << total << endl;

    r = backtrackAuxTransfer( Sid, Gid, currentRec, r);
    if(r<0)
    {
        //cout << "\t tra" << r << "/" << total << endl;
        return currentRec;
    }

        //cout << "\tno tra" << r << "/" << total << endl;

    r = backtrackAuxTransferLoss( Sid, Gid, currentRec, r);
    if(r<0)
    {
        //cout << "\t tl " << r << "/" << total << endl;
        return currentRec;
    }

        //cout << "\tno tl " << r << "/" << total << endl;

    // at this point, if r > 0 it means that a "Null" case have been chosen : either "Duplication and loss of a copy" or "transfer and loss of transfered"
    // to avoid potentially long loop of repeating these instance, I will now draw inside the space of non-null Event.

    total = Remind - r ; //<- this is the size of the non-null solution space == what was substracted to r

    r = ((long double) rand()/ RAND_MAX) * total; //random result
    //cout << "BT remind" << r << "/" << total << endl;

    r = backtrackAuxSpeciation( Sid, Gid,  currentRec, r);
    if(r<=0)
    {
        //cout << "\t spe" << r << "/" << total << endl;
        return currentRec;
    }

        //cout << "\tno spe" << r << "/" << total << endl;

    r = backtrackAuxDuplication( Sid, Gid,  currentRec, r);
    if(r<=0)
    {
        //cout << "\t dup" << r << "/" << total << endl;
        return currentRec;
    }

        //cout << "\tno dup" << r << "/" << total << endl;

    r = backtrackAuxTransfer( Sid, Gid, currentRec, r);
    if(r<=0)
    {
        //cout << "\t tra" << r << "/" << total << endl;
        return currentRec;        
    }

        //cout << "\tno tra" << r << "/" << total << endl;

    r = backtrackAuxTransferLoss( Sid, Gid, currentRec, r);
    if(r<=0)
    {
        //cout << "\t tl " << r << "/" << total << endl;
        return currentRec;        
    }

        //cout << "\tno tl " << r << "/" << total << endl;


    cerr << "ERROR : error during backtrack of gene " << Gid << " in species " << Sid << ". Did not sample a valid solution." << endl;
    cerr << "rolled:" << r << " on a total of :" << total << ", may it be an underflow? (" << "compare to the limit of long doubles : " << std::numeric_limits<long double>::min() << ")" << endl;
    exit(3);
}

/*
    Takes:
        - CIndexType Sid : species index
        - CIndexType Gid : gene index
        - shared_ptr<RecNode> currentRec : pointer onto which the tree is built
        - double r : chosen solution "cumulative proba"

    Returns:
        (double) chosen solution "cumulative proba" - probability of speciation events
                 if < 0, then currentRec was updated
*/
long double PsDTLModel::backtrackAuxSpeciation( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> & currentRec, long double r)
{
    bool speciesLeaf = is_SpeciesLeaf(Sid);
    bool geneLeaf = is_GeneLeaf(Gid);

    CCPcladeIdType sigma = speciesCladeIndexToCCPid[Sid];
    CCPcladeIdType gamma = geneCladeIndexToCCPid[Gid];

    if(speciesLeaf)
    {
        if((geneLeaf)&&(geneSpeciesCorrespondence[ Gid ] == Sid))
        {
            r-= SpeciationPropensities[ Sid ]; // correct species => proba is propensity to speciate.
     
            if(r<0)
            {
                currentRec = make_shared< RecNode > (Gid, Sid,0) ;
                currentRec->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );
                currentRec->set_event("leaf");
                return r; //end of the recursion
            }
        }
        else // species leaf -> no speciation possible
            return r;
    }


    FLOAT_TYPE bipS = speciesCCPdistribution->get_count_of_bipartition( sigma );

    CIndexType Sid1 = 0;
    CIndexType Sid2 = 0;
    string event = "";
    CIndexType Gid1 = 0;
    CIndexType Gid2 = 0;

    auto END_S = speciesCCPdistribution->tripartition_cend(sigma);
    for(auto Sit = speciesCCPdistribution->tripartition_cbegin(sigma) ; Sit != END_S; ++Sit)
    {

        FLOAT_TYPE spCCP = Sit->second /bipS;

        CIndexType SidP  = speciesCCPidToCladeIndex[ Sit->first ];
        CIndexType SidPP = speciesCCPidToCladeIndex[ speciesCCPdistribution->get_complementer( sigma, Sit->first ) ];


        //speciation Loss cases
        r -= ReconciliationMatrix[SidP][Gid] * ProbaGeneExtinct[SidPP] * SpeciationPropensities[Sid] * spCCP; 
        if(r<0)
        {
            Sid1 = SidP;
            Sid2 = SidPP;
            Gid1 = Gid;
            event = "SL";
            break;            
        }

        r -= ReconciliationMatrix[SidPP][Gid] * ProbaGeneExtinct[SidP] * SpeciationPropensities[Sid] * spCCP; 
        if(r<0)
        {
            Sid1 = SidPP;
            Sid2 = SidP;
            Gid1 = Gid;
            event = "SL";
            break;            
        }


        //speciation cases
        if(geneLeaf) // no speciation cases for gene leaf clades
            continue;
        
        FLOAT_TYPE bipG = geneCCPdistribution->get_count_of_bipartition( gamma );
        auto END_G = geneCCPdistribution->tripartition_cend(gamma);
        for(auto Git = geneCCPdistribution->tripartition_cbegin(gamma) ; Git != END_G ; ++Git)
        {

            FLOAT_TYPE gpCCP = Git->second / bipG;

            CIndexType GidP  = geneCCPidToCladeIndex[ Git->first ];
            CIndexType GidPP = geneCCPidToCladeIndex[ geneCCPdistribution->get_complementer( gamma, Git->first ) ];


            r -= ReconciliationMatrix[SidP ][GidP ] * ReconciliationMatrix[SidPP][GidPP] * SpeciationPropensities[Sid] * spCCP * gpCCP;
            if(r<0)
            {
                Sid1 = SidP;
                Sid2 = SidPP;
                Gid1 = GidP;
                Gid2 = GidPP;
                event= "S";
                break;
            }

            r -= ReconciliationMatrix[SidP ][GidPP] * ReconciliationMatrix[SidPP][GidP ] * SpeciationPropensities[Sid] * spCCP * gpCCP;
            if(r<0)
            {
                Sid1 = SidP;
                Sid2 = SidPP;
                Gid1 = GidPP;
                Gid2 = GidP;
                event= "S";
                break;
            }

        }
        if(r<0)
            break;
    }

    if(r<0)
    {
        ////creation of return Node
        currentRec = make_shared< RecNode > (Gid, Sid,0);
        currentRec->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );
        currentRec->set_event("speciation");
    
        //cout << Gid <<" " << Sid<<"-> " << Sid1<<","<<Gid1 << " " << Sid2<<","<<Gid2 << endl;
        currentRec->add_child( backtrackAux( Sid1, Gid1 ) ); //always
    
        if(event == "SL")
        { // in case of a loss
            currentRec->add_child( createLossNode( Sid2 ) );
        }
        else
        {
            currentRec->add_child( backtrackAux( Sid2, Gid2 ) ); 
        }
    }


    return r; //end of the recursion

}


/*
    Returns:
        ( shared_ptr<RecNode> ): pointer to a single node with a loss event and gene clade -1

*/
shared_ptr<RecNode> PsDTLModel::createLossNode( CIndexType Sid )
{
    shared_ptr<RecNode> lossNode ( new RecNode(-1, Sid,0) );
    //cout << "add BL later"<<endl;//dupNode->set_BranchLength( geneCCPdistribution->get_branch_length(gamma) );
    lossNode->set_event("loss");
    return lossNode;
}


/*
    Takes:
        - CIndexType Sid : species index
        - CIndexType Gid : gene index
        - shared_ptr<RecNode> currentRec : pointer onto which the tree is built
        - double r : chosen solution "cumulative proba"

    Returns:
        (double) chosen solution "cumulative proba" - probability of duplication events
                 if < 0, then currentRec was updated
*/
long double PsDTLModel::backtrackAuxDuplication( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> & currentRec, long double r)
{
    if(is_GeneLeaf(Gid))
        return r; // no possible diplication if the gene is a leaf 
    CCPcladeIdType gamma = geneCladeIndexToCCPid[Gid];

    FLOAT_TYPE bipG = geneCCPdistribution->get_count_of_bipartition( gamma );

    int Gid1 = 0;
    int Gid2 = 0;

    auto END_G = geneCCPdistribution->tripartition_cend(gamma);
    for(auto Git = geneCCPdistribution->tripartition_cbegin(gamma) ; Git != END_G ; ++Git)
    {
        FLOAT_TYPE gpCCP = Git->second / bipG;
        CIndexType GidP  = geneCCPidToCladeIndex[ Git->first ];
        CIndexType GidPP = geneCCPidToCladeIndex[ geneCCPdistribution->get_complementer( gamma, Git->first ) ];

        r -= ReconciliationMatrix[Sid][GidP ] * ReconciliationMatrix[Sid][GidPP] * DuplicationPropensities[Sid] * gpCCP;
        if(r<0)
        {
            Gid1 = GidP;
            Gid2 = GidPP;
            break;
        }
    }

    if(r<0)
    {
        currentRec = make_shared< RecNode >(Gid, Sid, 0);
        currentRec->set_event( "duplication" );
        currentRec->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );

        currentRec->add_child( backtrackAux(Sid , Gid1) );
        currentRec->add_child( backtrackAux(Sid , Gid2) );
    }

    return r;
}


/*
    Takes:
        - CIndexType Sid : species index
        - CIndexType Gid : gene index
        - shared_ptr<RecNode> currentRec : pointer onto which the tree is built
        - double r : chosen solution "cumulative proba"

    Returns:
        (double) chosen solution "cumulative proba" - probability of transfer events
                 if < 0, then currentRec was updated
*/
long double PsDTLModel::backtrackAuxTransfer( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> & currentRec, long double r)
{
    if(is_GeneLeaf(Gid))
        return r; // no possible diplication if the gene is a leaf 
    CCPcladeIdType gamma = geneCladeIndexToCCPid[Gid];

    FLOAT_TYPE bipG = geneCCPdistribution->get_count_of_bipartition( gamma );

    int Gid1 = 0;
    int Gid2 = 0;

    int destinationSpecies;

    auto END_G = geneCCPdistribution->tripartition_cend(gamma);
    for(auto Git = geneCCPdistribution->tripartition_cbegin(gamma) ; Git != END_G ; ++Git)
    {
        FLOAT_TYPE gpCCP = Git->second / bipG;
        CIndexType GidP  = geneCCPidToCladeIndex[ Git->first ];
        CIndexType GidPP = geneCCPidToCladeIndex[ geneCCPdistribution->get_complementer( gamma, Git->first ) ];


        r -= gpCCP * ReconciliationMatrix[Sid][GidP ]  * TransferProbas[Sid][GidPP] ;
        if(r<0)
        {
            Gid1 = GidP;
            Gid2 = GidPP;
            destinationSpecies = findTransferDestinationSpecies( Sid , GidPP );
            break;
        }

        r -= gpCCP * ReconciliationMatrix[Sid][GidPP] * TransferProbas[Sid][GidP ] ;
        if(r<0)
        {
            Gid1 = GidPP;
            Gid2 = GidP ;
            destinationSpecies = findTransferDestinationSpecies( Sid , GidP );
            break;
        }
    }

    if(r<0)
    {
        currentRec = make_shared< RecNode >(Gid, Sid, 0);
        currentRec->set_event( "branchingOut" );
        currentRec->set_BranchLength( geneCCPdistribution->get_branchlength(gamma) );

        currentRec->add_child( backtrackAux(Sid , Gid1) );

        shared_ptr<RecNode> TbNode = backtrackAux(destinationSpecies , Gid2) ;
        TbNode->set_Tb(true);

        currentRec->add_child( TbNode );
    }

    return r;
}


CIndexType PsDTLModel::findTransferDestinationSpecies(CIndexType Sid, CIndexType Gid )
{

    FLOAT_TYPE total = TransferProbas[Sid][Gid];

    int nbSpClades = speciesCladeIndexToCCPid.size();

    vector<bool> ProbaV(nbSpClades,true);

    for( const CIndexType& it : ListTransferIncompatibilities[ Sid ] )
    {
        ProbaV[it] = false;
    }

    CIndexType chosen = 0;
    FLOAT_TYPE r = ((double) rand()/ RAND_MAX) * total; //random result

    for( unsigned dest = 1 ; dest < nbSpClades ; dest++ )
    {
        if(ProbaV[dest])
            r-=  TransferPropensities[ dest ] * ReconciliationMatrix[dest][Gid] ;
        
        if(r<0)
        {
            chosen = dest;
            break;
        }
    }

    if(Sid==0)
    {
        cerr << "ERROR : could not find a transfer destination species for gene " << Gid << " from species " << Sid << endl;
        exit(4);
    }

    return chosen;
}


/*
    DOES NOT INCLUDE THE LOSS TRANSFER CASE (treated independenlty)

    Takes:
        - CIndexType Sid : species index
        - CIndexType Gid : gene index
        - shared_ptr<RecNode> currentRec : pointer onto which the tree is built
        - double r : chosen solution "cumulative proba"

    Returns:
        (double) chosen solution "cumulative proba" - probability of transfer events
                 if < 0, then currentRec was updated
*/
long double PsDTLModel::backtrackAuxTransferLoss( CIndexType Sid, CIndexType Gid, shared_ptr<RecNode> & currentRec, long double r)
{


    int nbSpClades = speciesCladeIndexToCCPid.size();

    vector<bool> ProbaV(nbSpClades,true);

    for( const CIndexType& it : ListTransferIncompatibilities[ Sid ] )
    {
        ProbaV[it] = false;
        nbSpClades -=1; //NEWNORM
    }

    if(nbSpClades == 0) //no valid species to receive the transfer --> proba of TL == 0 
        return r ;  

    CIndexType chosen = 0;

    FLOAT_TYPE normalizatorTL = ProbaGeneExtinct[Sid] /nbSpClades;
    for( unsigned dest = 1 ; dest < nbSpClades ; dest++ )
    {
        if(!ProbaV[dest])
            continue;
        
        r-= normalizatorTL * TransferPropensities[ dest ] * ReconciliationMatrix[ dest ][Gid] ;
        if(r<0)
        {
            chosen = dest;
            break;
        }
    }

    if(r<0)
    {
        currentRec = make_shared< RecNode >(Gid, Sid, 0);
        currentRec->set_event( "branchingOut" );
        currentRec->set_BranchLength( geneCCPdistribution->get_branchlength( geneCladeIndexToCCPid[ Gid ] ) );


        shared_ptr<RecNode> TbNode;

        currentRec->add_child( createLossNode(Sid) );
        TbNode = backtrackAux(chosen , Gid) ;

        TbNode->set_Tb(true);
        currentRec->add_child( TbNode );
    }

    return r;
}


/*
    Takes:
        - CIndexType Sid : id of the species clade

    Returns:
        (FLOAT_TYPE): probability of becoming extinct there

*/
FLOAT_TYPE PsDTLModel::computeExtinctionProba( CIndexType Sid, unsigned int verboseLevel)
{
    //loss
    FLOAT_TYPE E = LossPropensities[Sid] ;


    //speciation
    if( ! is_SpeciesLeaf( Sid ) ) // we are among the leaf clades
    {
        FLOAT_TYPE tmp = 0;
        //use the probability in the children clades
        CCPcladeIdType sigma = speciesCladeIndexToCCPid[ Sid ];

        FLOAT_TYPE bip = speciesCCPdistribution->get_count_of_bipartition( sigma );

        auto END = speciesCCPdistribution->tripartition_cend( sigma );
        for(auto sigmaPpointer = speciesCCPdistribution->tripartition_cbegin( sigma ); sigmaPpointer != END ; ++sigmaPpointer )
        {
            FLOAT_TYPE pCCP = sigmaPpointer->second/bip;
                
            CCPcladeIdType sigmaPP = speciesCCPdistribution->get_complementer(sigma, sigmaPpointer->first); //getSisterClade(sigma, *sigmaPpointer); // 
                   
            CIndexType CIP  = speciesCCPidToCladeIndex[sigmaPpointer->first];
            CIndexType CIPP = speciesCCPidToCladeIndex[sigmaPP];

            if(verboseLevel>2)
            {
                cout << "   " << Sid << "->" << sigma << ":" << sigmaPpointer->first;
                cout << "," << sigmaPP ;
                cout << "->" << CIP << "," << CIPP << " -> " << pCCP;
                cout << endl;   
            }
            
            tmp += pCCP * ProbaGeneExtinct[CIP] * ProbaGeneExtinct[CIPP] ; 
        }

        E += tmp * SpeciationPropensities[Sid];
    }
    else
    { // for leaves !
        E += SpeciationPropensities[Sid] * ( 1 - get_probaExtantGeneSampled(Sid) );
    }
    
    //duplication
    E += DuplicationPropensities[Sid] * pow(ProbaGeneExtinct[Sid],2);

    //transfer

    if(SumTransferExtinctionProba !=0)
    {
        //NEWNORM
        FLOAT_TYPE tmp = SumTransferExtinctionProba ;
        int DivFac = speciesCladeIndexToCCPid.size() ;
        for( const CIndexType& it : ListTransferIncompatibilities[ Sid ] )
        {
            FLOAT_TYPE correction = ProbaGeneExtinct[it] * TransferPropensities[it];
            tmp -= correction;
            DivFac -= 1;
            if(verboseLevel>2)
                cout << Sid << " accounting for incomp with " << it << " -> -" << correction << endl; 
        }
        if( DivFac != 0 )
            E += ( tmp/DivFac ) * ProbaGeneExtinct[Sid];
    }

    return E;
}

void PsDTLModel::initProbaGeneExtinct()
{
    int nbClades = speciesCladeIndexToCCPid.size();    

    bool create = false;

    if(ProbaGeneExtinct.size() == 0)
    {
        create = true;
        ProbaGeneExtinct.reserve(nbClades);
    }

    for(CIndexType CI = 0 ; CI < nbClades; CI++)
    { //for each species clade
        if(create)
            ProbaGeneExtinct.push_back( 0 );
        else
            ProbaGeneExtinct[CI] = 0;
    }

    SumTransferExtinctionProba = 0;

    return;
}


/*
    Takes:
        - CCPcladeIdType g1
        - CCPcladeIdType g2

    Returns:
        (int)  0 -> compatible
               1 -> c1 incomp c2 (c2 is a parent of c1)
               2 -> c2 incomp c1 (c1 is a parent of c2)
               3 -> both incompatible

*/
int PsDTLModel::getTransferIncompatibility( CCPcladeIdType g1 , CCPcladeIdType g2 )
{
    int s = g1.size();

    bool common = false; // true if there is at least 1 bit in common between g1 and g2
    bool b1not2 = false; // true if there is at least 1 bit that g1 has and g2 hasn't
    bool b2not1 = false; // true if there is at least 1 bit that g2 has and g1 hasn't

    for(unsigned i = 0 ; i < s ; i++)
    {
        if(g1[i])
        {
            if(g2[i])
            {
                common = true;
            }
            else
            {                
                b1not2 = true;
            }
        }
        else if(g2[i])
            b2not1 = true;
        
        if( common && b1not2 && b2not1 )
            break;
    }

    int incomp = 0;

    if(common)
    { // there is some bit in common
        if(b1not2)
            incomp += 2;
        if(b2not1)
            incomp += 1;
        //if both are true, then incomp = 3 -> full incompatibility

        //quick check:
        if( incomp == 0 )
            incomp = 3; //<- special case where g1 == g2.
    }

    return incomp;
}


/*empties the reconciliation matrix, along with the transferProba vector and matrix*/
void PsDTLModel::reset_recMat()
{
    for(auto it = ReconciliationMatrix.begin() ; it != ReconciliationMatrix.end() ; ++it)
    {
        it->clear();
    }
    
    ReconciliationMatrix.clear();  


    SumTransferProba.clear(); 

    for(auto it = TransferProbas.begin() ; it != TransferProbas.end() ; ++it)
    {
        it->clear();
    }
    TransferProbas.clear();
    
    recMatrixComputed = false;
}



void PsDTLModel::initializeRecMatrix()
{
    assert( geneCladeIndexToCCPid.size() > 0) ;//checks that the gene ccp has been set
    assert( speciesCladeIndexToCCPid.size() > 0); //checks that the species ccp has been set

    int nbSpClades = speciesCladeIndexToCCPid.size();
    int nbGeneClades = geneCladeIndexToCCPid.size();

    //checking if the matrix already has been initialized
    if( ReconciliationMatrix.size() > 0 )
    { //crude check 
        return;
    }

    ReconciliationMatrix.reserve(nbSpClades);

    ReconciliationMatrix.push_back(  vector<FLOAT_TYPE> () ); // first is dummy vector
    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1
        ReconciliationMatrix.push_back( vector <  FLOAT_TYPE > ( nbGeneClades, 0  ) );
    }

    SumTransferProba.reserve(nbGeneClades); 
    for(unsigned i = 0 ; i <  nbGeneClades;i++)
        SumTransferProba.push_back(0);

    TransferProbas.reserve(nbSpClades); 
    TransferProbas.push_back( vector < FLOAT_TYPE > () );
    for(unsigned i = 1 ; i <  nbSpClades;i++)
        TransferProbas.push_back( vector < FLOAT_TYPE > (nbGeneClades,0) );
}




// public methods

PsDTLModel::PsDTLModel(): ParentModel(),
                        TransferRates(), //     index : species clade id ; value : associated, branch-wise, loss rate
                        SpeciationRates(), //   index : species clade id ; value : associated, branch-wise, loss rate
                        DuplicationPropensities(), //  index : species clade id ; value : associated, branch-wise, duplication propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
                        LossPropensities(), //         index : species clade id ; value : associated, branch-wise, loss propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
                        TransferPropensities(), //     index : species clade id ; value : associated, branch-wise, loss propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
                        SpeciationPropensities(), //   index : species clade id ; value : associated, branch-wise, loss propensity (unproper use of the term) = rate / (1+ sum of rates of loss,transfer,duplication,speciations )
                        ProbaGeneExtinct(),  // 1st index : species clade id
                        ReconciliationMatrix(),  // 1st index : species clade id ; sigma
                        GeneLeavesProbas(), // keeps the likelihoods of single genes lineages
                        SumTransferExtinctionProba(0),
                        SumTransferProba(),
                        TransferProbas()
{
     //cout << "PsDTLModel::PsDTLModel" << endl; 
}



// makes some precomputations With ids
// and duplication and loss rates
void PsDTLModel::addSpeciesCCPdistribution(shared_ptr<TreesetMetadata> CCPdistribution , unsigned int verboseLevel)
{
    speciesCCPdistribution = CCPdistribution;

    CCPcladeIdType emptyClade(speciesCCPdistribution->get_number_of_leaves(),0ul);

    speciesCladeIndexToCCPid.push_back(emptyClade);
    speciesCCPidToCladeIndex[emptyClade] = 0;
    SpeciesIndexV.push_back( vector< pair< CIndexType, CIndexType > >() );
    SpeciesProbaV.push_back( vector< FLOAT_TYPE >() );


    //vector< CCPcladeIdType > clades = speciesCCPdistribution->get_bipartitions();

    auto END = speciesCCPdistribution->bipartition_cend();
    for(auto gamma_it = speciesCCPdistribution->bipartition_cbegin() ; gamma_it != END ; ++gamma_it)
    {
        speciesCCPidToCladeIndex[ gamma_it->first ] = speciesCladeIndexToCCPid.size(); // from the bitset that are used inside the CCP object to the integer type id 
        if(verboseLevel>1)
        {
            cout << "species : " << gamma_it->first << " " << speciesCCPidToCladeIndex[gamma_it->first] << " " << speciesCCPdistribution->get_branchlength( gamma_it->first ) ;
            if(speciesCCPdistribution->is_leaf( speciesCCPidToCladeIndex[gamma_it->first] ) )
                cout << " " << speciesCCPdistribution->get_name_of_leaf( speciesCCPidToCladeIndex[gamma_it->first] );
            cout <<endl;
        }
        speciesCladeIndexToCCPid.push_back(gamma_it->first); // from integer type id to the bitset that are used inside the CCP object
    
        vector< pair< CIndexType, CIndexType > > IV ; 
        vector< FLOAT_TYPE > PV ;

        fillVccp( speciesCladeIndexToCCPid.size()-1 , IV , PV , true );
        SpeciesIndexV.push_back( IV ) ;
        SpeciesProbaV.push_back( PV ) ;
    }
    /// additional steps



    /// filling default event rates to 0
    unsigned nbClades = speciesCladeIndexToCCPid.size();
    DuplicationRates.reserve(nbClades);
    DuplicationPropensities.reserve(nbClades);
    LossRates.reserve(nbClades);
    LossPropensities.reserve(nbClades);
    TransferRates.reserve(nbClades);
    TransferPropensities.reserve(nbClades);
    SpeciationRates.reserve(nbClades);
    SpeciationPropensities.reserve(nbClades);
    for(unsigned i = 0 ; i < nbClades;i++)
    {
        DuplicationRates.push_back(0);
        LossRates.push_back(0);
        TransferRates.push_back(0);
        SpeciationRates.push_back(1);
        DuplicationPropensities.push_back(0);
        LossPropensities.push_back(0);
        TransferPropensities.push_back(0);
        SpeciationPropensities.push_back(0);
    } 

}



void PsDTLModel::computePropensities()
{
    assert( DuplicationRates.size() > 0); //checks that the species ccp has been set

    unsigned nbClades = speciesCladeIndexToCCPid.size();
    for(unsigned i = 0 ; i < nbClades;i++)
    {
        FLOAT_TYPE d = DuplicationRates[i];
        FLOAT_TYPE l = LossRates[i];
        FLOAT_TYPE t = TransferRates[i];
        FLOAT_TYPE s = SpeciationRates[i];
        DuplicationPropensities[i] =    d / (1+d+t+l+s) ;
        LossPropensities[i] =           l / (1+d+t+l+s) ;
        TransferPropensities[i] =       t / (1+d+t+l+s) ;
        SpeciationPropensities[i] =     s / (1+d+t+l+s) ;
    }    
}

void PsDTLModel::computeProbaGeneExtinct(unsigned int verboseLevel)
{
    assert( DuplicationRates.size() > 0); //checks that the species ccp has been set
    assert( ListTransferIncompatibilities.size() > 0); //checks that the species transfer incompatibilities have been computed
    assert( SpeciationPropensities.back() != 0); // check the propensities have been computed 

    bool stop = false;

    int nbClades = speciesCladeIndexToCCPid.size();

    initProbaGeneExtinct();

    int nbRound = 0;
    int ConvergenceLimit = 10;
    FLOAT_TYPE ConvergenceLevel =0.99;
    while(!stop)
    {
        nbRound++;
        if(verboseLevel>1)
            cout << "computeProbaGeneExtinct round " << nbRound << endl;


        FLOAT_TYPE newSumTransferExtinctionProba = 0;
        //vector <FLOAT_TYPE> newProbaGeneExtinct(nbClades,0);
        
        for(CIndexType CI = 0 ; CI < nbClades; CI++)
        { //for each species clade
            if( CI == 0 )
            {//empty first element does not correspond to any meaningful clade
                continue; 
            }

            ProbaGeneExtinct[CI] = computeExtinctionProba( CI ,verboseLevel ) ;
            newSumTransferExtinctionProba +=  ProbaGeneExtinct[CI] * TransferPropensities[CI];

            if(verboseLevel>1)
                cout << " " << CI << " -> "<< ProbaGeneExtinct[CI] << endl;
        }

        // newSumTransferExtinctionProba /= nbClades; // normalization NEWNORM

        FLOAT_TYPE diff = 0;
        if( (SumTransferExtinctionProba == 0) )
        {
            if(newSumTransferExtinctionProba != 0)
                diff = 1;
        }
        else
            diff = abs( newSumTransferExtinctionProba - SumTransferExtinctionProba ) / SumTransferExtinctionProba;

        //replacing
        SumTransferExtinctionProba = newSumTransferExtinctionProba;
        //ProbaGeneExtinct = newProbaGeneExtinct;

        if(verboseLevel>1)
            cout << "  convergence level : " << 1-diff << endl;

        if((nbRound >= ConvergenceLimit)||(1-diff >= ConvergenceLevel))
            stop = true; // only 1 iteration for now
    }

    if(verboseLevel>0)
        cout << "probabilities of extinction computed (" <<nbRound << " rounds)."<<endl;

}

void PsDTLModel::computeTransferIncompatibilities(unsigned int verboseLevel)
{
    assert(DuplicationRates.size()>0);//checks that the species ccp has been set

    int nbClades = speciesCladeIndexToCCPid.size();

    ListTransferIncompatibilities.reserve( nbClades );
    for(unsigned c1 = 0 ; c1 <  nbClades ; c1++)
    {
        ListTransferIncompatibilities.push_back( vector< CIndexType > () );
    }

    for(unsigned c1 = 1 ; c1 <  nbClades ; c1++)
    {
        CCPcladeIdType g1 = speciesCladeIndexToCCPid[c1];

        ListTransferIncompatibilities[c1].push_back(c1); // self incompatibility

        for(unsigned c2 = c1+1 ; c2 <  nbClades ; c2++)
        {
            int incomp = getTransferIncompatibility( g1 , speciesCladeIndexToCCPid[c2] );
            // 0 -> compatible
            // 1 -> c1 incomp c2
            // 2 -> c2 incomp c1
            // 3 -> both incompatible
            if(verboseLevel > 1)
            {
                cout << " incompatibility " << g1 << "|" << speciesCladeIndexToCCPid[c2] << " -> " << incomp << endl;
            }

            if(incomp > 0 )
            {
                if(incomp>=2)
                { // 2 and 3
                    ListTransferIncompatibilities[c2].push_back(c1);
                }
                if(incomp%2 == 1)
                { // 1 and 3
                    ListTransferIncompatibilities[c1].push_back(c2);
                }
            }
        }
    }

    if(verboseLevel > 0)
    {
        int s = 0;
        for(unsigned c1 = 1 ; c1 <  nbClades ; c1++)
        {
            s += ListTransferIncompatibilities[c1].size();
            if(verboseLevel>1)
            {
                cout << c1 << " incomp:";
                for(unsigned j = 0 ; j < ListTransferIncompatibilities[c1].size() ; j++)
                    cout << " " << ListTransferIncompatibilities[c1][j];
                cout << endl;
            }
        }

        cout << s << " transfer incompatibilities spread over " << nbClades << " species clade." << endl;
    }

}

void PsDTLModel::initializeGeneLeavesProbas()
{
    assert( DuplicationRates.size() > 0); //checks that there is a species CCP distribution.

    unsigned nbClades = speciesCladeIndexToCCPid.size();

    /// initialization of GeneLeavesProbas
    GeneLeavesProbas.reserve(nbClades);

    unsigned nbSpLeaves = speciesCCPdistribution->get_number_of_leaves();

    GeneLeavesProbas.push_back( vector<FLOAT_TYPE > () ); // first is dummy

    for( CIndexType Sid = 1 ; Sid < nbClades ; Sid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1
        GeneLeavesProbas.push_back( vector<FLOAT_TYPE > ( nbSpLeaves+1, -1 ) );
    }
}


/*
    co;pute the reconciliqtion probqbilitiews for all cells of the reconciliation matrix (|Gccp|*|Sccp'|)

    ProbaGeneOneChild must already have been computed

    Takes:
        - const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask --> only present for compatibility with PsDL.
                                    -> mask from gene bit sets to species bitset to speed-up computing of "impossible" scenarios in DL reconciliation
        - unsigned int verboseLevel : 0 : quiet (default)
                                      1 : prints some information to stdout


*/
void PsDTLModel::computeReconciliationMatrix( const map< boost::dynamic_bitset<>, boost::dynamic_bitset<>  > GSpMask , unsigned int verboseLevel)
{
    assert( geneCladeIndexToCCPid.size() > 0) ;//checks that the gene ccp has been set
    assert( ProbaGeneExtinct.size() > 0) ;// checks that gene extinction probabilities have been computed. Basically also ensures that species ccp have been set, DTL have been set

    int nbSpClades = speciesCladeIndexToCCPid.size();
    int nbGeneClades = geneCladeIndexToCCPid.size();

    initializeRecMatrix();

    //if(verboseLevel>0)
    //    cout << "Reconciliation matrix initialization done." << endl;

    int ConvergenceLimit = 10; //maximum number of rounds to perform to achieve convergence of values
    FLOAT_TYPE ConvergenceLevel = 0.99; // satisfying level of convergence

    for( CIndexType Gid = 1 ; Gid < nbGeneClades ; Gid++ ) 
    { // the clade with index 0 is a dummy clade, so we start at 1

        bool stop = false;
        CIndexType mask = 0;
        if( is_GeneLeaf(Gid) )
        { // special procedure to avoid re-computing probebility vectors of single gene lineages
            mask = geneSpeciesCorrespondence[ Gid ] ;
    
            if( GeneLeavesProbas[1][mask] != -1 ) // this probability has already been computed
            {
                SumTransferProba[Gid] = 0;
                for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ )
                {
                    ReconciliationMatrix[Sid][Gid] = GeneLeavesProbas[Sid][mask] ;
                    SumTransferProba[Gid]+= GeneLeavesProbas[Sid][mask] * TransferPropensities[Sid];
                }
                //SumTransferProba[Gid] /= nbSpClades; //NEWNORM

                stop = true;
            }
        }


        int nbRound = 0;
        while(!stop)
        { // convergence round
            nbRound++;
    
            FLOAT_TYPE newSumTransferProba = 0;
            //vector <FLOAT_TYPE> newProba(nbSpClades,0);
    
            for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
            { // the clade with index 0 is a dummy clade, so we start at 1
                //newProba[Sid] = ComputerecProba( Sid, Gid );
                ReconciliationMatrix[Sid][Gid] = ComputerecProba( Sid, Gid );

                //newSumTransferProba += newProba[Sid] * TransferPropensities[Sid];
                newSumTransferProba += ReconciliationMatrix[Sid][Gid] * TransferPropensities[Sid];

                if(verboseLevel>3)
                    cout << "P( "<< Sid << " , " << Gid << " ) = " << ReconciliationMatrix[Sid][Gid] << endl; //newProba[Sid] << endl;
            }

            // newSumTransferProba /= nbSpClades; // normalization //NEWNORM
    
            FLOAT_TYPE diff = 0;
            if( (SumTransferProba[Gid] == 0) )
            {
                if(newSumTransferProba != 0)
                    diff = 1;
            }
            else
                diff = abs( newSumTransferProba - SumTransferProba[Gid] ) / SumTransferProba[Gid];
    
            //replacing
            SumTransferProba[Gid] = newSumTransferProba;
            //for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ )
            //    ReconciliationMatrix[Sid][Gid] = newProba[Sid];

            if(verboseLevel>2)
            {
                cout << "compute Proba gene clade " << Gid <<" round " << nbRound ;
                cout << " convergence level : " << 1-diff << endl;
            }
    
            if((nbRound >= ConvergenceLimit)||(1-diff >= ConvergenceLevel))
                stop = true; // only 1 iteration for now

        }
        if(verboseLevel>1)
            cout << "gene clade " << Gid << " succesfully computed (" << nbRound << " rounds)." << endl;

        //updating some intermediation step computation container

        if( mask != 0 )
        { // special procedure to avoid re-computing probability vectors of single gene lineages
            if( GeneLeavesProbas[1][mask] == -1 ) // this probability has already been computed
            {
                for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ )
                    GeneLeavesProbas[Sid][mask] = ReconciliationMatrix[Sid][Gid];
            }
        }


        computeTransferProbas(Gid); // this will keep the probabilities of transferring a gene lineage gamma from each species lineage sigma (accounting for incompatibilities), so that we don't have to re-compute it for every direct parent of gamma. 
    }

    recMatrixComputed = true;

    if(verboseLevel>0)
        cout << "Reconciliation matrix computed." << endl;

}


/*

    Returns:
        (FLOAT_TYPE) : probability of the reconciliations across all possible position for the root of the gene tree (in the species tree)
                            across all gene and species topologies represented in their respoective CCP distributions

                OR
                    -1 if the reconciliation matrix has not been computed
*/
FLOAT_TYPE PsDTLModel::getOverAllProbability()
{
    //check if the matrix was computed
    if(!recMatrixComputed)
    {
        return -1;
    }


    FLOAT_TYPE Psum = 0;
    FLOAT_TYPE normalizator = 0;

    CIndexType rootGeneClade = geneCCPidToCladeIndex.size()-1; // the root clade is the last one

    int nbSpClades = speciesCladeIndexToCCPid.size();

    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { 
        Psum += ReconciliationMatrix[Sid][ rootGeneClade ];
        normalizator +=  1 - ProbaGeneExtinct[Sid];
    }

    //cout << Psum << '/' << normalizator << endl;

    return Psum / (normalizator);
}


/*
    Returns:
        ( shared_ptr<RecNode> ): backtracked reconciled tree
                            OR
                                empty pointer if the matrix is not ready to be backtracked
*/
shared_ptr<RecNode> PsDTLModel::backtrack( )
{
    //check if the matrix was computed
    if(!recMatrixComputed)
    {
        cerr << "Warning : tried to backtrack a non-computed reconciliation matrix" << endl;
        return  shared_ptr<RecNode>() ;
    }


    CIndexType rootGeneClade = geneCCPidToCladeIndex.size()-1; // the root clade is the last one

    CIndexType chosenSid = 0;

    FLOAT_TYPE  Psum = 0; // we use the unnormalized sum

    int nbSpClades = speciesCladeIndexToCCPid.size();

    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { 
        Psum += ReconciliationMatrix[Sid][ rootGeneClade ] ;
    }

    if( Psum == 0 )
    {
        cerr << "!ERROR! : the overall probability of the reconciliation is 0. possible underflow?" << endl;

        exit(3);
    }


    FLOAT_TYPE r = ((long double) rand()/ RAND_MAX) * Psum; //random result
    
    for( CIndexType Sid = 1 ; Sid < nbSpClades ; Sid++ ) 
    { 
        r -= ReconciliationMatrix[Sid][ rootGeneClade ] ;

        if(r<0)
        {
            chosenSid = Sid;
            break;
        }
    }

    //cout << "backtrack:" << chosenSid << "," << chosenLid << "," << rootGeneClade << endl;

    return backtrackAux(  chosenSid, rootGeneClade );
}


/*
    Takes:
        - shared_ptr<TreesetMetadata> speciesCCPdistrib 
        - vector <  FLOAT_TYPE  > DefaultRates : first element : duplication rates ; second element : loss rates
        - map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates : keys-> species bitset; value-> first element : duplication rates ; second element : loss rates 
        - unsigned int verboseLevel=0
        - map< string , double > extantSampling --> only here for compatibility with DL model.
        - double deltal = 0.05      --> only here for compatibility with DL model.
        - int minNbInt = 5          --> only here for compatibility with DL model.
*/
void PsDTLModel::prepareForComputation(shared_ptr<TreesetMetadata> speciesCCPdistrib , 
                                        vector <  FLOAT_TYPE  > DefaultRates, 
                                        map < boost::dynamic_bitset<> , vector< FLOAT_TYPE > > Rates, 
                                        map< string , FLOAT_TYPE >  extantSampling , 
                                        unsigned int verboseLevel, 
                                        double deltal  , int minNbInt  )
{

    addSpeciesCCPdistribution(speciesCCPdistrib);// makes some precomputations With ids
    
    set_DuplicationRates(DefaultRates[0]);
    set_LossRates(DefaultRates[1]);
    set_TransferRates(DefaultRates[2]);

    for( auto it : Rates )
    {
        set_DuplicationRates( it.first , it.second[0] );
        set_LossRates( it.first , it.second[1] );

        if(it.second.size()>2)
            set_TransferRates( it.first , it.second[2] );
    }

    set_probaExtantGeneSampled(extantSampling);

    
    computePropensities();
    computeTransferIncompatibilities(verboseLevel);                        
            
    computeProbaGeneExtinct(verboseLevel);  // maybe these could move to protected when they've been tested

    initializeGeneLeavesProbas();

}

void PsDTLModel::reset()
{
    fillSpeciesIndexAndProbaCCP();
    ProbaGeneExtinct.clear(); 
    reset_GeneLeavesProbas();
}

void PsDTLModel::reset_GeneLeavesProbas()
{
    for( unsigned i = 0 ; i < GeneLeavesProbas.size() ; i++)
        GeneLeavesProbas[i].clear();
    GeneLeavesProbas.clear();
}

/*
    adapts to deletions of bipiartitions in the ccp distribution

    MUST be followed by a reset if probabilities were already computed (as they won't correspond to the updated ccp distribution)
*/
void PsDTLModel::reMapSpeciesCCPdistribution()
{

    vector < CCPcladeIdType > New_speciesCladeIndexToCCPid; // from integer type id to the bitset that are used inside the CCP object
    map < CCPcladeIdType , CIndexType > New_speciesCCPidToCladeIndex; // from the bitset that are used inside the CCP object to the integer type id 

    // rates
    vector < FLOAT_TYPE > New_DuplicationRates; //  index : species clade id ; value : associated, branch-wise, duplication rate
    vector < FLOAT_TYPE > New_LossRates; //         index : species clade id ; value : associated, branch-wise, loss rate
    vector < FLOAT_TYPE > New_TransferRates;


    /// setup the new indexes 

    CCPcladeIdType emptyClade(speciesCCPdistribution->get_number_of_leaves(),0ul);

    New_speciesCladeIndexToCCPid.push_back(emptyClade);
    New_speciesCCPidToCladeIndex[emptyClade] = 0;


    auto END = speciesCCPdistribution->bipartition_cend();
    for(auto gamma_it = speciesCCPdistribution->bipartition_cbegin() ; gamma_it != END ; ++gamma_it)
    {
        New_speciesCCPidToCladeIndex[ gamma_it->first ] = New_speciesCladeIndexToCCPid.size(); // from the bitset that are used inside the CCP object to the integer type id 
        New_speciesCladeIndexToCCPid.push_back( gamma_it->first ); // from integer type id to the bitset that are used inside the CCP object

    }

    unsigned nbClades = New_speciesCladeIndexToCCPid.size();

    New_DuplicationRates.reserve(nbClades);
    New_LossRates.reserve(nbClades);
    New_TransferRates.reserve(nbClades);

    for(unsigned i = 0 ; i < nbClades;i++)
    {
        CCPcladeIdType gamma = New_speciesCladeIndexToCCPid[i];
        CIndexType oldIndex = speciesCCPidToCladeIndex[ gamma ];

        New_DuplicationRates.push_back( DuplicationRates[ oldIndex ] );
        New_LossRates.push_back( LossRates[ oldIndex ] );
        New_TransferRates.push_back( TransferRates[ oldIndex ] );


    }

    ///checks
    //cout << "new dup  rate: " << New_DuplicationRates.size() << endl;
    //cout << "new loss rate: " << New_LossRates.size() << endl;
    //cout << "new HGT rate: " << New_TransferRates.size() << endl;

    /// replacing the old with the new

    speciesCladeIndexToCCPid.clear();
    speciesCCPidToCladeIndex.clear();


    DuplicationRates.clear();
    LossRates.clear();
    TransferRates.clear();


    speciesCladeIndexToCCPid = New_speciesCladeIndexToCCPid;
    speciesCCPidToCladeIndex = New_speciesCCPidToCladeIndex;

    DuplicationRates = New_DuplicationRates;
    LossRates = New_LossRates;
    TransferRates = New_TransferRates;

    DuplicationPropensities.resize(nbClades);
    LossPropensities.resize(nbClades);
    TransferPropensities.resize(nbClades);
    SpeciationPropensities.resize(nbClades);

    // overkill because this should also be set later, but that makes this function more modular.
    computePropensities(); 

    fillSpeciesIndexAndProbaCCP();


    for(auto it = GeneLeavesProbas.begin() ; it != GeneLeavesProbas.end() ; ++it)
    {
        it->clear();
    }
    GeneLeavesProbas.clear();
    initializeGeneLeavesProbas();


    for(auto it = ListTransferIncompatibilities.begin() ; it != ListTransferIncompatibilities.end() ; ++it)
    {
        it->clear();
    }
    ListTransferIncompatibilities.clear();
    computeTransferIncompatibilities();


}

vector< vector<long double> > PsDTLModel::get_Rates()
{

    vector< vector<long double> > Rates = ParentModel::get_Rates();

    for(unsigned i = 0 ; i < TransferRates.size() ; i++)
    {
        Rates[i].push_back( TransferRates[i] );
    }
    return Rates;
}

void PsDTLModel::set_Rates( vector< vector<long double> > rateList )
{
    for( unsigned int i = 0 ; i < rateList.size() ; i++ )
    {
        DuplicationRates[i] = rateList[i][0] ;
        LossRates[i] = rateList[i][1] ;

        if(rateList[i].size()>2)
            TransferRates[i] = rateList[i][2];
    }
}

void PsDTLModel::set_Rates( vector<long double> rateList )
{
    set_DuplicationRates( rateList[0] );
    set_LossRates( rateList[1] );
    if(rateList.size()>2)
        set_TransferRates( rateList[2] );
}

void PsDTLModel::set_Rates( CIndexType Sid,  vector<long double> rateList )
{
    DuplicationRates[Sid] = rateList[0] ;
    LossRates[Sid] = rateList[1] ;
    TransferRates[Sid] = rateList[2];
}