/*

 * This class ensures the clean starting and ending of the program for the sampling algorithm of the PsDL model:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 23-02-2015
by: Wandrille Duchemin

Last modified the: 23-02-2018
by: Wandrille Duchemin


*/


#include "PsDLSampleOptionHandler.h"

using namespace std;

PsDLSampleOptionHandler::PsDLSampleOptionHandler() : PsDLOptionHandler() , sampleSize(100), burnin(100), thinning(1) , outputFile("")
{ 
    desc_.add_options()
//        ("output.file.prefix,p", boost::program_options::value< string>(&outputFile), "Prefix of the files where output will be written.\nReconciled trees will be written in prefix+\".trees.txt\".\nSampled rates will be written in prefix+\".rates.txt\"\ndefault: prints to stdout.")
        ("sample.size", boost::program_options::value< unsigned int>(&sampleSize), "desired number of sampled rates (in an output prefix was specified, they will be written in prefix+\".rates.txt\"). default:100")
        ("burnin", boost::program_options::value< unsigned int>(&burnin), "size of the burnin phase. default:100")
        ("thin", boost::program_options::value< unsigned int>(&thinning), "thinning rate. default:1")
        ;

}

// Getters

//string PsDLSampleOptionHandler::get_outputFile() const
//{
//    return outputFile;
//}



unsigned int PsDLSampleOptionHandler::get_sampleSize() const
{
    return sampleSize;
}

unsigned int PsDLSampleOptionHandler::get_burnin() const
{
    return burnin;
}    

unsigned int PsDLSampleOptionHandler::get_thinning() const
{
    return thinning;
}    


// Actions
void PsDLSampleOptionHandler::printUsage()
{
    cout << "sampling of rates on the PsDL model" << endl;
    cout << "This program is intended to do the DL reconciliation of a gene CCP distribution and a species CCP distribution" << endl;
    cout << "usage : PsDLSample -g <gene tree distribution file>  -s <species tree distribution file>"  << endl;

}

