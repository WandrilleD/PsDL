/*

This file contains helper functions
for the MPI version 


Created the: 24-05-2018
by: Wandrille Duchemin

Last modified the: 24-05-2018
by: Wandrille Duchemin

*/

#include "UtilsMPI.h"


/*
string bitset_To_string( boost::dynamic_bitset<> bs)
{
    string res = "";

    int s = bs.size();

    for(int i = 0 ; i < s ; i++)
    {
        string c = "0";
        if(bs[i])
            c="1";
        res += c ;
    }
    return res;
}*/

boost::dynamic_bitset<> string_To_bitset( string s)
{
	boost::dynamic_bitset <> bs( s.size() , 0 );
	//cout << "from archive " << it->second << endl;
	for(unsigned i = 0 ; i < s.size() ; i++)
	{
	    char c = s[i];
	    if(c == '1')
	        bs[i] = 1;
	}
	return bs;
}


/*
	Takes:
		- mpi::communicator &world, 
		-int rank
		- map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > & rateMap : object to broadcast from rank 0 to all

*/
void broadcastRates( mpi::communicator &world, int rank , map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > &rateMap )
{


    map< string , vector< FLOAT_TYPE > > newRateMap; // transform bitset id to string id for the broadcast (prefer string id to int id because int ids overflow very fast!)

    if(rank == 0)
    {

    	for(auto it = rateMap.begin() ; it != rateMap.end() ; ++it)
    	{
			string id = bitset_To_string( it->first );
			if( it->first != string_To_bitset( id ) )
			{
				cerr << "ERROR : clade bitset serialization problem :" << it->first << " <-> " << string_To_bitset( id ) << endl;
				cerr << "please contact the person in charge of the maintenance of this software : this is a grave issue." << endl;
				exit(5);
			}
    		newRateMap[ id ] = vector< FLOAT_TYPE >(it->second.size() ,0 );
    		for(unsigned i = 0 ; i <  it->second.size() ; i++ )
    			newRateMap[ id ][i] = it->second[i];
    	}


    }

    broadcast(world , newRateMap , 0);

    if(rank != 0)
    {
        //cout << "received rate " << newRateMap.size() << endl;

    	for(auto it = newRateMap.begin() ; it != newRateMap.end() ; ++it)
    	{
			boost::dynamic_bitset<> id = string_To_bitset( it->first );
			//cout << it->first << " -> " << id << endl;
    		rateMap[ id ] = vector< FLOAT_TYPE >(it->second.size() ,0 );
    		for(unsigned i = 0 ; i <  it->second.size() ; i++ )
    			rateMap[ id ][i] = it->second[i];
    	}
    }
}


/*
    Takes:
        - mpi::communicator &world, 
        - int rank
        - map< string ,  FLOAT_TYPE > &extantSamplingMap : object to broadcast from rank 0 to all

*/
void broadcastExtantSamplingRates( mpi::communicator &world, int rank , map< string ,  FLOAT_TYPE > &extantSamplingMap )
{
    broadcast(world , extantSamplingMap , 0);
}


/*
	Broadcasts the rates that are in the model of process of rank 0 to all the others.

    Takes:
        - mpi::communicator &world : mpi communicator
        - int rank  :rank of the process
        - ParentModel * model 
        - bool branchWise

*/
void broadcastRates( mpi::communicator &world, int rank , ParentModel * model , bool branchWise )
{
	//cout  << "broadcastRates " << rank <<endl; 

	vector< vector<long double> > Rates;
	vector<long double> DRates;

    Rates = model->get_Rates();
    if(rank == 0)
    {
    	for( unsigned i = 0 ; i < Rates[0].size() ; i++ )
    		DRates.push_back( Rates[0][i] );

    	/*
    	cout << "sending rates :"<< endl;
	    for( unsigned i = 0 ; i < Rates.size() ; i++ )
	    {
	    	for( unsigned j = 0 ; j < Rates[i].size() ; j++ )
			    cout <<  Rates[i][j] << " " ;
			cout << endl;
	    }*/
    }

    if(branchWise)
    {
    	//cout << "BC " << rank << " " << Rates.size() << endl;
    	for( unsigned i = 0 ; i < Rates.size() ; i++ )
    	{
    		//cout << rank << " " << i << endl;
    		vector <long double > TMP = Rates[i];
		    broadcast(world , TMP , 0);
		    for(unsigned j = 0 ; j < TMP.size() ; j++)
		    	Rates[i][j] = TMP[j];
    	}


	    if(rank != 0)
    	{
    		/*
    		cout << "received rates :"<< endl;
	    	for( unsigned i = 0 ; i < Rates.size() ; i++ )
	    	{
	    		for( unsigned j = 0 ; j < Rates[i].size() ; j++ )
				    cout <<  Rates[i][j] << " " ;
				cout << endl;
	    	}*/

			model->set_Rates( Rates );	
		}    	
    }
	else
	{
	    broadcast(world , DRates , 0);

	    if(rank != 0)
    	{	/*
    		cout << "receiving rates :"<< endl;
	    	for( unsigned i = 0 ; i < DRates.size() ; i++ )
	    	{
				cout << DRates[i] << " " ;
	    	}    		
			cout << endl;*/
			model->set_Rates( DRates );	
		}
	}

}


/*
    Takes:
        - mpi::communicator &world : mpi communicator
        - int rank  :rank of the process
        - map < int , vector< long double>  > &RateUpdateCount:
															count the transition of gene lineages, zill be updated in this function
                                                            key:   id of the species clade
                                                            value: array of three values, the count of respectively the transition from 1 lineage to 0m fro; 1 lineage to 1, from 1 lineage to many
                                                            AND maybe 4 counts of duplication, losses, transfer and speciation (in case of transfers)
                                                            !! will be updated with the values of all processes if rank == 0 !!
*/
void gatherRateUpdateCount( mpi::communicator &world, int rank , map < int , vector< long double>  > &RateUpdateCount )
{
	int n = -1;

    vector< int > RUcountsKeys;
    vector < vector< long double > > RUcounts;

    RUcountsKeys.reserve( RateUpdateCount.size() );
    //RUcounts.reserve( RateUpdateCount.size() )

	for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
    {
    	RUcountsKeys.push_back( it->first );

    	if( it == RateUpdateCount.begin())
    	{
			n = it->second.size();
			RUcounts.reserve( n );
    		for(unsigned i = 0 ; i < n ; i++)
    		{
				RUcounts.push_back( vector< long double>()  ); 
    			RUcounts.back().reserve( RateUpdateCount.size() );
    		}
    	}
		
		unsigned int i = 0;
    	for(auto r : it->second)
    	{
    		RUcounts[i].push_back( r );
    		i++;
    	}
	}

	/*
	for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
    {
    	cout << rank << " -> " << it->first << " :" ;
    	for(auto i : it->second)
    		cout << " " << i;
    	cout << endl; 
    }
	*/
    


    if(rank == 0)
    {
        vector< vector< int > > TotalRUcountsKeys;
        gather(world , RUcountsKeys , TotalRUcountsKeys , 0 );

        //cout << "received keys ";
        //cout << TotalRUcountsKeys.size() << endl;
        //zipping the info; skipping the 1st (because this is the one we're updating)
        for(unsigned i = 1 ; i < TotalRUcountsKeys.size() ; i++)
        {
            for(unsigned j = 0 ; j < TotalRUcountsKeys[i].size() ; j++)
            {
                int sp = TotalRUcountsKeys[i][j];
                if( RateUpdateCount.find( sp ) == RateUpdateCount.end() )
                {
                    RateUpdateCount[ sp ] = vector< long double >(n,0);
                }
            }
        }

        //cout << "read keys "<<endl;

        for(unsigned i = 0 ; i < n ; i++)
        {
        	vector< vector< long double > > TotalTcounts;
        	
			gather(world , RUcounts[i] , TotalTcounts , 0 );

        	//zipping the info; skipping the 1st (because this is the one we're updating)
        	for(unsigned k = 1 ; k < TotalRUcountsKeys.size() ; k++)
        	{
        	    for(unsigned j = 0 ; j < TotalRUcountsKeys[k].size() ; j++)
        	    {
        	        int sp = TotalRUcountsKeys[k][j];
        	        RateUpdateCount[ sp ][ i ] += TotalTcounts[k][j];
        	    }
        	}			        	
        }

        /*
		for(auto it = RateUpdateCount.begin() ; it != RateUpdateCount.end() ; ++it)
	    {
	    	cout << "gathered" << " -> " << it->first << " :" ;
	    	for(auto i : it->second)
	    		cout << " " << i;
	    	cout << endl; 
	    }*/
    }
    else
    {
        gather(world , RUcountsKeys , 0 );
        for(unsigned i = 0 ; i < n ; i++)        	
			gather(world , RUcounts[i] , 0 );
    }
}
