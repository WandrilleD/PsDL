/*

 * This class ensures the clean starting and ending of the program for the expectation maximisation algorithm of the PsDL model:
 * - reads and validates input parameters
 * - output usage information
 

Created the: 21-02-2015
by: Wandrille Duchemin

Last modified the: 27-04-2018
by: Wandrille Duchemin


*/


#include "PsDLMaxOptionHandler.h"

using namespace std;

PsDLMaxOptionHandler::PsDLMaxOptionHandler() : PsDLOptionHandler() , writeIntermediary(false)
{ 
    desc_.add_options()
        //("max.iter", boost::program_options::value< unsigned int>(&maxIteration), "maximum number of iterations to optimize rates. default:100")
        ("write.intermediary,I", "writes an updated species ccp distribution file for each optimization round. Necessitates that option --output.file.prefix/-p is set.")
        ;

}

// Getters

//unsigned int PsDLMaxOptionHandler::get_maxIteration() const
//{
//    return maxIteration;
//}    

bool PsDLMaxOptionHandler::get_writeIntermediary() const
{
    return writeIntermediary;
}



bool PsDLMaxOptionHandler::verify()
{
    bool pb = PsDLOptionHandler::verify();



    if(vm_.count("write.intermediary"))
    {
        if( get_outputFile() == "" )
        {
            cerr << "ERROR: option --write.intermediary/-I was given while option --output.file.prefix/-p is not set" << endl;
            pb = false;
        }

        writeIntermediary = true;
    }

    if(outputSample == 0 )
    {
        cerr << "ERROR: option --tree.sample/-n should be > 0 for this algorithm (the tree sample is used to estimate new rates and update de the species CCP distribution). 100 can be a good value." << endl;
        pb = false;

    }


    return pb;
}


// Actions
void PsDLMaxOptionHandler::printUsage()
{
    cout << "expectation maximisation on the PsDL model" << endl;
    cout << "This program is intended to do the DL reconciliation of a gene CCP distribution and a species CCP distribution" << endl;
    cout << "usage : PsDLMax -g <gene tree distribution file>  -s <species tree distribution file>"  << endl;

}

