 /*

This file contains a small executable to play with reconciliation of gene qnd species CCPs

Created the: 07-03-2018
by: Wandrille Duchemin

Last modified the: 07-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>

#include "PsDLUtils.h"
#include "PsDLOptionHandler.h"
#include "PsDLGridOptionHandler.h"
#include "PsDLModel.h"
#include "speciesCCPupdateUtils.h"


// includ eof the CCP library. local for now
#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{


    // reading and checking options

    shared_ptr<PsDLGridOptionHandler> optionHandler = make_shared<PsDLGridOptionHandler>();
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        return -99;
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;



    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( VerboseLevel > 0 )
        cout << "using random seed : "<< seed << endl;

    srand(seed);
    
    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names


    // **************
    // 1 reading data
    // **************

    
    // 1A reading tree data
    // **************

   // 1A.1 gene file names and files
    // **************



    vector <string> geneDistribFileNames;

    if( optionHandler->get_multiple_gene_families() )
    {
        pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
    }
    else
        geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );


    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
        exit(pb);
    }


    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , optionHandler->get_input_is_ale() , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "read gene distribution(s)."<<endl;


    // 1A.2 species tree data
    // **************


    pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, optionHandler->get_input_is_ale() , optionHandler->get_unrooted_species_tree() ); 

    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
        exit(pb);
    }

    // 1B reading gene species correspondence data
    // **************


    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }
    if(VerboseLevel > 1)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }

    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        GeneCladeSpeciesMasks.push_back( getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }


    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks[GeneFamilyId] );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }



    // 2 instanciating the model
    // **************

    // instanciation that does not rely of the gene trees

    PsDLModel * model = new PsDLModel();

    model->addSpeciesCCPdistribution(SpeciesCCPDistrib);// makes some precomputations With ids

    model->set_DuplicationRates( optionHandler->get_initialDupRate() ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
    model->set_LossRates( optionHandler->get_initialLossRate() ); //  takes : loss rate and set it as the global duplication rate for all the species tree
    
    model->set_deltal( 0.05 ) ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    model->set_minNbBLInterval( 5 ) ; // minimum number of interval for a given branch of the species tree

    double BLEN = getWeightedBranchLenSum( SpeciesCCPDistrib ); 
    double rootBranchRatio = 0.01;

    model->set_rootBranchLength( rootBranchRatio * BLEN ); // putting 5 percent of the total branch length of the tree at the top of the species tree

    model->divideSpeciesBranches(LowerVerboseLevel);    //   

    model->initializeGeneLeavesProbas();

    double extantSampling = optionHandler->get_extant_sampling();
    model->set_probaExtantGeneSampled(extantSampling);


    model->computeProbaGeneExtinct(LowerVerboseLevel);  // maybe these could move to protected when they've been tested
    model->computeProbaGeneOneChild(LowerVerboseLevel); // as they NEED to be executed only once and in this precise order


    // preparing output


    // building a grid of the likelihoods over some dulication and loss rates

    double DupRate = 0;
    double LossRate = 0;

    ofstream ofs;

    ofs.open(optionHandler->get_outputFile() + ".grid.txt" );

    double step = optionHandler->get_stepNb();
    double MinDupRate = optionHandler->get_MinDup();
    double MinLossRate = optionHandler->get_MinLoss();
    double MaxDupRate = optionHandler->get_MaxDup();
    double MaxLossRate = optionHandler->get_MaxLoss();

    bool isLog = optionHandler->get_isLog();

    for(unsigned i = 0 ; i < step ; i++)
    {
        DupRate = (( MaxDupRate-MinDupRate )/(step-1)) * i + MinDupRate ;
        //cout << "  "<<  MaxDupRate<< " " << MinDupRate << " " << step << " " << i <<  " >> " << DupRate << endl;
        if( isLog )
        {
            DupRate = pow(10,  DupRate );
            //cout << "     >> "<< DupRate << endl;
        }
        for(unsigned j = 0 ; j < step ; j++)
        {
            LossRate = (( MaxLossRate-MinLossRate )/(step-1)) * j + MinLossRate ;
            if( isLog )
                LossRate = pow(10, LossRate );

            model->set_DuplicationRates( DupRate ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
            model->set_LossRates( LossRate ); //  takes : loss rate and set it as the global duplication rate for all the species tree


            model->computeProbaGeneExtinct(optionHandler->get_verbose_level());  // maybe these could move to protected when they've been tested
            model->computeProbaGeneOneChild(optionHandler->get_verbose_level()); // as they NEED to be executed only once and in this precise order

            long double P = 0;

            //compute the overall probability for all gene families 
            for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
            {
                shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
        
                model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
                model->addgeneSpeciesCorrespondence( GtoSpCorr );
        
                model->computeReconciliationMatrix(  GeneCladeSpeciesMasks[GeneFamilyId], LowerVerboseLevel ); 
                
                P += log( model->getOverAllProbability() );


                model->prepareForNewGeneFamily();
                GeneCCPDistrib.reset();
        
            }


            ofs<< DupRate <<" "<< LossRate << " " << P  << endl;
            //cout << "overall proba : "<<model->getOverAllProbability()<<endl;

            model->reset();

        }
        ofs<< endl;

    }


    /// finishing ///

    delete model;
    SpeciesCCPDistrib.reset();

    return 0;

}



