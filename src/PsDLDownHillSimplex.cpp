/*

This file contains a small executable to
perform an nelder mead optimisation of the parameter of the PsDL model


Created the: 07-03-2018
by: Wandrille Duchemin

Last modified the: 08-03-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>

#include "PsDLUtils.h"
#include "PsDLModel.h"
#include "speciesCCPupdateUtils.h"
#include "PsDLOptionHandler.h"
#include "PsDLNelderMead.h"



#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>
#include <limits>

using namespace std;





/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    cerr << "operating on default parameter for the DHS for now" << endl;

    shared_ptr<PsDLOptionHandler> optionHandler = make_shared<PsDLOptionHandler>();
 
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        return -99;
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;

    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( optionHandler->get_verbose_level() > 0 )
        cout << "using random seed : "<< seed << endl;

    srand(seed);


    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names


    // **************
    // 1 reading data
    // **************

    
    // 1A reading tree data
    // **************


    
    // 1A reading tree data
    // **************

    // 1A.1 gene file names and files
    // **************



    vector <string> geneDistribFileNames;

    if( optionHandler->get_multiple_gene_families() )
    {
        pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
    }
    else
        geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );


    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
        exit(pb);
    }


    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , optionHandler->get_input_is_ale() , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "read gene distribution(s)."<<endl;

    // 1A.2 species tree data
    // **************


    pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, optionHandler->get_input_is_ale() , optionHandler->get_unrooted_species_tree() ); 

    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
        exit(pb);
    }

    if(optionHandler->get_verbose_level()>1)
        cout << "read species distribution"<<endl;


    // 1B reading gene species correspondence data
    // **************

    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }
    if(VerboseLevel > 1)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }

    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        GeneCladeSpeciesMasks.push_back( getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }


    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks[GeneFamilyId] );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }


    // 2 instanciating the model
    // **************

    // instanciation that does not rely of the gene trees

    bool BranchWiseRates = false;


    double InitialDupRate = optionHandler->get_initialDupRate();
    double InitialLossRate = optionHandler->get_initialLossRate();


    PsDLModel * model = new PsDLModel();

    model->addSpeciesCCPdistribution(SpeciesCCPDistrib, LowerVerboseLevel);// makes some precomputations With ids

    model->set_DuplicationRates( InitialDupRate ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
    model->set_LossRates( InitialLossRate ); //  takes : loss rate and set it as the global duplication rate for all the species tree
    
    model->set_deltal( 0.05 ) ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    model->set_minNbBLInterval( 5 ) ; // minimum number of interval for a given branch of the species tree

    double BLEN = getWeightedBranchLenSum( SpeciesCCPDistrib ); 
    double rootBranchRatio = 0.01;

    model->set_rootBranchLength( rootBranchRatio * BLEN ); // putting 5 percent of the total branch length of the tree at the top of the species tree

    model->divideSpeciesBranches(LowerVerboseLevel);    //   


    double extantSampling = optionHandler->get_extant_sampling();
    model->set_probaExtantGeneSampled(extantSampling);

    /*
    /// initiation of the data for species CCP distribtuion update
    vector< boost::dynamic_bitset<> > SpeciesIdToCladeId;

    unsigned int SpCCPupdateNbIteration = optionHandler->get_SpCCPUpdatingNbRound();
    double splitWeight = optionHandler->get_SpCCPUpdatingWeight();

    //// filling the clade id map
    unsigned int Sid = 0;
    SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
    Sid++;
    SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );

    while( SpeciesIdToCladeId.back().count() != 0 )
    {
        Sid++;
        SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );        
    }
    */

    //presuming fixed branch lengths for now


    unsigned int NbGeneDistrib = geneDistribFileNames.size();

    vector<double > startingPoint;
    startingPoint.push_back( InitialDupRate );
    startingPoint.push_back( InitialLossRate );

    unsigned int parameterization = 0;
    double Step = 1;
    double No_improve_thr =10e-3;
    unsigned int No_improv_break =10;
    unsigned int Max_iter =0;
    double Alpha =1;
    double Gamma = 2;
    double Rho = -0.5;
    double Sigma =0.5;


    PsDLNelderMead optimizer( model , GeneCCPDistribV, GeneCladeSpeciesMasks, GtoSpCorr, parameterization, startingPoint, 
                                Step,
                                No_improve_thr,
                                No_improv_break,
                                Max_iter,
                                Alpha,
                                Gamma,
                                Rho,
                                Sigma
                                );


    optimizer.optimizationRound();

    while(! optimizer.checkStopConditions())
        optimizer.optimizationRound();

    vector< double > bestPAram = optimizer.getBestParameters();
    long double bestLkh = optimizer.get_BestParametersLkh();


    cout << "****************************************"<< endl;
    cout << "final duplication rate: " << bestPAram[0] << endl;
    cout << "final loss rate: " << bestPAram[1] << endl;
    cout << "final likelihood " << bestLkh << endl;
    cout << "****************************************"<< endl;



/*    streambuf * buf;
    ofstream of;

    FILE *fp;


    string filename = optionHandler->get_outputFile();
    if(filename!="")
    {
        filename += ".updated.species.ale";
        of.open(filename.c_str() );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream OUT(buf);

    OUT << SpeciesCCPDistrib->output_as_ale() << endl;

    if(filename!="")
    {        
        of.close();
    }*/



    // output later
    /*
    // preparing output
    string filename = optionHandler->get_outputFile();


    streambuf * buf;
    ofstream of;

    FILE *fp;
    if(filename!="")
    {
        filename += ".trees.txt";
        of.open(filename.c_str() );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream OUT(buf);

    InitRecXMLToStream( OUT );

    FinishRecXMLToStream( OUT );

    if(filename!="")
    {
        of.close()
    }
    */

    return 0;

}