/*

This file contains a small executable to play with reconciliation of gene qnd species CCPs

Created the: 20-04-2018
by: Wandrille Duchemin

Last modified the: 04-05-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>

#include "PsDLUtils.h"
#include "PsDLOptionHandler.h"

#include "ParentModel.h"
#include "PsDLModel.h"
#include "PsDTLModel.h"

#include "speciesCCPupdateUtils.h"



#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <limits.h>       /* time */
//#include <ctime>

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{
    
    // reading and checking options

    shared_ptr<PsDLOptionHandler> optionHandler = make_shared<PsDLOptionHandler>();
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        return -99;
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;



    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( VerboseLevel > 0 )
        cout << "using random seed : "<< seed << endl;

    srand(seed);
    
    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names


    // **************
    // 1 reading data
    // **************

    
    // 1A reading tree data
    // **************

   // 1A.1 gene file names and files
    // **************



    vector <string> geneDistribFileNames;

    if( optionHandler->get_multiple_gene_families() )
    {
        pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
    }
    else
        geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );


    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
        exit(pb);
    }


    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , optionHandler->get_input_is_ale() , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "read gene distribution(s)."<<endl;


    // 1A.2 species tree data
    // **************


    pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, optionHandler->get_input_is_ale() , optionHandler->get_unrooted_species_tree() ); 

    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
        exit(pb);
    }

    // 1B reading gene species correspondence data
    // **************


    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }
    if(VerboseLevel > 1)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }

    // equivalence between gene and species bitsets
    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > x;
        GeneCladeSpeciesMasks.push_back( x );//getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }



    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > GeneCladeSpeciesMasks = getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) ;
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }
    

    // 1D eventually reading branch-wise input rates
    // ************************
    map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > rateMap;
    if(optionHandler->get_input_rate_file() != "")
    {

        int pb = readRatesFile(optionHandler->get_input_rate_file() , SpeciesCCPDistrib , rateMap);
        if(pb != 0)
        {
            if(pb == 1)
                cerr << "Unable to open the rate input file: " << optionHandler->get_input_rate_file() << endl ;
            exit(pb);

        }
    }


    // 2 instanciating the model
    // **************

    // instanciation that does not rely of the gene trees

    ParentModel * model = new PsDTLModel();


    vector<FLOAT_TYPE> DefaultRates( 3 , optionHandler->get_initialDupRate() );
    DefaultRates[1] = optionHandler->get_initialLossRate();
    DefaultRates[2] = optionHandler->get_initialLossRate();

    model->prepareForComputation( SpeciesCCPDistrib , 
                                  DefaultRates, 
                                  rateMap,
                                  LowerVerboseLevel);



    // preparing output

    /////

    string filename = optionHandler->get_outputFile();


    streambuf * buf;
    ofstream of;

    FILE *fp;
    if(filename!="")
    {
        filename += ".trees.xml";
        of.open(filename.c_str() );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream OUT(buf);

    InitRecXMLToStream( OUT );

    /// initiation of the data for species CCP distribtuion update
    
    unsigned int nbSamples = optionHandler->get_outputSample();

    /// not used -> unsigned int SpCCPupdateNbIteration = optionHandler->get_SpCCPUpdatingNbRound();
    /// not used -> double splitWeight = optionHandler->get_SpCCPUpdatingWeight();
    /// not used -> double speciesCCPcutOff = optionHandler->get_cutOff();
    /// not used -> bool isRelativeCutoff = optionHandler->get_isRelativeCutoff();

/*


    for(unsigned int SpCCPupdateIter = 0 ; SpCCPupdateIter < SpCCPupdateNbIteration ; SpCCPupdateIter++)
    {
        if(VerboseLevel>0)
            cout << "species CCP updating round " << SpCCPupdateIter+1 << "/" << SpCCPupdateNbIteration << endl;;
        //// filling the clade id map
        vector< boost::dynamic_bitset<> > SpeciesIdToCladeId;
        unsigned int Sid = 0;
        SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
        Sid++;
        SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
    
        while( SpeciesIdToCladeId.back().count() != 0 )
        {
            Sid++;
            SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );        
        }
        map< int, map< int , int > > splitCountsObservedInReconciliations;

*/

    /// computing reconciliations and backtracking fro all families
    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        shared_ptr<TreesetMetadata> GeneCCPDistrib = GeneCCPDistribV[ GeneFamilyId ];

        model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
        model->addgeneSpeciesCorrespondence( GtoSpCorr );

        
        model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 

        if(VerboseLevel>1)
            cout << "Gene family " << GeneFamilyId << " overall likelihood " << model->getOverAllProbability() << endl;;


        for(unsigned i = 0 ; i < nbSamples; i++ )
        {
            shared_ptr< RecNode > RT= model->backtrack();
            model->annotateReconciledTree( RT );

            WriteRecTreeToStream( OUT , RT , "gene family " + int2string(GeneFamilyId) + " , sample " + int2string(i+1) );

            if(VerboseLevel>1)
                cout << "\r finished backtrack "<< i+1 << "/" << nbSamples << " of family " << GeneFamilyId << "/" << geneDistribFileNames.size()-1 ;
        }
        if(VerboseLevel>1)
            cout << endl;
    


        model->prepareForNewGeneFamily();
        GeneCCPDistrib.reset();
    }

    FinishRecXMLToStream( OUT );


    if(filename!="")
    {        
        of.close();
    }


    delete model;
    SpeciesCCPDistrib.reset();

    return 0;



/*
        //cout << "splitCountsObservedInReconciliations" << endl;
        //for( auto it =  splitCountsObservedInReconciliations.begin() ; it != splitCountsObservedInReconciliations.end() ; ++it)
        //{
        //    cout << "  " << SpeciesIdToCladeId[ it->first ] <<endl;
        //    for( auto it2 =  it->second.begin() ; it2 != it->second.end() ; ++it2)
        //        cout <<  "\t" << SpeciesIdToCladeId[ it2->first ] << " -> +" << it2->second << endl;
        //
        //}
        
        ///updating CCP distribution with the splits observed in the reconciliations
        UpdateCCPdistribution( SpeciesCCPDistrib,  splitCountsObservedInReconciliations , SpeciesIdToCladeId , splitWeight);


        ///applying cutoff
        if(speciesCCPcutOff > 0)
        {
            
            int nbDeleted = CutOffCCPdistribution( SpeciesCCPDistrib, speciesCCPcutOff , isRelativeCutoff , LowerVerboseLevel);

            if(nbDeleted>0)
            { // re-map the species CCP inside the model.
                if(VerboseLevel>0)
                {
                    cout << " deleted " << nbDeleted << " clades in the CCP distribution (cut-off:" << speciesCCPcutOff << ")."<<endl;
                }
                
                model->reMapSpeciesCCPdistribution();
                
            }            
        }

        /// if there is some updating of the ccp (splitWeight>0 or speciesCCPcutOff>0) AND this is not the last round (SpCCPupdateIter+1 != SpCCPupdateNbIteration)
        if(  (( splitWeight + speciesCCPcutOff )>0)  && (SpCCPupdateIter+1 != SpCCPupdateNbIteration))
        { // then we need to re-compute the probability of gene extinction and gene transmission as the CCP distribution changed
            //cout << "RESET"<<endl;
            model->reset();
            model->computeProbaGeneExtinct();
            model->computeProbaGeneOneChild();
        }

    }

    FinishRecXMLToStream( OUT );


    if(filename!="")
    {        
        of.close();
    }




    filename = optionHandler->get_outputFile();
    if(filename!="")
    {
        filename += ".updated.species.ale";
        of.open(filename.c_str() );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();


    OUT << SpeciesCCPDistrib->output_as_ale() << endl;

    if(filename!="")
    {        
        of.close();
    }


    /// finishing ///

    delete model;
    SpeciesCCPDistrib.reset();

    return 0;
*/

}
