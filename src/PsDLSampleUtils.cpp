
/*

This file contains utilitary functions for PsDLsample 

Created the: 23-02-2015
by: Wandrille Duchemin

Last modified the: 06-03-2018
by: Wandrille Duchemin


*/


#include "PsDLSampleUtils.h"

double Uniform0to1( )
{
    return   ((double) rand()/ RAND_MAX) ;
}

double Exponential( double mean)
{
    return -mean * log( Uniform0to1() );
}


/*
    Draws rate scaling from one iteration to the next
        exp( random between log(1-alpha) and log(1+alpha)  )
*/
double drawRateScale( double alpha )
{
    return exp( log(1-alpha) + Uniform0to1() * ( log((1+alpha)/(1-alpha)) ) );
}


/*
    Draws new rate 
     oldRate * exp( random between log(1-alpha) and log(1+alpha)  )
*/
double drawNewRate( double oldRate , double alpha )
{
    return oldRate *  drawRateScale( alpha );
}

/* proba to accept new = min ( newP/currentP , 1 )*/
bool acceptProposition(long double currentP , long double newP, bool AreLog) 
{

    long double ratio ;

    if(AreLog)
        ratio = exp( newP - currentP )  ;
    else
        ratio = newP / currentP;

    //cout << "\tratio " << ratio << " << " << newP << "," << currentP  <<  "<>"  <<  exp(-newP) / exp(-currentP) << endl;

    if(ratio >1)
        return true;

    return (  Uniform0to1() <ratio );

}

/*
    Takes:
        - PsDLModel * model : pointer to the current PsDL model
        - vector< shared_ptr<TreesetMetadata> > &GeneCCPDistribV : vector of gene CCP distributions
        - map<string, string > &GtoSpCorr : correspondences between gene leaves and species leaves 
        - vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > &GeneCladeSpeciesMasks : vector of map : keys: bitset of gene clade
                                                                                                                     values: bitset of corresponding species clade (i.e : bitset of its species )
        - double &currentDupRate : current duplication rate, will be updated if the move if accepted
        - double &currentLossRate : current loss rate, will be updated if the move if accepted
        - long double &currentP : : current overall log proba, will be updated if the move if accepted
        - unsigned int verboseLevel: 0: quiet ; 1: verbose
        - double alpha = 0.1 : sort of temparature for the moves  in parameter space
        - double  minRate : extemum value for the sampled rates
        -  double maxRate : extemum value for the sampled rates
    Returns:
        (bool) : true if the new parameters have been accepted
*/
bool samplerStep( PsDLModel * &model , vector< shared_ptr<TreesetMetadata> > &GeneCCPDistribV, map<string, string > &GtoSpCorr, vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > &GeneCladeSpeciesMasks ,double &currentDupRate , double &currentLossRate , long double &currentP , unsigned int verboseLevel, double alpha , double  minRate, double maxRate )
{

    

    double newDup =  drawNewRate( currentDupRate  ,alpha);
    double newLoss = drawNewRate( currentLossRate ,alpha);

    if(newLoss< minRate)
        newLoss = minRate ;
    else if(newLoss > maxRate)
        newLoss = maxRate ;

    if(newDup< minRate)
        newDup = minRate ;
    else if(newDup > maxRate)
        newDup = maxRate ;



    PsDLModel * modelCopy = new PsDLModel( *model );// these two step could be aggregated, no?
    modelCopy->reset();                             // these two step could be aggregated, no?

    modelCopy->set_DuplicationRates( newDup ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
    modelCopy->set_LossRates( newLoss ); //  takes : loss rate and set it as the global duplication rate for all the species tree

    modelCopy->computeProbaGeneExtinct(verboseLevel);  // maybe these could move to protected when they've been tested
    modelCopy->computeProbaGeneOneChild(verboseLevel); // as they NEED to be executed only once and in this precise order

    long double newP = 0;

    //compute the overall probability for all gene families 
    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < GeneCCPDistribV.size() ; GeneFamilyId++ )
    {
        shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];

        model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
        model->addgeneSpeciesCorrespondence( GtoSpCorr );

        model->computeReconciliationMatrix( GeneCladeSpeciesMasks[ GeneFamilyId ], verboseLevel ); 
        newP +=  log( model->getOverAllProbability() );

        model->prepareForNewGeneFamily();
        GeneCCPDistrib.reset();

    }

    bool accepted = acceptProposition(currentP , newP, true) ;

    if( accepted )
    {

        //cout << "test"<<endl;
        delete model;
        model = modelCopy;
        //cout << model->getOverAllProbability() << endl;

        currentP = newP;
        currentDupRate = newDup;
        currentLossRate = newLoss;
    }
    else
    {
        delete modelCopy;
    }

    return accepted;
}


void InitSampleFile( ostream& OUT)
{
    OUT << "sample" << "\t" << "step" << "\t" << "Duplication_Rate" << "\t" << "Loss_Rate" << "\t" << "log(P)" << endl;
}

void WriteSampleToFile( ostream& OUT , unsigned int sampleNb , unsigned int  stepNumber , double DupRate , double LossRate , double currentP)
{
    OUT << sampleNb << "\t" << stepNumber << "\t" << DupRate << "\t" << LossRate << "\t" << currentP << endl;
}
