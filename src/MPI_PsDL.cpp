/*

This file contains an executable to use reconciliation of gene qnd species CCPs
using MPI


Created the: 24-03-2018
by: Wandrille Duchemin

Last modified the: 04-05-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>

#include "PsDLUtils.h"
#include "PsDLMaxOptionHandler.h"
#include "PsDLModel.h"
#include "PsDLMaxUtils.h"
#include "speciesCCPupdateUtils.h"

#include "MPI_PsDLUtils.h"

// includ eof the CCP library. local for now
#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <boost/mpi.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>


namespace mpi = boost::mpi;


#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{

    mpi::environment env;
    mpi::communicator world;




    // reading and checking options
    shared_ptr<PsDLMaxOptionHandler> optionHandler = make_shared<PsDLMaxOptionHandler>();
 
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        return -99;
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;



    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( VerboseLevel > 1 )
        cout << "process " << world.rank() << " using random seed : "<< seed << endl;

    srand(seed);
    
    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names

    vector <string> geneDistribFileNames;

    if (world.rank() == 0)
    {
        // **************
        // 1 reading data
        // **************


    
        // 1A reading tree data
        // **************
    
        // 1A.1 gene file names and files
        // **************

        if( optionHandler->get_multiple_gene_families() )
        {
            pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
        }
        else
            geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );
    
    
        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
            exit(pb);
        }


        // 1A.2 species tree data
        // **************
    
    
        pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, optionHandler->get_input_is_ale() , optionHandler->get_unrooted_species_tree() ); 
    
        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
            exit(pb);
        }

        /*
        boost::dynamic_bitset<> root { SpeciesCCPDistrib->get_number_of_leaves() , 0 };
        root.flip();
        for( auto it = SpeciesCCPDistrib->tripartition_cbegin( root ) ; it != SpeciesCCPDistrib->tripartition_cend( root ) ; ++it )
        {
    
            boost::dynamic_bitset<> sigmaPP = SpeciesCCPDistrib->get_complementer(root, it->first);
            int id1 = SpeciesCCPDistrib->get_bitset_id( it->first );
            int id2 = SpeciesCCPDistrib->get_bitset_id( sigmaPP );

            if((id1 == -1) || (id2 == -1))
            {
                cout << " P" << world.rank() <<" ";            
                cout << it->first << "|" << sigmaPP;
                cout << " -> " << id1 << "," << id2;
                cout << " -> " << it->second << endl;
                exit(1);
            }
    
        }
        cout << " P" << world.rank() <<" species distrib checks off after read" << endl;
        */

    }


    ////
    //// the server reads all gene file names 
    //// and divides them between the different processes (including itelf)

    if( world.rank() == 0 )
    {
        vector< vector< string > > dividedFileNames = divideFilesNames( geneDistribFileNames , world.size() );

        //cout << "divided gene families in: ";
        //for(unsigned i = 0 ; i < world.size() ; i++ )
        //    cout << dividedFileNames[i].size() << " ";
        //cout << endl;


        geneDistribFileNames.clear();
        geneDistribFileNames = dividedFileNames[0];

        for(unsigned i = 1 ; i < world.size() ; i++ )
        {
            //cout << "trying to send" <<endl;
            world.send(i, 0, dividedFileNames[i] );
        }

    }
    else
    {
        //cout << "trying to receive" << endl;
        world.recv(0,0, geneDistribFileNames );

        if(VerboseLevel>2)
            cout << "process " << world.rank() << " received " << geneDistribFileNames.size() << "file names" << endl;
    }


    ////
    //// processes read and build their respective gene families
    

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        if(VerboseLevel>2)
            cout << "process "<< world.rank() <<" is reading : "<< geneDistribFileNames[ GeneFamilyId ]<<endl;

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , optionHandler->get_input_is_ale() , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "process "<< world.rank() << "read gene distribution(s)."<<endl;




    // 1B reading gene species correspondence data
    // **************

    if( optionHandler->get_corr_input_filename() != "" )
    {
        cerr << " option for gene-species correspondence file disabled in MPI mode. sorry." << endl;
        cerr << " reverting to leaf name analysis to find out. excepted format: species"<< optionHandler->get_separator()<<"gene"<<endl;
        /*
        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }*/

    }
    /*else
    {*/
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    /*}*/


    // 1D eventually reading branch-wise input rates
    // ************************
    map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > rateMap;
    if(world.rank() == 0)
    {
        if(optionHandler->get_input_rate_file() != "")
        {
    
            int pb = readRatesFile(optionHandler->get_input_rate_file() , SpeciesCCPDistrib , rateMap);
            if(pb != 0)
            {
                if(pb == 1)
                    cerr << "Unable to open the rate input file: " << optionHandler->get_input_rate_file() << endl ;
                exit(pb);
    
            }
        }
    }


    if(VerboseLevel > 2)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }



    ///////////////////////////
    // model instanciations
    ///////////////////////////

    PsDLModel * model;// = new PsDLModel();


    //// setup of some variables - truly, only the serves need them ... but the're not so much that they should cause a problem.

    bool BranchWiseRates = optionHandler->get_branchWiseRates();

    double InitialDupRate = optionHandler->get_initialDupRate();
    double InitialLossRate = optionHandler->get_initialLossRate();

    //working with the assumption of an underlying Gamma distribution for rates. 
    //used to get some "default" values for extreme observations of p10 and p11 when re-computing rates
    double DupRateMean = InitialDupRate;
    double DupRateVar = DupRateMean;

    double LossRateMean = InitialLossRate;
    double LossRateVar = LossRateMean;

    double nbRateCategories = 10;
    double MinMinRate = 0.000000001; // minimum possible rate. to avoid underflows because of very low duplication of loss numbers 


    /// initiation of the data for species CCP distribtuion update
    unsigned int SpCCPupdateNbIteration = optionHandler->get_SpCCPUpdatingNbRound();
    double splitWeight = optionHandler->get_SpCCPUpdatingWeight();

    double speciesCCPcutOff = optionHandler->get_cutOff();
    bool isRelativeCutoff = optionHandler->get_isRelativeCutoff();

    double extantSampling = optionHandler->get_extant_sampling();

    // rates expectation maximisation process
    //pair<double,double> currentRates(InitialLossRate , InitialDupRate );
    long double logP = 0;

    //presuming fixed branch lengths for now

    unsigned int sampleSize = optionHandler->get_outputSample();
    unsigned int MaxNumberIteration = SpCCPupdateNbIteration; // optionHandler->get_maxIteration(); //<- should become some parameter



    broadcastSpeciesCCPdistrib(world, world.rank() , SpeciesCCPDistrib );

    /*
    boost::dynamic_bitset<> root { SpeciesCCPDistrib->get_number_of_leaves() , 0 };
    root.flip();
    for( auto it = SpeciesCCPDistrib->tripartition_cbegin( root ) ; it != SpeciesCCPDistrib->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = SpeciesCCPDistrib->get_complementer(root, it->first);
        int id1 = SpeciesCCPDistrib->get_bitset_id( it->first );
        int id2 = SpeciesCCPDistrib->get_bitset_id( sigmaPP );
        if((id1 == -1) || (id2 == -1))
        {
            cout << " P" << world.rank() <<" ";            
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << it->second << endl;
            exit(1);
        }

    }
    cout << " P" << world.rank() <<" species distrib checks off" << endl;

    */


    unsigned int NbGeneDistrib = geneDistribFileNames.size();


    // instanciation of the model
    // **************

    model = new PsDLModel();
    // instanciation that does not rely of the gene trees    
    model->addSpeciesCCPdistribution(SpeciesCCPDistrib, LowerVerboseLevel);// makes some precomputations With ids


    if( world.rank() == 0 )
    {

        model->set_DuplicationRates( InitialDupRate ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
        model->set_LossRates( InitialLossRate ); //  takes : loss rate and set it as the global duplication rate for all the species tree
   
        if(optionHandler->get_input_rate_file() != "")
        {
    
            for(auto it = rateMap.begin() ; it != rateMap.end() ; ++it)
            {
                model->set_DuplicationRates( it->first , it->second[0] );
                model->set_LossRates( it->first , it->second[1] );
            }
    
        }

    }

    /// sharing rates with all instances
    broadcastRates( world, world.rank() , model , BranchWiseRates );


    /// initializations ... this is quite a bit of computations that are made in each process ... 
    /// I would write a serialization for these later, but I'm not even sure that the time these computation take beat the time it would take to transmit all the model between the processes.

    model->set_deltal( 0.05 ) ; // maximum size of the considered "time" interval, defined in terms of species tree branch length (aka "branch length interval")
    model->set_minNbBLInterval( 5 ) ; // minimum number of interval for a given branch of the species tree

    model->set_rootBranchLength( 1.0 );

    model->divideSpeciesBranches(LowerVerboseLevel);    //   

    model->set_probaExtantGeneSampled(extantSampling);

    /*
    for( auto it = SpeciesCCPDistrib->tripartition_cbegin( root ) ; it != SpeciesCCPDistrib->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = SpeciesCCPDistrib->get_complementer(root, it->first);
        int id1 = SpeciesCCPDistrib->get_bitset_id( it->first );
        int id2 = SpeciesCCPDistrib->get_bitset_id( sigmaPP );
        if((id1 == -1) || (id2 == -1))
        {
            cout << " P" << world.rank() <<" ";            
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << it->second << endl;
            exit(1);
        }

    }
    cout << " P" << world.rank() <<" species distrib checks off 2" << endl;
    */

    model->computeProbaGeneExtinct(LowerVerboseLevel);  // maybe these could move to protected when they've been tested

    /*
    for( auto it = SpeciesCCPDistrib->tripartition_cbegin( root ) ; it != SpeciesCCPDistrib->tripartition_cend( root ) ; ++it )
    {

        boost::dynamic_bitset<> sigmaPP = SpeciesCCPDistrib->get_complementer(root, it->first);
        int id1 = SpeciesCCPDistrib->get_bitset_id( it->first );
        int id2 = SpeciesCCPDistrib->get_bitset_id( sigmaPP );
        if((id1 == -1) || (id2 == -1))
        {
            cout << " P" << world.rank() <<" ";            
            cout << it->first << "|" << sigmaPP;
            cout << " -> " << id1 << "," << id2;
            cout << " -> " << it->second << endl;
            exit(1);
        }

    }
    cout << " P" << world.rank() <<" species distrib checks off 3" << endl;
    */

    model->computeProbaGeneOneChild(LowerVerboseLevel); // as they NEED to be executed only once and in this precise order

    model->initializeGeneLeavesProbas();


    ///////////////////
    ////    can only happen when all processes have access to the species CCP
    ///////////////////

    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        GeneCladeSpeciesMasks.push_back( getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }


    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks[GeneFamilyId] );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }


    bool reMapSpeciesCCP = false; // in the  expectation maximization algorithm the ccp distribution is updated.
    
    if(VerboseLevel>0)
        cout << "process " << world.rank() << " ready to start computing reconciliations." << endl;

    int roundCount = 1;

    bool CONTINUE = true;

    while(CONTINUE)
    {
        ////// log probability ////////
        logP = 0;

        ////// RATE OPTIMIZATION //////
        map < int , array<double,3>  > transitionCount;// RATE OPT NEW


        ////// SPECIES CCP UPDATE /////
        map< int, map< int , int > > splitCountsObservedInReconciliations; 

        /////////////////////////////////////////////////////////

        /// computing reconciliations and backtracking fro all families
        for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        {

            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];

            ///matrix computation
    
            model->addGeneCCPdistribution(GeneCCPDistrib, LowerVerboseLevel);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );
    
            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 



            /// doing stuff
            logP += log( model->getOverAllProbability() );

            //////////////// ONLY FOR PSDL MAX /////////////////////
            // backtracking over all gene families 
        
            for(unsigned i = 0 ; i < sampleSize  ; i++)
            {
                shared_ptr<RecNode> RT = model->backtrack();
                //cout << "BT : \n"<< RT->getRecXML() <<endl;
        
                ////// RATE OPT NEW //////
                countTransitions( RT ,transitionCount , 1.0 );
                //////////////////////////

                ////// SP CCP UPDATE /////
                if( splitWeight > 0)
                    AddReconciledTreeSpeciesSplitsToMap(  RT , splitCountsObservedInReconciliations);
                //////////////////////////    
            }
            //////////////// END ONLY FOR PSDL MAX /////////////////

            ////resetting matrix
            if(VerboseLevel>0)
                    cout << "process " << world.rank() << " finished backtrack of family " << GeneFamilyId << "/" << NbGeneDistrib-1 <<endl;
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
        }


        /// post-treatment !!

        //cout << "process " << world.rank() << " logP= " <<  logP << endl;

        if (world.rank() == 0) 
        {


            ////////////////// computing total logP ////////////////////////////

            long double totallogP = 0;
            vector< long double > logPs ;

            gather(world, logP, logPs,  0);

            for(unsigned i = 0 ; i < logPs.size() ; i++ )
                totallogP += logPs[i];
            logP = totallogP;
            //cout << "The totallogP value is " << totallogP << std::endl;
            
            //////////////////////////////////////////////

            /////////////////// RATE OPTIMIZATION /////////////////////////////////

            double DefaultMinDup  = getMeanExtremeQuantileGammaD( DupRateMean , DupRateVar , nbRateCategories, false);
            double DefaultMaxDup  = getMeanExtremeQuantileGammaD( DupRateMean , DupRateVar , nbRateCategories, true);
            double DefaultMinLoss = getMeanExtremeQuantileGammaD( LossRateMean , LossRateVar , nbRateCategories, false);
            double DefaultMaxLoss = getMeanExtremeQuantileGammaD( LossRateMean , LossRateVar , nbRateCategories, true);
    
            /// enacting a lower limit for rates to avoid some underflows ( generation of impossible reconciliations )
            if(DefaultMinDup < MinMinRate)
            {
                DefaultMinDup = MinMinRate;
                if(DefaultMinDup > DefaultMaxDup)
                    DefaultMaxDup = DefaultMinDup;
            }
    
            if(DefaultMinLoss < MinMinRate)
            {
                DefaultMinLoss = MinMinRate;
                if(DefaultMinLoss > DefaultMaxLoss)
                    DefaultMaxLoss = DefaultMinLoss;
            }


            /// 1. gathering data
            gatherTransitionCounts( world, world.rank() , transitionCount ); // transition count is updated with the data of all processes

            if( BranchWiseRates )
            {
                model->set_DuplicationRates( DefaultMinDup );
                model->set_LossRates( DefaultMinLoss );
            }
    
            // NB : only useful if not branch wide ... 
            double SumWeightedDupRates = 0; // BW duplication rates weighted by the number of observation they have
            double SumWeightedLossRates = 0; // BW loss rates weighted by the number of observation they have
            double sumObservations = 0;
        
            /// 2. computing new branch-wise rates
            for(auto it = transitionCount.begin() ; it != transitionCount.end() ; ++it)
            {
                double nbObservations = it->second[0] + it->second[1] + it->second[2] ;
                double p10 = it->second[0] / nbObservations;
                double p11 = it->second[1] / nbObservations;
                //cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << "|" << it->second[0] <<","<< it->second[1] <<","<< it->second[2] << endl;
                double pExtinct = model->get_ProbaGeneExtinct(it->first,0);
    
                boost::dynamic_bitset<> sigma = model->get_CCPindexFromSpeciesCladeIndex( it->first );
                double Blen = model->get_speciesCCPdistribution()->get_branchlength(sigma);
                if( model->get_speciesCCPdistribution()->get_number_of_leaves() == sigma.count() )
                { // special root procedure
                    Blen  = model->get_rootBranchLength();
                }
    
                pair < double, double > rates = estimateNewRates( p10, p11, Blen, pExtinct );
    
                ///handling particular cases 
                if( rates.first == -1 )
                    rates.first = DefaultMaxDup;
                else if( rates.first == -2 )
                    rates.first = DefaultMinDup;
    
                if( rates.second == -1 )
                    rates.second = DefaultMaxLoss;
                else if( rates.second == -2 )
                    rates.second = DefaultMinLoss;
    
                if(VerboseLevel>2)
                    cout << " sp: "<< it->first << " p10:"<< p10 <<" p11:"<< p11 << " l:"<< Blen << " p:" << pExtinct << " -> D:" << rates.first << " L:"<< rates.second<<endl;
    
                
                model->set_DuplicationRates( it->first , rates.first );
                model->set_LossRates( it->first , rates.second );
                
                if( !BranchWiseRates )
                {    
                    SumWeightedDupRates  += nbObservations * rates.first ;
                    SumWeightedLossRates += nbObservations * rates.second;
                    sumObservations += nbObservations;
                }
    
    
            }
    
            /// 3. update of rates mean and variance
            pair<double,double> newRates(-1 , -1 );

            vector < long double> Drates( model->get_DuplicationRates() );
            DupRateMean = getMean(Drates);
            DupRateVar =  getVar( Drates , DupRateMean);
    
            vector < long double> Lrates(  model->get_LossRates() );
            LossRateMean = getMean(Lrates);
            LossRateVar =  getVar( Lrates , LossRateMean);
    
    
            if(VerboseLevel>2)
            {
                for(unsigned i = 0 ; i < Drates.size();i++)
                    cout << Drates[i] << " : " << Lrates[i] << endl;
    
            }
    
            /// 4. updates of tree-wide rates if need be
            if(! BranchWiseRates )
            {
                /*
                Here I use the rates weighted by the number of time a branch of the species tree were observed. 
                This is especially important as we use a species CCP distributions, and we want to avoid scenarii where, 
                for instance a lot of branches where nothing happens are observed once each, 
                and they influence the overall rate a lot while the branch where a loss happened in every reconciliations is only once once
                */
                newRates.first = SumWeightedDupRates / sumObservations;
                newRates.second = SumWeightedLossRates / sumObservations;
    
                model->set_DuplicationRates( newRates.first ); //  takes : duplication rate and set it as the global duplication rate for all the species tree
                model->set_LossRates( newRates.second ); //  takes : loss rate and set it as the global duplication rate for all the species tree

            }



            //////////////////////////////////////////////

            if(VerboseLevel>0)
            {
                cout << "****************************************"<< endl;
                cout << "event rates and species CCP distribution optimization round " << roundCount << endl;;

                if(!BranchWiseRates)
                {
                    cout << "new duplication rate: " << newRates.first;
                    cout << "\tnew loss rate: " << newRates.second <<endl;
                }
                else
                {
                    cout << "mean Duplication rate : " << DupRateMean << " variance : "<< DupRateVar << endl;
                    cout << "mean Loss rate : " << LossRateMean<< " variance : "<< LossRateVar << endl;
                }
    
                cout << "new log(P): "<< logP;
                cout << endl;
                cout << "****************************************"<< endl;
    
            }


            ////// SP CCP UPDATE /////

            if( splitWeight > 0)
            {

                //// filling a clade id map
                vector< boost::dynamic_bitset<> > SpeciesIdToCladeId;
                unsigned int Sid = 0;
                SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
                Sid++;
                SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
        
                while( SpeciesIdToCladeId.back().count() != 0 )
                {
                    Sid++;
                    SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );        
                }


                gatherOservedSplit( world, world.rank() , splitCountsObservedInReconciliations );

                UpdateCCPdistribution( SpeciesCCPDistrib,  splitCountsObservedInReconciliations , SpeciesIdToCladeId , splitWeight);

                SpeciesIdToCladeId.clear();
            }
    
            ///applying cutoff
            if(speciesCCPcutOff > 0)
            {
                
                int nbDeleted = CutOffCCPdistribution( SpeciesCCPDistrib, speciesCCPcutOff , isRelativeCutoff , LowerVerboseLevel);
    
                if(nbDeleted>0)
                { // re-map the species CCP inside the model.
                    if(VerboseLevel>0)
                    {
                        cout << " deleted " << nbDeleted << " clades in the CCP distribution (cut-off:" << speciesCCPcutOff << ")."<<endl;
                    }
                    reMapSpeciesCCP = true;
                    
                }
                else
                    reMapSpeciesCCP = false;

            }
            //////////////////////////

            if((optionHandler->get_writeIntermediary() ) && ( roundCount != MaxNumberIteration ))
            {
                //if((splitWeight+speciesCCPcutOff) >0)
                //    MakeBipCoherentWithRootSplits(SpeciesCCPDistrib);

                string fileName = optionHandler->get_outputFile() + ".round" + int2string( roundCount ) + ".updated.species.ale";
                OutputCCPdistribution(fileName, SpeciesCCPDistrib);
            }
        } 
        else 
        { // non server just bring send their data
            gather(world, logP, 0);
            gatherTransitionCounts( world, world.rank() , transitionCount );
            if( splitWeight > 0 )
                gatherOservedSplit( world, world.rank() , splitCountsObservedInReconciliations );

        }


        roundCount++;
        /// stop conditions are checked by the server
        if(world.rank() == 0)
        {
            if(roundCount > MaxNumberIteration )
                CONTINUE = false;
        }
        broadcast(world , CONTINUE , 0 ); // sharing the stop condition


        /// preparation of next rounds
        if(CONTINUE)
        { //// we continue computing --> send data to process and perform pre-computations for next round
            
            if((splitWeight+speciesCCPcutOff) >0)
            {
                broadcastSpeciesCCPdistrib(world, world.rank() , SpeciesCCPDistrib );
            }


            broadcast( world , reMapSpeciesCCP , 0 );
            if( reMapSpeciesCCP )
                model->reMapSpeciesCCPdistribution();

           /// sharing rates with all instances
            broadcastRates( world, world.rank() , model , BranchWiseRates );

            model->reset();
            model->computeProbaGeneExtinct();
            model->computeProbaGeneOneChild();
        }

        for( auto it = splitCountsObservedInReconciliations.begin(); it != splitCountsObservedInReconciliations.end() ; ++it)
        {
            it->second.clear();
        }
        splitCountsObservedInReconciliations.clear();


    }


    /// main loop is finished.

    if(world.rank() == 0)
    { // output

    
    
        string filename = optionHandler->get_outputFile();
        if(filename!="")
        {
            //if((splitWeight+speciesCCPcutOff) >0)
            //    MakeBipCoherentWithRootSplits(SpeciesCCPDistrib);

            filename += ".updated.species.ale";
            OutputCCPdistribution( filename , SpeciesCCPDistrib );

        }
        else
            cout << SpeciesCCPDistrib->output_as_ale() << endl;
    


        streambuf * buf;
        ofstream of;
    
        FILE *fp;

    
        if(BranchWiseRates)
        {
            filename = optionHandler->get_outputFile();
            if(filename!="")
            {
                filename += ".rates.txt";
                of.open(filename.c_str() );
                buf = of.rdbuf();
            }
            else
                buf = cout.rdbuf();
        
            ostream OUT(buf);
        
            
            OutputBranchWiseRates(  OUT , model );
        
            if(filename!="")
            {        
                of.close();
            }
    
        }
    
        if(VerboseLevel>0)
        {            
    
            cout << "****************************************"<< endl;
            if(!BranchWiseRates)
            {
                cout << "final duplication rate: " << model->get_DuplicationRates(0) << endl;
                cout << "final loss rate: " << model->get_LossRates(0) << endl;
            }
            else
            {
                cout << "final mean duplication rate: " << DupRateMean << endl;
                cout << "final mean loss rate: " << LossRateMean << endl;

            }
            cout << "final log(P): "<< logP << endl;
            cout << "****************************************"<< endl;
    
            pair<Tree,double> res = SpeciesCCPDistrib->get_posterior_proba_tree(false);
            cout << "max species posterior probability:       " << res.second << endl;
            cout << "max species posterior probability tree : " << res.first.output_as_newick() << endl;
        
            cout << "****************************************"<< endl;
        }


    }



    delete model;
    SpeciesCCPDistrib.reset();
    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        GeneCCPDistribV[GeneFamilyId].reset();
    GeneCCPDistribV.clear();



    if(VerboseLevel>1)
        cout << "process " << world.rank() << " finishing" << endl;

    return 0;

}
