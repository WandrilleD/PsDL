/*

This file contains a small executable to play with reconciliation of gene qnd species CCPs

Created the: 04-05-2018
by: Wandrille Duchemin

Last modified the: 18-05-2018
by: Wandrille Duchemin

*/



//#include <sys/stat.h>
//#include <fstream>
#include "Utils.h"
#include "PsDLMaxUtils.h"
#include "PsDLUtils.h"

#include "OptionHandler.h"

#include "PsDLModel.h"
#include "PsDTLModel.h"
#include "speciesCCPupdateUtils.h"

#include "NelderMeadOptimizer.h"


#include <CCPCPP/treeset_metadata.h>


#include <sstream>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#include <ctime>

#include <limits>       // std::numeric_limits

#include <boost/program_options.hpp>

using namespace std;




/////////////////////////////////////////////////
// Main 
/////////////////////////////////////////////////

int main(int argc, char ** argv)
{
    //cout << "     double : " << std::numeric_limits<double>::min() << " " << std::numeric_limits<double>::max() << '\n';                double : 2.22507e-308 1.79769e+308
    //cout << "long double : " << std::numeric_limits<long double>::min() << " " << std::numeric_limits<long double>::max() << '\n'; long double : 3.3621e-4932 1.18973e+4932
    

    // reading and checking options

    shared_ptr<OptionHandler> optionHandler = make_shared<OptionHandler>();
    optionHandler->readArguments(argc, argv);

    int pb = 0;

    if(!optionHandler->verify())
    {
        optionHandler->printUsage();
        return -99;
    }

    unsigned int VerboseLevel = optionHandler->get_verbose_level();
    unsigned int LowerVerboseLevel = VerboseLevel;
    if(LowerVerboseLevel>0)
        LowerVerboseLevel-=1;  



    // initializing some objects

    ////print some preliminary info

    /* initialize random seed: */
    int seed = optionHandler->get_seed() ;
    if( seed < 0 )
        seed = time(NULL);

    if( VerboseLevel > 0 )
        cout << "using random seed : "<< seed << endl;

    srand(seed);
 
    string algorithm = optionHandler->get_algorithm() ;

    bool WithTransfer = optionHandler->get_withTransfer();

    if( VerboseLevel > 0 )
    {
        cout << "Algorithm : "<< algorithm;    
        cout << " - ";
        if(!WithTransfer)
            cout << "no ";
        cout << "transfers." << endl;
    }
    


    shared_ptr<TreesetMetadata> SpeciesCCPDistrib = make_shared<TreesetMetadata>();

    map< string , string > GtoSpCorr; // correspondence betzeen gene and species names


    // **************
    // 1 reading data
    // **************

    
    // 1A reading tree data
    // **************

   // 1A.1 gene file names and files
    // **************



    vector <string> geneDistribFileNames;

    int fileFormat = guessFileFormat( optionHandler->get_gene_input_filename() );
    if(fileFormat == -1)
    {
        cerr << "Unable to open the gene input file: " << optionHandler->get_gene_input_filename() << endl ;
        exit(1);
    }
    else if( fileFormat == 3 )
    {
        pb = readGeneDistributionFile( optionHandler->get_gene_input_filename() , geneDistribFileNames );
    }
    else
        geneDistribFileNames.push_back( optionHandler->get_gene_input_filename() );

    if(VerboseLevel>1)
    {
        cout << "Detected gene file format : " ;
        if(fileFormat == 1)
            cout << "newick" << endl;
        if(fileFormat == 2)
            cout << "ale" << endl;
        if(fileFormat == 3)
            cout << "multiple files" << endl;           
    }

    vector< shared_ptr<TreesetMetadata> > GeneCCPDistribV;

    for(unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {

        GeneCCPDistribV.push_back( make_shared<TreesetMetadata>() );

        pb = buildCCPDistributionFromFile( geneDistribFileNames[ GeneFamilyId ] , GeneCCPDistribV.back() , fileFormat , true ); // handle the gene trees as unrooted

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the gene input file: " << geneDistribFileNames[ GeneFamilyId ] << endl ;
            if(pb == 2)
                cerr << "Gene input file: \"" << geneDistribFileNames[ GeneFamilyId ] << "\" was not in a valid format (newick or ale)." << endl ;

            exit(pb);
        }


    }

    if(VerboseLevel>1)
        cout << "read gene distribution(s)."<<endl;


    // 1A.2 species tree data
    // **************


    pb = buildCCPDistributionFromFile( optionHandler->get_species_input_filename() ,  SpeciesCCPDistrib, 3 , optionHandler->get_unrooted_sp() ); // detects format

    if( pb != 0 )
    {
        if(pb == 1)
            cerr << "Unable to open the species input file: " << optionHandler->get_species_input_filename() << endl ;
        if(pb == 2)
            cerr << "Species input file: \"" << optionHandler->get_species_input_filename() << "\" was not in a valid format (newick or ale)." << endl ;
        exit(pb);
    }

    if(VerboseLevel>1)
        cout << "read species distribution."<<endl;


    // 1B reading gene species correspondence data
    // **************


    if( optionHandler->get_corr_input_filename() != "" )
    {

        pb = readGeneSpeciesCorrespondenceFile(optionHandler->get_corr_input_filename(), GtoSpCorr);

        if( pb != 0 )
        {
            if(pb == 1)
                cerr << "Unable to open the correspondence input file: " << optionHandler->get_corr_input_filename() << endl ;
            exit(pb);
        }

    }
    else
    {
        //deduce correspondences from the leaf names
        for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
        {
            shared_ptr< TreesetMetadata >  GeneCCPDistrib = GeneCCPDistribV[GeneFamilyId];
            for(unsigned i = 1 ; i <= GeneCCPDistrib->get_number_of_leaves() ; i++ )
            {
                pair<string , string> p = splitLeafName( GeneCCPDistrib->get_name_of_leaf(i) , optionHandler->get_separator() );

                GtoSpCorr[GeneCCPDistrib->get_name_of_leaf(i)] = p.first ;
            }
        }

    }
    if(VerboseLevel > 2)
    {
        for( auto it = GtoSpCorr.begin(); it != GtoSpCorr.end() ; ++it)
            cout << "associating leaf "<< it->first << " to species " << it->second << endl;
    }

    // equivalence between gene and species bitsets
    // equivalence between gene and species bitsets
    vector< map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > > GeneCladeSpeciesMasks;

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > x;
        GeneCladeSpeciesMasks.push_back( x );//getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) );
    }



    // 1C checking that correspondances are correct
    // ***************************

    for( unsigned GeneFamilyId = 0 ; GeneFamilyId < geneDistribFileNames.size() ; GeneFamilyId++ )
    {
        map< boost::dynamic_bitset<>, boost::dynamic_bitset<> > GeneCladeSpeciesMasks = getGeneCladeSpeciesMasks( GeneCCPDistribV[GeneFamilyId] , SpeciesCCPDistrib , GtoSpCorr) ;
        boost::dynamic_bitset<> pb =  verifyLeafCorrespondences( GeneCladeSpeciesMasks );
        if( pb.count() > 0 )
        { // the bitset has no valid species association
            string leafName =  GeneCCPDistribV[GeneFamilyId]->get_name_of_leaf( pb );
            cerr << "ERROR: gene leaf "<< leafName << " from file "<< geneDistribFileNames[GeneFamilyId]<< " could not be associated to any associated species. Aborting." << endl;
            exit(2);
        }
    }
    

    // 1D eventually reading branch-wise input rates
    // ************************
    map< boost::dynamic_bitset<> , vector< FLOAT_TYPE > > rateMap;
    if(optionHandler->get_input_rate_file() != "")
    {

        int pb = readRatesFile(optionHandler->get_input_rate_file() , SpeciesCCPDistrib , rateMap);
        if(pb != 0)
        {
            if(pb == 1)
                cerr << "Unable to open the rate input file: " << optionHandler->get_input_rate_file() << endl ;
            exit(pb);

        }


        //cout << "rates:" << endl;
        //for(auto it = rateMap.begin() ; it!=rateMap.end();++it)
        //{            
        //    cout << it->first << "-> ";
        //    for(auto r:it->second)
        //        cout << " " << r ;
        //    cout << endl;
        //}
    }

    // 1E eventually reading extant genes sampling rates
    // ************************

    map< string ,  FLOAT_TYPE > ExtantSamplingMap;
    if( optionHandler->get_extant_sampling() != "" ) 
    {
        int pb = readExtantSamplingFile(optionHandler->get_extant_sampling() , SpeciesCCPDistrib, ExtantSamplingMap );
        if(pb != 0)
        {
            if(pb == 1)
                cerr << "Unable to open the extant sampling input file: " << optionHandler->get_extant_sampling() << endl ;
            exit(pb);

        }
        
    }




    // 1F preparing output
    // ************************

    string Prefix = optionHandler->get_outputPrefix();
    unsigned int nbOutputSamples = optionHandler->get_outputSample();


    streambuf * buf;
    ofstream of;

    FILE *fp;
    if((Prefix!="") && (nbOutputSamples>0))
    {
        of.open( Prefix + ".trees.xml" );
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream OUT(buf);

    if(nbOutputSamples>0)
        InitRecXMLToStream( OUT );





    streambuf * bufSAMPLE;
    ofstream ofSAMPLE;

    FILE *fpSAMPLE;
    if((Prefix!="") && (algorithm == "sample"))
    {
        ofSAMPLE.open( Prefix + ".sampled.rates.txt" );
        bufSAMPLE = ofSAMPLE.rdbuf();
    }
    else
        bufSAMPLE = cout.rdbuf();

    ostream OUTSAMPLE(bufSAMPLE);



    /// initiation of the data for species CCP distribtuion update
    

    // **************
    // 2A instanciating some variables
    // **************



    //generic parameters
    bool STOP = false;
    int roundNumber = 1;

    int roundNumberToDo = 0; 

    int NbGeneDistrib = geneDistribFileNames.size();

    bool BranchWiseRates = optionHandler->get_branch_wise();


    vector<FLOAT_TYPE> DefaultRates( 2 , optionHandler->get_initialDupRate() );
    DefaultRates[1] = optionHandler->get_initialLossRate();
    if(WithTransfer)
        DefaultRates.push_back( optionHandler->get_initialTransferRate() );


    // MAX algorithm specific parameters 
    bool MAX_writeIntermediary = false;
    bool MAX_NoRateUpdate = false;
    double MAX_SpCCPUpdatingWeight = 0;
    double MAX_cutOff = 0;
    bool MAX_relativeCutOff = false;
    int MAX_sampleSize = 0;

    bool MAX_force_resolve_mode_ = false;
    vector< int > MAX_speciesCladeToForceResolution; 



    map < int , vector< long double>  > RateUpdateCount;
    vector < long double > RateDefaultMinimums;
    vector < long double > RateDefaultMaximums;
    vector < long double > RateMeans;
    vector < long double > RateVars;
    vector < long double > CurrentRate = DefaultRates;

    double nbRateCategories = 10;
    double MinMinRate = 0.000000001; // minimum possible rate. to avoid underflows because of very low duplication of loss numbers 
    
    map< int, map< int , float > > splitCountsObservedInReconciliations; // SP CCP UPDATE
    float SpeciationLossWeights = optionHandler->get_MAX_SpeciationLossWeights();
    bool OneVotePerReconciledTree = optionHandler->get_MAX_OneVotePerReconciledTree();


    vector< boost::dynamic_bitset<> > SpeciesIdToCladeId;

    if(algorithm == "max")
    {
        MAX_writeIntermediary = optionHandler->get_MAX_writeIntermediary();
        MAX_NoRateUpdate = optionHandler->get_MAX_NoRateUpdate();
        
        roundNumberToDo = optionHandler->get_MAX_SpCCPUpdatingNbRound();

        MAX_SpCCPUpdatingWeight = optionHandler->get_MAX_SpCCPUpdatingWeight();
        MAX_cutOff = optionHandler->get_MAX_cutOff();
        MAX_relativeCutOff = optionHandler->get_MAX_relativeCutOff();
        MAX_sampleSize = optionHandler->get_MAX_sampleSize();

        MAX_force_resolve_mode_ = optionHandler->get_MAX_force_resolve_mode();
        if( MAX_force_resolve_mode_ )
        {
            roundNumberToDo = -1 ; // we change the stopping condition.
            
            MAX_SpCCPUpdatingWeight = 0;
        }



        /// filling some containers used to frame the new rates computations.
        RateMeans.reserve(2 + WithTransfer);
        RateVars.reserve(2 + WithTransfer);

        RateMeans.push_back( DefaultRates[0] );
        RateMeans.push_back( DefaultRates[1] );
        if(WithTransfer)
            RateMeans.push_back( DefaultRates[2] );
        RateVars.push_back( DefaultRates[0] );
        RateVars.push_back( DefaultRates[1] );
        if(WithTransfer)
            RateVars.push_back( DefaultRates[2] );
        

        if(optionHandler->get_input_rate_file() != "")
        { // use real mean and var instead.


            vector< vector<long double> > ZipRates( 2+WithTransfer , vector<long double>(  rateMap.size() ) );

            unsigned i = 0;

            for(auto it : rateMap)
            {
                for(unsigned j = 0 ; j < it.second.size() ; j++)
                {
                    ZipRates[j][i] = it.second[j];
                }
                i++;
            }
            
            for(unsigned i = 0 ; i < ZipRates.size() ; i++)
            {
                RateMeans[i] = getMean( ZipRates[i] );
                RateVars[i] = getVar( ZipRates[i] , RateMeans[i] );
                if(RateVars[i] == 0) // a variance to 0 would cause problems. -> use the mean, as when we use a single rate as input
                    RateVars[i] = RateMeans[i]; 
            }

        }

        RateDefaultMinimums.reserve(2+WithTransfer);
        RateDefaultMaximums.reserve(2+WithTransfer);
        RateDefaultMinimums.push_back(0);RateDefaultMinimums.push_back(0);
        RateDefaultMaximums.push_back(0);RateDefaultMaximums.push_back(0);
        if(WithTransfer)
        {
            RateDefaultMaximums.push_back(0); RateDefaultMinimums.push_back(0);
        }
    }


    // GRID algorithm specific parameters 
    vector < vector< long double > > GRID_rateLists;
    vector< long double > GRID_LkhList;

    if( algorithm == "grid" )
    {

        vector < double > minimums ;
        vector < double > maximums ;

        minimums.push_back( optionHandler->get_GRID_MinDup() );
        maximums.push_back( optionHandler->get_GRID_MaxDup() );
        minimums.push_back( optionHandler->get_GRID_MinLoss() );
        maximums.push_back( optionHandler->get_GRID_MaxLoss() );

        if(WithTransfer)
        {
            minimums.push_back( optionHandler->get_GRID_MinTrans() );
            maximums.push_back( optionHandler->get_GRID_MaxTrans() );
        }

        GRID_rateLists = GRID_generate_rateList( optionHandler->get_GRID_stepNb(), 
                                                 minimums , 
                                                 maximums , 
                                                 optionHandler->get_GRID_NoLog() );

        roundNumberToDo = GRID_rateLists.size();


        //setting starting rates.
        for(unsigned i = 0 ; i < GRID_rateLists[ roundNumber-1 ].size() ; i++ )
        {
            DefaultRates[i] = GRID_rateLists[ roundNumber-1 ][i];
        }
        
        for(auto it = rateMap.begin() ; it!=rateMap.end();++it)
            it->second.clear();
        rateMap.clear(); // clearing in case of an impromptu input rate file. 

    }

    // SAMPLE algorithm specific parameter
    int SAMPLE_sampleSize = optionHandler->get_SAMPLE_sampleSize();
    int SAMPLE_burnin = optionHandler->get_SAMPLE_burnin();
    int SAMPLE_thinning = optionHandler->get_SAMPLE_thinning();
    double CurrentLogP = 0;
    double MaxRate = 1000; 

    double accepted = 0; 

    double SD = 1;

    if(algorithm == "sample")
    {
        //setting starting rates.
        for(unsigned i = 0 ; i < DefaultRates.size() ; i++ )
        {
            DefaultRates[i] = SAMPLE_drawNewRate( DefaultRates[i], SD*2, MinMinRate,MaxRate);
            CurrentRate[i] = DefaultRates[i];
        }
        
        for(auto it = rateMap.begin() ; it!=rateMap.end();++it)
            it->second.clear();
        rateMap.clear(); // clearing in case of an impromptu input rate file.

        roundNumberToDo = SAMPLE_burnin + SAMPLE_thinning*SAMPLE_sampleSize;
    }



    // NELDERMEAD algorithm specific parameters
    NelderMeadOptimizer * NMoptimizer;

    if(algorithm == "nelderMead")
    {
    
        unsigned int parameterization = 0;
        long double Step = 3; // this is the difference between the different points during the optimization, in log units. ; Here, a value of 3 means a div/mult. by a factor ~20 (exp(3))
        long double No_improve_thr =10e-3;
        unsigned int No_improv_break =100;
        unsigned int Max_iter =0;
        double Alpha =1;
        double Gamma = 2;
        double Rho = 0.5;
        double Sigma =0.5;


        NMoptimizer = new NelderMeadOptimizer(
                                DefaultRates,
                                Step,
                                No_improve_thr,
                                No_improv_break,
                                Max_iter,
                                Alpha,
                                Gamma,
                                Rho,
                                Sigma
                                );


        DefaultRates = NMoptimizer->ReceiveLikelihoodAndGetNextParameterToTest( 0, VerboseLevel );

        roundNumberToDo = -1;
    }



    // **************
    // 2B instanciating the model
    // **************

    // instanciation that does not rely of the gene trees
    ParentModel * model;

    if( WithTransfer )
    {
        model = new PsDTLModel();
    }
    else
        model = new PsDLModel();




    model->prepareForComputation( SpeciesCCPDistrib , 
                                  DefaultRates, 
                                  rateMap,
                                  ExtantSamplingMap,
                                  LowerVerboseLevel);



    if( MAX_force_resolve_mode_ )
    {
        MAX_speciesCladeToForceResolution.push_back( model->get_CCPindexFromSpeciesCladeIndex( SpeciesCCPDistrib->get_gamma() ) );
    }



    // **************
    // 3 main loop
    // **************

    //cout << "number of rounds to do"  << roundNumberToDo << endl;

    //main loop
    if(roundNumberToDo!=-1) // -1 means infinite until some other stop condition
    {
        STOP = (roundNumberToDo<roundNumber);
    }




    while(!STOP)
    {


        if(algorithm == "max")
        {
            if(VerboseLevel>0)
                cout << "Round " << roundNumber << endl; 
            RateDefaultMinimums[0] = getMeanExtremeQuantileGammaD( RateMeans[0] , RateVars[0] , nbRateCategories, false);
            RateDefaultMaximums[0] = getMeanExtremeQuantileGammaD( RateMeans[0] , RateVars[0] , nbRateCategories, true);   
            RateDefaultMinimums[1] = getMeanExtremeQuantileGammaD( RateMeans[1] , RateVars[1] , nbRateCategories, false);
            RateDefaultMaximums[1] = getMeanExtremeQuantileGammaD( RateMeans[1] , RateVars[1] , nbRateCategories, true);

            if(WithTransfer)
            {
                RateDefaultMinimums[2] = getMeanExtremeQuantileGammaD( RateMeans[2] , RateVars[2] , nbRateCategories, false);
                RateDefaultMaximums[2] = getMeanExtremeQuantileGammaD( RateMeans[2] , RateVars[2] , nbRateCategories, true);

            }


            for(unsigned int i = 0 ; i < RateDefaultMinimums.size() ; i++)
            {
                if( RateDefaultMinimums[i] < MinMinRate )
                {
                    RateDefaultMinimums[i] = MinMinRate;
                    if(RateDefaultMinimums[i] > RateDefaultMaximums[i])
                        RateDefaultMaximums[i] = RateDefaultMinimums[i];
                }
            }    
    
            if(VerboseLevel>1)
            {
                cout << "  default dup      "<< RateDefaultMinimums[0] << " " << RateDefaultMaximums[0] << endl;
                cout << "  default loss     "<< RateDefaultMinimums[1] << " " << RateDefaultMaximums[1] << endl;
                if(WithTransfer)
                    cout << "  default transfer "<< RateDefaultMinimums[2] << " " << RateDefaultMaximums[2] << endl;
            }
            //////////////////////////
    
            //// filling the clade id map
            SpeciesIdToCladeId.clear();

            unsigned int Sid = 0;
            SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
            Sid++;
            SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );
        
            while( SpeciesIdToCladeId.back().count() != 0 )
            {
                Sid++;
                SpeciesIdToCladeId.push_back( model->get_CCPindexFromSpeciesCladeIndex( Sid ) );        
            }

            for(auto it : splitCountsObservedInReconciliations)
                it.second.clear();
            splitCountsObservedInReconciliations.clear();


            for(auto it : RateUpdateCount)
                it.second.clear();
            RateUpdateCount.clear();
        }


        long double totalP = 0;

        /// computing reconciliations and backtracking  all families
        for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        {
    
            shared_ptr<TreesetMetadata> GeneCCPDistrib = GeneCCPDistribV[ GeneFamilyId ];
    
            model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );
    
            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 

            totalP += log( model->getOverAllProbability() );
            
            if(algorithm == "max")
            {
                for(unsigned i = 0 ; i < MAX_sampleSize; i++ )
                {
                    shared_ptr< RecNode > RT= model->backtrack();

                    ////// RATE OPT //////
                    if(WithTransfer)
                    {
                        MAX_countTransitionsAndEvents( RT , RateUpdateCount , 1.0 );
                        //MAX_countEvents( RT , RateUpdateCount , 1.0 );
                    }
                    else
                        MAX_countTransitions( RT , RateUpdateCount , 1.0 );
                    //////////////////////////
    
                    ////// SP CCP UPDATE /////
                    AddReconciledTreeSpeciesSplitsToMap(  RT , splitCountsObservedInReconciliations , SpeciationLossWeights , OneVotePerReconciledTree );
                    //////////////////////////    

                }
            }

            if(VerboseLevel>1)
            {
                cout << " Finished computations "<< roundNumber << "/" << roundNumberToDo << " of family " << GeneFamilyId << "/" << NbGeneDistrib-1 ;
                cout << "(overall log likelihood : " << log( model->getOverAllProbability() ) << ")"<< endl;
            }
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
        }


        //stop conditions
        roundNumber+=1;

        if(roundNumberToDo!=-1) // -1 means infinite until some other stop condition
        {
            STOP = (roundNumberToDo<roundNumber);
        }


        //preparation of next round
        if( algorithm == "grid" )
        {
            GRID_LkhList.push_back(totalP);
            if(!STOP)
                model->set_Rates( GRID_rateLists[ roundNumber-1 ] );
        }
        else if( algorithm == "sample" )
        {



            bool accept = SAMPLE_acceptProposition( CurrentLogP , totalP );
            if( roundNumber-1 == 1 )
                accept = true; // always accepting on the first round.

            if(VerboseLevel>2)
                cout << "round number " << roundNumber-1  << "likelihood ratio : " <<   exp( totalP - CurrentLogP ) << " accepted : " << accept  << " acceptance ratio " <<  (accepted+accept) / (roundNumber-1)    << endl;

            if(accept)
            {
                accepted += 1;
                CurrentLogP = totalP ;

                for(unsigned i = 0 ; i < CurrentRate.size() ; i++ )
                    DefaultRates[i] = CurrentRate[i];
            }

            if(VerboseLevel>3)
                cout << "sampled rates : ";
            // drawing new solution
            for(unsigned i = 0 ; i < CurrentRate.size() ; i++ )
            {
                CurrentRate[i] = SAMPLE_drawNewRate( DefaultRates[i], SD, MinMinRate,MaxRate);
                if(VerboseLevel>3)
                    cout <<  CurrentRate[i] << " ";
            }
            if(VerboseLevel>3)
                cout <<  endl;


            model->set_Rates( CurrentRate );

            // output acceptedRates -> in DefaultRates
            if(VerboseLevel>0)
            {
                if( ( roundNumber-1 == SAMPLE_burnin ) )
                    cout << "burnin finished." << endl;
                else if( roundNumber-1 < SAMPLE_burnin )
                    cout << "burnin : " << roundNumber-1 << "/" << SAMPLE_burnin << "\r";
                else
                    cout << "sampling : "<< roundNumber-SAMPLE_burnin << "/" << SAMPLE_thinning*SAMPLE_sampleSize << "\r";
            }
            



            if( roundNumber-1 > SAMPLE_burnin )
            {
                //cout << "sample " << roundNumber-SAMPLE_burnin << "(" << (roundNumber-SAMPLE_burnin)%SAMPLE_thinning <<")" << endl;

                if( (roundNumber-SAMPLE_burnin)%SAMPLE_thinning == 0 )
                {
                    //output rates.
                    for(unsigned i = 0 ; i < DefaultRates.size() ; i++ )
                    {
                        OUTSAMPLE << DefaultRates[i] << " ";
                    }
                    OUTSAMPLE << endl;
                }
            }
            else if( (roundNumber)%30 == 0 )
            { // every 30 round, test the acceptance ratio.

                double acceptanceRatio  = (accepted) / 30;
                //cout << acceptanceRatio << endl;
                double Target = 0.234; // according  to https://projecteuclid.org/download/pdf_1/euclid.aoap/1034625254

                SD *= min( 2.0 , max( 0.5 , acceptanceRatio/Target) );

                accepted = 0;
            }
        }
        else if( algorithm == "max" )
        {

            if( BranchWiseRates )
                model->set_Rates( RateDefaultMinimums );

            // NB : container only useful if not branch wide ... 
            vector < long double > EventSums(2 + 1*WithTransfer,0);

            double sumObservations = MAX_estimateNewRates( model , 
                                                           RateUpdateCount ,
                                                           EventSums , 
                                                           WithTransfer,
                                                           RateDefaultMinimums, 
                                                           RateDefaultMaximums);

            ////// RATE OPT //////////
    
            vector< vector<long double> > Rates = model->get_Rates();

            if(VerboseLevel>2)
            {
                cout << "inferred rates : " <<  "d : l : t" << endl;
                for(unsigned i = 0 ; i < Rates.size();i++)
                {
                    for(unsigned j = 0 ; j < Rates[i].size() ; j++)
                        cout << Rates[i][j] << " : " ;
                    cout << endl;
                }
            }
    
            vector < long double >newRates;
            //updates of bigger rates if need be
            if(! BranchWiseRates )
            {
                /*
                Here I use the rates weighted by the number of time a branch of the species tree were observed. 
                This is especially important as we use a species CCP distributions, and we want to avoid scenarii where, 
                for instance a lot of branches where nothing happens are observed once each, 
                and they influence the overall rate a lot while the branch where a loss happened in every reconciliations is only once once
                */

                if(WithTransfer)
                {
                    //newRates = MAX_estimateNewRates_DTL( EventSums[0] , EventSums[1] , EventSums[2] , EventSums[3] );// <-- old DTL way to estimate best rates. doesn't work well
                    for(unsigned int i = 0 ; i < 3 ; i++ )
                        newRates.push_back( EventSums[i] / sumObservations );

                }
                else
                {
                    for(unsigned int i = 0 ; i < 2 ; i++ )
                        newRates.push_back( EventSums[i] / sumObservations );
                }

                for(unsigned int i = 0 ; i < 2+WithTransfer; i++ )
                {
                    if( newRates[i] == -1 )
                        newRates[i] = RateDefaultMaximums[i];
                    else if( newRates[i] == -2 )
                        newRates[i] = RateDefaultMinimums[i];
                }

                model->set_Rates( newRates );
            }
            //////////////////////////
            ////// SP CCP UPDATE /////
            if(MAX_SpCCPUpdatingWeight>0)
                UpdateCCPdistribution( SpeciesCCPDistrib,  splitCountsObservedInReconciliations , SpeciesIdToCladeId , MAX_SpCCPUpdatingWeight);
            ///applying cutoff
            if( (MAX_cutOff > 0)&&( ! MAX_force_resolve_mode_ ) )
            {            
                int nbDeleted = CutOffCCPdistribution( SpeciesCCPDistrib, MAX_cutOff , MAX_relativeCutOff , LowerVerboseLevel);
                if(nbDeleted>0)
                { // re-map the species CCP inside the model.
                    if(VerboseLevel>0)
                        cout << " deleted " << nbDeleted << " clades in the CCP distribution (cut-off:" << MAX_cutOff << ")."<<endl;
                    model->reMapSpeciesCCPdistribution();
                }            
            }

            if( MAX_force_resolve_mode_ )
            {
                MAX_speciesCladeToForceResolution = forceSpeciesCladeResolution( MAX_speciesCladeToForceResolution, 
                                                                                 SpeciesCCPDistrib ,
                                                                                 splitCountsObservedInReconciliations , 
                                                                                 SpeciesIdToCladeId , 
                                                                                 MAX_cutOff );
                model->reMapSpeciesCCPdistribution();

                if(MAX_speciesCladeToForceResolution.size() == 0)
                    STOP = true;
            }
            //////////////////////////

            if(VerboseLevel>0)
            {
                cout << "****************************************"<< endl;

                    if(!BranchWiseRates)
                    {
                        cout << "Duplication rate: " << CurrentRate[0];
                        cout << "\tLoss rate: " << CurrentRate[1] ;
                        if(WithTransfer)
                            cout << "\tTransfer rate: " << CurrentRate[2] ;
                    }
                    else
                    {
                        cout << "mean Duplication rate : " << RateMeans[0] << " variance : "<< RateVars[0] << endl;
                        cout << "mean Loss rate : " << RateMeans[1] << " variance : "<< RateVars[1] << endl;
                        if(WithTransfer)
                            cout << "mean Transfer rate : " << RateMeans[2] << " variance : "<< RateVars[2] << endl;
                    }
    

                    cout << "\tlog(P) "<< totalP;
                cout << endl;
            }

            /// update of rates mean and variance
            vector< vector<long double> > ZipRates( 2+WithTransfer , vector<long double>(  Rates.size() ) );

            for(unsigned i = 0 ; i < Rates.size() ; i++)
            {
                for(unsigned j = 0 ; j < Rates[i].size() ; j++)
                {
                    ZipRates[j][i] = Rates[i][j];
                }
            }
            
            for(unsigned i = 0 ; i < ZipRates.size() ; i++)
            {
                RateMeans[i] = getMean( ZipRates[i] );
                RateVars[i] = getVar( ZipRates[i] , RateMeans[i] );
            }
        
            if(VerboseLevel>0)
            {
                if(!BranchWiseRates)
                {
                    cout << "new Duplication rate: " << newRates[0];
                    cout << "\tnew Loss rate: " << newRates[1] ;
                    if(WithTransfer)
                        cout << "\tnew Transfer rate: " << newRates[2] ;
                    cout << endl;
                }
                else
                {
                    cout << "mean Duplication rate : " << RateMeans[0] << " variance : "<< RateVars[0] << endl;
                    cout << "mean Loss rate : " << RateMeans[1] << " variance : "<< RateVars[1] << endl;
                    if(WithTransfer)
                        cout << "mean Transfer rate : " << RateMeans[2] << " variance : "<< RateVars[2] << endl;
                }
    
                cout << "****************************************"<< endl;
    
            }
    

            CurrentRate = newRates;

            if((MAX_writeIntermediary ) && ( !STOP ))
            {
                string fileName = Prefix + ".round" + int2string( roundNumber ) + ".updated.species.ale";
                OutputCCPdistribution(fileName, SpeciesCCPDistrib);

                if(BranchWiseRates)
                {
                    if(Prefix!="")
                    {
                        of.open(Prefix + ".round" + int2string( roundNumber ) + ".rates.txt" );
                        buf = of.rdbuf();
                    }
                    else
                        buf = cout.rdbuf();
                
                    ostream OUT(buf);
                
                    
                    OutputBranchWiseRates(  OUT , model );
                
                    if(Prefix!="")
                    {        
                        of.close();
                    }
            
                }

                
            }




        }
        else if( algorithm == "nelderMead" )
        {

            if(VerboseLevel>0)
                cout << " --> total log P of tested parameters " << totalP << endl; 


            vector < long double > newParams = NMoptimizer->ReceiveLikelihoodAndGetNextParameterToTest( totalP, VerboseLevel );

            if( newParams.size() == 0)
            {
                STOP = true; 
                newParams = NMoptimizer->getBestParameters();

                //if(VerboseLevel>0)
                //{
                    cout << "Nelder-Mead optimization found ML rates after "<< NMoptimizer->get_iters() <<" iterations:" << endl;
                    cout << "d : "  << newParams[0] << endl;
                    cout << "l : "  << newParams[1] << endl;
                    if(WithTransfer)
                        cout << "t : "  << newParams[2] << endl;
                    cout << "logP : " << NMoptimizer->get_BestParametersLkh() << endl;
                //}
            }
            else
            {
                if(VerboseLevel>0)
                {
                    cout << "Nelder-Mead optimization testing parameters:" << endl;
                    cout << "d : "  << newParams[0] << endl;
                    cout << "l : "  << newParams[1] << endl;
                    if(WithTransfer)
                        cout << "t : "  << newParams[2] << endl;
                }
            }
            model->set_Rates( newParams );
        }


        if((!STOP)||(nbOutputSamples>0))
            model->resetForNewParameters( LowerVerboseLevel );
    }


    if(algorithm=="sample")
    {
        if(VerboseLevel>0)
        {
            cout << "Rate sampling finished."<<endl;
            cout << "Acceptance ratio : " <<  (accepted) / (roundNumber-SAMPLE_burnin)    << endl;
        }
    }

    if(nbOutputSamples>0)
    {
        if(VerboseLevel>0)
            cout << "Reconciliation output round." << endl; 


        /// computing reconciliations and backtracking fro all families
        for(unsigned GeneFamilyId = 0 ; GeneFamilyId < NbGeneDistrib ; GeneFamilyId++ )
        {
    
            shared_ptr<TreesetMetadata> GeneCCPDistrib = GeneCCPDistribV[ GeneFamilyId ];
    
            model->addGeneCCPdistribution(GeneCCPDistrib);// makes some precomputations With ids  
            model->addgeneSpeciesCorrespondence( GtoSpCorr );
    
            
            model->computeReconciliationMatrix( GeneCladeSpeciesMasks[GeneFamilyId] , LowerVerboseLevel ); 
        
    
            for(unsigned i = 0 ; i < nbOutputSamples; i++ )
            {
                shared_ptr< RecNode > RT= model->backtrack();
                model->annotateReconciledTree( RT );
    
                WriteRecTreeToStream( OUT , RT , "gene family " + geneDistribFileNames[GeneFamilyId]  + " , sample " + int2string(i+1) );
    
            }

            if(VerboseLevel>1)
            {
                cout << " Finished "<< nbOutputSamples <<" backtracks of family " << GeneFamilyId << "/" << NbGeneDistrib-1 ;
                cout << "(overall log likelihood : " << log( model->getOverAllProbability() ) << ")"<< endl;
            }
        
    
    
            model->prepareForNewGeneFamily();
            GeneCCPDistrib.reset();
        }

        FinishRecXMLToStream( OUT );


        if(Prefix!="")
        {
            of.close();
        }

    }




    if(Prefix!="")
    {
        of.open(Prefix + ".updated.species.ale");
        buf = of.rdbuf();
    }
    else
        buf = cout.rdbuf();

    ostream spOUT(buf);


    spOUT << SpeciesCCPDistrib->output_as_ale() << endl;

    if(Prefix!="")
    {        
        of.close();
    }

    if(BranchWiseRates)
    {
        if(Prefix!="")
        {
            of.open(Prefix + ".rates.txt" );
            buf = of.rdbuf();
        }
        else
            buf = cout.rdbuf();
    
        ostream OUT(buf);
    
        
        OutputBranchWiseRates(  OUT , model );
    
        if(Prefix!="")
        {        
            of.close();
        }

    }

    if(algorithm == "grid")
    {
        if(Prefix!="")
        {
            of.open(Prefix + ".grid.txt" );
            buf = of.rdbuf();
        }
        else
            buf = cout.rdbuf();
    
        ostream OUT(buf);
    
        for(unsigned int i = 0 ; i < GRID_LkhList.size() ; i++)
        {
            for(unsigned int j = 0 ; j < GRID_rateLists[i].size() ; j++ )
                OUT << GRID_rateLists[i][j] << " ";
            OUT <<  GRID_LkhList[i] << endl;
        }
    
        if(Prefix!="")
        {        
            of.close();
        }
    }


    delete model;
    SpeciesCCPDistrib.reset();



    if( VerboseLevel > 0 )
        cout << "Program end." << endl;
    return 0;
}