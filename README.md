


This repository contains a program (currently name asgRd)
which implements 2 probabilistic models of reconciliation integrating uncertainties on both the genes and the species phylogneies.

The first model considers gene Duplication and Loss.
The second model considers gene Duplication, Loss and transfers.
Tentative description of these models can be found in [the documents/ folder ](documents/)

The proposed software is able to efficiently compute gene reconciled trees and estimates rates of duplication and loss (and transfers) either in a tree-wide or in a branch wide fashion. 
The rates may either be sampled accordling to their liklelihood or optimized to maximized likelihood.

Additionnaly the software can use sampled gene-species reconciliations to modify the species tree distribution so that it reflect the support that gene histories lend to the different possible speciations. 


## requirements

To install properly, this program requires:

 * The [ccpcpp library](https://gitlab.com/drenal/ccpcpp). As the moment inaccesible without an explicit authorisation from its maintainer, sadly; but that should change in the upcoming months. I can provide a copy upon request.
 * [Boost](https://www.boost.org/) (>1.4)
 * Boost program_options
 
 Additionnaly, to build the paralellized version of the software you need:

 * Boost serialize
 * Boost mpi

## compilation

Use your favorite test editor to open the ```Makefile``` file and edit the lines:

```
CCPCPP_INCLUDE=/usr/local/include
CCPCPP_LIB=/usr/local/lib


BOOST_INCLUDE=/usr/include
BOOST_LIB=/usr/lib
```

So that they reflect the folders in which the ccpcpp and boost libraries are installed.

Then, simply type:
```
make
```
which will compile both the non-mpi and the mpi version of the software.


Alternatively, if you don't the mpi libraries installed, you can type:
```
make bin/asgRd
```
which will only compute the non-mpi version of the software


## usage

To run asgRd, you can type :
```
./bin/asgRd 
```
Or, to launch it on 4 processes :
```
mpirun -np 4 ./bin/asgRd 
```

```
You shall be presented with a message like this :
ERROR: No input gene trees file was specified!
asgRd 0.1
usage : asgRd [max|sample|grid|nelderMead] -g geneTree -s speciesTree 
for more information, type : asgRd --help 
for more information on a specific algorithm, type : asgRd [max|sample|grid|all|nelderMead] --help 
```

Typing ```./bin/asgRd all --help``` will print a long list of option and subprograms description.

Succintly, asgRd has 4 subprogams
 * grid algorithm : 	computation of the model likelihood along a grid of (tree-wise) duplication, loss (and transfer if applicable) rates.
 * max algorithm : 	optimization of the model likelihood via a maximum expactation method. This algorithm can also be applied on the species tree distribution (by gradually centering it on the most sampled speciations in the reconciliations).
	Note : at the moment this option is not the one yielding the best likelihoods when there is transfers ; maybe prefer the nelderMead algorithm to find the best rates.
 * sample algorithm : 	sampling of duplication, loss and transfer rates according to their likelihood.
 * nelderMead algorithm : 	optimization of rates via the Nelder-Mead method (aka. downhill simplex method).
	Note : at the moment this option is the one yielding the best likelihoods when there is transfers

NB : the grid algorithm is mostly usefull for debugging/fine analysis of the parameters likelihood space purpose.



2 parameters are needed to launch a simple analysis: the gene distributions and the species distribution file.

The gene distribution file can either be a file containing:
 * a gene tree distribution (1 newick tree per line)
 * a gene .ale file : a gene Conditional clade probabilities.
 * a list of files containing gene tree disctributions (in either .ale of newick format) : 1 gene family per line

The species distribution file can either be a file containing:
* a species tree distribution (1 newick tree per line)
* a species .ale file : a gene Conditional clade probabilities.
 
The ```test/``` folder contains simple instances to play with :
```
./bin/asgRd max -g tests/distribs.ale.txt -s tests/spTree.trees.ale -p tests/test_output -v 2
```

NB: the -p option specifies a prefix for output files. Here: ```tests/test_output.updated.species.ale```, the species CCP distribution with probabilities reflecting the support from gene reconciliations.

