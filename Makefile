CXX=g++
MPICXX=mpic++

ODIR=obj
BDIR=bin
SDIR=src
IDIR=include

CCPCPP_INCLUDE=/usr/local/include
CCPCPP_LIB=/usr/local/lib


BOOST_INCLUDE=/usr/include
BOOST_LIB=/usr/lib

MPIBOOST_INCLUDE=-I/usr/include/boost/mpi 
MPIBOOST_FLAGS=  -lboost_mpi -lboost_serialization


CPPFLAGS= -O3 -I$(IDIR) -I$(BOOST_INCLUDE) -I$(CCPCPP_INCLUDE) -std=c++11 -I$(IDIRCCPLIB)
LDFLAGS= -L$(BOOST_LIB) -L$(CCPCPP_LIB) 
LIBS= -lm -lboost_program_options -lCCPCPP


NP_FILES = NamePending.cpp OptionHandler.cpp Utils.cpp PsDLUtils.cpp PsDLMaxUtils.cpp PsDTLModel.cpp PsDLModel.cpp ParentModel.cpp ReconciledTree.cpp speciesCCPupdateUtils.cpp NelderMeadOptimizer.cpp
NP_SRCS = $(addprefix $(SDIR)/, $(NP_FILES))
NP_OBJS = $(subst .cpp,.o,$(NP_FILES))
NP_OBJS := $(addprefix $(ODIR)/, $(NP_OBJS))


NP_MPI_FILES = NamePending_MPI.cpp OptionHandler.cpp Utils.cpp UtilsMPI.cpp PsDLUtils.cpp PsDLMaxUtils.cpp MPI_PsDLUtils.cpp PsDTLModel.cpp PsDLModel.cpp ParentModel.cpp ReconciledTree.cpp speciesCCPupdateUtils.cpp NelderMeadOptimizer.cpp
NP_MPI_SRCS = $(addprefix $(SDIR)/, $(NP_MPI_FILES))
NP_MPI_OBJS = $(subst .cpp,.o,$(NP_MPI_FILES))
NP_MPI_OBJS := $(addprefix $(ODIR)/, $(NP_MPI_OBJS))


_EXES= asgRd asgRd_mpi
EXES=$(patsubst %,$(BDIR)/%,$(_EXES))


all: $(EXES)

debug: CPPFLAGS = -O3 -I$(IDIR) -I$(BOOST_INCLUDE) -g -std=c++11 -I$(IDIRCCPLIB)
debug: all


$(BDIR)/asgRd: $(NP_OBJS)

$(BDIR)/asgRd: $(ODIR)/NamePending.o
	mkdir -p bin 
	$(CXX) -o $@ $^ $(CPPFLAGS) $(LDFLAGS) $(LIBS)


$(BDIR)/asgRd_mpi: $(NP_MPI_OBJS)



# mpi rules

$(ODIR)/NamePending_MPI.o: $(SDIR)/NamePending_MPI.cpp
	mkdir -p $(ODIR)
	@echo "Compiling $<"
	$(MPICXX) $(MPIBOOST_INCLUDE) $(MPIBOOST_FLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBS) -c -MD -o $@ $<
	@cp $(ODIR)/$*.d $(ODIR)/$*.P; \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	    -e '/^$$/ d' -e 's/$$/ :/' < $(ODIR)/$*.d >> $(ODIR)/$*.P; \
	rm -f $(ODIR)/$*.d

$(ODIR)/MPI_PsDLUtils.o: $(SDIR)/MPI_PsDLUtils.cpp
	mkdir -p $(ODIR)
	@echo "Compiling $<"
	$(MPICXX) $(MPIBOOST_INCLUDE) $(MPIBOOST_FLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBS) -c -MD -o $@ $<
	@cp $(ODIR)/$*.d $(ODIR)/$*.P; \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	    -e '/^$$/ d' -e 's/$$/ :/' < $(ODIR)/$*.d >> $(ODIR)/$*.P; \
	rm -f $(ODIR)/$*.d

$(ODIR)/UtilsMPI.o: $(SDIR)/UtilsMPI.cpp
	mkdir -p $(ODIR)
	@echo "Compiling $<"
	$(MPICXX) $(MPIBOOST_INCLUDE) $(MPIBOOST_FLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBS) -c -MD -o $@ $<
	@cp $(ODIR)/$*.d $(ODIR)/$*.P; \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	    -e '/^$$/ d' -e 's/$$/ :/' < $(ODIR)/$*.d >> $(ODIR)/$*.P; \
	rm -f $(ODIR)/$*.d



$(BDIR)/asgRd_mpi: $(ODIR)/NamePending_MPI.o
	mkdir -p bin 
	$(MPICXX) -o $@ $^ $(MPIBOOST_INCLUDE) $(MPIBOOST_FLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBS)


#general rules

$(ODIR)/%.o: $(SDIR)/%.cpp
	mkdir -p $(ODIR)
	@echo "Compiling $<"
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(LIBS) -c -MD -o $@ $<
	@cp $(ODIR)/$*.d $(ODIR)/$*.P; \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	    -e '/^$$/ d' -e 's/$$/ :/' < $(ODIR)/$*.d >> $(ODIR)/$*.P; \
	rm -f $(ODIR)/$*.d


$(ODIRCCPLIB)/%.o: $(SDIRCCPLIB)/%.cpp
	mkdir -p $(ODIRCCPLIB)
	@echo "Compiling $<"
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(LIBS) -c -MD -o $@ $<




$(BDIR)/%: $(ODIR)/%.o
	mkdir -p bin 
	$(CXX) -o $@ $^ $(CPPFLAGS) $(LDFLAGS) $(LIBS)



all: $(EXES)


clean:
	   rm -f $(ODIR)/*.o *~ $(SDIR)/*~   
	   rm -rf $(ODIR) 